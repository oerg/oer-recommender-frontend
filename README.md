## OER Recommender

**OER Recommender** is a project implemented by *Learning and Skill Analytics Group* at [*Leibniz Information Center for Science and Technology*](https://www.tib.eu/) in Hanover, Germany. 

### This project was the base code for the following projects:

- [ADAPT](https://www.projekt-adapt.de/) (2021-2024)
- [ADSEE](https://adsee.eu/) (2019-2022)
- [BIPER](https://www.tib.eu/en/forschung-entwicklung/projektuebersicht/projektsteckbrief/biper) (2021-2022)
- [DALIA](https://dalia.education/) (2023-2025)
- [OEduverse](https://oeduverse.eu/) (2019-2022)
- [OSCAR](https://projects.tib.eu/oscar-ai/about-the-project/) (2019-2023)
- [WBSmart](https://wbsmart.eu/) (2021–2024)


### License

This project is published under the [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/).


## How to use

**Requirements**: You should have [NodeJs 16](https://nodejs.org/en/blog/release/v16.16.0) and [Yarn](https://yarnpkg.com/) installed on your system.

To be able to run and compile please follow these steps:

1. Clone the current repo using `git`
2. Copy `env.sample` to `.env` file and enter the required configurations.
3. Install the dependency packages using `yarn install`

Then you will have the following commands at hand:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
