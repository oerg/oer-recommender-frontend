import React from 'react';
import StepItem from './StepItem';
import AdjustIcon from '@material-ui/icons/Adjust';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import { Grid } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export default function Steps(props) {

    const { t } = useTranslation(['landing']);

    const stepItems = [
        {
            "title": `1. ${t("Set goals")}`,
            "description": t("Set your learning goals towards your dreams"),
            "icon": AdjustIcon
        },
        {
            "title": `2. ${t("Start learning")}`,
            "description": t("Learn the topics of your target learning goal by our personalized recommendations"),
            "icon": MenuBookIcon
        },
        {
            "title": `3. ${t("Test yourself")}`,
            "description": t("Assess your achieved knowledge on course and topic level"),
            "icon": EqualizerIcon
        },
        {
            "title": `4. ${t("Get help")}`,
            "description": t("At any stage of learning, you can ask for help from our experts"),
            "icon": LiveHelpIcon
        }
    ]

    return (
        <Grid spacing={4} {...props} container>
            {
                stepItems.map(
                    (step, index) =>
                        <Grid item key={index} xs={3}>
                            <StepItem {...step} />
                        </Grid>
                )
            }
        </Grid>
    )
}
