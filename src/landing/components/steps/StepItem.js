import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(
    {
        title: {
            letterSpacing: 2,
            fontSize: "1.25rem",
            textTransform: "uppercase",
            fontWeight: "400",
            margin: "23px auto 0",
            color: "#2cccc4",
            textAlign: "center",
            wordWrap: "break-word",
        },
        wrapper: {
            borderRadius: 25,
            boxShadow: "5px 5px 20px 0 rgba(49,41,51,0.4)",
            backgroundColor: "#ffffff",
            width: "100%",
            height: "100%",
            padding: 30,
        },
        body: {
            textAlign: "center",
            marginTop: 20,
            fontStyle: "italic",
            wordWrap: "break-word",
            fontSize: "1rem",
            lineHeight: "1.6",
            fontFamily: "'Open Sans',sans-serif",
        },
        icon: {
            textAlign: "center",
        }
    }
)

export default function StepItem({ icon: Icon, title, description }) {
    const classes = useStyles()

    return (
        <div
            className={classes.wrapper}
        >
            <Grid container>
                <Grid item xs={12} className={classes.icon}>
                    <Icon style={{ fontSize: 80, color: "#00535f" }} />
                </Grid>
                <Grid item xs={12} className={classes.title}>
                    {title}
                </Grid>
                <Grid item xs={12} className={classes.body}>
                    {description}
                </Grid>
            </Grid>
        </div>
    )
}
