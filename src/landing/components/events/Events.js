import { Button, Container, Divider, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SlideshowIcon from '@material-ui/icons/Slideshow';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import clsx from 'classnames';
import React, { useEffect } from 'react';

const useStyles = makeStyles({
    root: {
        marginTop: '20px',
        marginBottom: 80
    },
    wrapper: {
        padding: '15px',
    },
    headRow: {
        height: 60,
        fontWeight: "bold",
    },
    timeCol: {
        minWidth: 130,
    }
});


function Section({ title, children, className }) {
    const classes = makeStyles({
        root: {
            marginTop: '15px',
            marginLeft: '5px',
        },
        body: {
            marginLeft: '10px',
            padding: '10px',
            textAlign: 'justify',
        },
        title: {
            fontWeight: 'bold',
        },
    })();

    return (
        <div className={clsx(classes.root, className)}>
            <Typography variant="h6" className={classes.title}>
                {title}
            </Typography>
            <Typography variant="body1" component="div" className={classes.body}>
                {children}
            </Typography>
        </div>
    )
}

const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(even)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
}))(TableRow);

function Events() {
    const classes = useStyles();  
    const [expandedPanel, setExpandedPanel] = React.useState('panel1');
    
    useEffect(
        () => {
            window.document.getElementById("root").scrollTo({
                top: 0,
                behavior: "smooth",
            })
        },
        []
    )

    const handleChange = (panel) => (event, newExpanded) => {
        setExpandedPanel(newExpanded ? panel : null);
    };

    return (
        <Container maxWidth="md" className={classes.root}>
            <Paper elevation={2} className={classes.wrapper}>
                <MuiAccordion square expanded={expandedPanel === 'panel1'} onChange={handleChange('panel1')} style={{ marginTop: '80px' }}>
                    <MuiAccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography variant="h4" className={classes.title}>1st eDoer OER Recommender Workshop</Typography>
                    </MuiAccordionSummary>
                    <MuiAccordionDetails>
                        <Typography>                        
                            <Section title="Aim">
                            During this workshop we present our open, AI driven learning recommender platform, which aims at exploiting labor market data together with the vast amount of Open Educational Resources (OERs) available worldwide with the explicit goal of scaffolding learners’ study activities towards their career. Presentations will cover the general concept of eDoer, and three associated research efforts. 
                            </Section>
                            <Divider />
                            <Section title="Tentative Agenda">
                                <TableContainer component={Paper} >
                                    <Table size="small" stickyHeader>
                                        <TableHead>
                                            <TableRow className={classes.headRow}>
                                                <TableCell component="th" className={classes.timeCol}>Time</TableCell>
                                                <TableCell component="th">Topic</TableCell>
                                                <TableCell component="th">Presenter(s)</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <StyledTableRow >
                                                <TableCell>
                                                    09:00 - 09:05
                                                </TableCell>
                                                <TableCell>
                                                    Welcome
                                                </TableCell>
                                                <TableCell>
                                                    Gábor Kismihók (TIB)
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    09:05 - 09:30
                                                </TableCell>
                                                <TableCell>
                                                    eDoer AI driven open learning concept and demonstration
                                                    <Button startIcon={<VideoLibraryIcon />} color="secondary">
                                                        <a href="https://www.youtube.com/watch?v=R_LYWQPPXBU&ab_channel=OERRecommendation" target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Watch</em></a>
                                                    </Button>
                                                    <Button startIcon={<SlideshowIcon />} color="secondary">
                                                        <a href='https://labs.tib.eu/edoer/files/slides/First eDoer workshop intro.pptx' target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Slides</em></a>
                                                    </Button>
                                                </TableCell>
                                                <TableCell>
                                                    Gábor Kismihók (TIB), Abdolali Faraji (TIB)
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    09:30 - 10:00
                                                </TableCell>
                                                <TableCell>
                                                    OER Quality and Topic Prediction
                                                    <Button startIcon={<VideoLibraryIcon />} color="secondary">
                                                        <a href="https://www.youtube.com/watch?v=ewxaBZWg1Bw&ab_channel=OERRecommendation" target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Watch</em></a>
                                                    </Button>
                                                    <Button startIcon={<SlideshowIcon />} color="secondary">
                                                        <a href='https://labs.tib.eu/edoer/files/slides/Edoer_workshop_Extracting_Topics_from_Open_Educational_Resources.pptx' target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Slides 1</em></a>
                                                    </Button>
                                                    <Button startIcon={<SlideshowIcon />} color="secondary">
                                                        <a href='https://labs.tib.eu/edoer/files/slides/Edoer_Workshop_Metadata_Analysis_of_Open_Educational_Resources.pptx' target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Slides 2</em></a>
                                                    </Button>
                                                </TableCell>
                                                <TableCell>
                                                    Mohammadreza Tavakoli (TIB)
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    10:00 - 10:30
                                                </TableCell>
                                                <TableCell>
                                                    Learning recommendations in eDoer for healthy research career development: the OSCAR framework
                                                    <Button startIcon={<VideoLibraryIcon />} color="secondary">
                                                        <a href="https://www.youtube.com/watch?v=6hvVRG2hnO4&ab_channel=OERRecommendation" target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Watch</em></a>
                                                    </Button>
                                                    <Button startIcon={<SlideshowIcon />} color="secondary">
                                                        <a href='https://labs.tib.eu/edoer/files/slides/eDoer in the OSCAR framework.pptx' target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Slides</em></a>
                                                    </Button>
                                                </TableCell>
                                                <TableCell>
                                                    Hasan Abu-Rasheed (University of Siegen)
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    10:30 - 11:00
                                                </TableCell>
                                                <TableCell>
                                                    Labour Market Intelligence
                                                    <Button startIcon={<VideoLibraryIcon />} color="secondary">
                                                        <a href="https://www.youtube.com/watch?v=-1XlB0tW8Ic&ab_channel=OERRecommendation" target="_blank" rel="noopener noreferrer"><em style={{color: "#195e83"}}>Watch</em></a>
                                                    </Button>                                                    
                                                </TableCell>
                                                <TableCell>
                                                    Jarno Vrolijk (University of Amsterdam)
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    <i>11:00 - 11:30</i>
                                                </TableCell>
                                                <TableCell>
                                                    <i>Break</i>
                                                </TableCell>
                                                <TableCell>
                                                    -
                                                </TableCell>
                                            </StyledTableRow>
                                            <StyledTableRow >
                                                <TableCell>
                                                    11:30 - 12:30
                                                </TableCell>
                                                <TableCell>
                                                    eDoer Governance meeting
                                                </TableCell>
                                                <TableCell>
                                                    -
                                                </TableCell>
                                            </StyledTableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Section>
                            <Divider />
                            <Section title="Event Details">
                                <ul>
                                    <li><strong>Date</strong>: Thursday, 22nd of April, 2021</li>
                                    <li><strong>Time</strong>: 09:00 - 12:30 CET</li>
                                    <li><strong>Registration</strong>: <a style={{ textDecoration: 'underline' }} href="https://us02web.zoom.us/meeting/register/tZEvf-Cvrz8sH90WTvI4fqD5zeHSSwXLvaXX" target="_blank" rel="noopener noreferrer">https://us02web.zoom.us/meeting/register/tZEvf-Cvrz8sH90WTvI4fqD5zeHSSwXLvaXX</a></li>
                                </ul> 
                            </Section>  
                        </Typography>
                    </MuiAccordionDetails>
                </MuiAccordion>
                <MuiAccordion square expanded={expandedPanel === 'panel2'} onChange={handleChange('panel2')} className={classes.root}>                
                    <MuiAccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography variant="h4" className={classes.title}>1st eDoer OER Recommender Hackathon</Typography>
                    </MuiAccordionSummary>
                    <MuiAccordionDetails>
                        <Typography>                        
                            <Section title="Aim">
                                Predicting the Quality of Educational Resources
                            </Section>
                            <Divider />
                            <Section title="Description">
                                In this hackathon, we provide a data-set of educational resources from youtube. The resources are in the area of Data Science and have been labeled (0 or 1) by subject matter experts according to the question: Is this resource covered its target subjects well? The aim of the hackathon is predicting the label for the educational resources. 
                            </Section>
                            <Divider />
                            <Section title="Event Details">
                                <ul>
                                    <li><strong>Date</strong>: 14th-15th of December 2020 </li>
                                    <li><strong>Time</strong>: 09.00-17.00 CET</li>
                                </ul> 
                            </Section>  
                        </Typography>
                    </MuiAccordionDetails>
                </MuiAccordion>                
            </Paper>
        </Container>        

    
    );
}

export default Events
