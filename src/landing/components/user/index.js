import { Container } from '@material-ui/core'
import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { useRouteMatch } from 'react-router-dom'
import Card from '../../../dashboard/components/Card/Card'
import CardBody from '../../../dashboard/components/Card/CardBody'
import CardHeader from '../../../dashboard/components/Card/CardHeader'
import Footer from '../../../dashboard/views/Footer'
import Navbar from '../../../dashboard/views/Navbar'
import PublicProflle from '../../../dashboard/views/Profile/PublicProflle'
import useStyles from '../../../dashboard/views/styles/EducationalContentStyles'
import { UserContext } from '../../User'

function UserPage() {
    const classes = useStyles();
    const match = useRouteMatch();
    const user = useContext(UserContext);
    const { t } = useTranslation(['landing']);

    return (
        <div>
            <Navbar basePath={match.url} userData={user.data} user={user} path={match.path} />
            <Container maxWidth="lg" style={{
                padding: '25px',
                display: "flex",
                flexFlow: "column",
                height: "calc(100% - 85px)"
            }}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Public Profile")}
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <PublicProflle showShare={false} />
                    </CardBody>
                </Card>
            </Container>
            <Footer />
        </div>
    )
}

export default UserPage
