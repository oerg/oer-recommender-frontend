import { makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import SkillLogo from '../../../dashboard/components/Logos/Skill';
import { useLanguage } from '../../../i18n';


const useStyles = makeStyles({
    root: {
        display: "flex",
        padding: 30,
        justifyContent: "flex-start",
        maxWidth: "100%",
        textAlign: "left",
    },
    picture: {
        width: 110,
        height: 110,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    logo: {
        width: 110,
        height: 110,
        borderRadius: "50%"
    },
    rightPart: {
        flexGrow: 1,
        marginLeft: 12,
    },
    title: {
        textTransform: "uppercase",
        letterSpacing: 1,
        fontWeight: 700,
        fontSize: "1.25rem",
        color: "#2cccc4 !important",
        padding: 0,
        margin: 0
    },
    description: {
        textAlign: "Left",
        height: 52,
        overflow: "hidden",
        fontFamily: "'Open Sans',sans-serif",
        fontSize: "1rem",
        lineHeight: 1.6,
        margin: "8px 0",
    }
})

export default function LandingSkillItem({ id, picture, title, description }) {

    const classes = useStyles()
    const language = useLanguage();

    return (
        <div className={classes.root}>
            <div className={classes.picture}>
                <SkillLogo size={"large"} picture={picture} className={classes.logo} />
            </div>
            <div className={classes.rightPart}>
                <p className={classes.description}>
                    {
                        description &&
                        <>
                                {description.substring(0, 100)}
                                {description.length > 100 && "..."}
                        </>
                    }
                </p>
                <Typography className={classes.title} variant='h6' component={Link} to={`/${language}/dashboard/skills/${id}`} target="_blank">
                    {title}
                </Typography>
            </div>
        </div>
    )
}
