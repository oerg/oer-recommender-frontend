import axios from "axios";
import { requestAddHandlers } from "../../../dashboard/views/helpers";

export function loadLandingSkills(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/skills/landing/`
        ),
        onLoad,
        onError,
        onEnd
    )
}