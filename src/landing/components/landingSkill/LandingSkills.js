import { Grid, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import LandingSkillItem from './LandingSkillItem';
import { loadLandingSkills } from './utils';

const useStyles = makeStyles({
    root: {
        textAlign: "center",
        position: "relative",
        display: "block",
    },
    headTitle: {
        textAlign: 'center',
        letterSpacing: "0px",
        fontSize: "3rem",
        lineHeight: 1.2,
        fontWeight: 700,
        textTransform: "none",
        margin: "59px auto 0",
        fontFamily: "'Open Sans',sans-serif !important",
        wordWrap: "break-word",
        position: "relative",
    },
    body: {
        margin: "61px auto 60px",
        display: "flex",
    },
})

export default function LandingSkills() {
    const { t } = useTranslation(['landing'])
    const [skills, setSkills] = useState([])
    const [loadingSkills, setLoadingSkills] = useState(false)
    const classes = useStyles()

    useEffect(
        () => {
            setLoadingSkills(true)
            loadLandingSkills(
                (res) => {
                    setSkills(res)
                },
                null,
                () => setLoadingSkills(false)
            )
        },
        []
    )

    return (
        !loadingSkills && skills && skills.length > 0 &&
        <div className={classes.root}>
            <h6 className={classes.headTitle}>{t("Popular Courses")}!</h6>
            <Grid container className={classes.body}>
                {
                    skills.map(
                        (skill, index) =>
                            <Grid item key={index} xs={12} md={6}>
                                <LandingSkillItem {...skill} />
                            </Grid>
                    )
                }
            </Grid>
        </div>
    )
}
