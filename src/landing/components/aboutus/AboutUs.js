import { Avatar, Card, CardActions, CardHeader, Collapse, Container, Divider, Grid, IconButton, makeStyles, Paper, Typography } from '@material-ui/core';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TwitterIcon from '@material-ui/icons/Twitter';
import WebIcon from '@material-ui/icons/Web';
import clsx from 'classnames';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
// import BMBFLogo from '../../../dashboard/views/Footer/images/bmbf.jpg';
// import EULogo from '../../../dashboard/views/Footer/images/euflag.jpg';
import OEDLOGO from '../../../dashboard/views/Footer/images/oeduverse.png';
import ADDLogo from '../../../dashboard/views/Footer/images/adsee.png';
import OSCARLogo from '../../../dashboard/views/Footer/images/oscar.png';
import ADAPTLogo from '../../../dashboard/views/Footer/images/adapt.png';
import BIPERLogo from '../../../dashboard/views/Footer/images/biper.png';
import WBLogo from '../../../dashboard/views/Footer/images/wbsmart.png';

const useStyles = makeStyles({
    root: {
        marginTop: 100,
        marginBottom: 20,
    },
    wrapper: {
        padding: '15px',
    },
    projectLi: {
        display: "flex",
        alignItems: "center",
        "& > a": {
            marginLeft: 5
        },
        marginTop: 15
    },
});

function Person({ name, position, image, email, website, facebook, linkedin, twitter, className }) {
    const classes = makeStyles({
        root: {
            textAlign: "left",
            minHeight: "130px",
            transition: 'background 0.5s',
            '&:hover': {
                backgroundColor: '#EEE',
                '& img': {
                    filter: 'grayscale(0%)',
                }
            },
            '& img': {
                filter: 'grayscale(100%)',
                transition: 'all 0.5s'
            },
        },
        avatarRoot: {
            width: '100px',
            height: '100px',
        },
        actions: {
            justifyContent: 'center',
        },
        gray: {
            color: '#CCC',
            transition: 'color 0.5s'
        },
        website: {
            color: '#005c5f',
        },
        email: {
            color: '#DB4437',
        },
        facebook: {
            color: '#4267B2',
        },
        linkedin: {
            color: '#2867b2',
        },
        twitter: {
            color: '#1da1f2',
        }
    })();
    const { t } = useTranslation(["landing"])


    const [isOn, setIsOn] = useState(false);
    const haveLinks = useMemo(() => (
        email || website || facebook || linkedin || twitter
    ), [email, website, facebook, linkedin, twitter])

    return (
        <Card className={clsx(classes.root, className)} onMouseEnter={() => setIsOn(true)} onMouseLeave={() => setIsOn(false)}>
            <CardHeader avatar={
                <Avatar src={image} variant="rounded" alt={name} classes={{
                    root: classes.avatarRoot,
                }} />
            } title={name} subheader={position} />
            <Collapse in={haveLinks && isOn}>
                <CardActions className={classes.actions}>
                    {website &&
                        <IconButton title={t("Visit website")} href={website} target="_blank">
                            <WebIcon className={clsx(classes.gray, isOn && classes.website)} />
                        </IconButton>
                    }
                    {email &&
                        <IconButton title={t("Send an Email")} href={`mailto:${email}`}>
                            <AlternateEmailIcon className={clsx(classes.gray, isOn && classes.email)} />
                        </IconButton>
                    }
                    {facebook &&
                        <IconButton title={t("Facebook Page")} href={facebook} target="_blank">
                            <FacebookIcon className={clsx(classes.gray, isOn && classes.facebook)} />
                        </IconButton>
                    }
                    {linkedin &&
                        <IconButton title={t("LinkedIn Page")} href={linkedin} target="_blank">
                            <LinkedInIcon className={clsx(classes.gray, isOn && classes.linkedin)} />
                        </IconButton>
                    }
                    {twitter &&
                        <IconButton title={t("Twitter Timeline")} href={twitter} target="_blank">
                            <TwitterIcon className={clsx(classes.gray, isOn && classes.twitter)} />
                        </IconButton>
                    }
                </CardActions>
            </Collapse>
        </Card>
    )
}

function Section({ title, children, className }) {
    const classes = makeStyles({
        root: {
            marginTop: '15px',
            marginLeft: '5px',
        },
        body: {
            marginLeft: '10px',
            padding: '10px',
            textAlign: 'justify',
        },
        title: {
            fontWeight: 'bold',
        },
    })();

    return (
        <div className={clsx(classes.root, className)}>
            <Typography variant="h6" className={classes.title}>
                {title}
            </Typography>
            <Typography variant="body1" component="div" className={classes.body}>
                {children}
            </Typography>
        </div>
    )
}

function AboutUs() {
    const classes = useStyles();
    const { t } = useTranslation(["landing"])

    useEffect(
        () => {
            window.document.getElementById("root").scrollTo({
                top: 0,
                behavior: "smooth",
            })
        },
        []
    )

    const people = useMemo(
        () => [
            {
                name: "Mohammadreza Tavakoli",
                position: `${t("Backend Developer")}, ${t("Data Scientist")}, ${t("Recommender System Designer")}`,
                image: 'Mohammadreza Tavakoli.jpg',
            },
            {
                name: "Ali Faraji",
                position: `${t("Frontend Developer")}, ${t("DevOps")}, ${t("Recommender System Designer")}`,
                image: 'Ali Faraji.jpg',
                email: "ali.faraji90@gmail.com",
                // website: "https://www.afrj.info",
                // facebook: "https://www.facebook.com/ali.sgn89/",
                // linkedin: "https://www.linkedin.com/in/ali-faraji90",
            },
            {
                name: "Gábor Kismihók",
                position: t("Principle Investigator"),
                image: "Gabor Kismihok.jpg",
                twitter: "https://twitter.com/kismihok",
            },
            {
                name: "Stefan T. Mol",
                position: t("Principle Investigator"),
                image: "Stefan Mol.png",
            },
            {
                name: "Jarno Vrolijk",
                position: t("Data Scientist"),
                image: 'Jarno Vrolijk.jpg',
            },
            {
                name: "Alan Berg",
                position: `${t("DevOps")}, ${t("Security Expert")}`,
                image: "Alan Berg.png",
            },
            {
                name: "Mohammadreza Molavi",
                position: `${t("Data Scientist")}, ${t("Recommender System Designer")}`,
                image: 'Mohammadreza Molavi.jpg',
            },
            {
                name: "Mirette Elias",
                position: t("Web Accessibility Expert"),
                image: 'Mirette Elias.jpeg',
            },
            {
                name: "Sherzod Hakimov",
                position: `${t("Knowledge Engineer")}, ${t("Data Scientist")}`,
                image: 'Sherzod Hakimov.png',
            },
            {
                name: "Eleni Ilkou",
                position: t("Knowledge Engineer"),
                image: 'Eleni Ilkou.png',
            },
            {
                name: "Hasan Abu-Rashid",
                position: `${t("Knowledge Engineer")}, ${t("XAI researcher")}`,
                image: 'Hasan Abu-Rashid.jpg',
            },
            {
                name: "Christian Weber",
                position: `${t("Knowledge Engineer")}, ${t("AI researcher")}`,
                image: 'Christian Weber.png',
            },
        ].map(i => ({ ...i, image: i.image ? process.env.PUBLIC_URL + '/us/' + i.image : null })),
        [t]
    );

    return (
        <Container maxWidth="md" className={classes.root}>
            <Paper elevation={2} className={classes.wrapper}>
                <Typography variant="h5">
                    {t("Personalised, Open Educational Content Recommendation to Support Career Development")}
                </Typography>
                <Divider />
                <Section title={t("Contributors")}>
                    <Grid container spacing={3} justifyContent="center">
                        {people.map((p, ind) => (
                            <Grid key={ind} item xs={12} md={6}>
                                <Person {...p} />
                            </Grid>
                        ))}
                    </Grid>
                </Section>
                <Section title={t("Aim")}>
                    {t("Exploiting Labour Market Intelligence together with OER recommendations with the explicit goal of scaffolding learners’ study activities towards their career.")}
                </Section>
                <Section title={t("State-of-the-Art")}>
                    {t("This research project departs from the recent, dramatic changes in global societies, forcing many citizens to re-skill themselves to (re)gain employment. Therefore, learners need to develop skills with a view to being qualified to fill current/future jobs. Subsequently, high-quality, personalized educational content and services are also essential to serve this high demand for learning. Free and Open Educational Resources can play a key role in this regard, as they are available in a wide range of learning and occupational contexts globally. However, their applicability so far has been limited, due to low metadata quality and complex quality control. These issues resulted in a lack of personalised functions, like recommendation and search.")}
                </Section>
                <Section title={t("Our Approach")}>
                    <img src={process.env.PUBLIC_URL + '/images/edoer-process.png'} variant="rounded" alt={"Edoer Process"} /> <br></br>
                    {t("This project aims at developing a personalised Open Educational Content Recommendation method to match courses targets with free/open learning content. This is done by:")}
                    <ol>
                        <li>{t("supporting learners to set their skill targets (as courses) based on accurate and relevant labour market information")}</li>
                        <li>{t("building an individual learning pathway towards personal learning objectives")}</li>
                        <li>{t("recommending relevant open/free educational content to help learners to master skills they target")}</li>
                        <li>{t("Providing assessment and feedback for learners to identify their expertise levels, and help them fine tune their learning objectives")}</li>
                    </ol>
                </Section>
                <Section title={t("Projects")}>
                    <ul>
                        <li className={classes.projectLi}><img alt={"ADSEE"} height={40} width={80} title={"ADSEE"} src={ADDLogo} /> <a href="https://www.tib.eu/en/research-development/project-overview/project-summary/adsee" target="_blank" rel="noopener noreferrer">ADSEE - Applied Data Science Educational Ecosystem</a></li>
                        <li className={classes.projectLi}><img alt={"OSCAR"} height={40} width={80} title={"OSCAR"} src={OSCARLogo} /><a href="https://www.tib.eu/en/research-development/project-overview/project-summary/oscar" target="_blank" rel="noopener noreferrer">OSCAR - Online, open learning recommendations and mentoring towards Sustainable research CAReers</a></li>
                        <li className={classes.projectLi}><img alt={"OEduverse"} height={40} width={80} title={"OEduverse"} src={OEDLOGO} /><a href="https://oeduverse.eu/" target="_blank" rel="noopener noreferrer">OEduverse - Advancing sustainable research careers for graduate students through training in mental wellbeing, open science, and communication skills</a></li>
                        <li className={classes.projectLi}><img alt={"ADAPT"} height={40} width={80} title={"ADAPT"} src={ADAPTLogo} /><a href="https://www.tib.eu/en/forschung-entwicklung/projektuebersicht/projektsteckbrief/adapt" target="_blank" rel="noopener noreferrer">ADAPT - Implementation of an Adaptive Continuing Education Support System in the Professional Field of Nursing</a></li>
                        <li className={classes.projectLi}><img alt={"BIPER"} height={40} width={80} title={"BIPER"} src={BIPERLogo} /><a href="https://www.tib.eu/en/forschung-entwicklung/projektuebersicht/projektsteckbrief/biper" target="_blank" rel="noopener noreferrer">BIPER - Business Informatics Programme Reengineering</a></li>
                        <li className={classes.projectLi}><img alt={"WBsmart"} height={40} width={80} title={"WBsmart"} src={WBLogo} /><a href="https://wbsmart.eu/" target="_blank" rel="noopener noreferrer">WBsmart -  AI-based digital continuing education space for elderly care</a></li>
                    </ul>
                </Section>

                <Section title={t("Research questions that we are working on")}>
                    <ul>
                        <li>{t("How to define the required skills for a job based on labour market information (e.g. vacancy announcements, existing standard taxonomies such as ESCO)?")} (<strong>Jarno Vrolijk</strong>)</li>
                        <li>{t("How to extract the topics that need to be covered in order to achieve a course?")} (<strong>Mohammadreza Molavi, Mohammadreza Tavakoli</strong>)</li>
                        <li>{t("How to recommend personalised educational content to learners?")} (<strong>Ali Faraji, Mohammadreza Molavi, Mohammadreza Tavakoli</strong>)</li>
                        <li>{t("How to evaluate the quality of available online educational resources?")} (<strong>Mirette Elias, Mohammadreza Tavakoli</strong>)</li>
                        <li>{t("How to extract properties (e.g. difficulty level, teaching style, and keywords) from educational contents?")} (<strong>Ali Faraji, Mohammadreza Tavakoli</strong></li>
                        <li>{t("How to develop a dashboard that offers the mentioned features effectively?")} (<strong>Ali Faraji, Alan Berg, Mohammadreza Molavi, Mohammadreza Tavakoli</strong>)</li>
                        <li>{t("How to create semantic-based solutions in such labour-market-driven educational systems?")} (<strong>Sherzod Hakimov, Eleni Ilkou, Hasan Abu-Rashid</strong>)</li>
                        <li>{t("How can knowledge-graph-based explainability be integrated into educational recommendations?")} (<strong>Hasan Abu-Rashid, Christian Weber</strong>)</li>
                    </ul>
                </Section>

                <Section title={t("Publications")}>
                    <ul>
                        <li>Tavakoli, M., Elias, M., Kismihók, G., & Auer, S. (2021). <a href="https://dl.acm.org/doi/abs/10.1145/3448139.3448208" target="_blank" rel="noopener noreferrer">Metadata Analysis of Open Educational Resources.</a> In the 11th International Learning Analytics and Knowledge (LAK'2021), April 12--16, 2021. ACM.</li>
                        <li>Tavakoli, M., Hakimov, S., Ewerth, R., & Kismihók, G. (2020, July). <a href="https://ieeexplore.ieee.org/abstract/document/9155881" target="_blank" rel="noopener noreferrer">A recommender system for open educational videos based on skill requirements.</a> In 2020 IEEE 20th International Conference on Advanced Learning Technologies (ICALT) (pp. 1-5). IEEE.</li>
                        <li>Tavakoli, M., Mol, S. T., and Kismihók, G. (2020). <a href="https://www.scitepress.org/PublicationsDetail.aspx?ID=l5Pcp0p15O8=&t=1" target="_blank" rel="noopener noreferrer">Labour Market Information Driven, Personalized, OER Recommendation System for Lifelong Learners.</a> In the 12th International Conference on Computer Supported Education (CSEDU 2020)</li>
                        <li>Tavakoli, M., Faraji, A., Mol, S. T., & Kismihók, G. (2020, October). <a href="https://ieeexplore.ieee.org/abstract/document/9274175" target="_blank" rel="noopener noreferrer">OER Recommendations to Support Career Development.</a> In 2020 IEEE Frontiers in Education Conference (FIE) (pp. 1-5). IEEE.</li>
                        <li>Tavakoli, M., Elias, M., Kismihók, G., & Auer, S. (2020, July). <a href="https://ieeexplore.ieee.org/abstract/document/9155928" target="_blank" rel="noopener noreferrer">Quality prediction of open educational resources a metadata-based approach.</a> In 2020 IEEE 20th International Conference on Advanced Learning Technologies (ICALT) (pp. 29-31). IEEE.</li>
                        <li>Elias, M., Oelen, A., Tavakoli, M., Kismihok, G., & Auer, S. (2020, September). <a href="https://link.springer.com/chapter/10.1007/978-3-030-57717-9_36" target="_blank" rel="noopener noreferrer">Quality Evaluation of Open Educational Resources.</a> In European Conference on Technology Enhanced Learning (pp. 410-415). Springer, Cham.</li>
                        <li>Molavi, M., Tavakoli, M., & Kismihók, G. (2020, September). <a href="https://link.springer.com/chapter/10.1007/978-3-030-57717-9_44" target="_blank" rel="noopener noreferrer">Extracting Topics from Open Educational Resources.</a> In European Conference on Technology Enhanced Learning (pp. 455-460). Springer, Cham.</li>
                        <li>Ilkou, E., Abu-Rasheed, H., Tavakoli, M., Hakimov, S., Kismihók, G., Auer, S., & Nejdl, W. (2021). <a href="https://link.springer.com/chapter/10.1007/978-3-030-88361-4_32" target="_blank" rel="noopener noreferrer">EduCOR: An Educational and Career-Oriented Recommendation Ontology.</a> International Semantic Web Conference. Springer, Cham, 2021.</li>
                        <li>Elias, M., Tavakoli, M., Lohmann, S., Kismihok, G., & Auer, S. (2020, October). <a href="https://dl.acm.org/doi/abs/10.1145/3373625.3418021" target="_blank" rel="noopener noreferrer">An OER Recommender System Supporting Accessibility Requirements.</a> In The 22nd International ACM SIGACCESS Conference on Computers and Accessibility (pp. 1-4).</li>
                    </ul>
                </Section>

            </Paper>
        </Container>
    )
}

export default AboutUs
