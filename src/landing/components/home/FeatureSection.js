import { Grid, isWidthUp, Typography, withWidth } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useMemo } from "react";
import { useTranslation } from "react-i18next";
// import CancelIcon from "@material-ui/icons/Cancel";
import calculateSpacing from "./calculateSpacing";
import FeatureCard from "./FeatureCard";

// const iconSize = 30;

// const features = [
//     {
//         color: "#6200EA",
//         headline: "Personalized learning content",
//         text:
//             "Define your curriculum based on your preferences and have eDoer suggest free educational resources.",
//         icon: <img src={`${process.env.PUBLIC_URL}/images/personalized-image.jpg`} width="90%" alt="" />,
//         mdDelay: "200",
//         smDelay: "200"
//     },
//     {
//         color: "#00C853",
//         headline: "Labour-market oriented education",
//         text:
//             "Acquire skills that the labor market needs.",
//         // icon: <BuildIcon style={{ fontSize: iconSize }} />,
//         icon: <img src={`${process.env.PUBLIC_URL}/images/skills-image.jpg`} width="90%" alt="" />,
//         mdDelay: "0",
//         smDelay: "0"
//     },
//     {
//         color: "#0091EA",
//         headline: "Knowledge assessment",
//         text:
//             "Test your newly acquired knowledge.",
//         icon: <img src={`${process.env.PUBLIC_URL}/images/assessment-image.jpg`} width="90%" alt="" />,
//         mdDelay: "200",
//         smDelay: "200"
//     },
//     {
//       color: "#d50000",
//       headline: "Feature 4",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <ComputerIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "0",
//       smDelay: "200"
//     },
//     {
//       color: "#DD2C00",
//       headline: "Feature 5",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <BarChartIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "200",
//       smDelay: "0"
//     },
//     {
//       color: "#64DD17",
//       headline: "Feature 6",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <HeadsetMicIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "400",
//       smDelay: "200"
//     },
//     {
//       color: "#304FFE",
//       headline: "Feature 7",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <CloudIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "0",
//       smDelay: "0"
//     },
//     {
//       color: "#C51162",
//       headline: "Feature 8",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <CodeIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "200",
//       smDelay: "200"
//     },
//     {
//       color: "#00B8D4",
//       headline: "Feature 9",
//       text:
//         "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et.",
//       icon: <CancelIcon style={{ fontSize: iconSize }} />,
//       mdDelay: "400",
//       smDelay: "0"
//     }
// ];

function FeatureSection(props) {
    const { width } = props;
    const { t } = useTranslation(["landing"]);

    const features = useMemo(
        () => [
            {
                color: "#6200EA",
                headline: t("Personalized learning content"),
                text:
                    t("Define your curriculum based on your preferences and have eDoer suggest free educational resources."),
                icon: <img src={`${process.env.PUBLIC_URL}/images/personalized-image.jpg`} width="90%" alt="" />,
                mdDelay: "200",
                smDelay: "200"
            },
            {
                color: "#00C853",
                headline: t("Labour-market oriented education"),
                text:
                    t("Acquire skills that the labor market needs."),
                // icon: <BuildIcon style={{ fontSize: iconSize }} />,
                icon: <img src={`${process.env.PUBLIC_URL}/images/skills-image.jpg`} width="90%" alt="" />,
                mdDelay: "0",
                smDelay: "0"
            },
            {
                color: "#0091EA",
                headline: t("Knowledge assessment"),
                text:
                    t("Test your newly acquired knowledge."),
                icon: <img src={`${process.env.PUBLIC_URL}/images/assessment-image.jpg`} width="90%" alt="" />,
                mdDelay: "300",
                smDelay: "300"
            },
        ],
        [t]
    );
    return (
        <div style={{ backgroundColor: "#FFFFFF" }}>
            <div className="container-fluid lg-p-top lg-mg-bottom">
                <Typography variant="h3" align="center" className="lg-mg-bottom">
                    {t("Features")}
                </Typography>
                <div className="container-fluid">
                    <Grid container spacing={calculateSpacing(width)}>
                        {features.map(element => (
                            <Grid
                                item
                                xs={6}
                                md={4}
                                data-aos="zoom-in-up"
                                data-aos-delay={
                                    isWidthUp("md", width) ? element.mdDelay : element.smDelay
                                }
                                key={element.headline}
                            >
                                <FeatureCard
                                    Icon={element.icon}
                                    color={element.color}
                                    headline={t(element.headline)}
                                    text={t(element.text)}
                                />
                            </Grid>
                        ))}
                    </Grid>
                </div>
            </div>
        </div>
    );
}

FeatureSection.propTypes = {
    width: PropTypes.string.isRequired
};

export default withWidth()(FeatureSection);
