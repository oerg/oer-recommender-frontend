import { makeStyles, Tab, Tabs } from '@material-ui/core';
import AppsIcon from '@material-ui/icons/Apps';
import CastForEducationIcon from '@material-ui/icons/CastForEducation';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import { Alert } from '@material-ui/lab';
import Pagination from '@material-ui/lab/Pagination';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import SearchItem from './components/SearchItem';
import TabLabel from './components/TabLabel';
import TabPanel from './components/TabPanel';
import { emptyText, extractAuthor, extractDescription, extractIndustry as rawExtractIndustry, extractJobAddress, extractLocation as rawExtractLocation, extractOerAddress, extractSkillAddress, extractTitle, extractTopicAddress } from './utils';

const useStyles = makeStyles((theme) => ({
    pagination: {
        marginTop: theme.spacing(1),
    },
    ulPagination: {
        justifyContent: 'center'
    }
}))

function SearchResults({ jobs, skills, topics, oers, loadingJobs, loadingTopics, loadingSkills, loadingOers, className,
    onJobPageChange, onSkillPageChange, onTopicPageChange, onOerPageChange, searchText }) {
    const [value, setValue] = useState(-1);
    const [jobPage, setJobPage] = useState(1)
    const [skillPage, setSkillPage] = useState(1)
    const [topicPage, setTopicPage] = useState(1)
    const [oerPage, setOerPage] = useState(1)
    const classes = useStyles();
    const { t } = useTranslation(['landing']);

    useEffect(
        () => {
            if (skills.count > 0) 
                setValue(v => v === -1 ? 1 : v)
            else if (jobs.count > 0) 
                setValue(v => v === -1 ? 0 : v)
            else if (topics.count > 0)
                setValue(v => v === -1 ? 2 : v)
            else if (oers.count > 0)
                setValue(v => v === -1 ? 3 : v)
        },
        [jobs, skills, topics, oers]
    )

    const extractLocation = useMemo(
        () => rawExtractLocation(t),
        [t]
    );

    const extractIndustry = useMemo(
        () => rawExtractIndustry(t),
        [t]
    );

    const handleChange = useCallback(
        (event, newValue) => {
            setValue(newValue);
        },
        []
    );

    const handleJobPage = useCallback((event, value) => {
        setJobPage(value);
        onJobPageChange(value)
    }, [onJobPageChange]);

    const handleSkillPage = useCallback((event, value) => {
        setSkillPage(value);
        onSkillPageChange(value)
    }, [onSkillPageChange]);

    const handleTopicPage = useCallback((event, value) => {
        setTopicPage(value);
        onTopicPageChange(value)
    }, [onTopicPageChange]);

    const handleOerPage = useCallback((event, value) => {
        setOerPage(value);
        onOerPageChange(value)
    }, [onOerPageChange]);

    return (
        <div className={className}>
            { searchText && searchText.length < 3 &&
                <Alert severity='warning' style={{
                    marginBottom: 8,
                }}>
                    {t("Your keyword is too short. The results may be unrelated.")}
                </Alert>
            }
            <Tabs
                value={value}
                indicatorColor="secondary"
                textColor="primary"
                onChange={handleChange}
                centered
            >
                <Tab label={<TabLabel text={t("Journeys")} loading={loadingJobs} newItemsCount={jobs.count} icon={WorkOutlineIcon} />} />
                <Tab label={<TabLabel text={t("Courses")} loading={loadingSkills} newItemsCount={skills.count} icon={AppsIcon} />} />
                <Tab label={<TabLabel text={t("Topics")} loading={loadingTopics} newItemsCount={topics.count} icon={ChatBubbleOutlineIcon} />} />
                <Tab label={<TabLabel text={t("Contents")} title={t("Educational Packages")} loading={loadingOers} newItemsCount={oers.count} icon={CastForEducationIcon} />} />
            </Tabs>
            <TabPanel value={value} index={0}>
                {jobs.results &&
                    (jobs.results.length === 0 ?
                        <em>{t("No result.")}</em>
                        :
                        <>
                            { !(searchText && searchText.length < 3) && jobs.results[0].similarity < 0.1 &&
                                <Alert severity='warning'>
                                    {t("We couldn't find accurate results for your search. The following results may be related.")}
                                </Alert>
                            }
                            {jobs.results.map(
                                (job, index) => (
                                    <SearchItem
                                        item={job}
                                        key={index}
                                        formatTitle={extractTitle}
                                        lang={job.lang}
                                        formatTopText={extractIndustry}
                                        formatBotText={extractLocation}
                                        formatDescription={extractDescription}
                                        formatAddress={extractJobAddress}
                                        isPrivate={job.is_private}
                                    />
                                )
                            )}
                            <Pagination count={Math.ceil(jobs.count / 10.0)} variant="outlined" shape="rounded" page={jobPage} onChange={handleJobPage}
                                className={classes.pagination} classes={{ ul: classes.ulPagination }} />
                        </>
                    )}
            </TabPanel>
            <TabPanel value={value} index={1}>
                {skills.results &&
                    (skills.results.length === 0 ?
                        <em>{t("No result.")}</em>
                        :
                        <>
                            { !(searchText && searchText.length < 3) && skills.results[0].similarity < 0.1 &&
                                <Alert severity='warning'>
                                    {t("We couldn't find accurate results for your search. The following results may be related.")}
                                </Alert>
                            }
                            {skills.results.map(
                                (skill, index) => (
                                    <SearchItem
                                        item={skill}
                                        key={index}
                                        formatTitle={extractTitle}
                                        lang={skill.lang}
                                        formatTopText={emptyText}
                                        formatBotText={emptyText}
                                        formatDescription={extractDescription}
                                        formatAddress={extractSkillAddress}
                                    />
                                )
                            )}
                            <Pagination count={Math.ceil(skills.count / 10.0)} variant="outlined" shape="rounded" page={skillPage} onChange={handleSkillPage}
                                className={classes.pagination} classes={{ ul: classes.ulPagination }} />
                        </>
                    )}
            </TabPanel>
            <TabPanel value={value} index={2}>
                {topics.results &&
                    (topics.results.length === 0 ?
                        <em>{t("No result.")}</em>
                        :
                        <>
                            { !(searchText && searchText.length < 3) && topics.results[0].similarity < 0.1 &&
                                <Alert severity='warning'>
                                    {t("We couldn't find accurate results for your search. The following results may be related.")}
                                </Alert>
                            }
                            {
                                topics.results.map(
                                    (topic, index) => (
                                        <SearchItem
                                            item={topic}
                                            key={index}
                                            formatTitle={extractTitle}
                                            lang={topic.lang}
                                            formatTopText={emptyText}
                                            formatBotText={emptyText}
                                            formatDescription={extractDescription}
                                            formatAddress={extractTopicAddress}
                                        />
                                    )
                                )
                            }
                            <Pagination count={Math.ceil(topics.count / 10.0)} variant="outlined" shape="rounded" page={topicPage} onChange={handleTopicPage}
                                className={classes.pagination} classes={{ ul: classes.ulPagination }} />
                        </>
                    )}
            </TabPanel>
            <TabPanel value={value} index={3}>
                {oers.results &&
                    (oers.results.length === 0 ?
                        <em>{t("No result.")}</em>
                        :
                        <>
                            { !(searchText && searchText.length < 3) && oers.results[0].similarity < 0.1 &&
                                <Alert severity='warning'>
                                    {t("We couldn't find accurate results for your search. The following results may be related.")}
                                </Alert>
                            }
                            {
                                oers.results.map(
                                    (topic, index) => (
                                        <SearchItem
                                            item={topic}
                                            key={index}
                                            formatTitle={extractTitle}
                                            formatTopText={extractAuthor}
                                            formatBotText={emptyText}
                                            formatDescription={extractDescription}
                                            formatAddress={extractOerAddress}
                                        />
                                    )
                                )
                            }
                            <Pagination count={Math.ceil(oers.count / 10.0)} variant="outlined" shape="rounded" page={oerPage} onChange={handleOerPage}
                                className={classes.pagination} classes={{ ul: classes.ulPagination }} />
                        </>
                    )}
            </TabPanel>
        </div>
    )
}

SearchResults.propTypes = {
    loadingJobs: PropTypes.bool,
    loadingSkills: PropTypes.bool,
    loadingTopics: PropTypes.bool,
    loadingOers: PropTypes.bool,
}

export default SearchResults

