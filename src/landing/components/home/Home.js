import { Container, makeStyles } from "@material-ui/core";
import React from "react";
import Actions from "../callToActions/Actions";
import LandingSkills from "../landingSkill/LandingSkills";
import Steps from "../steps/Steps";
import HeadSection from "./components/HeadSection";
import Quotes from "../quotes"

const useStyles = makeStyles({
    stepsWrapper: {
        alignItems: "stretch",
        height: 375,
        marginTop: -75,
        marginBottom: 40,
    }
})

function Home({ onJobClick, onSkillClick, onTopicClick, exploreOpen, onExploreOpenChange, selectedItem }) {
    const classes = useStyles()

    return (<div style={{ position: "relative" }}>
        <div style={{
            marginTop: 85,
            backgroundColor: "#ffffff",
        }}>
            <HeadSection
                onJobClick={onJobClick}
                onSkillClick={onSkillClick}
                onTopicClick={onTopicClick}
            />
            <Container >
                <Steps className={classes.stepsWrapper} />
            </Container>
            <Actions />
            <Container maxWidth='lg'>
                <LandingSkills />
            </Container>
            <Container>
                <Quotes />
            </Container>

        </div>
    </div>);
}

export default Home;
