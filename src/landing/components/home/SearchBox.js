import {
    Box, CircularProgress, Grid, isWidthUp, Paper, TextField, Typography, withStyles, withWidth
} from "@material-ui/core";
import AnimatedNumber from "animated-number-react";
import clsx from 'classnames';
import React, { useCallback, useEffect, useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import GoalLogo from "../../../dashboard/components/Logos/Goal";
import OergLogo from "../../../dashboard/components/Logos/Oerg";
import SkillLogo from "../../../dashboard/components/Logos/Skill";
import TopicLogo from "../../../dashboard/components/Logos/Topic";
import useSearch from "../../../dashboard/views/Search/useSearch";
import { loadGeneralReport } from "../../utils";

const styles = (theme) => ({
    card: {
        boxShadow: theme.shadows[4],
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        padding: `${theme.spacing(7)}px ${theme.spacing(3)}px`,
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        color: '#0d717d'
    },
    image: {
        maxWidth: "100%",
        verticalAlign: "middle",
        borderRadius: theme.shape.borderRadius,
        boxShadow: theme.shadows[4],
    },
    searchText: {
        height: 40,
        fontSize: "1.5em",
        fontFamily: "Inter",
        textAlign: "center",
        color: "#0d717d",
        "&::placeholder": {
            opacity: 1,
        }
    },
    searchBox: {

    },
    searchResult: {
        minWidth: 700,
    },
    statsWrapper: {
        textAlign: "center",
        height: "100%",
        fontFamily: "Inter",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    }
});

function HeadSection(props) {
    const { classes, width, onJobClick, onSkillClick, onTopicClick, className } = props;
    const searchRef = useRef(null);
    const [reports, setReports] = useState({})
    const [loadingReports, setLoadingReports] = useState(false)
    const [openSearch] = useSearch()

    const { t } = useTranslation(["landing"]);

    const handleSearch = useCallback(
        () => {
            openSearch(
                null,
                {
                    noActions: true,
                },
                false
            )
        },
        [openSearch]
    )

    useEffect(() => {
        setLoadingReports(true);
        loadGeneralReport(
            data => {
                setReports(data);
            },
            null,
            () => setLoadingReports(false)
        )
    }, [])

    const formatValue = (value) => value.toFixed(0);

    return (
        <Paper
            className={clsx(classes.card, className)}
        >
            <Grid container>
                <Grid item xs={12}>
                    <Box mb={4}>
                        <Typography
                            variant={isWidthUp("lg", width) ? "h3" : "h4"}
                            style={{ fontFamily: "Inter" }}
                        >
                            <span style={{ fontFamily: "Lobster", fontSize: "1.1em", color: "#1f838f" }}>
                                eDoer —
                            </span> {t("Your Personal Learning Tool")}
                        </Typography>
                    </Box>
                    <Grid container spacing={2} style={{
                        marginBottom: 16,
                        height: 100,
                        alignItems: "stretch"
                    }}>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.statsWrapper} onClick={onJobClick} style={{ cursor: "pointer" }}>
                                {loadingReports ?
                                    <CircularProgress size={24} />
                                    :
                                    <>
                                        <GoalLogo className={classes.logo} title={t("Journeys")} />
                                        &nbsp; &nbsp;
                                        <AnimatedNumber
                                            value={reports.goals_addable}
                                            formatValue={formatValue}
                                        />
                                    </>

                                }
                                &nbsp; {t("Journeys")}
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.statsWrapper} onClick={onSkillClick} style={{ cursor: "pointer" }}>
                                {loadingReports ?
                                    <CircularProgress size={24} />
                                    :
                                    <>
                                        <SkillLogo className={classes.logo} />
                                        &nbsp; &nbsp;
                                        <AnimatedNumber
                                            value={reports.skills_addable}
                                            formatValue={formatValue}
                                        />
                                    </>
                                }
                                &nbsp; {t("Courses")}
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.statsWrapper} onClick={onTopicClick} style={{ cursor: "pointer" }}>
                                {loadingReports ?
                                    <CircularProgress size={24} />
                                    :
                                    <>
                                        <TopicLogo className={classes.logo} />
                                        &nbsp; &nbsp;
                                        <AnimatedNumber
                                            value={reports.topics_learnable}
                                            formatValue={formatValue}
                                        />
                                    </>
                                }
                                &nbsp; {t("Topics")}
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.statsWrapper}>
                                {loadingReports ?
                                    <CircularProgress size={24} />
                                    :
                                    <>
                                        <OergLogo className={classes.logo} />
                                        &nbsp; &nbsp;
                                        <AnimatedNumber
                                            value={reports.oers_published}
                                            formatValue={formatValue}
                                        />
                                    </>

                                }
                                &nbsp; <span title={t("Educational Packages")}>{t("Edu. Packages")}</span>
                            </Paper>
                        </Grid>
                    </Grid>
                    <Box style={{
                            marginTop: 70
                        }}
                    >
                        <TextField
                            placeholder={t("Search eDoer...")}
                            fullWidth
                            color="primary"
                            variant="outlined"
                            ref={searchRef}
                            autoFocus
                            onClick={handleSearch}
                            className={classes.searchBox}
                            inputProps={{
                                readOnly: true,
                                className: classes.searchText,
                            }}
                            // InputProps={{
                                // endAdornment: <InputAdornment position="end">
                                //     <Button
                                //         variant="contained"
                                //         color="primary"
                                //         style={{ height: 55 }}
                                //         onClick={handleSearch}
                                //     >
                                //         {t("Search")}
                                //     </Button>
                                // </InputAdornment>
                            // }}
                        />
                    </Box>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default withWidth()(
    withStyles(styles, { withTheme: true })(HeadSection)
);
