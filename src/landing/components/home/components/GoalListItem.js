import { ListItem, ListItemText } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';

function GoalListItem({ item, ...props }) {
    const language = useLanguage();
    return <ListItem
        {...props}
        component={Link}
        to={`/${language}/dashboard/goals/${item.id}`}
        button
    >
        <ListItemText
            primary={item.title}
            
        />
    </ListItem>;
}

export default GoalListItem;
