import { CardActionArea, CardHeader, Tooltip } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React, { useCallback, useMemo } from 'react';
import { Link } from 'react-router-dom';
import LockIcon from '@material-ui/icons/Lock';
import LangIcon from '../../../../dashboard/components/LangIcon';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
        marginTop: 8,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    description: {
        textOverflow: "ellipsis", 
        overflow: "hidden", 
        whiteSpace: "nowrap"
    }
}));

export default function SearchItem({ item, onClick, formatTopText, formatTitle, lang, formatBotText, formatDescription, formatAddress, isPrivate, ...props }) {
    const classes = useStyles()
    const handleClick = useCallback(
        () => onClick && onClick(item),
        [ item, onClick ]
    );

    const description = useMemo(
        () => formatDescription(item),
        [formatDescription, item]
    );

    return (
        <Link to={formatAddress(item)} {...props}>
            <Card className={classes.root} variant="outlined">
                <CardActionArea onClick={handleClick}>
                    <CardHeader
                        action={isPrivate &&
                            <Tooltip title="Private Journey">
                                <LockIcon />
                            </Tooltip>
                        }
                        title={<>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            {formatTopText(item)}
                        </Typography>
                        {formatTitle(item)} <LangIcon language={lang} small />
                        </>}
                        subheader={formatBotText(item)}
                    />
                    { description &&
                    <CardContent>
                        <Typography variant="body2" component="p" className={classes.description}>
                            {description}
                        </Typography>
                    </CardContent>
                    }

                </CardActionArea>
            </Card>
        </Link>
    )
}
