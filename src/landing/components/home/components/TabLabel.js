import React from 'react'
import { Badge, CircularProgress, makeStyles } from "@material-ui/core";
import { useMemo } from "react";

const useLabelStyles = makeStyles((theme) => ({
    root: {
        display: "inline-flex",
    },
    badge: {
      marginLeft: theme.spacing(1)
    },
    badgeNumber: ({number}) => ({
      right: theme.spacing(number > 9 ? -2 : -1)
    })
})) 

export default function TabLabel({ text, loading, newItemsCount, title, ...props }) {
    const classes = useLabelStyles({number: newItemsCount});
    const visibleIcon = useMemo(
        () => loading ? <CircularProgress color="secondary" size={22} /> : <props.icon />,
        [ loading, props.icon ]
    )

    return (
        <div className={classes.root} title={title ? title : text}>
            {visibleIcon}
            <Badge
                className={classes.badge}
                badgeContent={newItemsCount}
                color="secondary"
                classes={{
                  badge: classes.badgeNumber
                }}
            >
                {text}
            </Badge>
        </div>
    );
}