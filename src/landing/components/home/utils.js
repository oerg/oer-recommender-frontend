import axios from "axios";
import React from 'react';
import { getIndustryLabel, requestAddHandlers } from "../../../dashboard/views/helpers";
import { useLanguage } from "../../../i18n";

export function extractTitle(item) {
    return item.title;
}

export function extractLocation(t) {
    return (item) => {
        var text = '';
        if (item.country !== "") {
            text = item.country;
            if (item.city && item.city !== "")
                text += `, ${item.city}`;
        } else
            text = t("General")
        
        return <small>{text}</small>;
    }
}

export function extractIndustry(t) {
    return (item) => <small>{item.industry ? t(getIndustryLabel(item.industry)) : t("General Industry")}</small>;
}

export function extractAuthor(item) {
    return item.author ? `By ${item.author}` : "";
}

export function emptyText(item) {
    return "";
}

export function extractDescription(item) {
    return item.description;
}

export function extractJobAddress(item) {
    const language = useLanguage();
    return `/${language}/dashboard/goals/${item.id}`
}

export function extractSkillAddress(item) {
    const language = useLanguage();
    return `/${language}/dashboard/skills/${item.id}`
}

export function extractTopicAddress(item) {
    const language = useLanguage();
    return `/${language}/dashboard/topics/${item.id}`
}

export function extractOerAddress(item) {
    const language = useLanguage();
    return `/${language}/dashboard/oer-groups/${item.id}`
}

export function loadPart(part, searchStr, page, onLoad, onError, onEnd, addable=null) {
    if (!addable) {
        requestAddHandlers(
            axios.get(
                `/${part}/search/?search-term=${searchStr}&sensitive=True&page=${page}&learnable=True`
            ),
            onLoad,
            onError,
            onEnd
        )
    } else {
        requestAddHandlers(
            axios.get(
                `/${part}/search/?search-term=${searchStr}&sensitive=True&page=${page}&addable=${addable}`
            ),
            onLoad,
            onError,
            onEnd
        )
    }
}