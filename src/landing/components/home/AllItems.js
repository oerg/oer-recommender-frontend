import { List, ListItem, ListSubheader, Typography } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Skeleton } from '@material-ui/lab';
;

function AllItems({ items: propItems, title, loading, itemComponent: propItemComponent, ...props }) {
    const { t } = useTranslation(['landing']);
    const items = useMemo(
        () => {
            const orderedItems = {}
            for (const item of propItems) {
                const firstChar = item.title.charAt(0).toUpperCase();
                if (!orderedItems.hasOwnProperty(firstChar)) {
                    orderedItems[firstChar] = [];
                }
                orderedItems[firstChar].push(item);
            }
            return orderedItems;
        },
        [ propItems ]
    );

    const ItemComponent = useMemo(
        () => propItemComponent ? propItemComponent : ListItem,
        [ propItemComponent ]
    );

    return <div {...props}>
        { loading ? 
            <div>
                <Skeleton />
                <Skeleton width="60%" />
                <Skeleton width="60%" />
                <Skeleton width="60%" />
            </div>
        :
            <>
                <Typography variant='h6'>{title}</Typography>
                { !items || Object.keys(items).length === 0 ?
                    <i>{t("No items.")}</i>
                :
                    <List subheader={<li />}>
                    { Object.keys(items).map(
                        (itemGroup, index) => <li key={`section-${index}`}>
                            <ul>
                                <ListSubheader>{itemGroup}</ListSubheader>
                                { items[itemGroup].map(
                                    (item, itemIndex) => <ItemComponent item={item} key={`s${index}i${itemIndex}`} />
                                )}
                            </ul>
                        </li>
                    )}
                    </List>
                }
            </>
        }
    </div>;
}

export default AllItems;
