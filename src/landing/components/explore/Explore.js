import AppsIcon from '@material-ui/icons/Apps';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../i18n';
import Column1 from './Column1';
import Column2 from './Column2';
import ExploreArea from './components/ExploreArea';
import ExploreCollapse from './components/ExploreCollapse';
import ExploreColumn from './components/ExploreColumn';
import { cancelGetAll, getAll, loadReports } from './utils';

function Explore({ open, defaultSelectedItem, isDiscovery, ...props }) {
    const { t } = useTranslation(['landing']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);
    const [counts, setCounts] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);
    const [loadingData, setLoadingData] = useState(false);
    const [goals, setGoals] = useState(null);
    const [skills, setSkills] = useState(null);
    const [topics, setTopics] = useState(null);

    const items = useMemo(
        () => [
            {
                text: t("Journeys"),
                icon: WorkOutlineIcon,
                key: 'goals',
                count_keys: isDiscovery ? ['goals_all'] : ['goals_addable'],
            },
            {
                text: t("Courses"),
                icon: AppsIcon,
                key: 'skills',
                count_keys: isDiscovery ? ['skills_addable', 'skills_not_addable'] : ['skills_addable'],
            },
            {
                text: t("Learning Topics"),
                icon: ChatBubbleOutlineIcon,
                key: 'topics',
                count_keys: isDiscovery ? ['topics_learnable', 'topics_not_learnable'] : ['topics_learnable'],
            },
        ],
        [t, isDiscovery]
    );     

    const setData = useCallback(
        (section, data) => {
            switch (section) {
                case "goals":
                    setGoals(data);
                    break;
                case "skills":
                    setSkills(data);
                    break;
                case "topics":
                    setTopics(data);
                    break;
                default:
                    break;
            }
        },
        []
    );

    const getData = useCallback(
        (section) => {
            switch (section) {
                case "goals":
                    return goals;
                case "skills":
                    return skills;
                case "topics":
                    return topics;
                default:
                    return null;
            }
        },
        [goals, skills, topics]
    );
    // load counts
    useEffect(
        () => {
            loadReports(
                language,
                res => {
                    setCounts(res);
                }
            )
        },
        [language]
    );

    // disable body scrolling
    const defaultBodyOverflowStyles = useMemo(
        () => window.document.getElementById("root").style.overflowY,
        []
    );
    useEffect(
        () => {
            const rootDiv = window.document.getElementById("root");
            if (open) {
                rootDiv.style.overflowY = "hidden";
                return () => rootDiv.style.overflowY = defaultBodyOverflowStyles;
            } else {
                rootDiv.style.overflowY = defaultBodyOverflowStyles;
            }
        },
        [open, defaultBodyOverflowStyles]
    );

    const handleDeselect = useCallback(
        () => setSelectedItem(null),
        []
    );

    const handleSelect = useCallback(
        (e, item, lang) => {
            cancelGetAll();
            setSelectedItem(item);
            if (item !== null && (lang || !getData(item.key))) {
                setLoadingData(true);
                getAll(
                    item.key,
                    !isDiscovery,
                    lang ? lang : language,
                    ({ data }) => {
                        const sorted = data.sort((a, b) => a.title.localeCompare(b.title));
                        setData(item.key, sorted);
                    },
                    null,
                    () => setLoadingData(false)
                );
            }
        },
        [setData, getData, language, isDiscovery]
    );


    useEffect(() => {
        if (selectedItem === null && defaultSelectedItem !== null && defaultSelectedItem !== items.indexOf(selectedItem)) {
            setSelectedItem(items[defaultSelectedItem])
            handleSelect(null, items[defaultSelectedItem], language);
        }
    }, [defaultSelectedItem, handleSelect, items, selectedItem])


    const handleLanguageChange = useCallback(
        (e) => {
            setLanguage(e.target.value);
            setGoals(null);
            setSkills(null);
            setTopics(null);
            handleSelect(e, selectedItem, e.target.value);
        },
        [selectedItem, handleSelect]
    );

    return <ExploreArea isDiscovery={isDiscovery} {...props}>
        <ExploreCollapse in={open}>
            <ExploreColumn
                width={400}
                onClick={handleDeselect}
            >
                <Column1
                    items={items}
                    counts={counts}
                    selectedItem={selectedItem}
                    onSelect={handleSelect}
                    language={language}
                    onLanguageChange={handleLanguageChange}
                />
            </ExploreColumn>
        </ExploreCollapse>
        <ExploreCollapse in={open && (selectedItem !== null || loadingData)}>
            <ExploreColumn
                width={800}
            >
                <Column2
                    loading={loadingData}
                    language={language}
                    goals={goals}
                    skills={skills}
                    topics={topics}
                    selectedSection={selectedItem ? selectedItem.key : ""}
                    isDiscovery={isDiscovery}
                />
            </ExploreColumn>
        </ExploreCollapse>
    </ExploreArea>
}

export default Explore;
