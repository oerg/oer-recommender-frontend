import { Box } from '@material-ui/core';
import React from 'react';

function ExploreArea({isDiscovery, ...props}) {
    return <Box 
        display="flex" 
        justifyContent="flex-start" 
        alignItems="stretch" 
        height={`calc(100vh - ${isDiscovery ? '84' : '64'}px)`} // 64px is the app bar height
        position="fixed" 
        left={0}
        top={isDiscovery ? 84 : 64} // 64px is the app bar height
        zIndex={1401}
        {...props} 
    />
}

export default ExploreArea;
