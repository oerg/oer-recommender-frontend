import { Collapse, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles({
    fullHeight: {
        height: "100%",
    }
})

function ExploreCollapse({ classes: propClasses, ...props }) {
    const classes = useStyles();
    return <Collapse {...props} classes={{ 
        root: classes.fullHeight, 
        wrapper: classes.fullHeight, 
        wrapperInner: classes.fullHeight,
        ...propClasses 
    }} />;
}

export default ExploreCollapse;
