import React, { useCallback } from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Chip, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';

function ExploreListItem({ text, iconComponent: IconComponent, onClick, item, count, ...props }) {
    const handleClick = useCallback(
        (e) => {
            e.stopPropagation();
            onClick && onClick(e, item);
        },
        [onClick, item]
    );

    return <ListItem button onClick={handleClick} {...props}>
        <ListItemIcon>
            <IconComponent />
        </ListItemIcon>
        <ListItemText primary={<>{text} {count ? <Chip size='small' label={count > 99 ? "99+" : count} /> : null}</>} />
        <ListItemIcon>
            <ArrowForwardIosIcon />
        </ListItemIcon>
    </ListItem>;
}

export default ExploreListItem;
