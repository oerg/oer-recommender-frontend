import { FormControl, InputLabel, List, MenuItem, Select, Typography } from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getExistingLanguages } from '../../../data';
import { useLanguage } from '../../../i18n';
import ExploreListItem from './components/ExploreListItem';

function Column1({ selectedItem, onSelect, language, onLanguageChange, items, counts }) {
    const { t } = useTranslation(['landing']);
    const [allLanguages, setAllLanguages] = useState([])
    const systemLanguage = useLanguage();

    useEffect(
        () => {
            getExistingLanguages(
                systemLanguage, 
                (langs) => {
                    setAllLanguages(langs)
                }
            )
        }, 
        [systemLanguage, getExistingLanguages]
    )
    

    const handleClick = useCallback(
        e => e.stopPropagation(),
        []
    );

    return <>
        <Typography
            variant='h5'
            style={{ padding: 16 }}
        >
            {t("All the contents")}
        </Typography>
        <List>
            { items && items.map(
                (item, index) => (
                    <ExploreListItem
                        key={index}
                        item={item}
                        text={item.text}
                        iconComponent={item.icon}
                        count={counts && item.count_keys.map(key => counts[key]).reduce((ps, a) => ps + a, 0)}
                        onClick={onSelect}
                        selected={selectedItem === item}
                    />
                )
            )}
        </List>
        <FormControl variant='outlined' style={{
            width: "100%",
            padding: "0 10px",
        }}>
            <InputLabel 
                id="explore-language-selector-label"
                style={{
                    padding: "0 10px",
                }}
            >
                {t("Language")}
            </InputLabel>
            <Select 
                labelId='explore-language-selector-label'
                label={t("Language")}
                value={language}
                onChange={onLanguageChange}
                onClick={handleClick}
            >
                { allLanguages.map(
                    (lang, index) => <MenuItem key={index} value={lang.code}>
                        {lang.label}
                    </MenuItem>
                )}
            </Select>
        </FormControl>
    </>;
}

export default Column1;
