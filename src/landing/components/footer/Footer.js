import {
    Box, Grid,
    List, ListItem, Paper, Typography,
    withStyles,
    withWidth
} from "@material-ui/core";
import transitions from "@material-ui/core/styles/transitions";
import PropTypes from "prop-types";
import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import ChangeLanguageButton from "../../../dashboard/components/ChangeLanguageButton";
import BMBFLogo from '../../../dashboard/views/Footer/images/bmbf.jpg';
import EULogo from '../../../dashboard/views/Footer/images/euflag.jpg';
import EULogo2 from '../../../dashboard/views/Footer/images/euflag2.png';
import TibLogo from '../../../dashboard/views/Footer/images/TIB_Logo.png';
import UALogo from '../../../dashboard/views/Footer/images/UA_Logo.svg';
import { useLanguage } from "../../../i18n";



const styles = theme => ({
    footerInner: {
        backgroundColor: theme.palette.common.darkBlack,
        paddingTop: theme.spacing(8),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingBottom: theme.spacing(6),
        [theme.breakpoints.up("sm")]: {
            paddingTop: theme.spacing(10),
            paddingLeft: theme.spacing(16),
            paddingRight: theme.spacing(16),
            paddingBottom: theme.spacing(10)
        },
        [theme.breakpoints.up("md")]: {
            paddingTop: theme.spacing(10),
            paddingLeft: theme.spacing(10),
            paddingRight: theme.spacing(10),
            paddingBottom: theme.spacing(10)
        }
    },
    brandText: {
        fontFamily: "'Baloo Bhaijaan', cursive",
        fontWeight: 400,
        color: theme.palette.common.white
    },
    footerLinks: {
        marginTop: theme.spacing(2.5),
        marginBot: theme.spacing(1.5),
        color: theme.palette.common.white
    },
    infoIcon: {
        color: `${theme.palette.common.white} !important`,
        backgroundColor: "#33383b !important"
    },
    socialIcon: {
        fill: theme.palette.common.white,
        backgroundColor: "#33383b",
        borderRadius: theme.shape.borderRadius,
        "&:hover": {
            backgroundColor: theme.palette.primary.light
        }
    },
    link: {
        cursor: "Pointer",
        color: theme.palette.common.white,
        transition: transitions.create(["color"], {
            duration: theme.transitions.duration.shortest,
            easing: theme.transitions.easing.easeIn
        }),
        "&:hover": {
            color: theme.palette.primary.light
        }
    },
    whiteBg: {
        backgroundColor: theme.palette.common.white
    },
    list: {
        marginBottom: "0",
        padding: "0",
        marginTop: theme.spacing(2),
    },
    inlineBlock: {
        display: "inline-block",
        padding: "0px",
        width: "auto",
        margin: "5px 10px",
        textAlign: "center",
        '& a': {
            color: '#DDD',
            '&:hover': {
                color: '#8f9296',
            }
        },
    },
});

function Footer(props) {
    const { classes, attribute } = props;
    const { t } = useTranslation(["landing"]);
    const language = useLanguage();

    return (
        <footer style={{ paddingTop: 0 }}>
            <div className={classes.footerInner}>
                <Grid container spacing={2} justifyContent="center">
                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.uva.nl/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("University of Amsterdam")} height={60}
                                    title={t("University of Amsterdam")} src={UALogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.tib.eu/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Technische Informationsbibliothek (TIB)")} height={60}
                                    title={t("Technische Informationsbibliothek (TIB)")} src={TibLogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("European Education and Culture Executive Agency")} height={60}
                                    title={t("European Education and Culture Executive Agency")} src={EULogo2} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("European Education and Culture Executive Agency")} height={60}
                                    title={t("European Education and Culture Executive Agency")} src={EULogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.bmbf.de/bmbf/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Federal Ministry of Education and Research")} height={60}
                                    title={t("Federal Ministry of Education and Research")} src={BMBFLogo} />
                            </a>
                        </Paper>
                    </Grid>
                </Grid>
                <Box textAlign="center">
                    <List className={classes.list}>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/imprint/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Imprint")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://labs.tib.eu/edoer/tos" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Terms of Use")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/data-protection/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Data Protection")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <Link to={`/${language}/aboutus`} className={classes.block}>
                                {t("About the Project")}
                            </Link>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <Link to={`/${language}/events`} className={classes.block}>
                                {/* <Badge color="primary" variant="dot"> */}
                                {t("Events")}
                                {/* </Badge> */}
                            </Link>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="mailto:edoer@admin.tib.eu" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Contact Us")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://tib.eu/umfragen/index.php/survey/index/sid/887411/newtest/Y/lang/en" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Send Feedback")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <ChangeLanguageButton style={{
                                height: 30,
                                width: 40,
                            }} />
                        </ListItem>
                    </List>
                    {attribute && <Typography className={classes.inlineBlock} style={{ color: "#8f9296" }} paragraph>
                        {t("Photos used on this page are obtained from freepik.com")}:&nbsp;
                        <a href="https://www.freepik.com/photos/coffee-man" className={classes.link}
                            target="_blank" rel="noopener noreferrer">
                            [1]
                        </a>&nbsp;
                        <a href="https://www.freepik.com/photos/black-lady" className={classes.link}
                            target="_blank" rel="noopener noreferrer">
                            [2]
                        </a>&nbsp;
                        <a href="https://www.freepik.com/photos/computer-typing" className={classes.link}
                            target="_blank" rel="noopener noreferrer">
                            [3]
                        </a>&nbsp;
                        <a href="https://www.freepik.com/photos/school-book" className={classes.link}
                            target="_blank" rel="noopener noreferrer">
                            [4]
                        </a>
                    </Typography>}
                    {/*<Typography style={{ color: "#8f9296", marginTop: '15px' }} paragraph>*/}
                    {/*    {t("eDoer is licensed under")} <a rel="license noopener noreferrer" href="http://creativecommons.org/licenses/by-nc/4.0/" target="_blank">*/}
                    {/*        <img alt="Creative Commons License" style={{ borderWidth: 0 }} src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" />*/}
                    {/*    </a>*/}
                    {/*</Typography>*/}
                </Box>
            </div>
        </footer>
    );
}

Footer.propTypes = {
    theme: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    width: PropTypes.string.isRequired,
    isStatic: PropTypes.bool,
};

Footer.defaultProps = {
    isStatic: false,
}

export default withWidth()(withStyles(styles, { withTheme: true })(Footer));
