import { Box, Container, Divider, makeStyles, Paper, Typography } from '@material-ui/core'
import React from 'react'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles({
    root: {
        marginTop: 100,
        marginBottom: 20,
    },
    wrapper: {
        padding: 30,
    },
    title: {
        fontWeight: 'bold',
    },
});

function TermsOfUse() {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])

    return (
        <Container maxWidth="md" className={classes.root}>
            <Paper elevation={2} className={classes.wrapper}>
                <Typography variant="h5">
                    {t("Terms of use of eDoer")}
                </Typography>
                <Divider />
                <p>
                    Last update: 13.06.2022
                </p>
                <p>
                    eDoer is an online platform which offers goal based personalized education by Technische Informationsbibliothek (TIB), Welfengarten 1B, 30167 Hannover, Germany (hereinafter &quot;TIB&quot;).
                </p>
                <p>
                    <a href="https://www.tib.eu/en/service/terms-of-use" rel="noopener noreferrer" target="_blank">Terms of use of the TIB</a>&nbsp;(BenO TIB) apply accordingly. Acc. to &sect; 1 (2) BenO TIB &nbsp;the terms of use of eDoer supplementBenOTIB
                </p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 1 Scope of application
                </Typography>
                <p>
                    This research project departs from the recent, dramatic changes in global societies, forcing many citizens to re-skill themselves to (re)gain employment. Therefore, learners need to develop skills with a view to being qualified to fill current/future jobs. On the other hand, online learning is getting more and more attention from learners which has led to the significance of developing online learning platforms. Accordingly, high-quality, personalized educational content and services are essential to serve this high demand for learning. Free and Open Educational Resources can play a key role in this regard, as they are available in a wide range of learning and occupational contexts globally. However, their applicability so far has been limited, due to low metadata quality and complex quality control. These issues resulted in a lack of personalized functions, like recommendation and search.
                </p>
                According to the above mentioned issues, we are developing eDoer which helps our users. Our users might be:
                <Box pl={3}>
                    <p>
                        a. Learners: eDoer shall help Learners to select their journeys, receive personalized free educational recommendations, test their knowledge, and request for help from experts/mentors
                    </p>
                    <p>
                        b. Experts: eDoer shall help Experts to create and maintain up-to-date educational contents and pathways with the help AI-based recommendations. Experts&rsquo; opinions are automatically approved by the system. Moreover, the Experts can help learners as mentors.
                    </p>
                    <p>
                        c. content curators: eDoer shall help content curators to create and maintain up-to-date educational contents and pathways with the help AI-based recommendations. Content curators&rsquo; opinions must be approved by other content curators or experts.
                    </p>
                </Box>
                <p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 2 Registration and admission
                </Typography>
                <ol>
                    <li>Users must register with eDoer, providing the necessary personal data, in order to perform their duties. <br />
                    Learners can register with eDoer in order to select their journeys, receive personalized educational recommendations through their journeys, test their knowledge, and submit mentoring requests.<br />
                    Experts and content curators can create and maintain educational contents. Experts can also help in resolving mentoring requests. The expert role can only be assigned by the Site Administrator (TIB). To get the expert role, experts must register as a normal user and afterward press the &ldquo;Become an Expert&rdquo; button on the course/topic page they are expert in from the content curation part in order to apply for the role. Their application will be checked by the website administrators and they will be informed in a few days.
                    </li>
                    <li>Users are obliged to fill in the mandatory fields truthfully and completely when registering, to keep their data up to date, to keep their access data secret, and to protect it from access by third parties. Joint use of a user account is excluded. In the event of a violation, &sect; 8 No. 3 of the terms of use shall apply. In the course of the registration process, users acknowledge the <a href="https://www.tib.eu/en/service/terms-of-use" rel="noopener noreferrer" target="_blank">terms of Use of the TIB</a>&nbsp;and the terms of use of eDoer (this document), declare their consent to the <a href="https://www.tib.eu/en/service/data-protection/" rel="noopener noreferrer" target="_blank">eDoer&#39;s privacy statement</a>. </li>
                    <li>The registration is completed with the transmission of the password (admission). Admission establishes a user relationship between the TIB and the registered users.</li>
                    <li>There is no entitlement to admission. The TIB is entitled to revoke the admission and to delete the account if a violation of these terms of use has occurred or is to be expected. The right to disable access remains unaffected.</li>
                    <li>In the event of updates to these terms of use, registered users will be notified of the changes two weeks in advance via e-mail. If users do not agree to the changes, they must object to the changes within a period of two weeks. Upon expiry of the two-week period, the registered users are deemed to have given their consent. In the event of an objection, the user relationship shall end. </li>
                    <li>The user relationship may be terminated at any time and without notice by submitting a declaration in textform to TIB. In the event of termination the user account on eDoer will be deactivated and personal data of the user profile deleted in accordance with &sect; 7 within one month of receipt of the declaration. </li>
                    <li>Users guarantee that their published content does not violate any rights of third parties (e.g. copyrights, protection of the performing artist, personal and publicity rights, data protection). If other persons are involved in the production, the registered users confirm that all those involved are aware of the content of this agreement and have given their consent.</li>
                    <li>Users guarantee that their published content does not violate applicable law or morality, in particular that it does not constitute a criminal offence or a misdemeanour (e.g. violation of copyright, trademark law, competition law) or that it does not constitute offensive, discriminatory, racist, pornographic or extremist content.</li></ol><p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 3 Information for learners, experts, and content curators
                </Typography>
                <ol>
                    <li>Experts can login into eDoer in order to create and maintain educational contents (i.e. journeys, courses, topics, educational packages). Experts&rsquo; maintaining-suggestions regarding the courses/topics that they are expert in are accepted by eDoer automatically. Furthermore, experts can determine to do mentoring in their expertise area. In this case, they can decide to accept/reject mentoring requests and take care of the ones they have accepted.</li>
                    <li>Content curators can login into eDoer in order to create and maintain educational contents (i.e. journeys, courses, topics, educational packages). Maintaining suggestions will be reviewed by other content curators and/or experts before acceptance. </li>
                    <li>Learners can register with eDoer to select their journeys, receive personalized educational recommendations through their journeys, test their knowledge, and submit mentoring requests. However, the educational contents can be viewed without logging in into eDoer.</li>
                </ol>
                <Typography variant="h6" className={classes.title}>
                    &sect; 4 Rights and Obligations of Experts
                </Typography>
                <ol>
                    <li>Experts register with eDoer as a normal user and request to become an expert from the content curation part. The role is assigned by the Site Administrator (TIB).</li>
                    <li>Experts are responsible for maintaining the contents by adding/removing courses/topics/educational packages. Uploading any content without having the required right is not permitted. </li>
                    <li>Experts agree to maintain the contents correct and up-to-date. This comprises that they</li>
                    <ol type="a">
                        <li>Update the contents if needed by creating suggestions. The expert suggestions are automatically accepted and applied by eDoer.</li>
                        <li>Consider and review all maintenance-suggestions as a neutral Expert </li>
                        <li>Are responsible for the legal requirements, &nbsp;correctness and appropriateness of the content they upload</li>
                        <li>Avoid any actual or perceived conflict of interest, e.g., they should not promote their created resources if they do not include high-quality related content.</li>
                    </ol>
                    <li>Experts can receive mentoring requests from learners if they select &ldquo;Show mentoring request for me. (In case your account has been approved as a mentor)&rdquo; from the settings part of their profile settings. After that they (as a mentor) can accept/reject the requests. The mentors are responsible for resolving their accepted requests. They shall also take care of the privacy of the mentees (including their personal and contact information)</li>
                </ol>
                <Typography variant="h6" className={classes.title}>
                    &sect; 5 Rights and Obligations of Content Curators
                </Typography>
                <ol>
                    <li>Content curators register with eDoer by providing the mandatory information (email and password).For becoming a content curator, users select &ldquo;I want to participate in the content curation process and provide content for other learners.&rdquo; in the settings of the profile settings.</li>
                    <li>Content curators are responsible for creating and maintaining educational contents (i.e. journeys, courses, topics, educational packages). This includes: </li>
                    <ol type="a">
                        <li>Creating journeys, courses, topics and educational packages</li>
                        <li>checking the legal requierements when Uploading any content Creating maintenance suggestions if they find out that a content needs to be updated. It should be mentioned that the suggestions will be validated by other content curators and experts.</li>
                        <li>Content curators are responsible for the legal requirements, &nbsp;correctness and appropriateness of the content they upload/suggest</li>
                    </ol>
                </ol>
                <Typography variant="h6" className={classes.title}>
                    &sect; 6 Rights and Obligations of Learners
                </Typography>
                <ol start="1"><li>Learners can register with eDoer by providing the mandatory information (email and password)</li><li>They are able to permanently delete their account and all the related information from their profile settings</li><li>Learners can select their journeys, learn through them, and test their knowledge. Also, they can submit a mentoring request if they have any problem in each of their journeys.</li><li>Learners are able to report contents (journeys, courses, topics, educational packages) in case they have any problem (e.g., for being inappropriate or irrelevant) with them.</li></ol><p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 7 Deletion of user accounts and personal data
                </Typography>
                <p>
                    If the user relationship ends, user accounts will be deactivated immediately. The stored personal data will be deleted promptly.
                </p>
                <p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 8 Rights and Obligations of the TIB
                </Typography>
                <ol start="1"><li>The TIB reserves the right to change the scope of functions, the appearance of the platform and these terms of use at any time. Users will be informed of major changes two weeks in advance. </li><li>The TIB is entitled to terminate its service at any time. If the service is discontinued, the TIB is obliged to ensure the provision of publications and long-term archiving by other TIB services or services of other organizations. </li><li>If there are concrete indications of a violation of these conditions of use, TIB is entitled to disable access to or delete content, to restrict the use of the services or to disable access of the registered users concerned. When choosing a measure, the legitimate interests of the registered users shall be taken into account, in particular whether there are indications that the registered users are not responsible for the violation.</li><li>TIB does not review, control or monitor the data sets and information stored and made accessible on the repository with regard to their legality, either before they are made available online or afterwards. There is no examination of the actual legal situation, in particular with regard to protectability and rights ownership. The TIB does not investigate on its own initiative on circumstances that indicate illegal activities.</li></ol><p></p>
                <p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 9 Data protection
                </Typography>
                <p>
                    If users obtain access to and knowledge of personal data of other users in the course of the editorial process, they are obliged to maintain confidentiality. When processing personal data, the provisions of the General Data Protection Regulation and the Lower Saxony Data Protection Act must be observed. This means in particular that
                </p>
                <ol type="A">
                    <li>a transfer of personal data to third parties (persons or institutions outside the editorial process or not directly involved) is not permitted, </li>
                    <li>personal data that can be viewed in eDoer may only be processed within the scope of this research project in order to publish the results and improve the quality of the offered services. </li>
                </ol>
                <Typography variant="h6" className={classes.title}>
                    &sect; 10 Limitations of liability
                </Typography>
                <p>
                    The TIB accepts no liability that the platform and its contents are always available uninterrupted, unchanged and complete, or that they are up-to-date and free of errors of an editorial or technical nature. No liability is assumed for damages resulting from reliance on the contents of the platform or its use.
                </p>
                <p>
                    In principle, TIB is only liable for damage caused intentionally or by gross negligence. In the event of injury to life, body or health, statutory provisions shall apply.
                </p>
                <p>
                    Liability for damages resulting from unauthorized use of password-protected logins by third parties is not assumed.
                </p>
                <p>
                    Insofar as liability is limited or excluded, this shall also apply to the benefit of senior employees, representatives, and vicarious agents of TIB.
                </p>
                <p></p>
                <Typography variant="h6" className={classes.title}>
                    &sect; 11 Miscellaneous
                </Typography>
                <ol start="1"><li>The user relationship and these terms of use are subject to German law.</li><li>The place of jurisdiction shall be Hannover.</li><li>In the event of disputes, the TIB shall endeavor to reach an amicable settlement.</li><li>Should one or more provisions of these special terms and conditions be or become invalid, this shall not affect the validity of the remaining provisions of these special terms and conditions. The invalid provision shall then be replaced by another legally permissible provision which corresponds as far as possible to the sense and purpose of the invalid provision.</li></ol>
            </Paper>
        </Container >
    )
}

export default TermsOfUse