import {
    Button, CircularProgress, Dialog, DialogActions, DialogContent, TextField, Typography,
    withStyles
} from "@material-ui/core";
import Axios from "axios";
import { withSnackbar } from "notistack";
import PropTypes from "prop-types";
import React, { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

const styles = (theme) => ({
    dialogContent: {
        paddingTop: theme.spacing(2),
    },
    dialogActions: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },
});

function ChangePassword(props) {
    const { open, onClose, classes, enqueueSnackbar } = props;
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState('');
    const [hasError, setHasError] = useState(false);
    const { t } = useTranslation(["landing"])

    const sendPasswordEmail = useCallback((email) => {
        setIsLoading(true);
        Axios.post('/members/password-reset/', { email })
            .then(res => {
                setIsLoading(false);
                onClose();
                enqueueSnackbar(
                    t("A link has been send to your email. Please follow the instructions."),
                    {
                        variant: "success",
                    }
                )
            })
            .catch(err => {
                setHasError(true);
                setIsLoading(false);
                enqueueSnackbar(
                    t("This email does not exist."),
                    {
                        variant: "error",
                    }
                )
            })
    }, [setIsLoading, onClose, enqueueSnackbar, t]);

    return (
        <Dialog
            open={open}
            onClose={onClose}
            disableEscapeKeyDown={isLoading}
            maxWidth="xs"
        >
            <form
                onSubmit={(e) => {
                    e.preventDefault();
                    setHasError(false);
                    sendPasswordEmail(email);
                }}
            >
                <DialogContent className={classes.dialogContent}>
                    <Typography paragraph>
                        {t("Enter your email address below and we will send you instructions on how to reset your password.")}
                    </Typography>
                    <TextField
                        variant="outlined"
                        margin="dense"
                        required
                        fullWidth
                        label={t("Email Address")}
                        autoFocus
                        type="email"
                        autoComplete="off"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        error={hasError}
                    />
                </DialogContent>
                <DialogActions className={classes.dialogActions}>
                    <Button onClick={onClose} disabled={isLoading}>
                        {t("Cancel")}
                    </Button>
                    <Button
                        type="submit"
                        variant="contained"
                        color="secondary"
                        disabled={isLoading}
                    >
                        {t("Reset Password")}
                        {isLoading && <CircularProgress size={12} />}
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}

ChangePassword.propTypes = {
    onClose: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withSnackbar(withStyles(styles, { withTheme: true })(ChangePassword));
