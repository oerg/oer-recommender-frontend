import {
    Button,
    Checkbox,
    FormControlLabel,
    FormHelperText,
    SvgIcon,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import classNames from "classnames";
import Cookies from "js-cookie";
import React, {Fragment, useCallback, useContext, useRef, useState} from "react";
import GoogleLogin from 'react-google-login';
import {useTranslation} from "react-i18next";
import {useLocation, withRouter} from "react-router-dom";
import {useLanguage} from "../../../i18n";
import {UserContext} from '../../User';
import ButtonCircularProgress from "./components/ButtonCircularProgress";
import FormDialog from "./components/FormDialog";
import HighlightedInformation from "./components/HighlightedInformation";
import VisibilityPasswordTextField from "./components/VisibilityPasswordTextField";
import {loginWithGoogle} from "./utils";

const styles = (theme) => ({
    forgotPassword: {
        marginTop: theme.spacing(2),
        color: theme.palette.primary.main,
        cursor: "pointer",
        "&:enabled:hover": {
            color: theme.palette.primary.dark,
        },
        "&:enabled:focus": {
            color: theme.palette.primary.dark,
        },
    },
    disabledText: {
        cursor: "auto",
        color: theme.palette.text.disabled,
    },
    formControlLabel: {
        marginRight: 0,
    },
});

function LoginDialog(props) {
    const {
        history,
        classes,
        onClose,
        openChangePasswordDialog,
        open,
    } = props;
    const { t } = useTranslation(["landing"]);
    const language = useLanguage();

    const user = useContext(UserContext);

    const [isLoading, setIsLoading] = useState(false);
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [statusError, setStatusError] = useState(null);
    const [status, setStatus] = useState(null);
    const [asDeveloper, setAsDeveloper] = useState(
        () => Cookies.get("as_dev") === "true"
    )
    const locationState = useLocation().state;
    const loginEmail = useRef();
    const loginPassword = useRef();

    // follow up redirection after login
    const followUp = useCallback(
        () => {
            if (locationState && locationState.backUrl) {
                history.replace(locationState.backUrl)
            } else if (asDeveloper) {
                history.push(`/${language}/dashboard/discovery/`)
            } else {
                history.push(`/${language}/dashboard`)
            }
        },
        [locationState, language, history, user, asDeveloper]
    )

    const login = useCallback(() => {
        setIsLoading(true);
        setStatus(null);
        setStatusError(null);
        user.login(loginEmail.current.value, loginPassword.current.value, (response => {
            if (response.logged_in) {
                followUp()
            } else {
                if (typeof response === 'object' && !("logged_in" in response))
                    if (response.error && response.error.includes('invalid_grant'))
                        setStatusError(t("Invalid username or password."))
                    else
                        setStatusError(t("There is a problem with logging in. Please try again."))
                setStatus("invalid");
                loginEmail.current.select();
                loginEmail.current.focus();
                setIsLoading(false);
            }
        }))
    }, [followUp, user, t]);

    const handleGoogleLogin = useCallback(
        authObj => {
            setIsLoading(true);
            setStatus(null);
            setStatusError(null);
            loginWithGoogle(
                authObj,
                () => {
                    user.googleLogin(
                        authObj.tokenId,
                        response => {
                            if (response.logged_in) {
                                followUp()
                            } else {
                                if (typeof response === 'object' && !("logged_in" in response))
                                    setStatusError(t("There is a problem with logging in. Please try again."));
                                setStatus("invalid");
                                loginEmail.current.select();
                                loginEmail.current.focus();
                                setIsLoading(false);
                            }
                        }
                    )
                },
                () => {
                    setStatus("invalid");
                },
                () => setIsLoading(false)
            );
        },
        [followUp, user, t]
    )

    const handleAsDeveloper = useCallback(
        e => {
            setAsDeveloper(e.target.checked)
            Cookies.set("as_dev", e.target.checked)
        },
        []
    )

    return (
        <Fragment>
            <FormDialog
                open={open}
                onClose={onClose}
                loading={isLoading}
                onFormSubmit={(e) => {
                    e.preventDefault();
                    login();
                }}
                hideBackdrop
                headline={t("Login")}
                content={
                    <Fragment>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            error={status === "invalid"}
                            required
                            fullWidth
                            label={t("Email Address")}
                            inputRef={loginEmail}
                            autoFocus
                            autoComplete="off"
                            type="text"
                            onChange={() => {
                                if (status === "invalid") {
                                    setStatus(null);
                                }
                            }}
                        />
                        <VisibilityPasswordTextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            error={status === "invalid"}
                            label={t("Password")}
                            inputRef={loginPassword}
                            autoComplete="off"
                            onChange={() => {
                                if (status === "invalid") {
                                    setStatus(null);
                                }
                            }}
                            onVisibilityChange={setIsPasswordVisible}
                            isVisible={isPasswordVisible}
                        />
                        <FormControlLabel
                            className={classes.formControlLabel}
                            control={<Checkbox color="primary" onChange={handleAsDeveloper} checked={asDeveloper} />}
                            label={<Typography variant="body1">{t("Login as a Content Developer")}</Typography>}
                        />
                        {status === "verificationEmailSend" && (
                            <HighlightedInformation>
                                {t("We have sent instructions on how to reset your password to your email address")}
                            </HighlightedInformation>
                        )}
                        {status === "invalid" &&
                            <FormHelperText error>
                                {statusError ?
                                    statusError
                                    :
                                    t("Credentials are not correct. Please try again.")
                                }
                            </FormHelperText>
                        }
                    </Fragment>
                }
                actions={
                    <Fragment>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            disabled={isLoading}
                            size="large"
                        >
                            {t("Login")}
                            {isLoading && <ButtonCircularProgress />}
                        </Button>
                        <GoogleLogin
                            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                            onSuccess={handleGoogleLogin}
                            onFailure={handleGoogleLogin}
                            isSignedIn={false}
                            render={
                                renderProps =>
                                    <Button
                                        fullWidth
                                        onClick={() => {
                                            setIsLoading(true);
                                            renderProps.onClick();
                                        }}
                                        disabled={renderProps.disabled || isLoading}
                                        variant="outlined"
                                        color="primary"
                                        style={{ marginTop: 8 }}
                                        startIcon={
                                            <SvgIcon viewBox="0 0 48 48" htmlColor="#db3236">
                                                <path d="M23.4 46.9c-12.5 0-23-10.2-23-22.7s10.5-22.7 23-22.7c6.9 0 11.9 2.7 15.6 6.3l-4.4 4.4c-2.7-2.5-6.3-4.4-11.2-4.4C14.2 7.7 7.1 15 7.1 24.2c0 9.1 7.1 16.5 16.3 16.5 5.9 0 9.3-2.4 11.5-4.5 1.8-1.8 2.9-4.3 3.4-7.8H23.5v-6.2h20.7c.2 1.1.3 2.4.3 3.9 0 4.7-1.3 10.4-5.4 14.5-3.9 4.1-9 6.3-15.7 6.3zm52.7-14.6c0 8.4-6.6 14.6-14.7 14.6s-14.7-6.2-14.7-14.6c0-8.5 6.6-14.6 14.7-14.6 8.1-.1 14.7 6.1 14.7 14.6zm-6.4 0c0-5.3-3.8-8.9-8.3-8.9-4.4 0-8.3 3.6-8.3 8.9 0 5.2 3.8 8.9 8.3 8.9 4.5-.1 8.3-3.7 8.3-8.9zm38.3 0c0 8.4-6.6 14.6-14.7 14.6s-14.7-6.2-14.7-14.6c0-8.5 6.6-14.6 14.7-14.6 8.1-.1 14.7 6.1 14.7 14.6zm-6.5 0c0-5.3-3.8-8.9-8.3-8.9-4.4 0-8.3 3.6-8.3 8.9 0 5.2 3.8 8.9 8.3 8.9 4.5-.1 8.3-3.7 8.3-8.9zm37-13.8v26.3c0 10.8-6.4 15.2-13.9 15.2-7.1 0-11.4-4.8-13-8.6l5.6-2.3c1 2.4 3.4 5.2 7.4 5.2 4.8 0 7.8-3 7.8-8.6v-2.1h-.2c-1.4 1.8-4.2 3.3-7.7 3.3-7.3 0-14-6.4-14-14.6 0-8.3 6.7-14.7 14-14.7 3.5 0 6.3 1.6 7.7 3.3h.2v-2.4h6.1zm-5.7 13.8c0-5.2-3.4-8.9-7.8-8.9s-8.1 3.8-8.1 8.9c0 5.1 3.7 8.8 8.1 8.8 4.4 0 7.8-3.7 7.8-8.8zm16-29.2V46h-6.2V3.1h6.2zm24.9 34l5 3.3c-1.6 2.4-5.5 6.5-12.2 6.5-8.3 0-14.5-6.4-14.5-14.6 0-8.7 6.3-14.6 13.8-14.6 7.6 0 11.3 6 12.5 9.3l.7 1.7-19.6 8.1c1.5 2.9 3.8 4.4 7.1 4.4s5.5-1.7 7.2-4.1zm-15.3-5.3l13.1-5.4c-.7-1.8-2.9-3.1-5.4-3.1-3.4 0-7.9 2.9-7.7 8.5z" /><path fill="none" d="M0 1h180v59.5H0z" />
                                            </SvgIcon>
                                        }
                                    >
                                        {t("Login with Google")}
                                        {isLoading && <ButtonCircularProgress />}
                                    </Button>
                            }
                        />
                        <Typography
                            align="center"
                            className={classNames(
                                classes.forgotPassword,
                                isLoading ? classes.disabledText : null
                            )}
                            color="primary"
                            onClick={isLoading ? null : openChangePasswordDialog}
                            role="button"
                        >
                            {t("Forgot Password?")}
                        </Typography>
                    </Fragment>
                }
            />
        </Fragment>
    );
}

export default withRouter(withStyles(styles)(LoginDialog));
