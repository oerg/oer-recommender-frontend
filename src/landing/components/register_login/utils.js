export function loginWithGoogle(authObj, onSuccess, onError, onEnd) {
    if (authObj.error || "tokenId" in authObj === false) {
        onError();
    } else {
        onSuccess();
    }
    onEnd();
}