import { makeStyles } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../i18n';
import ActionItem from './ActionItem';

const useStyles = makeStyles({
    action: {
        marginBottom: 50,
    }
})

export default function Actions() {
    const classes = useStyles()
    const { t } = useTranslation(['landing']);
    const language = useLanguage()

    const actionItems = useMemo(
        () => [
            {
                "title": t("Learn towards your dream"),
                "titleColor": "#434f67",
                "description": t("eDoer tries to provide whatever you need to learn by"),
                "bullets": [
                    t("helping you in setting your learning journeys"),
                    t("Offering personalized educational recommendations towards you learning goals"),
                    t("Providing tests to assess your achieved knowledge"),
                    t("Connecting you with experts in case you need help")
                ],
                "image": process.env.PUBLIC_URL + `/images/landing/learner.png`,
                backgroundImage: `url(${process.env.PUBLIC_URL}/images/landing/bg.jpg)`,
                action: {
                    label: t("Join eDoer"),
                    location: `/${language}/register`
                },
            },
            {
                "title": t("Build different learning pathways"),
                "titleColor": "#434f67",
                "description": t("eDoer, by providing intelligent recommendations, helps you to offer different learning paths for learners. eDoer analyzes existing labor market standards and educational materials in order to facilitate your contribution process."),
                "image": process.env.PUBLIC_URL + `/images/landing/curator.jpg`,
                action: {
                    label: t("Become a Content Curator"),
                    location: {
                        pathname: `/${language}/register`,
                        state: {
                            enableDiscovery: true,
                        }
                    }
                },
            },
            {
                "title": <>{t("Are you an expert?")} <br /> {t("Then help learners")} </>,
                "titleColor": "#ffffff",
                "description": t("If you are expert in any of our courses or topics, you can become a mentor and help other users in their learning process. Afterward, eDoer will connect the learners who have mentoring requests to you."),
                "descriptionColor": "#ffffff",
                "descriptionMargin": "69px 340px 0 0",
                "backgroundImage": `url(${process.env.PUBLIC_URL}/images/landing/mentor.jpg)`,
                action: {
                    label: t("Become a Mentor"),
                    location: {
                        pathname: `/${language}/register`,
                        state: {
                            enableMentoring: true,
                        }
                    }
                },
            },
        ], 
        [t, language]
    )


    return (
        <div>
            { actionItems.map(
                (step, index) => <ActionItem className={classes.action} key={index} {...step} />
            )}
        </div>
    )
}
