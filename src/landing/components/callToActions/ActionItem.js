import { Button, Container, makeStyles } from '@material-ui/core';
import clsx from 'classnames';
import React from 'react';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    wrapper: {
        backgroundSize: 'cover',
        height: 600,
        width: "100%",
    },
    container: {
        height: "100%",
    },
    root: {
        display: "flex",
        height: "100%",
        alignItems: "center",
    },
    leftPart: {
        flex: "0 0 55%",
    },
    rightPart: {
        backgroundSize: 'cover',
        backgroundPosition: "top center",
        flex: "0 0 45%",
        height: "100%",
    },
    title: {
        textTransform: "uppercase",
        letterSpacing: "normal",
        fontWeight: "700",
        fontSize: "2.25rem",
        margin: "80px 7px 0",
        lineHeight: "1.1",
    },
    body: {
        fontWeight: "bold",
        fontSize: "1.125rem",
        fontStyle: "normal",
        marginLeft: 15,
        marginTop: 45,
        marginRight: 100
    },
    actions: {
        marginLeft: 15,
        marginTop: 70,
        textAlign: "center"
    }
})

export default function ActionItem({ image, backgroundImage, title, titleColor,
    description, descriptionColor, descriptionMargin, bullets, action, className, }) {
    const classes = useStyles()

    return (
        <div
            style={{
                backgroundImage: backgroundImage,
            }}
            className={clsx(classes.wrapper, className)}
        >
            <Container className={classes.container}>
                <div className={classes.root}>
                    <div className={classes.leftPart}>
                        <h1 className={classes.title} style={{ color: titleColor }}>
                            {title}
                        </h1>
                        <p className={classes.body} style={{ color: descriptionColor, margin: descriptionMargin }}>
                            {description}
                        </p>
                        {bullets &&
                            <ul>
                                {
                                    bullets.map((item, index) => <li key={index}>{item}</li>)
                                }
                            </ul>
                        }
                        {action && <p className={classes.actions}>
                            <Button
                                component={Link}
                                to={action.location}
                                color="primary"
                                variant="contained"
                                style={{
                                    fontSize: "1.5rem",
                                    minWidth: 300,
                                }}
                            >
                                {action.label}
                            </Button>
                        </p>}
                    </div>
                    <div
                        className={classes.rightPart}
                        style={{
                            backgroundImage: `url(${image})`,
                        }}
                    />
                </div>

            </Container>
        </div>
    )
}
