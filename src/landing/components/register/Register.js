import DateFnsUtils from '@date-io/date-fns';
import { Box, Button, FormControl, FormHelperText, Grid, InputLabel, Link, MenuItem, Paper, Typography, withWidth } from '@material-ui/core';
import { green, red } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import axios from 'axios';
import { format } from 'date-fns';
import { Field, Form, Formik } from 'formik';
import { Checkbox, Select, TextField } from 'formik-material-ui';
import { DatePicker } from 'formik-material-ui-pickers';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import * as Yup from 'yup';
import CitySelect from '../../../dashboard/components/CitySelect';
import CountrySelect from '../../../dashboard/components/CountrySelect';

const styles = theme => ({
    fieldPaper: {
        padding: theme.spacing(3),
        marginTop: theme.spacing(1),
    },

    wrapper: {
        position: "relative",
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(5),
    },

    errorButton: {
        backgroundColor: red[500],
        '&:hover': {
            backgroundColor: red[700],
        }
    },

    successButton: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        }
    },

    emailProgress: {
        color: green[500],
    },

    tosLabel: {
        textAlign: 'justify',
    },

    hiddenOption: {
        display: 'none',
    },
});

const initValues = {
    username: "",
    password: "",
    passwordConfirm: "",
    first_name: "",
    last_name: "",
    gender: "",
    phone: "",
    country: "",
    city: "",
    birthDate: null,
    tos1: false,
    tos2: false,
}

function Register(props) {
    const { classes } = props;
    const { t } = useTranslation(['landing']);

    const genders = useMemo(() => [
        { name: t('Male'), code: 'ML' },
        { name: t('Female'), code: 'FM' },
        { name: t('Other'), code: 'OT' },
    ], [t]);

    const schema = useMemo(() => Yup.object({
        username: Yup.string()
            .required(t('Email address is required.'))
            .email(t('Email address is not valid.')),
        password: Yup.string()
            .required(t('You must choose a password.'))
            .min(8, t('Password must be at least 8 characters.')),
        passwordConfirm: Yup.string()
            .required(t('Password confirmation is required.'))
            .oneOf([Yup.ref('password'), null], t('Passwords do not match.')),
        // firstname: Yup.string().label('First Name').notRequired(),
        // lastname: Yup.string().label('Last Name').notRequired(),
        // gender: Yup.string().label('Gender').notRequired()
        //     .oneOf(['male', 'female', 'other', 'null'], 'Select a valid Gender.'),
        // birthDate: Yup.date().label('Birthdate').notRequired(),
        // phone: Yup.string().label('Telephone Number').notRequired(),
        // country: Yup.string().label('Country').notRequired().oneOf(allCountries.map(country => country.code), 'Select a valid country.'),
        // city: Yup.string().label('city').notRequired().notOneOf(['none'], 'Select a valid city.'),
        tos1: Yup.boolean().label('Terms of Use').required().oneOf([true], t('Please accept the TIB AV-Portal Conditions.')),
        tos2: Yup.boolean().label('Terms of Use').required().oneOf([true], t('Please accept the terms of use.')),
    }), [t]);

    const [registrationDone, setRegistrationDone] = useState(false);
    const [enableDiscovery, setEnableDiscovery] = useState(false);
    const [enableMentoring, setEnableMentoring] = useState(false);

    const locationState = useLocation().state

    useEffect(
        () => {
            if (locationState && locationState.enableDiscovery) {
                setEnableDiscovery(locationState.enableDiscovery)
            }
            if (locationState && locationState.enableMentoring) {
                setEnableMentoring(locationState.enableMentoring)
            }
        },
        [locationState]
    )

    const handleSubmit = useCallback(
        (values, { setStatus, setSubmitting, setFieldError }) => {
            setStatus(false);
            // parsing values
            const { birthDate: birthdateObj, passwordConfirm, tos1, tos2, first_name, last_name,
                username, password, ...restValues } = values;
            const birthdateFormatted = birthdateObj ? format(birthdateObj, 'yyyy-MM-dd') : null;
            const neededValues = {
                user: {
                    email: username, username, password, first_name, last_name
                },
                birthdate: birthdateFormatted,
                settings: {
                    discovery: enableDiscovery,
                    mentoring: enableMentoring,
                },
                ...restValues
            };
            // send request
            axios.post('/members/', neededValues).then(
                (res) => {
                    setSubmitting(false);
                    setRegistrationDone(true);
            }).catch((error) => {
                if (error.response) { //error from server
                    if (error.response.status === 400) { // data error
                        const { user: userErrors, ...errors } = error.response.data
                        for (let field in userErrors)
                            setFieldError(field, userErrors[field])
                        for (let field in errors)
                            setFieldError(field, errors[field])
                    } else {
                        setStatus('HTTP Error ' + error.response.status + ': ' + error.response.data);
                    }
                } else if (error.request) { // no response
                    setStatus(t('No response from the server.'));
                } else { // something else
                    setStatus(error.message);
                }
                setSubmitting(false)
            });
        },
        [t]
    )

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} md={8} lg={6}>
                <Formik
                    validationSchema={schema}
                    onSubmit={handleSubmit}
                    initialValues={initValues}
                >
                    {({ values, touched, errors, isSubmitting, status }) => (
                        <Form>
                            <Paper elevation={2} className={classes.fieldPaper}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Field component={TextField} name="username" type="email" required fullWidth label={t("Email Address")} />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Field component={TextField} name="password" type="password" label={t("Password")}
                                            fullWidth required />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Field component={TextField} name="passwordConfirm" type="password" label={t("Retype Password")}
                                            fullWidth required />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Field component={TextField} name="first_name" type="text" label={t("First Name")}
                                            fullWidth />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <Field component={TextField} name="last_name" type="text" label={t("Last Name")}
                                            fullWidth />
                                    </Grid>

                                    <Grid item xs={12} sm={6}>
                                        <FormControl error={touched.gender && !!errors.gender} fullWidth>
                                            <InputLabel htmlFor="gender-inp">{t("Gender")}</InputLabel>
                                            <Field component={Select} name="gender" inputProps={{ id: 'gender-inp' }}>
                                                <MenuItem value="" className={classes.hiddenOption}><em>{t("Select")}</em></MenuItem>
                                                {genders.map((g, index) => <MenuItem key={index} value={g.code}>{g.name}</MenuItem>)}
                                            </Field>
                                            {touched.gender && !!errors.gender && <FormHelperText error required>{errors.gender}</FormHelperText>}
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <Field variant="inline" component={DatePicker} disableFuture label={t("Birthdate")} name="birthDate" autoOk
                                                openTo="year" format="yyyy-MM-dd" views={["year", "month", "date"]} fullWidth />
                                        </MuiPickersUtilsProvider>
                                        {touched.birthDate && !!errors.birthDate && <FormHelperText error required>{errors.birthDate}</FormHelperText>}
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl error={touched.country && !!errors.country} fullWidth>
                                            <Field name="country">
                                                {({ field: { onChange, ...otherFields }, form }) => (
                                                    <CountrySelect disabled={isSubmitting} {...otherFields} onChange={
                                                        (e, v) => {
                                                            form.setFieldValue('country', v);
                                                            form.setFieldValue('city', '');
                                                            form.handleChange(e);
                                                        }
                                                    } />
                                                )}
                                            </Field>
                                            {touched.country && !!errors.country && <FormHelperText error required>{errors.country}</FormHelperText>}
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl error={touched.city && !!errors.city} fullWidth>
                                            <Field name="city">
                                                {({ field: { onChange, ...otherFields }, form }) => (
                                                    <CitySelect disabled={isSubmitting} {...otherFields} onChange={
                                                        (e, v) => {
                                                            form.setFieldValue('city', v);
                                                            form.handleChange(e);
                                                        }
                                                    } countryCode={values['country']} />
                                                )}
                                            </Field>
                                            {touched.city && !!errors.city && <FormHelperText error required>{errors.city}</FormHelperText>}
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field component={Checkbox} name="tos1" type="checkbox" required />
                                        <Typography variant="caption" component="span" className={classes.tosLabel}>
                                            {t("I accept the the Special Conditions of the TIB eDoer.")} (<Link href="https://labs.tib.eu/edoer/tos" rel="noopener noreferrer" target="_blank">{t("View Special Conditions")}</Link>)
                                        </Typography>
                                        {touched.tos1 && !!errors.tos1 && <Typography display="block" component={FormHelperText} error required>{errors.tos1}</Typography>}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Box display="inline-flex" component="span" alignItems="flex-start">
                                            <Field component={Checkbox} name="tos2" type="checkbox" required />
                                            <Typography variant="caption" component="span" style={{ flex: 1, marginTop: '7px' }} className={classes.tosLabel}>
                                                {t("I agree to the processing of my personal data provided here by Technische Informationsbibliothek (TIB). In accordance with the data protection decleration as well as the data protection info sheet  (where you can find our contact details), the data is processed exclusively by TIB in order to provide services of our portals.")}
                                                &nbsp;(<Link href="https://www.tib.eu/en/service/data-protection/" target="_blank">{t("The data protection declaration")}</Link> {t("and")}
                                                &nbsp;<Link href="https://labs.tib.eu/edoer/files/infosheet-data-protection.pdf" target="_blank">{t("the data protection info sheet ")}</Link>)
                                            </Typography>
                                        </Box>
                                        {touched.tos2 && !!errors.tos2 && <Typography display="block" component={FormHelperText} error required>{errors.tos2}</Typography>}
                                    </Grid>
                                    <Grid item xs={12} container>
                                        {registrationDone &&
                                            <Grid item xs={12}>
                                                <Alert severity='success'>
                                                    {t("The activation link has been sent to your email address. Please check  your spam/junk folder as well.")}
                                                </Alert>
                                            </Grid>
                                        }
                                    </Grid>
                                </Grid>
                            </Paper>
                            <Grid item xs={12}>
                                <Box textAlign="right" m={1} p={1}>
                                    {!!status && <Grid item xs={12}><FormHelperText error required>{status}</FormHelperText></Grid>}
                                    <Button variant="contained" type="submit" color="primary" disabled={isSubmitting || registrationDone}>{t("Continue")}</Button>
                                </Box>
                            </Grid>

                        </Form>
                    )}
                </Formik>
            </Grid>
        </Grid>
    )
}

Register.propTypes = {
    selectRegister: PropTypes.func.isRequired
};

export default withWidth()(
    withStyles(styles, { withTheme: true })(Register)
);
