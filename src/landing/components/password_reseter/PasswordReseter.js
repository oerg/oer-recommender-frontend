import { Box, Button, CircularProgress, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import axios from 'axios';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-material-ui';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import * as Yup from 'yup';
import { useParam } from '../../../dashboard/views/helpers';

function PasswordReseter({ location }) {
    const token = useParam('token');
    const history = useHistory();
    const { t } = useTranslation(['landing']);

    const [isLoading, setIsLoading] = useState(false);
    const [isValid, setIsValid] = useState(false);

    const classes = makeStyles({
        root: {
            marginTop: '15px',
        },
        box: {
            padding: '15px',
        }
    })();

    useEffect(() => {
        setIsLoading(true);
        axios.post('/members/password-reset/validate_token/', { token })
            .then(res => {
                setIsValid(true);
            })
            .catch(err => {
                setIsValid(false);
            })
            .finally(() => setIsLoading(false))
    }, [token]);

    const handleSubmit = useCallback((values, { setStatus, setSubmitting, setFieldError }) => {
        setStatus(false);
        const { password } = values;
        axios.post('/members/password-reset/confirm/', { token, password })
            .then(res => {
                setTimeout(() => {
                    history.push(location); // go home
                }, 3000);
                setStatus(t('Password Changed Successfully.'))
            })
            .catch(error => {
                if (error.response) { //error from server
                    if (error.response.status === 400) { // data error
                        const { user: userErrors, ...errors } = error.response.data
                        for (let field in userErrors)
                            setFieldError(field, userErrors[field])
                        for (let field in errors)
                            setFieldError(field, errors[field])
                    } else {
                        setStatus('HTTP Error ' + error.response.status + ': ' + error.response.data);
                    }
                } else if (error.request) { // no response
                    setStatus(t('No response from the server.'));
                } else { // something else
                    setStatus(error.message);
                }
                setSubmitting(false)
            })
    }, [token, history, location, t]);

    const schema = useMemo(() => Yup.object({
        password: Yup.string()
            .required(t('You must choose a password.'))
            .min(8, t('Password must be at least 8 characters.')),
        passwordConfirm: Yup.string()
            .required(t('Password confirmation is required.'))
            .oneOf([Yup.ref('password'), null], t('Passwords do not match.')),

    }), [t])

    return (
        <Grid container justifyContent="center" className={classes.root}>
            <Grid item xs={12} sm={8} md={6}>
                <Paper className={classes.box}>
                    {isLoading ?
                        <CircularProgress />
                        :
                        isValid ?
                            <Formik
                                validationSchema={schema}
                                initialValues={{ password: '', passwordConfirm: '' }}
                                onSubmit={handleSubmit}>
                                {({ isSubmitting, status }) => (
                                    <Form>
                                        <Grid container>
                                            <Grid item xs={12} className={classes.box}>
                                                <Field component={TextField} name="password" type="password" label="Password"
                                                    fullWidth />
                                            </Grid>
                                            <Grid item xs={12} className={classes.box}>
                                                <Field component={TextField} name="passwordConfirm" type="password" label="Repeat Password"
                                                    fullWidth />
                                            </Grid>
                                            <Grid item xs={12} className={classes.box}>
                                                <Box textAlign="center">
                                                    {status}
                                                </Box>
                                                <Box textAlign="right">
                                                    <Button variant="contained" color="primary" type="submit" disabled={isSubmitting}>
                                                        {t("Reset Password")}
                                                    </Button>
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Form>
                                )}
                            </Formik>
                            :
                            <Typography variant="h5">
                                {t("This link is not valid.")}
                            </Typography>
                    }
                </Paper>
            </Grid>
        </Grid>
    )
}


export default PasswordReseter