import React from 'react';
import { useTranslation } from 'react-i18next';
import Carousel from 'react-material-ui-carousel';
import QuoteItem from './QuoteItem';

export default function Actions() {

    const { t } = useTranslation(['landing']);

    const quoteItems = [
        {
            "quote": t("Learning needs to be more agile and flexible - eDoer is the perfect tool to match learners with free educational reseources according to their learning goals!"),
            "image": `${process.env.PUBLIC_URL}/images/landing/quotes/auer.jpeg`,
            "name": "Sören Auer",
            "position": t("Director of the Technical Information Library (TIB)")
        },
        {
            "quote": t("eDoer is like a learning train, it offers you the first wagon and features to control the train towards your goals!"),
            "image": `${process.env.PUBLIC_URL}/images/landing/quotes/kismihok.jpg`,
            "name": "Gábor Kismihók",
            "position": t("Head of the Learning and Skill Analytics Group at TIB"),            
            "backgroundColor": "#e5d6d6"
        },
    ]


    return (
        <div style={{
            padding: "120px 0",
        }}>
            <Carousel
                indicators={false}
                interval={4000}
                autoPlay={true}
                animation='slide'
                changeOnFirstRender
                stopAutoPlayOnHover
                strictIndexing
            >
                {
                    quoteItems.map(
                        (quote, index) =>
                            <QuoteItem key={index} {...quote} />
                    )
                }
            </Carousel>
        </div>
    )
}
