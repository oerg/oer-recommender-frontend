import { makeStyles, Paper } from '@material-ui/core';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import React from 'react';

const useStyles = makeStyles({
    paper: {
        textAlign: "center",
        display: "flex",
    },
    rightPart: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "column",
        padding: "30px 30px"
    },
    quote: {
        flexGrow: 1,
        marginTop: 29,
        fontSize: "1.25rem",
        wordWrap: "break-word",
        textAlign: "left",
        color: "#111111",
        fontFamily: "'Open Sans',sans-serif",
    },
    rightBottom: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    }
})

export default function QuoteItem({ image, backgroundColor = "#d6e5e3", quote, name, position }) {

    const classes = useStyles()

    return (
        <Paper square className={classes.paper} style={{
            backgroundColor: backgroundColor,
        }}
        >
            <img src={image} height={300} alt={"Expert"} />
            <div className={classes.rightPart}>
                <p className={classes.quote}>{quote}</p>
                <div className={classes.rightBottom}>
                    <FormatQuoteIcon style={{
                        fontSize: 110
                    }}
                    />
                    <p>{name}, {position}</p>
                </div>
            </div>
        </Paper>
    )
}
