import { AppBar, Button, CircularProgress, Toolbar, withStyles } from "@material-ui/core"
import DashboardIcon from "@material-ui/icons/Dashboard"
import ExitToAppIcon from "@material-ui/icons/ExitToApp"
import ExpandLessIcon from "@material-ui/icons/ExpandLess"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import HowToRegIcon from "@material-ui/icons/HowToReg"
import LockOpenIcon from "@material-ui/icons/LockOpen"
import React, { useCallback, useContext, useEffect, useMemo, useState } from "react"
import { useTranslation } from "react-i18next"
import { Link } from "react-router-dom"
import { useLanguage } from "../../../i18n"
import { UserContext } from "../../User"
import WhiteButton from "../home/components/WhiteButton"
import clsx from "classnames"
import Cookies from "js-cookie"
import DemoDialog from "../DemoDialog"

const styles = theme => ({
    appBar: {
        height: 85,
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        height: "100%",
    },
    menuButtonText: {
        fontSize: theme.typography.body1.fontSize,
        fontWeight: theme.typography.h6.fontWeight
    },
    brandText: {
        fontFamily: "'Baloo Bhaijaan', cursive",
        fontWeight: 400
    },
    noDecoration: {
        textDecoration: "none !important",
        marginRight: theme.spacing(1),
    },
    slogan: {
        marginLeft: theme.spacing(2),
        // color: 'black',
    },
    redText: {
        color: "red",
    }
});

const DemoCookieId = "__demo_visited"

function NavBar(props) {
    const { t } = useTranslation(["landing"]);
    const [viewDemo, setViewDemo] = useState(false)
    const language = useLanguage();

    const {
        classes,
        exploreOpen,
        handleExploreOpen,
    } = props;

    const user = useContext(UserContext);

    const menuItems = useMemo(() => user.loggedIn ?
        [
            {
                name: t("My Dashboard"),
                link: `/${language}/dashboard`,
                icon: DashboardIcon
            },
            {
                name: t("Logout"),
                link: `/${language}/dashboard/logout`,
                icon: ExitToAppIcon
            }
        ] : [
            {
                name: t("Login as Guest"),
                link: {
                    pathname: `/${language}/`,
                    state: {
                        loginAsGuest: true,
                    }
                },
                icon: LockOpenIcon
            },
            {
                name: t("Register"),
                link: `/${language}/register`,
                icon: HowToRegIcon,
            },
            {
                name: t("Login"),
                link: {
                    pathname: `/${language}/`,
                    state: {
                        login: true,
                    }
                },
                icon: LockOpenIcon
            }
        ], [user.loggedIn, language, t]);

    const handleExplore = useCallback(
        () => handleExploreOpen(p => !p),
        [handleExploreOpen]
    )

    const handleCloseDemo = useCallback(
        () => {
            Cookies.set(DemoCookieId, "true")
            setViewDemo(false)
        },
        []
    )

    const handleOpenDemo = useCallback(
        () => {
            setViewDemo(true)
        },
        []
    )

    useEffect(
        () => {
            if(Cookies.get(DemoCookieId) !== "true") {
                const timeoutId = window.setTimeout(() => setViewDemo(true), 1000)
                return () => window.clearTimeout(timeoutId)
            }
        },
        []
    )

    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar className={classes.toolbar}>
                <Link to={`/${language}/`}>
                    <img src={`${process.env.PUBLIC_URL}/images/logo2.png`} height="60" alt="eDoer Logo" />
                </Link>
                <Button
                    variant="contained"
                    style={{
                        marginLeft: 32
                    }}
                    onClick={handleExplore}
                    endIcon={exploreOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                >
                    {t("Explore")}
                </Button>
                <div style={{
                    flex: 1,
                    textAlign: "right",
                }}>
                    <WhiteButton
                        color="secondary"
                        size="large"
                        classes={{ text: classes.menuButtonText }}
                        className={clsx(classes.noDecoration, classes.redText)}
                        variant="outlined"
                        onClick={handleOpenDemo}
                    >
                        {t("View Demo")}
                    </WhiteButton>
                    {user.loading ?
                        <CircularProgress size={25} color="secondary" />
                        :
                        menuItems.map(element => {
                            if (React.isValidElement(element)) {
                                return element
                            }
                            return (
                                <Link
                                    key={element.name}
                                    to={element.link}
                                    className={classes.noDecoration}
                                >
                                    <WhiteButton
                                        color="primary"
                                        size="large"
                                        startIcon={<element.icon />}
                                        classes={{ text: classes.menuButtonText }}
                                        variant="outlined"
                                    >
                                        {element.name}
                                    </WhiteButton>
                                </Link>
                            );
                        })}
                </div>
                <DemoDialog open={viewDemo} onClose={handleCloseDemo} />
            </Toolbar>
        </AppBar>
    );
}

export default withStyles(styles, { withTheme: true })(NavBar);
