import { Container } from '@material-ui/core';
import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import { useParams, useRouteMatch } from 'react-router-dom';
import Card from '../../../dashboard/components/Card/Card';
import CardBody from '../../../dashboard/components/Card/CardBody';
import CardHeader from '../../../dashboard/components/Card/CardHeader';
import Footer from '../../../dashboard/views/Footer';
import Navbar from '../../../dashboard/views/Navbar';
import SelfAssessmentReport from '../../../dashboard/views/SelfAssessment'
import useStyles from '../../../dashboard/views/styles/EducationalContentStyles'
import { UserContext } from '../../User';

function PublicSelfAssessment({ inDashboard }) {
    const classes = useStyles();
    const match = useRouteMatch();
    const user = useContext(UserContext);
    const urlParams = useParams();
    const { t } = useTranslation(['landing']);

    return (
        <div>
            {!inDashboard &&
                <Navbar basePath={match.url} userData={user.data} user={user} path={match.path} />
            }
            <Container maxWidth="lg" style={{
                padding: '25px',
                display: "flex",
                flexFlow: "column",
                height: "calc(100% - 85px)"
            }}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Self-assessment Report")}
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <SelfAssessmentReport id={urlParams.id} token={urlParams.token} />
                    </CardBody>
                </Card>
            </Container>
            {!inDashboard && <Footer />}
        </div>

    )
}

export default PublicSelfAssessment