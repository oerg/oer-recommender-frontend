import Axios from 'axios';
import Cookies from 'js-cookie';
import React, { createContext, useCallback, useEffect, useMemo, useState } from 'react';
import { useGoogleLogout } from 'react-google-login';
import { useHistory } from 'react-router-dom';

export const UserContext = createContext();

const appClient = process.env.REACT_APP_ID;
const appSecret = process.env.REACT_APP_SECRET;

function UserProvider(props) {

    const [userData, setUserData] = useState({})
    const [authToken, setAuthToken] = useState(() => Cookies.get('token'));
    const [userLoading, setUserLoading] = useState(true);
    const { signOut: googleLogout } = useGoogleLogout({
        clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID,
        onLogoutSuccess: () => { },
        onFailure: () => { }
    });

    const loadUser = useCallback(
        (onSuccess) => {
            setUserLoading(true);
            Axios.get('/members/')
            .then(res => {
                setUserData(res.data[0])
                onSuccess && onSuccess()
            })
            .catch(err => setUserData({}))
            .finally(() => setUserLoading(false));
        },
        []
    )

    useEffect(
        loadUser,
        []
    )

    const setToken = useCallback(
        (token, onAfterLoadUser) => {
            setAuthToken(token)
            if (token !== undefined) { // load user
                loadUser(onAfterLoadUser);
                Cookies.set(
                    'token',
                    token,
                    {
                        sameSite: 'LAX',
                    }
                );
            } else {
                Cookies.remove('token');
                setUserData({});
                setUserLoading(false);
            }
        },
        [loadUser]
    )

    const logout = useCallback((onSuccess = () => { }, onError = () => { }) => {
        setUserLoading(true);
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "LE",
            related_data: {}
        })
            .then(() => { })
            .catch(() => { });
        Axios.get('/members/logout/')
            .then(() => {
                googleLogout();
                setToken(undefined);
                onSuccess();
            })
            .catch(() => {
                if (onError) onError();
            })
            .finally(() => setUserLoading(false))
    }, [googleLogout])



    const login = useCallback((username, password, onLoad) => {
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "EN",
            related_data: {
                username_tried: username,
            }
        }).catch(() => { })
        Axios.post('/o/token/', new URLSearchParams({ grant_type: 'password', username, password }).toString(), {
            auth: {
                username: appClient,
                password: appSecret
            }
        }).then(res => {
            if (res.status === 200 && res.data && "access_token" in res.data) {
                setToken(
                    res.data.access_token,
                    () => {
                        onLoad({
                            logged_in: true,
                        });
                    }
                );
                Axios.post('/trackings/add-tracking/', {
                    tracking_type: "EN",
                    related_data: {
                        successful: true,
                    }
                }).catch(() => { })
            } else {
                setToken(undefined)
                onLoad({
                    logged_in: false,
                });
            }
        }).catch(err => {
            setToken(undefined);
            if (onLoad)
                if (err.response)
                    onLoad(err.response.data);
                else
                    onLoad(err);
        })
    }, [loadUser]);

    const googleLogin = useCallback((tokenId, onLoad) => {
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "EN",
            related_data: {
                google_token: tokenId,
            }
        }).catch(() => { })
        Axios.post(
            '/members/login-as-google/',
            {
                token: tokenId
            }
        ).then(res => {
            if (res.status === 200 && res.data && "access_token" in res.data) {
                setToken(
                    res.data.access_token,
                    () => {
                        onLoad({
                            logged_in: true,
                        });
                    }
                );
                Axios.post('/trackings/add-tracking/', {
                    tracking_type: "EN",
                    related_data: {
                        successful: true,
                    }
                }).catch(() => { })
            } else {
                setToken(undefined);
                onLoad({
                    logged_in: false,
                });
            }
        }).catch(err => {
            setToken(undefined)
            if (onLoad)
                if (err.response)
                    onLoad(err.response.data);
                else
                    onLoad(err);
        })
    }, [loadUser]);

    const guestLogin = useCallback((onLoad) => {
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "EN",
            related_data: {
                guest_login: true,
            }
        }).catch(() => { })
        Axios.post(
            '/members/login-as-guest/',
            {}
        ).then(res => {
            if (res.status === 200 && res.data && "access_token" in res.data) {
                setToken(
                    res.data.access_token,
                    () => {
                        onLoad({
                            logged_in: true,
                        });
                    }
                );
                Axios.post('/trackings/add-tracking/', {
                    tracking_type: "EN",
                    related_data: {
                        successful: true,
                    }
                }).catch(() => { })
            } else {
                setToken(undefined);
                onLoad({
                    logged_in: false,
                });
            }
        }).catch(err => {
            setToken(undefined)
            if (onLoad)
                if (err.response)
                    onLoad(err.response.data);
                else
                    onLoad(err);
        })
    }, [loadUser]);

    if (authToken !== undefined) {
        Axios.defaults.headers.common['Authorization'] = "Bearer " + authToken;
    } else {
        delete Axios.defaults.headers.common['Authorization'];
    }

    const user = useMemo(() => ({
        data: userData,
        setData: setUserData,
        loggedIn: !userLoading && !!userData && Object.keys(userData).length > 0,
        loading: userLoading,
        login,
        logout,
        reload: loadUser,
        googleLogin,
        guestLogin,
    }), [userData, logout, login, userLoading, loadUser, googleLogin, guestLogin]);

    return (
        <UserContext.Provider value={user}>
            {props.children}
        </UserContext.Provider>
    )
}

export function needLogin(Component, url = '/') {
    return function (props) {
        const { userData } = props;
        const history = useHistory();

        useEffect(
            () => {
                if (!userData) {
                    history.push(url)
                }
            },
            [userData, history]
        );

        return <Component {...props} />
    }
}

export default UserProvider;
