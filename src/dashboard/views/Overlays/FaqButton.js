import { Button, makeStyles, Portal } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Faq from '../Faq';

const useStyles = makeStyles(theme => ({
    root: {
        width: 53,
        height: 48,
        padding: 0,
        margin: 0,
        position: "fixed",
        right: 0,
        top: "calc( (100% - 48px) / 2)",
        transition: theme.transitions.create("width", {
            duration: 200
        }),
        "&:hover": {
            width: 62,
        },
    },
    button: {
        height: 48,
        padding: 0,
        margin: 0,
        color: "#005c5f",
    }
}))

function FaqButton({ user }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const [openForm, setOpenForm] = useState(false);

    const handleOpenForm = useCallback(
        () => {
            setOpenForm(true);
        },
        []
    );

    const handleCloseForm = useCallback(
        () => setOpenForm(false),
        []
    );

    return (
        <div className={classes.root}>
            <Button
                variant="outlined"
                color="primary"
                title={t("Help")}
                className={classes.button}
                onClick={handleOpenForm}
            >
                <HelpIcon fontSize="large" className="changeColor" />
            </Button>
            <Portal>
                <Faq open={openForm} onClose={handleCloseForm} />
            </Portal>
        </div>
    )
}

export default FaqButton
