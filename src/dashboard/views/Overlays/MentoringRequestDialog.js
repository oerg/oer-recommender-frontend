import { Button, Collapse, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles, TextField } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import SkillOrTopicInput from '../../components/SystemInputs/SkillOrTopicInput';
import { sendMenotringRequest } from './utils';

const useStyles = makeStyles(theme => ({
    paper: {
        minWidth: 500,
    },
    field: {
        marginTop: theme.spacing(2),
    }
}))

function MentoringRequestDialog(props) {
    const {
        disabled = false,
        titleProps,
        dialogContentProps,
        user,
        onClose,
        ...otherProps
    } = props;
    const classes = useStyles();

    const { t } = useTranslation(['dashboard']);

    const [selectedSubject, setSelectedSubject] = useState(null);
    const [loading, setLoading] = useState(false);
    const [text, setText] = useState('');
    const [done, setDone] = useState(false);

    const handleText = useCallback(
        e => setText(e.target.value),
        []
    );

    const handleSubmit = useCallback(
        () => {
            setLoading(true);
            if (!window.confirm(t("We will send your contact information to your mentor. Do you want to proceed?"))) {
                setLoading(false);
                return;
            }
            sendMenotringRequest(
                selectedSubject.object_id,
                selectedSubject.object_type,
                text,
                () => { setDone(true) },
                null,
                () => setLoading(false)
            );
        },
        [selectedSubject, text, t]
    );

    const handleClose = useCallback(
        () => {
            setSelectedSubject(null);
            setDone(false);
            setLoading(false);
            setText("");
            onClose && onClose();
        },
        [onClose]
    );

    return (
        <Dialog scroll="paper" {...otherProps} onClose={handleClose} classes={{
            paper: classes.paper
        }}>
            <DialogTitle {...titleProps}>
                {t("I need a mentor")}
            </DialogTitle>
            <DialogContent dividers {...dialogContentProps}>
                <Collapse in={done}>
                    <Alert variant="filled" severity="success">
                        {t("Your request has been sent to the mentor. They will contact you soon through your email address.")}
                    </Alert>
                </Collapse>
                <Collapse in={!done}>
                    <div>
                        <SkillOrTopicInput
                            loading={loading}
                            disabled={disabled}
                            label={t("What do you need help with?")}
                            inputProps={{
                                variant: "outlined",
                                required: true,
                            }}
                            onSelect={setSelectedSubject}
                        />
                    </div>
                    <div className={classes.field}>
                        <TextField
                            label={`${t("Explain Briefly")} (${t("optional")})`}
                            fullWidth
                            variant="outlined"
                            multiline
                            placeholder={t("Explanation")}
                            disabled={disabled || loading}
                            minRows={3}
                            maxRows={5}
                            value={text}
                            onChange={handleText}
                        />
                    </div>
                </Collapse>
            </DialogContent>
            <DialogActions>
                <Button
                    color="secondary"
                    variant="contained"
                    onClick={handleClose}
                    disabled={disabled || loading}
                >
                    {done ? t("Close") : t("Cancel")}
                </Button>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={handleSubmit}
                    disabled={disabled || loading || selectedSubject === null || done}
                >
                    {t("Request")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default MentoringRequestDialog
