import { Button, makeStyles, Paper, Typography } from '@material-ui/core'
import clsx from "classnames"
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { preAddress } from '../../../../App'
import { useLanguage } from '../../../../i18n'
import TopicLogo from '../../../components/Logos/Topic'

const useStyles = makeStyles({
    root: {
        cursor: "pointer",
        position: "relative",
        display: "flex",
        margin: "8px 8px 16px 8px",
        padding: 10
    },
    deleteIcon: {
        position: 'absolute',
        top: 4,
        right: 4
    },
    selected: {
        backgroundColor: "#CCC",
    },
    followButton: {
        position: "absolute",
        right: 10,
    }
})

function TopicSearchCard({ topic, noActions, className, selected: propSelected, onKeyDown, ...props }) {
    const rootRef = useRef(null)
    const systemLanguage = useLanguage()
    const classes = useStyles()
    const [isFocused, setIsFocused] = useState(false)

    useEffect(
        () => {
            if (propSelected && rootRef.current)
                rootRef.current.focus()
        },
        [propSelected]
    )

    const handleKeyDown = useCallback(
        e => {
            if (onKeyDown) {
                onKeyDown(e)
            }
        },
        [onKeyDown]
    )

    return (
        <Paper
            {...props}
            className={clsx(classes.root, className, { [classes.selected]: isFocused })}
            component={Button}
            href={`${preAddress}/${systemLanguage}/dashboard/topics/${topic.id}`}
            target="_blank"
            ref={rootRef}
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
            onKeyDown={handleKeyDown}
        >
            <TopicLogo size="large" />
            <div style={{
                flexGrow: 1,
                marginLeft: 8
            }}
            >
                <Typography
                    title={topic.title}
                    component={"div"}
                    style={{
                        width: 400,
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                        whiteSpace: "nowrap"
                    }}
                >
                    {topic.title}
                </Typography>
            </div>
        </Paper>
    )
}

export default TopicSearchCard