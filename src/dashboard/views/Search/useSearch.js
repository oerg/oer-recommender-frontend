import { useContext } from 'react'
import { SearchContext } from './SearchProvider'


function useSearch() {
    const context = useContext(SearchContext)

    return [context.open, context.close]
}

export default useSearch