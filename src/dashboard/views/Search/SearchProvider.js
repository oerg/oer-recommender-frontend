import { Portal } from '@material-ui/core'
import React, { useCallback, useContext, useMemo, useState } from 'react'
import { useLanguage } from '../../../i18n'
import { UserContext } from '../../../landing/User'
import SkillConfirmDialog from '../Education/components/SkillConfirmDialog'
import { doUnarchive } from '../Education/utils'
import { getJob, saveGoalJob, updateSkillsBatch } from '../Goals/utils'
import SearchDialog from './SearchDialog'

export const SearchContext = React.createContext()

function SearchProvider({ children, autoHandleSearch : propAutoHandleSearch = true }) {
    const user = useContext(UserContext)
    const [open, setOpen] = useState(false)
    const [onAddRequestCallback, setOnAddRequestCallback] = useState()
    const [props, setProps] = useState({})
    const [skillsToAdd, setSkillsToAdd] = useState([])
    const [goal, setGoal] = useState(null)
    const [skillsDialogOpen, setSkillsDialogOpen] = useState(false)
    const [callAutoHandleSearch, setCallAutoHandleSearch] = useState(null)
    const language = useLanguage()
    const autoHandleSearch = useMemo(
        () => {
            if (callAutoHandleSearch !== null && callAutoHandleSearch !== undefined)
                return callAutoHandleSearch
            return propAutoHandleSearch
        },
        [callAutoHandleSearch, propAutoHandleSearch]
    )

    const handleOpen = useCallback(
        (callback, props, autoHandleSearch = null) => {
            if (callback && callback.call)
                setOnAddRequestCallback(() => callback)
            else
                setOnAddRequestCallback(null)
            setCallAutoHandleSearch(autoHandleSearch)
            setProps(props)
            setOpen(true)
        },
        []
    )

    const handleClose = useCallback(
        () => setOpen(false),
        []
    )

    const handleAddRequest = useCallback(
        (item) => {
            if (autoHandleSearch) {
                if (item.object_type === "SK") {
                    updateSkillsBatch(
                        [item], 
                        true,
                        () => {
                            window.location.reload()
                        }
                    )
                } else { // object_type === "JO"
                    setGoal(item)
                    getJob(
                        item.id,
                        (data) => {
                            setSkillsToAdd(data.jobskill_set.filter(js => js.skill_addable).sort((a, b) => a.order - b.order));
                            setSkillsDialogOpen(true)
                            // setLoading(false);
                        },
                        null,
                        () => {
                            if (onAddRequestCallback) {
                                onAddRequestCallback(item)
                            }
                        }
                    )
                }
                handleClose()
            } else if (onAddRequestCallback) {
                onAddRequestCallback(item)
            } else {
                handleClose()
            }
        },
        [autoHandleSearch, onAddRequestCallback]
    )

    const handleUnarchiveRequest = useCallback(
        (item) => {
                if (item.object_type === "SK") {
                    doUnarchive(
                        item.id,
                        res => {
                            if (res.done) {
                                window.location.reload()
                            }
                        }
                    )
                }
                handleClose()
        },
        [autoHandleSearch, onAddRequestCallback]
    )

    const handleCancelSkillDialog = useCallback(
        () => {
            setSkillsDialogOpen(false);
        },
        []
    )

    const handleConfirmSkillDialog = useCallback(
        (skills, is_add, goal) => {
            if (is_add && goal) {
                saveGoalJob(
                    goal,
                    true   
                )
            }            
            updateSkillsBatch(
                skills.filter(skill => !skill.is_current).map(skill => ({"id": skill.skill, "is_add": is_add})),
                is_add,
                null,
                null,
                () => {
                    setSkillsDialogOpen(false)
                    window.location.reload()
                }
            )
        },
        [language, user]
    );

    const contextValue = useMemo(
        () => ({
            open: handleOpen,
            close: handleClose,
        }),
        [handleOpen, handleClose]
    )

    return (
        <SearchContext.Provider value={contextValue}>
            {children}
            <Portal>
                <SearchDialog
                    {...props}
                    onClose={handleClose}
                    open={open}
                    onAddRequest={handleAddRequest}
                    onUnarchiveRequest={handleUnarchiveRequest}
                />
                <SkillConfirmDialog
                    open={skillsDialogOpen}
                    onCancel={handleCancelSkillDialog}
                    onAccept={handleConfirmSkillDialog}
                    isAdd={true}
                    skills={skillsToAdd}
                    goal={goal}
                    showFollow={true}
                />
            </Portal>
        </SearchContext.Provider>
    )
}

export default SearchProvider