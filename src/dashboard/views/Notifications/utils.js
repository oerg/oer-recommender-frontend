import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function doMarkRead(notif, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.put(
            '/notifications/' + notif.id + '/',
            {
                status: "RE",
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}

export function markAllAsRead(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            '/notifications/read-all/'
        ),
        onLoad,
        onError,
        onEnd
    );
}