import axios from "axios";
import { requestAddHandlers } from "../helpers";

export const notifCheckDelay = 15000;

export function loadNotifs(lastNotifs, onLoad, onError, onEnd) {
    var url = '/notifications?ordering=-created_at&page=1';
    if (lastNotifs && lastNotifs.length > 0) {
        url += '&created_at__gt=' + lastNotifs[0]['created_at'];
    }
    requestAddHandlers(
        axios.get(url),
        response => {
            if (response.status === 401) { // expired session probably
                // window.alert(t("Your session has expired."));
                window.location.reload();
            } else {
                onLoad(response.data);
            }
        },
        onError,
        onEnd,
        true
    )
}


export function getName(userData) { // name or email
    let user = userData ? userData.user : null;
    if (user) {
        if ((user.first_name && user.first_name.length > 0) || 
            (user.last_name && user.last_name.length > 0)) {
            return (user.first_name ? user.first_name + ' ' : '') + (user.last_name ? user.last_name : '');
        }
        return user.email;
    }
    return '';
}

export function doAction(action, history, onLoad, onError, onEnd) {
    var req = null;
    switch(action['method']) {
        case 'POST':
            req = axios.post(action['link'], action['extra_fields']).then(onLoad).catch(onError).finally(onEnd);
        break;
        case 'PUT':
            req = axios.put(action['link'], action['extra_fields']).then(onLoad).catch(onError).finally(onEnd);
        break;
        case 'GET':
        default:
            if (onLoad) onLoad();
            history.push(action['link']);
            if (onEnd) onEnd();
            return;
    }
    requestAddHandlers(req, onLoad, onError, onEnd);
}
