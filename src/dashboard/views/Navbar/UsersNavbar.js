import { AppBar, Badge, Button, ButtonGroup, ClickAwayListener, Collapse, Hidden, IconButton, InputAdornment, Paper, Popper, TextField } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import CloseIcon from '@material-ui/icons/Close';
import HowToRegIcon from "@material-ui/icons/HowToReg";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import NotificationsIcon from '@material-ui/icons/Notifications';
import SearchIcon from '@material-ui/icons/Search';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import SearchResults from '../../../landing/components/home/SearchResults';
import { loadPart } from '../../../landing/components/home/utils';
import ChangeLanguageButton from '../../components/ChangeLanguageButton';
import Notifications from '../Notifications';
import { doMarkRead, markAllAsRead } from '../Notifications/utils';
import '../styles/blink.css';
import useStyles from '../styles/NavbarStyles';
import SideBar from './components/SideBar';
import UserMenu from './components/UserMenu';
import { doAction, getName, loadNotifs, notifCheckDelay } from './utils';

const logoImage = `${process.env.PUBLIC_URL}/images/logo.png`;

function LearningNavbar({ basePath, user, userData, enqueueSnackbar, closeSnackbar, ...props }) {
    const classes = useStyles();
    const isXtraSmall = useMediaQuery(theme => theme.breakpoints.down("xs"))
    const history = useHistory();
    const searchInput = useRef(null);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [notifRef, setNotifRef] = useState(null);
    const [openNotifs, setOpenNotifs] = useState(false);
    const [notifs, setNotifs] = useState([]);
    const [loadingNotifs, setLoadingNotifs] = useState(false);
    const [openUserMenu, setOpenUserMenu] = useState(false);
    const unreadNotifsCount = useMemo(
        () => notifs.filter(n => n['status'] === "UR").length,
        [notifs]
    );

    const searchRef = useRef(null);
    const userButtonRef = useRef(null);

    const linksData = useMemo(() => {
        var links = user.loggedIn ? []
            :
            [
                {
                    location: { 
                        pathname: `/${language}/`, 
                        state: {
                            login: true, 
                            backUrl: history.location.pathname
                        }
                    },
                    icon: <LockOpenIcon />,
                    label: t('Login'),
                },
                {
                    location: `/${language}/register`,
                    icon: <HowToRegIcon />,
                    label: t('Register'),
                },
            ];
        return links;
    }, [user.loggedIn, basePath, t]);

    const sideBarLinks = useMemo(
        () => [
            ...linksData,
            {
                label: "-",
            },
            <ChangeLanguageButton key={-1} fullWidth />,
        ],
        [linksData, basePath, userData]
    )

    const [openSearch, setOpenSearch] = useState(false)

    const [jobs, setJobs] = useState([])
    const [skills, setSkills] = useState([])
    const [topics, setTopics] = useState([])
    const [oers, setOers] = useState([])

    const [loadingJobs, setLoadingJobs] = useState(false)
    const [loadingSkills, setLoadingSkills] = useState(false)
    const [loadingTopics, setLoadingTopics] = useState(false)
    const [loadingOers, setLoadingOers] = useState(false)

    const loadJobs = useCallback(
        (page) => {
            setLoadingJobs(true);
            loadPart(
                'jobs',
                searchInput.current.value,
                page,
                res => {
                    setJobs(res);
                },
                null,
                () => setLoadingJobs(false)
            );
        },
        [],
    )

    const loadSkills = useCallback(
        (page) => {
            setLoadingSkills(true);
            loadPart(
                'skills',
                searchInput.current.value,
                page,
                res => {
                    setSkills(res);
                },
                null,
                () => setLoadingSkills(false),
                true
            );
        },
        [],
    )

    const loadTopics = useCallback(
        (page) => {
            setLoadingTopics(true);
            loadPart(
                'skills/topics',
                searchInput.current.value,
                page,
                res => {
                    setTopics(res);
                },
                null,
                () => setLoadingTopics(false)
            );
        },
        [],
    )

    const loadOers = useCallback(
        (page) => {
            setLoadingOers(true);
            loadPart(
                'oers',
                searchInput.current.value,
                page,
                res => {
                    setOers(res);
                },
                null,
                () => setLoadingOers(false)
            );
        },
        [],
    )

    const handleJobPageChange = useCallback(
        (page) => {
            loadJobs(page)
        },
        [loadJobs],
    )

    const handleSkillPageChange = useCallback(
        (page) => {
            loadSkills(page)
        },
        [loadSkills],
    )

    const handleTopicPageChange = useCallback(
        (page) => {
            loadTopics(page)
        },
        [loadTopics],
    )

    const handleOerPageChange = useCallback(
        (page) => {
            loadOers(page)
        },
        [loadOers],
    )

    const handleCloseSearch = useCallback(
        () => {
            setOpenSearch(false)
        },
        [],
    )

    // load notifications
    const loadNewNotifs = useCallback(
        () => {
            loadNotifs(
                notifs,
                r => setNotifs( // add new notifs
                    nts => {
                        if (r['results'] && r['results'].length > 0)
                            return [...r['results'], ...nts]
                        return nts;
                    }
                )
            )
        },
        [notifs]
    )

    // call load new notifications priodically
    useEffect(
        () => {
            loadNewNotifs();
        },
        [loadNewNotifs]
    );
    useEffect(
        () => {
            if (user.loggedIn) {
                const req = setInterval(
                    loadNewNotifs,
                    notifCheckDelay
                );
                return () => clearInterval(req); // clear interval on end
            }
        },
        [loadNewNotifs, user.loggedIn]
    )

    // show popup notifs
    // useEffect(
    //     () => {
    //         // select notifs with popup set and unread and not shown in popup
    //         notifs.filter(n => n['payload']['popup'] && n['status'] === "UR" && !n['shown']).forEach(
    //             notif => {
    //                 enqueueSnackbar(
    //                     notif['payload']['text'],
    //                     {
    //                         autoHideDuration: 4000,
    //                         action: <NotifActions notif={notif} onAction={handleActionNotif} loading={loadingNotifs} />,
    //                         onClose: () => notifs['shown'] = true,
    //                     }
    //                 )
    //             }
    //         )
    //     },
    //     [notifs]
    // );

    const handleCloseNotifs = useCallback(
        () => {
            setOpenNotifs(false);
        },
        []
    );

    const handleToggleNotifs = useCallback(
        e => {
            setNotifRef(e.currentTarget);
            setOpenNotifs(o => !o);
        },
        []
    );

    const handleSearch = useCallback(
        (e) => {
            if (e) e.preventDefault();
            if (searchInput.current.value.length === 0)
                return;
            loadJobs(1)
            loadSkills(1)
            loadTopics(1)
            loadOers(1)
            setOpenSearch(true)
        },
        [loadJobs, loadSkills, loadTopics, loadOers]
    )


    const handleReadNotif = useCallback(
        notif => {
            setLoadingNotifs(true);
            doMarkRead(
                notif,
                r => {
                    notif['status'] = "RE"; // mark as read locally
                    setNotifs(n => [...n]); // refresh notifications
                },
                null,
                () => setLoadingNotifs(false)
            )
        },
        []
    );

    const handleActionNotif = useCallback(
        (action, notif) => {
            doAction(
                action,
                history,
                () => handleReadNotif(notif)
            );
        },
        [handleReadNotif]
    );

    const handleClickAway = useCallback(
        () => setOpenSearch(false),
        []
    );

    const handleSearchTextChange = useCallback(
        e => {
            if (searchInput.current.value.length > 0) {
                setOpenSearch(true);
                handleSearch();
            } else {
                setOpenSearch(false);
            }
        },
        []
    );

    const handleMarkAll = useCallback(
        () => {
            setLoadingNotifs(true);
            markAllAsRead(
                () => {
                    setNotifs(
                        notifs => notifs.map(
                            n => ({...n, status: "RE"})
                        )
                    );
                },
                null,
                () => setLoadingNotifs(false)
            );
        },
        []
    );

    return (
        <AppBar className={classes.appBar} position="static">
            <div className={classes.container}>
                <Link to={`/${language}/`} title={t("Home")}>
                    <img src={logoImage} alt={t("eDoer Logo")} className={classes.logo} />
                </Link>
                <Hidden mdDown implementation="css" className={classes.appBarPart}>
                    <form onSubmit={handleSearch} className={classes.searchForm}>
                        <TextField
                            inputRef={searchInput}
                            onChange={handleSearchTextChange}
                            placeholder={t("Search eDoer...")}
                            color="primary"
                            variant="outlined"
                            fullWidth
                            ref={searchRef}
                            InputProps={{
                                className: classes.searchText,
                                endAdornment: <InputAdornment position="end">
                                    <IconButton
                                        style={{ height: 35, width: 35 }}
                                        onClick={handleSearch}
                                    >
                                        <SearchIcon />
                                    </IconButton>
                                </InputAdornment>,
                                style: { height: 48 }
                            }}
                        />
                        <Popper open={openSearch} anchorEl={searchRef.current} transition disablePortal style={{ zIndex: 10 }}>
                            {({ TransitionProps }) => (
                                <Collapse {...TransitionProps}>
                                    <Paper elevation={2} style={{ maxHeight: 500, maxWidth: 700, overflowY: "auto", display: "flex", flexFlow: "column" }}>
                                        <ClickAwayListener onClickAway={handleClickAway}>
                                            <div>
                                                <div style={{ paddingRight: 8, paddingTop: 8, textAlign: "right" }}>
                                                    <IconButton size="small" onClick={handleCloseSearch}>
                                                        <CloseIcon />
                                                    </IconButton>
                                                </div>
                                                <SearchResults
                                                    jobs={jobs}
                                                    skills={skills}
                                                    topics={topics}
                                                    oers={oers}
                                                    loadingJobs={loadingJobs}
                                                    loadingSkills={loadingSkills}
                                                    loadingTopics={loadingTopics}
                                                    loadingOers={loadingOers}
                                                    className={classes.searchResult}
                                                    onJobPageChange={handleJobPageChange}
                                                    onSkillPageChange={handleSkillPageChange}
                                                    onTopicPageChange={handleTopicPageChange}
                                                    onOerPageChange={handleOerPageChange}
                                                />
                                            </div>
                                        </ClickAwayListener>
                                    </Paper>
                                </Collapse>
                            )}
                        </Popper>
                    </form>
                </Hidden>
                <ButtonGroup variant="contained" color="inherit" className={classes.rightButton}>
                    <ChangeLanguageButton />

                    {user.loggedIn &&
                        <Button
                            ref={userButtonRef}
                            onClick={() => setOpenUserMenu(o => !o)}
                            className={classes.userButton}
                            classes={{
                                label: classes.userButtonLabel
                            }}
                            startIcon={isXtraSmall ? null : <AccountCircleIcon style={{ marginLeft: 5 }} />}
                            title={getName(userData)}
                        >
                            {isXtraSmall ? <AccountCircleIcon /> : getName(userData)}
                        </Button>
                    }
                    {user.loggedIn &&
                    <Button
                        ref={notifRef}
                        onClick={handleToggleNotifs}
                        size="small"
                    >
                        <Badge
                            badgeContent={unreadNotifsCount}
                            classes={{
                                badge: 'notifBadge'
                            }}>
                            <NotificationsIcon />
                        </Badge>
                    </Button>
                    }
                </ButtonGroup>
                <Hidden mdUp implementation="css" className={classes.menuButton}>
                    <SideBar links={sideBarLinks} logoPath={logoImage} />
                </Hidden>
                {user.loggedIn &&
                    <UserMenu
                        anchorEl={userButtonRef.current}
                        open={openUserMenu}
                        onClose={() => setOpenUserMenu(false)}
                    />
                }
                <Notifications show={openNotifs} onClose={handleCloseNotifs} anchor={notifRef} notifications={notifs}
                    onRead={handleReadNotif} onAction={handleActionNotif} loading={loadingNotifs} onMarkAllAsRead={handleMarkAll} />

            </div>
        </AppBar>
    )
}

LearningNavbar.propTypes = {
    handleDrawerToggle: PropTypes.func
}

export default withSnackbar(LearningNavbar);
