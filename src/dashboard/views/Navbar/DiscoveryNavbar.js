import { AppBar, Avatar, Badge, Button, ButtonGroup, Hidden, Slide, Tab, Tabs } from '@material-ui/core';
import AppsIcon from '@material-ui/icons/Apps';
import CastForEducationIcon from '@material-ui/icons/CastForEducation';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import HowToRegIcon from "@material-ui/icons/HowToReg";
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import LockOpenIcon from "@material-ui/icons/LockOpen";
import MenuBookIcon from '@material-ui/icons/MenuBook';
import NotificationsIcon from '@material-ui/icons/Notifications';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import PeopleIcon from '@material-ui/icons/People';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import clsx from 'classnames';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Notifications from '../Notifications';
import { doMarkRead, markAllAsRead } from '../Notifications/utils';
import MentoringRequestDialog from '../Overlays/MentoringRequestDialog';
import '../styles/blink.css';
import useStyles from '../styles/NavbarStyles';
import SideBar from './components/SideBar';
import UserMenu from './components/UserMenu';
import { doAction, getName, loadNotifs, notifCheckDelay } from './utils';
const logoImage = `${process.env.PUBLIC_URL}/images/logo_dev.png`;

function DiscoveryNavbar({ exploreOpen, onExploreOpenChange, basePath, user, userData, enqueueSnackbar, closeSnackbar, ...props }) {
    const classes = useStyles();
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [notifRef, setNotifRef] = useState(null);
    const [openNotifs, setOpenNotifs] = useState(false);
    const [notifs, setNotifs] = useState([]);
    const [loadingNotifs, setLoadingNotifs] = useState(false);
    const [openUserMenu, setOpenUserMenu] = useState(false);
    const [requestMentoring, setRequestMentoring] = useState(false);
    const unreadNotifsCount = useMemo(
        () => notifs.filter(n => n['status'] === "UR").length,
        [notifs]
    );
    const location = useLocation();

    const userButtonRef = useRef(null);

    const linksData = useMemo(() => (
        [
            {
                location: `${basePath}/discovery/jobs`,
                icon: <WorkOutlineIcon />,
                label: t('Journeys'),
            },
            {
                location: `${basePath}/discovery/skills`,
                icon: <AppsIcon />,
                label: t('Courses'),
            },
            {
                location: `${basePath}/discovery/topics`,
                icon: <ChatBubbleOutlineIcon />,
                label: t('Topics'),
            },
            {
                location: `${basePath}/discovery/oer-groups`,
                icon: <CastForEducationIcon />,
                label: t('Packages'),
                alias: [
                    `${basePath}/discovery/oers`
                ]
            },
            {
                location: `${basePath}/discovery/questions`,
                icon: <LiveHelpIcon />,
                label: t('Questions'),
            },
        ]
    ), [basePath, t]);

    const sideBarLinks = useMemo(
        () => [
            ...linksData,
            {
                label: "-",
            },
            ...userData && Object.keys(userData).length > 0 && userData.settings.discovery ?
                [{
                    label: t("Participate in Content Curation"),
                    location: `${basePath}/discovery`,
                    icon: <PeopleIcon />,
                },
                ]
                :
                [],
        ],
        [linksData, basePath, userData, t]
    )

    const activeMenu = useMemo(
        () => {
            const active = linksData.find(
                menuItem => location.pathname.startsWith(menuItem.location)
                    || (menuItem.alias && menuItem.alias.some(alias => location.pathname.startsWith(alias)))
            );
            if (active) return active.label;
            history.push(linksData[0].location); // default page
            return linksData[0].label;
        },
        [linksData, basePath, location]
    );

    // load notifications
    const loadNewNotifs = useCallback(
        () => {
            loadNotifs(
                notifs,
                r => setNotifs( // add new notifs
                    nts => {
                        if (r['results'] && r['results'].length > 0)
                            return [...r['results'], ...nts]
                        return nts;
                    }
                )
            )
        },
        [notifs]
    )

    // call load new notifications priodically
    useEffect(
        loadNewNotifs,
        [loadNewNotifs]
    );
    useEffect(
        () => {
            if (user.loggedIn) {
                const req = setInterval(
                    loadNewNotifs,
                    notifCheckDelay
                );
                return () => clearInterval(req); // clear interval on end
            }
        },
        [loadNewNotifs, user.loggedIn]
    )

    // show popup notifs
    // useEffect(
    //     () => {
    //         // select notifs with popup set and unread and not shown in popup
    //         notifs.filter(n => n['payload']['popup'] && n['status'] === "UR" && !n['shown']).forEach(
    //             notif => {
    //                 enqueueSnackbar(
    //                     notif['payload']['text'],
    //                     {
    //                         autoHideDuration: 4000,
    //                         action: <NotifActions notif={notif} onAction={handleActionNotif} loading={loadingNotifs} />,
    //                         onClose: () => notifs['shown'] = true,
    //                     }
    //                 )
    //             }
    //         )
    //     },
    //     [notifs]
    // );

    const handleCloseNotifs = useCallback(
        () => {
            setOpenNotifs(false);
        },
        []
    );

    const handleToggleNotifs = useCallback(
        e => {
            setNotifRef(e.currentTarget);
            setOpenNotifs(o => !o);
        },
        []
    );

    const handleReadNotif = useCallback(
        notif => {
            setLoadingNotifs(true);
            doMarkRead(
                notif,
                r => {
                    notif['status'] = "RE"; // mark as read locally
                    setNotifs(n => [...n]); // refresh notifications
                },
                null,
                () => setLoadingNotifs(false)
            )
        },
        []
    );

    const handleActionNotif = useCallback(
        (action, notif) => {
            doAction(
                action,
                history,
                () => {
                    handleReadNotif(notif)
                    if (action.reload) {
                        user.reload()
                    }
                }
            );
        },
        [handleReadNotif, user]
    );

    const handleMarkAll = useCallback(
        () => {
            setLoadingNotifs(true);
            markAllAsRead(
                () => {
                    setNotifs(
                        notifs => notifs.map(
                            n => ({ ...n, status: "RE" })
                        )
                    );
                },
                null,
                () => setLoadingNotifs(false)
            );
        },
        []
    );

    const handleCloseMentoring = useCallback(
        () => setRequestMentoring(false),
        []
    );

    const handleRequestMentoring = useCallback(
        () => setRequestMentoring(true),
        []
    );

    const handleExplore = useCallback(
        () => onExploreOpenChange(o => !o),
        []
    );

    return <>
        <div style={{
            position: "absolute",
            top: 86,
            left: -5,
            zIndex: 4,
        }}>
            <Slide
                in
                direction='right'
            >
                <Button
                    title={t("Back to Learning Mode")}
                    component={Link}
                    to={basePath}
                    variant='contained'
                    className={classes.backButton}
                    startIcon={<MenuBookIcon />}
                >
                    {t("Learning")}
                </Button>
            </Slide>
        </div>
        <AppBar className={classes.appBar} position="static">
            <div className={classes.container}>
                <Link to={`/${language}/`} title={t("Home")}>
                    <img src={logoImage} alt={t("eDoer Logo")} className={classes.logo} />
                </Link>
                <Button
                    variant="contained"
                    color="secondary"
                    style={{
                        backgroundColor: "#005c5f",
                        color: "#FFF",
                        height: 35,
                        marginRight: 10,
                    }}
                    onClick={handleExplore}
                    endIcon={exploreOpen ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                >
                    {t("Explore")}
                </Button>
                <Hidden smDown implementation="css" className={clsx(classes.topMenuWrapper, classes.discoveryNav)}>
                    <Tabs scrollButtons="auto" className={classes.topMenu} value={activeMenu} id="navbar"
                        classes={{ indicator: classes.menuIndicator }} variant="scrollable">
                        {linksData.map(
                            (link, index) => (
                                <Tab
                                    component={Link}
                                    to={link.location}
                                    icon={link.icon}
                                    key={index}
                                    value={link.label}
                                    className={clsx(classes.menuItem)}
                                    classes={{
                                        wrapper: classes.menuItemWrapper
                                    }}
                                    label={link.label}
                                    {...link.props} />
                            )
                        )}
                    </Tabs>
                </Hidden>
                <ButtonGroup variant="contained" color="inherit" className={classes.rightButton}>
                    <Button
                        className={classes.userButton}
                        classes={{
                            label: classes.userButtonLabel
                        }}
                        title={t("Request a mentor")}
                        onClick={handleRequestMentoring}
                    >
                        <ContactMailIcon />
                    </Button>
                    <Button
                        ref={notifRef}
                        onClick={handleToggleNotifs}
                        title={t("Notifications")}
                    >
                        <Badge
                            badgeContent={unreadNotifsCount}
                            classes={{
                                badge: 'notifBadge'
                            }}>
                            {unreadNotifsCount > 0 ?
                                <NotificationsActiveIcon />
                                :
                                <NotificationsIcon />
                            }
                        </Badge>
                    </Button>
                    {user.loggedIn &&
                        <Button
                            ref={userButtonRef}
                            onClick={() => setOpenUserMenu(o => !o)}
                            className={classes.userButton}
                            classes={{
                                label: classes.userButtonLabel
                            }}
                            title={getName(userData)}
                        >
                            <Avatar style={{ width: 40, height: 40 }} src={userData.profile_picture} />
                        </Button>
                    }
                </ButtonGroup>
                <Hidden mdUp implementation="css" className={classes.menuButton}>
                    <SideBar links={sideBarLinks} logoPath={logoImage} />
                </Hidden>
                <UserMenu
                    anchorEl={userButtonRef.current}
                    open={openUserMenu}
                    onClose={() => setOpenUserMenu(false)}
                    menu={user.loggedIn ? undefined : [
                        {
                            location: {
                                pathname: `/${language}/`,
                                state: {
                                    login: true,
                                    backUrl: history.location.pathname
                                }
                            },
                            icon: <LockOpenIcon />,
                            label: t('Login'),
                        },
                        {
                            location: `/${language}/register`,
                            icon: <HowToRegIcon />,
                            label: t('Register'),
                        },
                    ]}
                />
                <Notifications show={openNotifs} onClose={handleCloseNotifs} anchor={notifRef} notifications={notifs}
                    onRead={handleReadNotif} onAction={handleActionNotif} loading={loadingNotifs} onMarkAllAsRead={handleMarkAll} />
                <MentoringRequestDialog
                    user={user}
                    open={requestMentoring}
                    onClose={handleCloseMentoring}
                />
            </div>
        </AppBar>
    </>
}

DiscoveryNavbar.propTypes = {
    handleDrawerToggle: PropTypes.func
}

export default withSnackbar(DiscoveryNavbar);
