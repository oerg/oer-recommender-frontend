import React from 'react';
import { Switch, Route } from "react-router-dom";
import DiscoveryNavbar from './DiscoveryNavbar';
import LearningNavbar from './LearningNavbar';
import UsersNavbar from './UsersNavbar';

function Navbar({ path, ...props }) {
    return <Switch>
        <Route path={`${path}discovery/`}>
            <DiscoveryNavbar {...props} />
        </Route>
        <Route path='/:lang(\w{2})?/users/:id(\d+)'>
            <UsersNavbar {...props} />
        </Route>
        <Route path='/:lang(\w{2})?/self-assessment-reports/:id(\d+)/:token(.+)'>
            <UsersNavbar {...props} />
        </Route>        
        <Route>
            <LearningNavbar {...props} />
        </Route>
    </Switch>
}

export default Navbar;
