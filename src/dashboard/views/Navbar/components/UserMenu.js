import { ClickAwayListener, Divider, Grow, ListItemIcon, makeStyles, MenuItem, MenuList, Paper, Popper } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PeopleIcon from '@material-ui/icons/People';
import PersonIcon from '@material-ui/icons/Person';
import SettingsBrightnessIcon from '@material-ui/icons/SettingsBrightness';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';

const useStyles = makeStyles(
    theme => ({
        menuIcon: {
            minWidth: 40
        }
    })
)

function UserMenu({ open, onClose, anchorEl, menu: propMenu, showMentoring = true, showParticipation = true }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const menus = useMemo(
        () => (propMenu ? propMenu : [
            {
                label: t("Profile Settings"),
                location: `/${language}/dashboard/profile/profile`,
                icon: <PersonIcon />,
            },
            {
                label: t("Learning Preferences"),
                location: `/${language}/dashboard/profile/preferences`,
                icon: <SettingsBrightnessIcon />
            },
            {
                label: t("My Journeys"),
                location: `/${language}/dashboard/my-journeys`,
                icon: <WorkOutlineIcon />
            },            
            {
                label: "-"
            },
            ...(showParticipation ? [
                {
                    label: t("Participate in Content Curation"),
                    location: `/${language}/dashboard/discovery`,
                    icon: <PeopleIcon />,
                },
                {
                    label: t("Show Areas Lacking Content"),
                    location: `/${language}/dashboard/my-area`,
                    icon: null,
                },
                {
                    label: "-"
                }
            ]
                : []
            ),
            ...(showMentoring ? [
                {
                    label: t("Mentoring Requests"),
                    location: `/${language}/dashboard/mentoring-requests`,
                    icon: null,
                },
                {
                    label: "-"
                }
            ]
                : []
            ),
            {
                label: t("Logout"),
                location: `/${language}/dashboard/logout`,
                icon: <ExitToAppIcon />
            },
        ]),
        [propMenu, t, showParticipation, showMentoring]
    );


    return (
        <Popper
            open={open}
            anchorEl={anchorEl}
            style={{ zIndex: 25 }}
            placement="bottom-end"
            transition
        >
            {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                    <Paper>
                        <ClickAwayListener onClickAway={onClose}>
                            <MenuList autoFocusItem={open} style={{ minWidth: 200 }} onClick={onClose}>
                                {
                                    menus.map(
                                        (menu, index) => (
                                            menu.label === '-' ?
                                                <Divider key={index} />
                                                :
                                                <MenuItem key={index} component={Link} to={menu.location}>
                                                    {<ListItemIcon className={classes.menuIcon}>
                                                        {menu.icon}
                                                    </ListItemIcon>}
                                                    {menu.label}
                                                </MenuItem>
                                        )
                                    )
                                }
                            </MenuList>
                        </ClickAwayListener>
                    </Paper>
                </Grow>
            )}
        </Popper>
    )
}

export default UserMenu
