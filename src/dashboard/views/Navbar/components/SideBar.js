import { IconButton, ListItem, ListItemIcon, ListItemText, SwipeableDrawer, List, Divider } from '@material-ui/core'
import React, { useCallback, useState } from 'react'
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../../i18n';

export default function SideBar({ links, logoPath, topContent }) {
    const [open, setOpen] = useState(false);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const handleToggle = useCallback(
        () => setOpen(o => !o),
        []
    )

    return (
        <>
            <IconButton
                onClick={handleToggle}
            >
                <MenuIcon />
            </IconButton>
            <SwipeableDrawer
                open={open}
                onOpen={handleToggle}
                onClose={handleToggle}
            >
                <div style={{ width: 240 }}>
                    <Link to={`/${language}/`} title={t("Home")} style={{ display: "block", textAlign: "center", margin: 16 }}>
                        <img src={logoPath} alt={t("eDoer Logo")} style={{ width: "80%" }} />
                    </Link>
                    {topContent}
                    <List
                        onClick={handleToggle}
                    >
                        {links.map((item, index) => (
                            React.isValidElement(item) ?
                                item
                            :
                                item.label === '-' ?
                                    <Divider key={index} />
                                :
                                    <ListItem
                                        button
                                        key={index}
                                        component={Link}
                                        to={item.location}
                                    >
                                        {item.icon &&
                                            <ListItemIcon>{item.icon}</ListItemIcon>
                                        }
                                        <ListItemText>{item.label}</ListItemText>
                                    </ListItem>
                        ))}
                    </List>
                </div>
            </SwipeableDrawer>
        </>
    )
}
