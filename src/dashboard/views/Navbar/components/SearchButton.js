import React from 'react'
import SearchIcon from '@material-ui/icons/Search'
import { Button, makeStyles } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import clsx from 'classnames'

const useStyles = makeStyles({
    label: {
        backgroundColor: "#EEE",
        color: "#555",
        height: "100%",
        padding: "8px 14px 6px 14px",
        minWidth: 175,
        justifyContent: "flex-start",
        textTransform: "none",
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
    },
    icon: {
        color: "#005c5f"
    },
    root: {
        padding: 2,
    }
})

function SearchButton({ className, classes, ...props }) {
    const styles = useStyles()
    const { t } = useTranslation(['dashboard'])

    return (
        <Button
            variant='contained'
            color='primary'
            startIcon={<SearchIcon />}
            {...props}
            className={clsx(styles.root, className)}
            classes={{
                ...classes,
                label: classes ? clsx(styles.label, classes.label) : styles.label,
                startIcon: classes ? clsx(styles.icon, classes.startIcon) : styles.icon,
            }}
        >
            {t("Search...")}
        </Button>
    )
}

export default SearchButton