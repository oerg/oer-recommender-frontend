import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function getSelfAssessment(id, token, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`assessments/questions/get-self-assessment/?self_assessment_id=${id}&token=${token}`),
        onLoad,
        onError,
        onEnd
    );
}