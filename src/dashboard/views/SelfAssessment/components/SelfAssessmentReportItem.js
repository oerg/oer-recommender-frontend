import { Grid, Paper, Typography } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import ProgressLine from '../../Education/components/ReportComponents/ProgressLine/ProgressLine';

function SelfAssessmentReportItem({ answer, options }) {
    const percentage = useMemo(
        () => answer && options ? (answer.answer + 1) * 100 / options.options.length : null,
        [answer])

    const { t } = useTranslation(['landing'])

    return (
        <Paper square>
            <Grid container>
                <Grid
                    item
                    xs={5}
                    style={{
                        display: "flex",
                        alignItems: "center",
                        padding: 24
                    }}
                >
                    <Typography variant="body">
                        {answer.question_raw_text}
                    </Typography>
                </Grid>
                <Grid
                    item
                    xs={2}
                    style={{
                        display: "flex",
                        alignItems: "center",
                        padding: 8
                    }}
                >
                    <Typography
                        variant="body"
                    >
                        {answer.answer === -1 ? <em>{t("No Answer.")}</em> : answer.answer_text}
                    </Typography>
                </Grid>
                <Grid
                    item
                    xs={5}
                    style={{
                        display: "flex",
                        alignItems: "center",
                        textAlign: "center"
                    }}
                >
                    {answer.answer === -1 ?
                        "-"
                    :
                        <ProgressLine
                            label={t("Two visual percentages")}
                            backgroundColor="lightgrey"
                            visualParts={[
                                {
                                    percentage: `${percentage.toFixed(2)}%`,
                                    text: `${answer.answer + 1} / ${options.options.length}`,
                                    tooltip: <> {options.options.map(
                                        (option, key) => (<>{option === answer.answer_text ? <em style={{ color: "#c3faf8" }}>{option}</em> : option} <br /></>))}
                                    </>,
                                    color: "#95b4e6"
                                }
                            ]}
                            style={{
                                width: 400
                            }}
                        />
                    }
                </Grid>
            </Grid>
        </Paper>
    )
}

export default SelfAssessmentReportItem