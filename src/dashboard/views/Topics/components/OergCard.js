import { IconButton, List, ListItem } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import LaunchIcon from '@material-ui/icons/Launch';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import OergLogo from '../../../components/Logos/Oerg';
import ShareButton from '../../../components/ShareButton';
import DetailWindow from './DetailWindow';

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around"
    },
    actionRight: {
        marginLeft: 'auto'
    },
    avatar: {
        backgroundColor: green[100],
    },
    cardHeaderTitle: {
        width: "95%",
        whiteSpace: "nowrap",
        overflow : "hidden",
        textOverflow: "ellipsis"
    },
    doneIcon: {
        cursor: "default",
    },
    title: {
        cursor: "pointer",
        paddingRight: theme.spacing(0.5)
    }
}));

export default function OergCard({ oerg, basePath }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    const [mouseOver, setMouseOver] = useState(false);
    const [showDescription, setShowDescription] = useState(false);

    const myUrl = useMemo(
        () => `${basePath}/oergs/${oerg.id}`,
        [ oerg, basePath ]
    );

    const handleMouseEnter = useCallback(
        () => setMouseOver(true),
        []
    );

    const handleMouseOut = useCallback(
        () => setMouseOver(false),
        []
    );

    const handleShowDetails = useCallback(
        () => setShowDescription(true),
        []
    );

    const handleCloseDescription = useCallback(
        () => setShowDescription(false),
        []
    );

    return (<>
        <Card className={classes.root}
            raised={mouseOver} 
            onMouseEnter={handleMouseEnter} 
            onMouseLeave={handleMouseOut}
            >
            <CardHeader
                avatar={<OergLogo />}
                title={oerg.title}
                titleTypographyProps={{
                    title: oerg.title,
                    variant: "h6",
                    className: classes.title,
                    onClick: handleShowDetails
                }}
            />
            <CardActions disableSpacing>
                <div className={classes.actionRight}>
                    <ShareButton title={oerg.skill_title} componentTitle={t("Share this Course")} url={myUrl}>
                        <ShareIcon />
                    </ShareButton>
                    <IconButton title={t("Learn more")} onClick={handleShowDetails}>
                        <LaunchIcon />
                    </IconButton>
                </div>
            </CardActions>
        </Card>
        <DetailWindow
            title={oerg.title}
            icon={<OergLogo />}
            open={showDescription}
            onClose={handleCloseDescription} 
            oergId={oerg.id}
        >
            <p>
            {oerg.description}
            </p>
            <i>{t("This Educational Package contains the following Educational Contents")}</i>
            <List>
                { oerg.oers.map(
                    (oer, index) => (
                        <ListItem key={index} button href={oer.url} target="_blank" component='a'>
                            {oer.title} &nbsp; <LaunchIcon />
                        </ListItem>
                    )
                )}
            </List>
        </DetailWindow>
    </>);
}
