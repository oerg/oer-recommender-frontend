import axios from "axios";
import { requestAddHandlers } from "../../helpers";

export function saveAnswers(skill_id, answers, questions, onLoad, onError, onEnd) {
    const parsedAnswers = answers.map(
        (answer, index) => ({
            question_id: questions[index].id,
            answer: answer,
        })
    )
    
    return requestAddHandlers(
        axios.post(
            '/assessments/questions/participate-self-assessment/',
            {
                skill_id,
                answers: parsedAnswers
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function loadSelfAssessment(skillId, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/skills/get-self-assessment-exam/?skill_id=${skillId}`
        ),
        onLoad,
        onError,
        onEnd
    )
}