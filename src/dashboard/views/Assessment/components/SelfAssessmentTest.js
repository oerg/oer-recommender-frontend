import { Button, Collapse, LinearProgress, Paper, Radio, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow } from '@material-ui/core'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import HtmlDiv from '../../../components/HtmlDiv'
import { saveAnswers } from './utils'

function SelfAssessmentTest({ skillId, questions, options, showSave = true, onSave, description, loading: propLoading, ...props }) {
    const { t } = useTranslation(['dashboard'])
    const [answers, setAnswers] = useState([])
    const [loading, setLoading] = useState(false)

    const isLoading = useMemo(
        () => loading || propLoading,
        [propLoading]
    )

    useEffect(
        () => {
            setAnswers(questions.map(q => -1))
        },
        [questions]
    )

    const handleSelectAnswer = useCallback(
        (index) => (e) => {
            setAnswers(
                answers => {
                    const newAnswers = [...answers]
                    newAnswers[index] = parseInt(e.target.value)
                    return newAnswers
                }
            )
        },
        []
    )

    const handleSubmit = useCallback(
        () => {
            setLoading(true)
            saveAnswers(
                skillId,
                answers,
                questions,
                (res) => {
                    onSave(res)
                },
                null,
                () => setLoading(false)
            )
        },
        [skillId, answers, questions, onSave]
    )

    return <>
        <Collapse in={isLoading}>
            <LinearProgress />
        </Collapse>
        {description && <Paper 
            dangerouslySetInnerHTML={{ __html: description }}
            style={{
                padding: 16,
            }}
        />}
        <TableContainer component={Paper} {...props}>
            <Table stickyHeader>
                <TableHead>
                    <TableRow>
                        <TableCell />
                        {options.map(
                            (option, index) => <TableCell key={index}>
                                {option}
                            </TableCell>
                        )}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {questions.map(
                        (question, index) => <TableRow key={index}>
                            <TableCell>
                                <HtmlDiv html={question.text} />
                            </TableCell>
                            {options.map(
                                (option, oIndex) => <TableCell key={`q${index}-${oIndex}`}>
                                    <Radio 
                                        value={oIndex} 
                                        onChange={handleSelectAnswer(index)}
                                        checked={answers[index] === oIndex}
                                        title={option} 
                                        disabled={isLoading}
                                    />
                                </TableCell>
                            )}
                        </TableRow>
                    )}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TableCell colSpan={options.length + 1} align="right">
                            <Collapse in={showSave}>
                                <Button
                                    title={t("Submit the results")}
                                    onClick={handleSubmit}
                                    variant='contained'
                                    color='primary'
                                >
                                    {t("Save")}
                                </Button>
                            </Collapse>
                        </TableCell>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    </>
}

export default SelfAssessmentTest