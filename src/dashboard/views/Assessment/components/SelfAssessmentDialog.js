import { Collapse, Dialog, DialogContent, DialogTitle, IconButton, makeStyles } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import SelfAssessmentTest from './SelfAssessmentTest'
import { loadSelfAssessment } from './utils'
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles({
    root: {
        minWidth: 800,
    },
    titlePart: {
        flex: 1,
    },
    titleWrapper: {
        display: "flex",
    }
})

function SelfAssessmentDialog({ title, skillId, onClose, open, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const [loading, setLoading] = useState(false)
    const [questions, setQuestions] = useState([])
    const [options, setOptions] = useState([])
    const [description, setDescription] = useState("")
    const [errorText, setErrorText] = useState(null)
    const [isDone, setIsDone] = useState(false)

    useEffect(
        () => {
            if (open) {
                if (skillId) {
                    setLoading(true)
                    setErrorText(null)
                    setIsDone(false)
                    loadSelfAssessment(
                        skillId,
                        data => {
                            setQuestions(data.questions)
                            setOptions(data.options[0].options)
                            setDescription(data.options[0].description)
                        },
                        err => {
                            setErrorText(err)
                        },
                        () => setLoading(false)
                    )
                } else {
                    setQuestions([])
                    setOptions([])
                }
            }
        },
        [skillId, open]
    )

    const closeProcedure = useCallback(
        () => {
            window.setTimeout(
                () => {
                    onClose(null, "done")
                },
                2000
            )
        },
        [onClose]
    )

    const handleSaveAnswers = useCallback(
        (res) => {
            if (res.done) {
                setIsDone(true)
                closeProcedure()
            }
        },
        [closeProcedure],
    )

    const handleClose = useCallback(
        (e) => {
            onClose(e, "close")
        },
        [onClose]
    )


    return <Dialog
        scroll='paper'
        classes={{
            paper: classes.root
        }}
        fullScreen
        {...props}
        open={open}
    >
        <DialogTitle>
            <div className={classes.titleWrapper}>
                <div className={classes.titlePart}>
                    {title}
                    <small>{t("Self-Assessment")}</small>
                </div>

                <IconButton size="small" onClick={handleClose} title={t("Cancel this test")}>
                    <CloseIcon />
                </IconButton>

            </div>
        </DialogTitle>
        <DialogContent>
            <Collapse in={isDone}>
                <Alert
                    severity='success'
                    variant='filled'
                >
                    {t("Your answers are saved. Thank you for participation.")}
                </Alert>
            </Collapse>
            {errorText ?
                <Alert severity='error'>
                    {errorText}
                </Alert>
                :
                <SelfAssessmentTest
                    skillId={skillId}
                    questions={questions}
                    options={options}
                    description={description}
                    loading={loading || isDone}
                    onSave={handleSaveAnswers}
                    showSave={!isDone}
                />
            }
        </DialogContent>
    </Dialog>
}

export default SelfAssessmentDialog