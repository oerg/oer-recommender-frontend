import { Button, Checkbox, Collapse, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, LinearProgress, List, ListItem, ListItemIcon, ListItemText, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { validateEmail } from '../../helpers';


function ShareDialog(rawProps) {
    const {
        emails,
        open,
        onAccept,
        onCancel,
        selfAssessmentId,
        loading,
        ...props
    } = rawProps;

    const [selectedEmails, setSelectedEmails] = useState(emails);
    const [inputEmail, setInputEmail] = useState("")

    useEffect(
        () => {
            setSelectedEmails(emails)
        },
        [emails]
    )

    const { t } = useTranslation(['dashboard']);

    const handleClose = useCallback(
        (e, reason) => {
            if (reason !== "backdropClick") {
                onCancel();
            }
        },
        []
    );

    const handleSelect = useCallback(
        (email) => () => {
            setSelectedEmails(
                ss => {
                    const pos = ss.indexOf(email)
                    if (pos === -1) {
                        return [...ss, email];
                    } else {
                        ss.splice(pos, 1);
                        return [...ss];
                    }
                }
            );
        },
        []
    );

    const handleAccept = useCallback(
        () => {
            onAccept(selfAssessmentId, selectedEmails)
        },
        [selectedEmails, onAccept, selfAssessmentId]
    )

    const handleAddEmail = useCallback(
        () => {
            setSelectedEmails(l => {
                if (l.includes(inputEmail))
                    return l;
                return [...l, inputEmail];
            })
            setInputEmail("")
        },
        [inputEmail]);

    const handleEnter = useCallback(
        e => {
            if (e.keyCode === 13 && inputEmail.length > 0) { // enter is pressed
                handleAddEmail(inputEmail);
            }
        },
        []
    );

    const handleInputEmail = useCallback(
        (e) => {
            setInputEmail(e.target.value);
        },
        []
    );

    return (
        <Dialog {...props} open={open} onClose={handleClose}>
            <DialogTitle>{t("Sharing the Self-assessment Report")}</DialogTitle>
            <DialogContent dividers>
                <Collapse in={loading}>
                    <LinearProgress color='secondary' />
                </Collapse>
                <DialogContentText>
                    {t("You can select among your mentors or type any email directly.")}
                </DialogContentText>
                <List>
                    {selectedEmails.map(
                        (s, index) => <ListItem key={index} button dense onClick={handleSelect(s)}>
                            <ListItemIcon>
                                <Checkbox edge="start" tabIndex={-1} disableRipple checked={selectedEmails.includes(s)} />
                            </ListItemIcon>
                            <ListItemText primary={s} />
                        </ListItem>
                    )}
                </List>
            </DialogContent>
            <TextField
                value={inputEmail}
                label={t("Add email")}
                onKeyDown={handleEnter}
                onChange={handleInputEmail}
                style={{
                    margin: "auto",
                    marginTop: 16,
                    width: "90%",
                }}
                autoFocus
                type="email"
            />
            <Button
                style={{
                    width: "90%",
                    margin: "auto",
                }}
                onClick={handleAddEmail}
                startIcon={<AddIcon />}
                disabled={!validateEmail(inputEmail) || loading}
            >
                {t("Add Email")}
            </Button>
            <DialogActions>
                <Button onClick={handleClose}>{t("Cancel")}</Button>
                <Button color="primary" disabled={selectedEmails.length === 0 || loading} onClick={handleAccept}>{t("Share")}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default ShareDialog

