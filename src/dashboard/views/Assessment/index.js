import { Accordion, AccordionDetails, AccordionSummary, Backdrop, Box, Button, Collapse, Grid, Paper, Typography } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import Axios from 'axios'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import { useLanguage } from '../../../i18n'
import { parseDateTime } from '../../../utils'
import Card from '../../components/Card/Card'
import CardBody from '../../components/Card/CardBody'
import CardHeader from '../../components/Card/CardHeader'
import GridContainer from '../../components/Grid/GridContainer'
import GridItem from '../../components/Grid/GridItem'
import SkillLogo from '../../components/Logos/Skill'
import RoseText from '../../components/Typography/Rose'
import { loadMySkills } from '../Education/utils'
import useStyles from '../styles/AssessmentStyles'
import Test from '../Test'
import SelfAssessmentDialog from './components/SelfAssessmentDialog'
import ShareDialog from './components/ShareDialogue'
import AssessmentReports from './Reports'
import { getSelfAssessmentReport, saveShares } from './utils'


function Assessment() {
    const classes = useStyles();

    const [skills, setSkills] = useState([])
    const [loading, setLoading] = useState(false)
    const [reports, setReports] = useState({});
    const [selfAssessmentReport, setSelfAssessmentReport] = useState([])
    const [activeSelfAssessmentSkill, setActiveSelfAssessmentSkill] = useState(null);

    const loadSelfAssessmentReport = useCallback((activeSkill) => {
        if (activeSkill) {
            setLoadingSelfAssessmentReports(true);
            getSelfAssessmentReport(
                activeSkill,
                res => {
                    setSelfAssessmentReport(res);
                },
                null,
                () => setLoadingSelfAssessmentReports(false)
            )
        }
    }, []);

    const loadAllMySkills = useCallback(
        () => {
            setLoading(true)
            loadMySkills(
                mySkills => {
                    setSkills(mySkills)
                },
                null,
                () => setLoading(false)
            )
            setReports({})
        },
        []
    )

    useEffect(() => {
        loadAllMySkills()
    }, [loadAllMySkills])


    const myCurrentSkills = useMemo(() => skills.filter(s => s.is_current && s.skill.assessmentable), [skills]);
    const myCurrentSelfAssessmentableSkills = useMemo(() => skills.filter(s => s.is_current && s.skill.self_assessmentable), [skills]);

    const [showExam, setShowExam] = useState(false);
    const [skillId, setSkillId] = useState(0);
    const [activeSkill, setActiveSkill] = useState('');
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const emptyReports = useMemo(() => Object.keys(reports).length === 0, [reports]);
    const [loadingReports, setLoadingReports] = useState(false);
    const [loadingSelfAssessmentReports, setLoadingSelfAssessmentReports] = useState(false)
    const [shareDialogOpen, setShareDialogOpen] = useState(false)
    const [selfAssessSkill, setSelfAssessSkill] = useState(null)
    const [showSelfAssessment, setShowSelfAssessment] = useState(false)
    const [selectedSelfAssessmentReportItem, setSelectedSelfAssessmentReportItem] = useState(null)
    const [shareLoading, setShareLoading] = useState(false)

    const locationState = useLocation().state;

    useEffect(
        () => {
            if (locationState) {
                if (locationState.exam_skill_id) {
                    setActiveSkill(locationState.exam_skill_id)
                }
                if (locationState.sa_skill_id) {
                    loadSelfAssessmentReport(locationState.sa_skill_id)
                    setActiveSelfAssessmentSkill(locationState.sa_skill_id)
                }
            }
        },
        [locationState, loadSelfAssessmentReport]
    )

    const loadReports = useCallback(() => {
        setLoadingReports(true);
        Axios.get('/members/get-assessment-report/')
            .then(res => {
                const parsed = {}
                res.data.forEach(r => parsed[r.skill_title] = r);
                setReports(parsed);
            })
            .catch(err => {
                setReports({});
            }).finally(() => setLoadingReports(false));
    }, []);

    useEffect(() => {
        if (activeSkill !== '' && emptyReports) loadReports(); // load all reports when needed
    }, [loadReports, activeSkill, emptyReports]);

    const handleExam = useCallback((skillId) => () => {
        if (window.confirm(t("Do you want to start the exam?"))) {
            setSkillId(skillId);
            setShowExam(true);
        }
    }, [t]);

    const handleExamDone = useCallback((done) => {
        setShowExam(false);
        setSkillId(0)
        if (done) {
            loadAllMySkills();
            loadReports()
        }
    }, [loadAllMySkills]);

    const openSkill = useCallback((skillId) => () => {
        setActiveSkill(prev => prev === skillId ? '' : skillId);
    }, []);

    const openSelfAssessmentSkill = useCallback((skillId) => () => {
        setActiveSelfAssessmentSkill(prev => {
            if (prev === skillId) {
                return null
            } else {
                loadSelfAssessmentReport(skillId)
                return skillId
            }
        }
        );
    }, [loadSelfAssessmentReport]);

    const handleSelfAssessment = useCallback(
        (selfAssessmentReportItem) => () => {
            setShareDialogOpen(true)
            setSelectedSelfAssessmentReportItem(selfAssessmentReportItem)
        },
        [],
    )


    const handleCancelShareDialog = useCallback(
        () => {
            setShareDialogOpen(false);
        },
        []
    );

    const handleConfirmShareDialog = useCallback(
        (id, emails) => {
            setShareLoading(true)
            saveShares(
                id,
                emails,
                null,
                null,
                () => {
                    setShareLoading(false)
                    setShareDialogOpen(false)
                }
            )
        },
        []
    );

    const handleStartSelfAssessment = useCallback(
        (skill) => (e) => {
            e.stopPropagation()
            setSelfAssessSkill(skill)
            setShowSelfAssessment(true)
        },
        []
    )

    const handleSelfAssessmentClose = useCallback(
        (e, reason) => {
            setShowSelfAssessment(false)
            if (reason === "done") {
                loadSelfAssessmentReport(activeSelfAssessmentSkill)
            }
        },
        [activeSelfAssessmentSkill]
    )

    return (
        <>
            <GridContainer>
                <GridItem xs={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h3 className={classes.cardTitleWhite}>
                                {t("Exams")}
                            </h3>
                            <h4 className={classes.cardCategoryWhite}>
                                {t("Testing your knowledge in your courses")}
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Box width="600px" m="auto" textAlign="center" py={2}>
                                <Collapse in={loading}>
                                    <Skeleton animation="wave" height={90} />
                                    <Skeleton animation="wave" height={90} />
                                    <Skeleton animation="wave" height={90} />
                                </Collapse>
                                {myCurrentSkills.length === 0 ?
                                    <i>{t("There are no assessible courses in your profile.")}</i>
                                    :
                                    myCurrentSkills.map((skill, ind) => (
                                        <Accordion key={ind} expanded={activeSkill === skill.skill.id} onChange={openSkill(skill.skill.id)}>
                                            <AccordionSummary
                                                classes={{
                                                    content: classes.examRow
                                                }}
                                            >
                                                <SkillLogo picture={skill.skill.picture} />
                                                <RoseText className={classes.headerSkill}>{skill.skill.title}</RoseText>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <Paper style={{ width: '100%' }}>
                                                    <Grid container spacing={2} alignItems="center">
                                                        <Grid item xs={12} md={8}>
                                                            <Box textAlign="center">
                                                                {t("Last 6 exams")}
                                                            </Box>
                                                            <AssessmentReports reports={reports[skill.skill.title]} loading={loadingReports}
                                                                loadNow={activeSkill === skill.skill.id} />
                                                        </Grid>
                                                        <Grid item xs={12} md={4}>
                                                            <Box p={3}>
                                                                <Button variant="contained" color="primary" onClick={handleExam(skill.skill.id)}>
                                                                    {t("Take an exam")}
                                                                </Button>
                                                            </Box>
                                                        </Grid>
                                                    </Grid>
                                                </Paper>
                                            </AccordionDetails>
                                        </Accordion>
                                    ))}
                            </Box>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>

            <GridContainer>
                <GridItem xs={12}>
                    <Card>
                        <CardHeader color="rose">
                            <h3 className={classes.cardTitleWhite}>
                                {t("Self-assessments")}
                            </h3>
                            <h4 className={classes.cardCategoryWhite}>
                                {t("Self-assess yourself in your target courses")}
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <Box width="600px" m="auto" textAlign="center" py={2}>
                                {myCurrentSelfAssessmentableSkills.length === 0 ?
                                    <i>{t("There are no self-assessible courses in your profile.")}</i>
                                    :
                                    <>
                                        {myCurrentSelfAssessmentableSkills.map((skill, ind) => (
                                            <Accordion key={ind} expanded={activeSelfAssessmentSkill && (activeSelfAssessmentSkill === skill.skill.id)} onChange={openSelfAssessmentSkill(skill.skill.id)}>
                                                <AccordionSummary
                                                    classes={{
                                                        content: classes.examRow
                                                    }}
                                                >
                                                    <SkillLogo picture={skill.skill.picture} />
                                                    <RoseText className={classes.headerSkill}>{skill.skill.title}</RoseText>
                                                </AccordionSummary>
                                                <AccordionDetails>
                                                    <div style={{ width: '100%' }}>
                                                        <Button
                                                            variant='contained'
                                                            color='secondary'
                                                            onClick={handleStartSelfAssessment(skill)}
                                                            style={{
                                                                display: 'inline-block',
                                                                width: '100%'
                                                            }}
                                                        >
                                                            {t("Take a new test")}
                                                        </Button>
                                                        <Typography
                                                            variant="h6"
                                                            style={{
                                                                display: 'inline-block',
                                                                marginTop: 28,
                                                                marginBottom: 8
                                                            }}
                                                        >
                                                            {t('Your previous self-tests')}
                                                        </Typography>
                                                        <div style={{ width: '100%' }}>
                                                            {loadingSelfAssessmentReports ?
                                                                <>
                                                                    <Skeleton variant="text" />
                                                                    <Skeleton variant="rect" />
                                                                </>
                                                                :
                                                                selfAssessmentReport.length === 0 ?
                                                                    <i>{t("There are no self-assessment in this course.")}</i>
                                                                    :
                                                                    selfAssessmentReport.map((selfAssessmentReportItem, index) => (
                                                                        <Paper
                                                                            key={index}
                                                                            style={{
                                                                                margin: 10
                                                                            }}
                                                                        >
                                                                            <Grid container item spacing={2} alignItems="center">
                                                                                <Grid item xs>
                                                                                    <Typography variant='caption'>
                                                                                        {parseDateTime(selfAssessmentReportItem.created_at)}
                                                                                    </Typography>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Button
                                                                                        variant="contained"
                                                                                        color="primary"
                                                                                        component={Link}
                                                                                        to={`/${language}/dashboard/self-assessment-reports/${selfAssessmentReportItem.id}`}
                                                                                        target="_blank"
                                                                                        style={{
                                                                                            margin: 4,
                                                                                            color: "#FFF"
                                                                                        }}
                                                                                    >
                                                                                        {t("Show result")}
                                                                                    </Button>
                                                                                    <Button
                                                                                        variant="contained"
                                                                                        color="primary"
                                                                                        onClick={handleSelfAssessment(selfAssessmentReportItem)}
                                                                                        style={{
                                                                                            margin: "4px 8px"
                                                                                        }}
                                                                                    >
                                                                                        {t("Share result")}
                                                                                    </Button>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Paper>
                                                                    ))
                                                            }
                                                            <ShareDialog
                                                                loading={shareLoading}
                                                                open={shareDialogOpen}
                                                                onCancel={handleCancelShareDialog}
                                                                onAccept={handleConfirmShareDialog}
                                                                emails={selectedSelfAssessmentReportItem ? selectedSelfAssessmentReportItem.mentors : []}
                                                                selfAssessmentId={selectedSelfAssessmentReportItem ? selectedSelfAssessmentReportItem.id : undefined}
                                                            />
                                                        </div>
                                                    </div>
                                                </AccordionDetails>
                                            </Accordion>
                                        ))
                                        }
                                    </>
                                }
                            </Box>
                        </CardBody>
                    </Card>
                </GridItem>
                <Backdrop className={classes.backdrop} open={showExam}>
                    <Test exam skillId={skillId} open={showExam} onClose={handleExamDone} />
                </Backdrop>
                <SelfAssessmentDialog
                    open={showSelfAssessment}
                    skillId={selfAssessSkill ? selfAssessSkill.skill.id : null}
                    onClose={handleSelfAssessmentClose}
                />
            </GridContainer>
        </>

    )
}

export default Assessment
