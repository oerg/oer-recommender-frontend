import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { CartesianGrid, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
import { Box, CircularProgress } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { useTranslation } from 'react-i18next';

function AssessmentReports({ loading, reports, loadNow }) {
    const { t } = useTranslation([ 'dashboard' ]);

    const r = useMemo(() => {
        if (reports) {
            return reports.last_6_scores.map(
                (score, ind) => ({ 
                    score: score ? Math.round(score * 10000)/100 : 0, 
                    title: t("Exam") + `${ind + 1}`
                }))
        }
        return [];
    }, [reports, t]);

    return (
        loading || !reports ?
        <Box p={3} textAlign="center">
            <CircularProgress />
        </Box>
    :
        (loadNow && 
            <Box height="150px" width="100%">
                <ResponsiveContainer width="90%">
                    <LineChart data={r}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="title" tick={false} />
                        <YAxis domain={[0, 100]} />
                        <Tooltip />
                        <Line dataKey="score" stroke={green[900]} />
                    </LineChart>
                </ResponsiveContainer>
            </Box>

        )
    )
}

AssessmentReports.propTypes = {
    loading: PropTypes.bool,
    reports: PropTypes.object,
    loadNow: PropTypes.bool,
}

AssessmentReports.defaultProps = {
    loadNow: true,
}

export default AssessmentReports
