import { Box, Grid, List, ListItem } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import FeedbackForm from '../../components/FeedbackForm';
import useStyles from '../styles/FooterStyles';
import EULogo from './images/euflag.jpg';
import EULogo2 from './images/euflag2.png';
import TibLogo from './images/TIB_Logo.png';
import UALogo from './images/UA_Logo.svg';
import BMBFLogo from './images/bmbf.jpg';
import ChangeLanguageButton from '../../components/ChangeLanguageButton';

function Footer() {
    const classes = useStyles();
    const { t } = useTranslation([ 'landing' ]);
    const language = useLanguage();

    const [showOpenFeedbackForm, setShowOpenFeedbackForm] = useState(false);

    const handleShowFeedbackForm = useCallback(
        e => {
            e.preventDefault();
            setShowOpenFeedbackForm(true);
        },
        []
    );

    const handleCloseFeedbackForm = useCallback(
        () => setShowOpenFeedbackForm(false),
        []
    );

    return (
        <footer className={classes.footer}>
            <div className={classes.container}>
                <Box textAlign="center">
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item>
                            <a href="https://www.uva.nl/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("University of Amsterdam")} height={60}
                                    title={t("University of Amsterdam")} src={UALogo} />
                            </a>
                        </Grid>

                        <Grid item>
                            <a href="https://www.tib.eu/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Technische Informationsbibliothek (TIB)")} height={60}
                                    title={t("Technische Informationsbibliothek (TIB)")} src={TibLogo} />
                            </a>
                        </Grid>

                        <Grid item>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Education, Audiovisual and Culture Executive Agency")} height={60}
                                    title={t("Education, Audiovisual and Culture Executive Agency")} src={EULogo2} />
                            </a>
                        </Grid>
                        <Grid item>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Education, Audiovisual and Culture Executive Agency")} height={60}
                                    title={t("Education, Audiovisual and Culture Executive Agency")} src={EULogo} />
                            </a>
                        </Grid>
                        <Grid item>
                            <a href="https://www.bmbf.de/bmbf/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Federal Ministry of Education and Research")} height={60}
                                title={t("Federal Ministry of Education and Research")} src={BMBFLogo} />
                            </a>
                        </Grid>
                    </Grid>
                    <List className={classes.list}>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/imprint/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Imprint")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://labs.tib.eu/edoer/tos" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Terms of Use")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/data-protection/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Data Protection")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <Link to={`/${language}/aboutus`} className={classes.block}>
                                {t("About the Project")}
                            </Link>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <Link to={`/${language}/events`} className={classes.block}>
                                {/* <Badge color="secondary" variant="dot"> */}
                                {t("Events")}
                                {/* </Badge> */}
                            </Link>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="mailto:edoer@admin.tib.eu" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Contact Us")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a onClick={handleShowFeedbackForm} href="#feedback" className={classes.block}>
                                {t("Report a Problem")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://tib.eu/umfragen/index.php/survey/index/sid/887411/newtest/Y/lang/en" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Send Feedback")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <ChangeLanguageButton style={{
                                height: 30,
                                width: 40,
                            }} />
                        </ListItem>
                    </List>
                    {/*<span className={classes.right}>*/}
                    {/*    {t("eDoer is licensed under")} <a rel="license noopener noreferrer" href="http://creativecommons.org/licenses/by-nc/4.0/" target="_blank">*/}
                    {/*        <img alt="Creative Commons License" style={{ borderWidth: 0 }} src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" />*/}
                    {/*    </a>*/}
                    {/*</span>*/}
                    <FeedbackForm
                        open={showOpenFeedbackForm}
                        onClose={handleCloseFeedbackForm}
                    />
                </Box>
            </div>
        </footer>
    );
}

export default Footer
