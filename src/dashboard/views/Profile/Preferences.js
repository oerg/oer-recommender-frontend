import { Accordion, AccordionDetails, AccordionSummary, Box, Grid, makeStyles, Paper, Slider } from '@material-ui/core';
import { purple, teal } from '@material-ui/core/colors';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import Axios from 'axios';
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import { UserContext } from '../../../landing/User';
import Button from '../../components/CustomButton/Button';
import Face from '../../components/Face';
import Snackbar from '../../components/Snackbar/Snackbar';
import DangerText from '../../components/Typography/Danger';
import MutedText from '../../components/Typography/Muted';
import PrimaryText from '../../components/Typography/Primary';
import SuccessText from '../../components/Typography/Success';
import { isMyFirstTime, saveVisit } from '../helpers';


const useStyles = makeStyles({
    heading: {
        flexBasis: '20%',
        flexShrink: 0,
        color: purple[900]
    },
    question: {
        flexBasis: '60%',
        flexShrink: 0,
        color: teal[900]
    }
});



// const tutorialSteps = [
//     [
//         {
//             element: "#questions",
//             intro: "Answering these questions will make content recommendations more tailored to your needs."
//         },
//         {
//             element: "#question-prefLength",
//             intro: "This is a question about a property of educational materials. This question asks you regarding your preference for the property.",
//         },
//         {
//             element: "#answers-prefLength",
//             intro: "These are the possible values for the target property of the question. We have also provided a sample educational resource for each value that helps you in selecting your preference."
//         }
//     ]
// ]

const pageCode = 'pSettings';

function Preferences() {
    const classes = useStyles();
    const user = useContext(UserContext);
    const userData = useMemo(() => user.data, [user]);
    const history = useHistory();
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const answerLabels = {
        prefLength: {
            ST: {
                text: t("Short"),
                url: "https://www.youtube.com/watch?v=pDYCMvfnydE"
            },
            MD: {
                text: t("Medium"),
                url: "https://www.youtube.com/watch?v=Kes8YRV73Io"
            },
            LN: {
                text: t("Long"),
                url: "https://www.youtube.com/watch?v=zFvoXxeoosI"
            },
        },
        prefDetail: {
            LO: {
                text: t("Low Detail"),
                url: "https://www.youtube.com/watch?v=Cpjj02Os2oQ"
            },
            ME: {
                text: t("Medium Detail"),
                url: "https://www.youtube.com/watch?v=-qDiqnEVaLk",
            },
            HI: {
                text: t("High Detail"),
                url: "https://www.youtube.com/watch?v=fkS3FkVAPWU"
            },
        },
        prefStrategy: {
            TH: {
                text: t('Theory Only'),
                url: "https://www.youtube.com/watch?v=HQqqNBZosn8",
            },
            EX: {
                text: t('Examples Only'),
                url: "https://www.youtube.com/watch?v=LrRh-V-hYEc",
            },
            TE: {
                text: t('Theory and Examples'),
                url: "https://www.youtube.com/watch?v=6SPDvPK38tw",
            },
        },
        prefMultitopic: {
            true: {
                text: t("Yes"),
                url: "https://python.swaroopch.com/data_structures.html",
            },
            false: {
                text: t("No"),
                url: "http://www.openbookproject.net/thinkcs/python/english3e/tuples.html",
            },
        },
        prefClass: {
            true: {
                text: t("Yes"),
                url: "https://www.youtube.com/watch?v=4b4MUYve_U8",
            },
            false: {
                text: t("No"),
                url: "https://www.youtube.com/watch?v=zPG4NjIkCjc",
            },
        },
    }
    
    const questions = [
        "prefLength",
        "prefDetail",
        "prefStrategy",
        "prefMultitopic",
        "prefClass",
        "prefFormats",
    ]

    const getAnswerLabel = (question, answer) => {
        if (!(question in answerLabels))
            return ''
        if (answer in answerLabels[question])
            return <SuccessText>{answerLabels[question][answer].text}</SuccessText>
        return <DangerText>{t("No Answer")}</DangerText>
    }
    
    const getAnswerValue = (answer) => {
        if (answer === 'true')
            return true;
        if (answer === 'false')
            return false;
        return answer;
    }

    const [preferences, setPreferences] = useState(() => ({ // load profile for the first time
        "prefLength": userData.pref_length,
        "prefStrategy": userData.pref_strategy,
        "prefClass": userData.pref_class,
        "prefMultitopic": userData.pref_multitopic,
        "prefDetail": userData.pref_detail,
        "prefFormats": userData.format_types.length === 0 ? {
            "VI": 0,
            "BC": 0,
            "WE": 0,
            "SL": 0,
            // eslint-disable-next-line
        } : userData.format_types.reduce((acc, item) => ((acc[item.title] = item.pref, acc)), {})
    }));

    const [questionOpen, setQuestionOpen] = useState(questions[0]);
    const [errorMessage, setErrorMessage] = useState('');
    const [isSaving, setIsSaving] = useState(false);
    const [isChanged, setIsChanged] = useState(false);

    const isFirstTime = isMyFirstTime(pageCode, userData);

    useEffect(() => { // reload uer if changed.
        return () => {
            isChanged && user.reload();
        }
    }, [isChanged, user])

    const selectAnswer = useCallback((question, answer, qIndex) => () => {
        setPreferences(prev => ({
            ...prev,
            [question]: answer,
        }));
        setQuestionOpen(questions.length === qIndex + 1 ? null: questions[qIndex + 1])
    }, []);

    const toggleOpen = useCallback((question) => () => {
        setQuestionOpen(prev => prev === question ? null : question)
    }, []);

    const updateFormat = useCallback((format) => (e, newValue) => {
        setPreferences(prev => {
            const newPref = {...prev}
            newPref.prefFormats[format] = newValue;
            return newPref;
        })
    }, []);

    const handleSample = useCallback((url) => () => {
        window.open(url, '_blank', {}, false);
    }, []);

    const QuestionView = ({ title, question, name, children}) => (
        <Accordion expanded={questionOpen === name} onChange={toggleOpen(name)}>
            <AccordionSummary>
                <PrimaryText className={classes.heading}>
                    {title}
                </PrimaryText>
                <MutedText className={classes.question} id={`question-${name}`}>
                    {question}
                </MutedText>
                {getAnswerLabel(name, preferences[name])}
            </AccordionSummary>
            <AccordionDetails>
                {children}
            </AccordionDetails>
        </Accordion>
    );

    const answersView = useMemo(() => {
        const answers = {}
        let i = 0;
        for (const q in answerLabels) {
            answers[q] = []
            for (const key in answerLabels[q]) {
                answers[q].push(
                    <Grid item xs={12} sm={6} md={3} key={key}>
                        <Paper>
                            <Box textAlign="center" p={2}>
                                <Button color="success" onClick={selectAnswer(q, getAnswerValue(key), i)} disabled={isSaving}>
                                    {answerLabels[q][key].text}
                                </Button>
                                <Button onClick={handleSample(answerLabels[q][key].url)} startIcon={<PlayCircleOutlineIcon />}>{t("Watch a Sample")}</Button>
                            </Box>
                        </Paper>
                    </Grid>
                )
            }
            i = i + 1;
        }
        return answers;
    }, [selectAnswer, isSaving, handleSample, t]);

    const saveData = () => {
        setIsSaving(true);
        Axios.put('/members/set-preference/', preferences).then(
            res => {
                setErrorMessage(t('Data Saved.'));
                setIsChanged(true);
                if (isFirstTime) {
                    history.push(`/${language}/dashboard/edu?reg=1`);
                }
            }
        ).catch(
            err => {
                setErrorMessage(err.message);
            }
        ).finally(() => setIsSaving(false))
    }

    useEffect(() => {
        return () => {
            if (isFirstTime) {
                saveVisit(pageCode, user.setData);
            }
        }
    }, [isFirstTime, user.setData])

    return (
        <Grid container spacing={3} id="questions">
            <Grid item xs={12}>
                <QuestionView title={t("Educational Content Length")} question={t("Which length do you prefer? Long, medium or short?")}
                    name="prefLength">
                    <Grid container spacing={3} justify="center" id="answers-prefLength">
                        {answersView['prefLength']}
                    </Grid>
                </QuestionView>
                <QuestionView title={t("Level of Detail")} question={t("How much detail (e.g. scientific, programming, ...) do you prefer?")}
                    name="prefDetail">
                    <Grid container spacing={3} justify="center">
                        {answersView['prefDetail']}
                    </Grid>
                </QuestionView>
                <QuestionView title={t("Content Type")} question={t("Which type of content do you prefer?")}
                    name="prefStrategy">
                    <Grid container spacing={3} justify="center">
                        {answersView['prefStrategy']}
                    </Grid>
                </QuestionView>
                <QuestionView title={t("Multi-Topic Content")} question={t("Do you prefer contents that cover more than one topic?")}
                    name="prefMultitopic">
                    <Grid container spacing={3} justify="center">
                        {answersView['prefMultitopic']}
                    </Grid>
                </QuestionView>
                <QuestionView title={t("Class-based Content")} question={t("Do you prefer content given in a classroom?")}
                    name="prefClass">
                    <Grid container spacing={3} justify="center">
                        {answersView['prefClass']}
                    </Grid>
                </QuestionView>
                <QuestionView title={t("Content Format")} question={t("Please rate the degree to which you like each of the following content delivery formats from 1: Dislike very much to 5: Like very much.")}
                    name="prefFormats">
                    <Grid container spacing={3} justify="center">
                        <Grid item xs={12} sm={6}>
                            <Paper>
                                <Box p={2}>
                                    <MutedText>{t("Videos")} <Face score={preferences["prefFormats"]["VI"]} /></MutedText>
                                    <Slider min={1} max={5} step={1} valueLabelDisplay="auto" marks disabled={isSaving}
                                     value={preferences["prefFormats"]["VI"]} onChange={updateFormat("VI")} />
                                     <Box textAlign="center" p={1}>
                                        <Button onClick={handleSample('https://www.youtube.com/watch?v=b0L47BeklTE')} startIcon={<PlayCircleOutlineIcon />}> {t("Watch a Sample")}</Button>
                                     </Box>
                                </Box>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper>
                                <Box p={2}>
                                    <MutedText>{t("Book Chapters")} <Face score={preferences["prefFormats"]["BC"]} /></MutedText>
                                    <Slider min={1} max={5} step={1} valueLabelDisplay="auto" marks disabled={isSaving}
                                     value={preferences["prefFormats"]["BC"]} onChange={updateFormat("BC")} />
                                    <Box textAlign="center" p={1}>
                                        <Button onClick={handleSample('https://automatetheboringstuff.com/2e/chapter5/')} startIcon={<PlayCircleOutlineIcon />}> {t("Watch a Sample")}</Button>
                                    </Box>
                                </Box>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper>
                                <Box p={2}>
                                    <MutedText>{t("Webpages")} <Face score={preferences["prefFormats"]["WE"]} /></MutedText>
                                    <Slider min={1} max={5} step={1} valueLabelDisplay="auto" marks disabled={isSaving}
                                     value={preferences["prefFormats"]["WE"]} onChange={updateFormat("WE")} />
                                    <Box textAlign="center" p={1}>
                                        <Button onClick={handleSample('https://scikit-learn.org/stable/auto_examples/linear_model/plot_ols.html')} startIcon={<PlayCircleOutlineIcon />}> {t("Watch a Sample")}</Button>
                                    </Box>
                                </Box>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper>
                                <Box p={2}>
                                    <MutedText>{t("Slides")} <Face score={preferences["prefFormats"]["SL"]} /></MutedText>
                                    <Slider min={1} max={5} step={1} valueLabelDisplay="auto" marks disabled={isSaving}
                                     value={preferences["prefFormats"]["SL"]} onChange={updateFormat("SL")} />
                                    <Box textAlign="center" p={1}>
                                        <Button onClick={handleSample('https://ocw.mit.edu/courses/sloan-school-of-management/15-071-the-analytics-edge-spring-2017/lecture-and-recitation-notes/MIT15_071S17_Unit2_WineRegression.pdf')} startIcon={<PlayCircleOutlineIcon />}> {t("Watch a Sample")}</Button>
                                    </Box>
                                </Box>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Paper>
                                <Box p={2}>
                                    <MutedText>{t("Papers")} <Face score={preferences["prefFormats"]["PA"]} /></MutedText>
                                    <Slider min={1} max={5} step={1} valueLabelDisplay="auto" marks disabled={isSaving}
                                     value={preferences["prefFormats"]["PA"]} onChange={updateFormat("PA")} />
                                    <Box textAlign="center" p={1}>
                                        <Button onClick={handleSample('https://www.sciencedirect.com/science/article/pii/S1474034621002573')} startIcon={<PlayCircleOutlineIcon />}> {t("Watch a Sample")}</Button>
                                    </Box>
                                </Box>
                            </Paper>
                        </Grid>                        
                    </Grid>
                </QuestionView>
                <Grid item xs={12}>
                    <Box textAlign="right" m={3}>
                        <Button color="rose" onClick={saveData} disabled={isSaving}>
                            {t("Save")}
                        </Button>
                    </Box>
                </Grid>
            </Grid>
            <Snackbar open={errorMessage !== ''} message={errorMessage} color="rose"
                closeNotification={() => setErrorMessage('')} close />
        </Grid>
    )
}

export default Preferences
