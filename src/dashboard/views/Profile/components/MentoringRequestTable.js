import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DataGrid } from '@mui/x-data-grid';
import { renderGeneralCell } from '../utils';
import { Button } from '@material-ui/core';

function alwaysFalse() {
    return false;
}


function MentoringRequestTable( {requests, loading, onDeleteRequest, criticalError} ) {
    const { t } = useTranslation(['dashboard']);
    const [pageSize, setPageSize] = useState(10)

    const Mailto = ({ email, subject, body, children }) => {
        return (
          <a href={`mailto:${email}?subject=${encodeURIComponent(subject) || ''}&body=${encodeURIComponent(body) || ''}`}>{children}</a>
        );
      };

    const renderContactCell = (params) => {
        const { t } = useTranslation(['dashboard']);
        if (params.row.mentor === null) {
            return <span>{t("Waiting for mentors")}</span>
        }
        const mentor = params.row.mentor.name.split(" ")[0]
        const firstName = mentor.charAt(0).toUpperCase() + mentor.slice(1)
        return (
            <span title={`${t("send mail to:")} ${params.row.mentor.contact}`} style={{textOverflow: "ellipsis", overflow: "hidden"}}>
                <Mailto
                    email={params.row.mentor.contact}
                    subject={t("[eDoer Mentoring] Request")}
                    body={t("Hello") + " " + firstName + ","}
                >
                    {params.row.mentor.contact}
                </Mailto>
            </span>            
        )
    }

    const renderDeleteButton = (params) => {
        const { t } = useTranslation(['dashboard']);
        return (
            <strong>
                <Button
                    variant="contained"
                    color="info"
                    size="sm"
                    style={{ marginLeft: 16 }}
                    onClick={() => {
                        onDeleteRequest(params.row.id)
                    }}
                >
                    {t("Close Request")}
                </Button>  
            </strong>
        )
    }


    const renderMentorCell = (params) => {
        const { t } = useTranslation(['dashboard']);
        if (params.row.mentor === null) {
            return <span>{t("Waiting for mentors")}</span>
        }
        const mentor = params.row.mentor.name
        return (
            <span style={{textOverflow: "ellipsis", overflow: "hidden"}}>
                {mentor}
            </span>            
        )
    }

    const columns = useMemo(
        () => [
            {
                field: "request_title",
                headerName: t("On"),
                width: 150,
                renderCell: renderGeneralCell,
                sortable: false
            },
            {
                field: "request_updated_at",
                headerName: t("Accepting Date"),
                sortable: true,
                width: 150,
                renderCell: renderGeneralCell,
            },
            {
                field: "request_text",
                headerName: t("Request Text"),
                sortable: false,
                width: 250,
                renderCell: renderGeneralCell,
            },
            {
                field: "mentor",
                headerName: t("Mentor"),
                sortable: false,
                width: 150,
                renderCell: renderMentorCell,
            },
            {
                field: "mentor_contact",
                headerName: t("Contact"),
                sortable: false,
                width: 250,
                renderCell: renderContactCell,
            },
            {
                field: "delete_request",
                headerName: t("Delete"),
                sortable: false,
                width: 200,
                renderCell: renderDeleteButton,
            }                    
        ],
        [t]
    );

    const handlePageSizeChange = useCallback(
        (pageSize) => {
            setPageSize(pageSize)
        },
        [],
    )

    return (
        <DataGrid
            style={{
                width: "100%",
                minHeight: 400,
            }}
            columns={columns}
            rows={requests}
            isCellEditable={alwaysFalse}
            loading={loading}
            onPageSizeChange={handlePageSizeChange}
            pageSize={pageSize}
            rowsPerPageOptions={[10, 25, 50]} 
        />
    )
}

export default MentoringRequestTable

