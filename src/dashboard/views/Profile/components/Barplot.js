import React, { useMemo } from 'react';
import ReactApexChart from 'react-apexcharts';
import { useTranslation } from 'react-i18next';

function Barplot({ itemTitles, points, stats, title, height }) {
    const { t } = useTranslation(['dashboard']);

    const chartInfo = useMemo(
        () => ({
            series: [{
                name: t("By this User"),
                data: points
            }, {
                name: t("Median points on eDoer"),
                data: stats
            }],
            options: {
                chart: {
                    type: 'bar',
                    toolbar: {
                        show: false
                    },
                    height: 430
                },
                plotOptions: {
                    bar: {
                        horizontal: true,
                        dataLabels: {
                            position: 'top',
                        },
                    }
                },
                colors: ['#d5f5e3', '#fadbd8'],
                dataLabels: {
                    enabled: true,
                    offsetX: -6,
                    style: {
                        fontSize: '13px',
                        colors: ['#000']
                    }
                },
                stroke: {
                    show: true,
                    width: 1,
                    colors: ['#EEE']
                },
                tooltip: {
                    shared: true,
                    intersect: false
                },
                xaxis: {
                    categories: itemTitles,
                    min: 0
                },
                yaxis: {
                    labels: {
                        align: 'left',
                        style: {
                            fontSize: '12px',
                            fontWeight: 'bold',
                            color: '#263238'
                        },
                    }
                },
                title: {
                    text: title,
                    align: 'center'
                },
                legend: {
                    position: 'bottom',
                    horizontalAlign: 'right',
                    fontWeight: 800,
                },
                noData: {
                    text: t("No achieved points"),
                    align: 'center',
                    verticalAlign: 'middle',
                    offsetX: 0,
                    offsetY: 0
                }                
            },
          }),
          [ itemTitles, points, stats, title, t ]
    )

    return (
        <ReactApexChart options={chartInfo.options} series={chartInfo.series} type="bar" height={height} />
    )

}

export default Barplot
