import { Avatar, Button, CircularProgress, Collapse, Divider, Grid, IconButton, List, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import Pagination from '@material-ui/lab/Pagination';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Help from '../../components/Help';
import ToggleDiv from '../../components/ToggleDiv';
import Barplot from './components/Barplot';
import StatListItem from './components/StatListItem';
import { getPublicProfile, getPublicReports } from './utils';
import CloseIcon from '@material-ui/icons/Close';
import { withSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
    },
    typo: {
        padding: theme.spacing(1)
    },
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        padding: theme.spacing(1),
    },
    // paper: {
    //     marginTop: theme.spacing(3)
    // },
    pagination: {
        margin: theme.spacing(2),
        justifyContent: 'center'
    },
    ulPagination: {
        justifyContent: 'center'
    },
    loadingWrapper: {
        textAlign: "center",
    }

}));

const NUMBER_OF_ITEM_IN_EACH_PAGE = 5.0

function PublicProflle({ userData: propUserData, showShare=true, enqueueSnackbar }) {
    const classes = useStyles();
    const [skillCont, setSkillCont] = useState()
    const [topicCont, setTopicCont] = useState()
    const [loadingSkillCont, setLoadingSkillCont] = useState(false)
    const [loadingTopicCont, setLoadingTopicCont] = useState(false)
    const [skillContPage, setSkillContPage] = useState(1)
    const [topicContPage, setTopicContPage] = useState(1)
    const [loading, setLoading] = useState(false);
    const [topSkill, setTopSkill] = useState([])
    const [topTopic, setTopTopic] = useState([]);
    const [textCopied, setTextCopied] = useState(false);
    const { t } = useTranslation(['dashboard']);
    const [userData, setUserData] = useState();
    const [showPublicLink, setShowPublicLink] = useState(true);
    const linkDom = useRef(null);
    const language = useLanguage();
    const urlParams = useParams();

    const loadSkillCont = useCallback(
        (page, id) => {
            setLoadingSkillCont(true);
            getPublicReports(
                "skill-contributions",
                id,
                page,
                res => {
                    setSkillCont(res)
                    if (page === 1) {
                        setTopSkill(res.results)
                    }
                },
                null,
                () => setLoadingSkillCont(false)
            );
        },
        [],
    )

    const loadTopicCont = useCallback(
        (page, id) => {
            setLoadingTopicCont(true);
            getPublicReports(
                "topic-contributions",
                id,
                page,
                res => {
                    setTopicCont(res)
                    if (page === 1) {
                        setTopTopic(res.results)
                    }
                },
                null,
                () => setLoadingTopicCont(false)
            );
        },
        [],
    )

    const topSkillReport = useMemo(() => {
        return {
            skills: topSkill.map(o => o.skill_title),
            points: topSkill.map(o => o.points),
            stats: topSkill.map(o => o.points_stats[2])
        }
    }, [topSkill])

    const topTopicReport = useMemo(() => {
        return {
            topics: topTopic.map(o => o.topic_title),
            points: topTopic.map(o => o.points),
            stats: topTopic.map(o => o.points_stats[2])
        }
    }, [topTopic])

    useEffect(() => {
        setLoading(true);
        if (urlParams.id) {
            getPublicProfile(
                urlParams.id,
                res => {
                    setUserData(res);
                    loadSkillCont(skillContPage, res.user.id);
                    loadTopicCont(topicContPage, res.user.id);
                },
                () => {
                    enqueueSnackbar(
                        t("User not found!"),
                        {
                            variant: "error",
                            persist: true
                        }
                    );
                },
                () => setLoading(false)
            )
        } else {
            setUserData(propUserData);
            setLoading(false);
            loadSkillCont(skillContPage, propUserData.user.id);
            loadTopicCont(topicContPage, propUserData.user.id);
        }
    }, [loadTopicCont, loadSkillCont, topicContPage, skillContPage, propUserData, urlParams, t, enqueueSnackbar])

    const publicLink = useMemo(
        () =>  `${process.env.PUBLIC_URL}/${language}/users/${userData? userData.user.id : ''}`,
        [ userData, language ]
    );

    const handleSkillContPage = useCallback((event, value) => {
        setSkillContPage(value);
    }, []);

    const handleTopicContPage = useCallback((event, value) => {
        setTopicContPage(value);
    }, []);

    const handleCopy = useCallback(
        () => {
            navigator.clipboard.writeText(
                linkDom.current ?
                    linkDom.current.href
                    :
                    publicLink
            );
            setTextCopied(true);
            setTimeout(
                () => setTextCopied(false),
                2000
            )
        },
        [ publicLink ]
    );

    const handleHidePublic = useCallback(
        () => setShowPublicLink(false),
        []
    );

    return (loading ?
        <div style={{ padding: 16, textAlign: "center"}}>
            <CircularProgress />
        </div>
        :
        !userData ?
            null
        :
        <Grid container className={classes.wrapper} spacing={1}>
            { showShare &&
                <Grid item xs={12} style={{ marginBottom: 8 }}>
                    <Collapse in={showPublicLink}>
                        <Alert 
                            severity='info'
                            action={
                                <IconButton onClick={handleHidePublic} size="small">
                                    <CloseIcon />
                                </IconButton>
                            }
                        >
                            {t("This is how others will see your profile: ")}
                            <span id="public_link">
                                <em><a ref={linkDom} href={publicLink}>{t("My Public Profile")}</a></em>
                            </span> (
                            <Button
                                    onClick={handleCopy}
                                    disabled={textCopied}
                                    title={t("Copy public link to your clipboard.")}
                                >
                                    { textCopied ? t("Copied!") : t("Copy Link") }
                            </Button> )
                        </Alert>
                    </Collapse>
                </Grid>
            }
            <Grid item container xs={12}>
                <Grid item>
                    <Avatar style={{ width: 55, height: 55 }} src={userData.profile_picture} />
                </Grid>
                <Grid item xs style={{ height: 58 }}>
                    <Typography variant='h4' className={classes.typo}>
                        <strong>{userData["user"]["first_name"]} {userData["user"]["last_name"]}</strong>
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                {
                    userData["city"] && userData['country'] &&
                    <Typography component="div" className={classes.typo}>
                        {userData["city"]}, {userData["country"]}
                    </Typography>
                }
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Paper elevation={2} className={classes.paper}>
                    <ToggleDiv
                        defaultOpen
                        title={t("Top Contributions")}
                        help={
                            <Help>
                                {t("Here you can check top contributions (on courses and topics) from this user.")}
                            </Help>
                        }
                    >
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6}>
                                { 
                                    <Barplot
                                        title={t("Top Courses")}
                                        itemTitles={topSkillReport.skills}
                                        points={topSkillReport.points}
                                        stats={topSkillReport.stats}
                                        height= {topSkillReport.skills.length > 0 ? ((topSkillReport.skills.length * 100) + 75) : 300}
                                    />
                                }
                            </Grid>
                            <Grid item xs={12} md={6}>
                                {
                                    <Barplot
                                        title={t("Top Topics")}
                                        itemTitles={topTopicReport.topics}
                                        points={topTopicReport.points}
                                        stats={topTopicReport.stats}
                                        height= {topTopicReport.topics.length > 0 ? ((topTopicReport.topics.length * 100) + 75) : 300}
                                    />
                                }
                            </Grid>
                        </Grid>
                    </ToggleDiv>
                </Paper>
            </Grid>
            <Grid item xs={6}>
                <Paper elevation={2} className={classes.paper}>
                    <ToggleDiv
                        defaultOpen
                        title={t("Contributions on Courses")}
                        help={
                            <Help>
                                {t("Here you can check all course contributions from this user.")}
                            </Help>
                        }
                    >
                        {
                            loadingSkillCont ?
                                <div className={classes.loadingWrapper}>
                                    <CircularProgress />
                                </div>
                                :
                                skillCont &&
                                (skillCont["results"].length === 0 ?
                                    <div style={{ margin: 12, textAlign: "center" }}>
                                        <em>{t("No result.")}</em>
                                    </div>
                                    :
                                    <div>
                                        <List>
                                            {skillCont["results"].map(
                                                (item, ind) => (<React.Fragment key={ind}>
                                                    <StatListItem
                                                        title={item.skill_title}
                                                        points={item.points}
                                                        pointStats={item.points_stats}
                                                        betterThan={item.better_than}
                                                        link={`/${language}/dashboard/skills/${item.skill_id}`}
                                                    />
                                                    <Divider component="li" />
                                                </React.Fragment>)
                                            )}
                                        </List>
                                        <div className={classes.pagination}>
                                            <Pagination count={Math.ceil(skillCont.count / NUMBER_OF_ITEM_IN_EACH_PAGE)} variant="outlined" shape="rounded" page={skillContPage} onChange={handleSkillContPage}
                                                classes={{ ul: classes.ulPagination }} />
                                        </div>
                                    </div>
                                )}
                    </ToggleDiv>
                </Paper>
            </Grid>

            <Grid item xs={6}>
                <Paper elevation={2} className={classes.paper}>
                    <ToggleDiv
                        defaultOpen
                        title={t("Contributions on Topics")}
                        help={
                            <Help>
                                {t("Here you can check all topic contributions from this user.")}
                            </Help>
                        }
                    >
                        {
                            loadingTopicCont ?
                                <div className={classes.loadingWrapper}>
                                    <CircularProgress />
                                </div>
                                :
                                topicCont &&
                                (topicCont["results"].length === 0 ?
                                    <div style={{ margin: 12, textAlign: "center" }}>
                                        <em>{t("No result.")}</em>
                                    </div>
                                    :
                                    <div>
                                        <List>
                                            {topicCont["results"].map(
                                                (item, ind) => (<React.Fragment key={ind}>
                                                    <StatListItem
                                                        key={ind}
                                                        title={item.topic_title}
                                                        points={item.points}
                                                        pointStats={item.points_stats}
                                                        betterThan={item.better_than}
                                                        link={`/${language}/dashboard/topics/${item.topic_id}`}
                                                    />
                                                    <Divider component="li" />
                                                </React.Fragment>)
                                            )}
                                        </List>
                                        <div className={classes.pagination}>
                                            <Pagination count={Math.ceil(topicCont.count / NUMBER_OF_ITEM_IN_EACH_PAGE)} variant="outlined" shape="rounded" page={topicContPage} onChange={handleTopicContPage}
                                                classes={{ ul: classes.ulPagination }} />
                                        </div>
                                    </div>
                                )}
                    </ToggleDiv>
                </Paper>
            </Grid>
        </Grid>
    )
}


export default withSnackbar(PublicProflle)