import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SettingsBrightnessIcon from '@material-ui/icons/SettingsBrightness';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import CustomTabs from '../../components/CustomTabs/CustomTabs';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import MentoringRequests from './MentoringRequests';
import Preferences from './Preferences';
import ProfileSettings from './ProfileSettings';
import PublicProflle from './PublicProflle';
// import PublicProflle from './PublicProflle';

const routes = {
    profile: 0,
    preferences: 1,
    public: 2,
    requests: 3
}

Object.keys(routes).forEach(key => routes[routes[key]] = key);

function Profile({ user, userData }) {
    const match = useRouteMatch();
    const history = useHistory();
    const [tabIndex, setTabIndex] = useState(0);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    useEffect(() => {
        if ("section" in match.params && match.params.section in routes)
            setTabIndex(routes[match.params.section])
        else
            setTabIndex(0)
    }, [match.params])

    const handleTabChange = (e, newTab) => {
        history.push(`/${language}/dashboard/profile/` + routes[newTab]);
        setTabIndex(newTab);
    }

    return (
        <GridContainer>
            <GridItem xs={12}>
                <CustomTabs title={t("Profile")} headerColor="rose" tabIndex={tabIndex} 
                 onTabChange={handleTabChange} tabs={[
                    {
                        tabName: t("Profile"),
                        tabContent: <ProfileSettings />,
                        tabIcon: AccountBoxIcon,
                    },
                    {
                        tabName: t("learning preferences"),
                        tabContent: <Preferences />,
                        tabIcon: SettingsBrightnessIcon,
                    },
                    {
                        tabName: t("Public Profile"),
                        tabContent: <PublicProflle userData={userData}/>,
                        tabIcon: AccountBoxIcon,
                    },
                    {
                        tabName: t("Mentoring Requests"),
                        tabContent: <MentoringRequests userData={userData}/>,
                        tabIcon: AccountBoxIcon,
                    },                                        
                ]}>

                </CustomTabs>
            </GridItem>
        </GridContainer>
    )
}

export default Profile
