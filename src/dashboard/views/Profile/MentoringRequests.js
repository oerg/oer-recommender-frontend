import { Grid, Paper, Typography } from '@material-ui/core';
import { withSnackbar } from 'notistack';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import useStyles from '../styles/SkillsStyle';
import MentoringRequestTable from './components/MentoringRequestTable';
import { deleteRequest, getMentoringRequests } from './utils';

function MentoringRequests({ enqueueSnackbar }) {
    const classes = useStyles();

    const { t } = useTranslation(['dashboard']);

    const [loading, setLoading] = useState(false);
    const [requests, setRequests] = useState([])
    const [criticalErrorDelete, setCriticalErrorDelete] = useState(null)


    const loadRequests = useCallback(
        () => {
            setLoading(true);
            getMentoringRequests((data) => {
                setRequests(data);
                setLoading(false);
            });
        },
        [],
    )

    const handleDeleteRequest = useCallback(
        (requestId) => {
            if (window.confirm(t("Do you want to delete this mentoring request?"))) {
                setLoading(true);
                deleteRequest(requestId, (data) => { // load
                    loadRequests()
                    setLoading(false);
                    enqueueSnackbar(
                        t("The mentoring request has been successfully deleted."),
                        {
                            variant: "success"
                        }
                    )
                },
                    error => { // on error
                        if (error) {
                            setLoading(false)
                            enqueueSnackbar(
                                t("You cannot delete this request."),
                                {
                                    variant: "warning"
                                }
                            )
                            if (error.status === 404) {
                                setCriticalErrorDelete(t("404: Request was not found!"));
                            } else if (error.status >= 500) {
                                setCriticalErrorDelete(error.status + ": Server error!")
                            } else
                                setCriticalErrorDelete(error.data);
                        }
                    })
            }
        },
        [deleteRequest],
    )

    useEffect(() => {
        loadRequests()
    }, [loadRequests]);


    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose" className={classes.titleWithButton}>
                        <div className={classes.cardTitleDiv}>
                            <h3 className={classes.cardTitleWhite}>
                                {t("Mentoring Requests")}
                            </h3>
                            <h4 className={classes.cardCategoryWhite}>
                                {t("Watch your mentoring requests")}
                            </h4>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Paper>
                            <Grid item xs={12} className={classes.col} align="center">
                                <Typography variant="h5" component="h2" style={{ margin: "20px" }}>
                                    <i> {t("Mentoring Requests")} </i>
                                </Typography>
                            </Grid>
                            <MentoringRequestTable
                                requests={requests}
                                loading={loading}
                                onDeleteRequest={handleDeleteRequest}
                                criticalError={criticalErrorDelete}
                            />
                        </Paper>
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    )
}

export default withSnackbar(MentoringRequests)
