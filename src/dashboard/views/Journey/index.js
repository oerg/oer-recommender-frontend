import { Button, Collapse, Grid, Paper, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Alert, Skeleton } from '@material-ui/lab';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import GoalCard from '../../components/JobItem/GoalCard';
import SkillConfirmDialog from '../Education/components/SkillConfirmDialog';
import { loadMyGoals } from '../Education/utils';
import { saveGoalJob, updateSkillsBatch } from '../Goals/utils';
import useSearch from '../Search/useSearch';
import useStyles from '../styles/HistoryStyles';


function Journey() {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard']);
    const [loadingGoals, setLoadingGoals] = useState(false)
    const [goals, setGoals] = useState([])
    const [skillsDialogOpen, setSkillsDialogOpen] = useState(false)
    const [skillsToUpdate, setSkillsToUpdate] = useState([])
    const [openSearch] = useSearch()

    const loadAllMyGoals = useCallback(
        () => {
            setLoadingGoals(true)
            loadMyGoals(
                myGoals => {
                    setGoals(myGoals)
                },
                null,
                () => setLoadingGoals(false)
            )
        },
        []
    )

    useEffect(
        () => {
            loadAllMyGoals()
        },
        [loadAllMyGoals]
    )

    const handleCancelSkillDialog = useCallback(
        () => {
            setSkillsDialogOpen(false);
            setSkillsToUpdate([]);
        },
        []
    );

    const handleConfirmSkillDialog = useCallback(
        (skills, is_add, goal) => {
            if (is_add && goal) {
                saveGoalJob(
                    goal,
                    true   
                )
            }
            updateSkillsBatch(
                skills,
                is_add,
                () => {},
                null,
                () => {
                    setSkillsDialogOpen(false);
                    setSkillsToUpdate([]);
                }
            )
        },
        []
    );

    const handleRemoveJourney = useCallback(
        (skillsToRemove, goal) => {
            loadAllMyGoals()
            if (skillsToRemove.length > 0) {
                setSkillsToUpdate(skillsToRemove);
                setSkillsDialogOpen(true);
            }
        },
        [loadAllMyGoals]
    );

    const openJournetSearch = useCallback(
        () => {
            openSearch(
                null,
                {
                    journeyOnly: true,
                }
            )
        },
        [openSearch]
    )

    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("My Journeys")}
                        </h3>
                        <h4 className={classes.cardCategoryWhite}>
                            {t("Managing my own list of journeys")}
                        </h4>
                    </CardHeader>
                    <CardBody>
                        <Paper style={{
                            marginTop: 24,
                            padding: 16,
                        }}>
                            <div
                                style={{
                                    display: "flex",
                                    alignItems: "flex-start"
                                }}
                            >
                                <Typography
                                    variant='h5'
                                    style={{
                                        padding: 16,
                                        flex: "1",
                                    }}
                                >
                                    {t("Journey list")}
                                </Typography>
                                <Button
                                    variant='outlined'
                                    color="primary"
                                    onClick={openJournetSearch}
                                >
                                    {t("Follow a new Journey")}
                                </Button>
                            </div>
                            <Collapse in={loadingGoals}>
                                <Skeleton animation="wave" height={90} />
                                <Skeleton animation="wave" height={90} />
                            </Collapse>
                            <Collapse in={!loadingGoals}>
                                {goals.length > 0 ?
                                    <Grid container>
                                        {goals.map((goal, index) => (
                                            <Grid item xs={12} md={4} key={index}>
                                                <GoalCard
                                                    goal={goal}
                                                    onRemoveJourney={handleRemoveJourney}
                                                />
                                            </Grid>
                                        ))
                                        }
                                    </Grid>
                                    :
                                    <Alert
                                        severity='info'
                                        action={<Button
                                            startIcon={<AddIcon />}
                                            title={t("Follow a new journey")}
                                            onClick={openJournetSearch}
                                        >
                                            {t("Follow")}
                                        </Button>}
                                        style={{
                                            margin: "auto",
                                            width: 400
                                        }}
                                    >
                                        {t("No journeies followed.")}
                                    </Alert>
                                }
                            </Collapse>
                        </Paper>
                    </CardBody>
                </Card>
            </GridItem>
            <SkillConfirmDialog
                open={skillsDialogOpen}
                onCancel={handleCancelSkillDialog}
                onAccept={handleConfirmSkillDialog}
                isAdd={false}
                skills={skillsToUpdate}
                showFollow={true}
            />
        </GridContainer>
    )
}

export default Journey
