import { Box, Button, CircularProgress, Divider, FormControl, FormControlLabel, Grid, IconButton, makeStyles, Paper, Radio, RadioGroup, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import HtmlDiv from '../../components/HtmlDiv';
import SnackbarContent from '../../components/Snackbar/SnackbarContent';
import useStyles from '../styles/TestStyle';
import { checkQuestions, loadQuestions, shuffle } from './utils';

function Question({ text, choices, answer, disabled, onAnswerUpdate, number, identifier }) {
    const shuffledChoices = useMemo(
        () => shuffle([...choices]),
        [ choices ]
    );
    const classes = makeStyles({
        hasNewLine: {
            whiteSpace: 'pre-wrap',
        },
        hasNewLineBold: {
            whiteSpace: 'pre-wrap',
            fontWeight: "bold"
        }                  
    })();

    return (
        <FormControl component="fieldset">
            <RadioGroup value={answer} onChange={onAnswerUpdate}>
                <Grid container spacing={3}>
                    <Grid item xs={12} className={classes.hasNewLineBold}>
                        {number})
                        <HtmlDiv
                            html={text}
                        />
                    </Grid>
                        { shuffledChoices.map((ch, ind) => (
                            <Grid item xs={12} key={ind}>
                                <FormControlLabel 
                                    value={ch.id} 
                                    control={<Radio disabled={disabled} />} 
                                    label={<HtmlDiv html={ch.text} />} />
                            </Grid>
                        ))}
                </Grid>
                <Box p={3}>
                    <Divider />
                </Box>
            </RadioGroup>
        </FormControl>
    )
}

function Test({ skillId, onClose, topicId, exam, enqueueSnackbar, open, ...props }) {
    const classes = useStyles();

    const [isLoading, setIsLoading] = useState(false);
    const [isChecking, setIsChecking] = useState(false);
    const [checked, setChecked] = useState(false);
    const [questions, setQuestions] = useState(null);
    const [results, setResults] = useState({});
    const [examId, setExamId] = useState(0);
    const [status, setStatus] = useState(null);
    const { t } = useTranslation([ 'dashboard' ]);

    const updateQuestions = useCallback(
        (skillId, topicId) => {
            if (topicId || skillId) { // only valid skill ids
                setIsLoading(true);
                loadQuestions(
                    exam,
                    skillId,
                    topicId,
                    res => {
                        if (exam) {
                            if ("questions" in res) {
                                setQuestions(res.questions.map(qo => ({ answer: '', ...qo })))
                                setExamId(res.assessment);
                            } else {
                                setStatus(res.data.result);
                            }
                        }
                        else
                            setQuestions(res.map(qo => ({ answer: '', ...qo })));
                        setIsLoading(false)
                    },
                    () => {
                        enqueueSnackbar(
                            t("There is no more exam for this course."),
                            {
                                variant: "warning"
                            }
                        )
                        onClose(false)
                    },
                    null
                )
            }
        }, 
        [ exam ]
    );

    const handleAnswer = useCallback((question, ind) => (e) => {
        const newQuestion = { ...question, answer: Number(e.target.value)};
        setQuestions(p => {
            p.splice(ind, 1, newQuestion);
            return [...p];
        });
    }, []);

    useEffect(() => {
        if (open) {
            // load questions
            setStatus(null);
            setChecked(false);
            setIsChecking(false);
            updateQuestions(skillId, topicId);
        }
    }, [skillId, topicId, updateQuestions, open]);

    const handleTest = useCallback(() => {
        setIsChecking(true);
        checkQuestions(
            questions,
            exam,
            examId,
            skillId,
            topicId,
            res => {
                // load results
                setResults(res);
                setChecked(true);
            },
            null,
            () => setIsChecking(false)
        )
    }, [questions, skillId, topicId, exam, examId]);

    return ( (topicId || skillId > 0) &&
        <Paper className={classes.root}>
            <Box textAlign="right">
                <IconButton onClick={() => onClose(false)}>
                    <CloseIcon/>
                </IconButton>
            </Box>
            <Box p={3}>
                { !status ? (
                isLoading || questions === null ? 
                    <Box textAlign="center">
                        <CircularProgress />
                    </Box>
                :
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                        { questions.length === 0 ? onClose(true)
                        :
                            questions.map((q, ind) => (
                                <Question text={q.text} choices={q.choices} answer={q.answer} key={ind} number={ind + 1}
                                    onAnswerUpdate={handleAnswer(q, ind)} disabled={isChecking || checked} identifier={q.identifier} />
                            ))
                        }
                        </Grid>
                        <Grid item xs={12}>
                            { !checked ? 
                            <Box textAlign="right">
                                <Button variant="contained" color="primary" onClick={handleTest}>
                                    {t("Continue")}
                                </Button>
                            </Box>
                            :
                            <>
                            <SnackbarContent className={classes.result} message={
                                <Typography align="left" component="div">
                                    { results.total_answers > 0 &&
                                        <p>{results.correct_answers} out of {results.total_answers} is/are correct.</p>
                                    }
                                    {exam ? 
                                    <div>
                                        {t("Your score is")} {Math.round(results.success * 10000)/100}.
                                    </div>
                                    :
                                    <>
                                    <div>
                                        {results.finished_topics.length > 0 &&
                                            <>
                                            {t("The following topics are passed")}: <br />
                                            <ul>
                                            {results.finished_topics.map((t, ind) => (
                                                <li key={ind}>
                                                    {t}
                                                </li>
                                            ))}
                                            </ul>
                                            </>
                                        }
                                    </div>
                                    <p>
                                        {results.ongoing_topics.length > 0 &&
                                            <>
                                            {t("The following topics are not passed")}: <br />
                                            <ul>
                                            {results.ongoing_topics.map((t, ind) => (
                                                <li key={ind}>
                                                    {t}
                                                </li>
                                            ))}
                                            </ul>
                                            </>
                                        }
                                    </p>
                                    </>
                                    }
                                </Typography>
                            }/>
                            <Box textAlign="right">
                                <Button onClick={() => onClose(true)} variant="contained" color="primary">{t("Ok")}</Button>
                            </Box>
                            </>
                            }
                        </Grid>
                    </Grid>
                ): 
                    <SnackbarContent message={status} />
                }
            </Box>
        </Paper>
    )
}

Test.propTypes = {
    skillId: PropTypes.number.isRequired,
    onClose: PropTypes.func,
    exam: PropTypes.bool,
}

Test.defaultProps = {
    onClose: () => {},
    exam: false
}

export default withSnackbar(Test)
