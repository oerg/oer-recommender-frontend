import { makeStyles } from "@material-ui/core";
import { blue, green, yellow } from "@material-ui/core/colors";
import {
  grayColor,
  hexToRgb, successColor,
  whiteColor
} from "./variables";

const dashboardStyle = theme => ({
  breadcrumbs: {
    paddingLeft: theme.spacing(2),
  },
  keyword: {
    margin: theme.spacing(0.5),
    backgroundColor: '#008080',
    color: '#FFF',
  },
  description: {
    maxHeight: 85,
    overflowY: 'auto',
  },
  col: {
    overflow: 'auto',
  },
  edoerSugg: {
    backgroundColor: '#00b3b3',
    color: '#FFF',
  },
  addSugg: {
    backgroundColor: '#008080',
    color: '#FFF',
  },
  delSugg: {
    backgroundColor: '#009999',
    color: '#FFF'
  },
  movSugg: {
    backgroundColor: '#00b3b3',
    color: '#FFF',
  },
  editSugg: {
    backgroundColor: '#00cccc',
    color: '#FFF',
  },
  suggestionsBox: {
    backgroundColor: '#AAA',
  },
  hasPadding: {
    padding: theme.spacing(1),
  },

  hasMargin: {
    margin: theme.spacing(1),
  },

  hasYMargin: {
    margin: `${theme.spacing(1)}px ${theme.spacing(0)}px`,
  },

  successText: {
    color: successColor[0]
  },
  upArrowCardCategory: {
    width: "16px",
    height: "16px"
  },
  stats: {
    color: grayColor[0],
    display: "inline-flex",
    fontSize: "12px",
    lineHeight: "22px",
    "& svg": {
      top: "4px",
      width: "16px",
      height: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      top: "4px",
      fontSize: "16px",
      position: "relative",
      marginRight: "3px",
      marginLeft: "3px"
    }
  },
  cardCategory: {
    color: grayColor[0],
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    paddingTop: "10px",
    marginBottom: "0"
  },
  cardCategoryWhite: {
    color: "rgba(" + hexToRgb(whiteColor) + ",.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitle: {
    color: grayColor[2],
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: grayColor[1],
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  cardTitleWhite: {
    color: whiteColor,
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    flexGrow: 1,
    "& small": {
      color: grayColor[1],
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  headerSkill: {
    fontWeight: "bold",
    flexBasis: "33.3Related Jobs%",
    flexShrink: 0,
  },
  AS: { //assigned row
    backgroundColor: blue[100],
  },
  FI: { // finished row
    backgroundColor: green[100],
  },
  ON: { // ongoing row
    backgroundColor: yellow[100],
  },
  EX: {
    backgroundColor: blue[300],
  },
  paperTitle: {
    margin: '15px',
    padding: '15px 5px 5px 5px',
    display: 'block',
    fontSize: "1.5em",
  },
  company: {
    marginLeft: 8,
  },
  titleWithButton: {
    display: "flex",
    alignItems: "center"
  },
  cardTitleDiv: {
    flex: "1 1 auto",
  },
  cardButtonDiv: {
    flex: "0 0 auto",
    "& .MuiButtonBase-root": {
      height: "100%",
      color: "#FFF",
      margin: "0px 5px",
      borderColor: "#FFF",
    }
  },
  addButtonDisabled: {
    color: "#CCC !important",
    borderColor: "#CCC !important",
  },
  logo: {
    marginRight: 16
  },
  lightBorders: {
    borderColor: "#BBB"
  },
  descriptionBox: {
    marginTop: 5,
    marginBottom: 25,
    padding: "20px 0px",
    "& > div": {
      padding: "0px 20px",
      maxHeight: 175,
      overflowY: "auto",  
    }
  },
});

export default makeStyles(dashboardStyle);