const { makeStyles } = require("@material-ui/core");
const { defaultFont, grayColor, primaryColor, container } = require("./variables");

export default makeStyles(theme => ({
    block: {
        color: "inherit",
        padding: "15px",
        textTransform: "uppercase",
        borderRadius: "3px",
        textDecoration: "none",
        position: "relative",
        display: "block",
        ...defaultFont,
        fontWeight: "500",
        fontSize: "12px"
    },
    left: {
        float: "left!important",
        display: "block"
    },
    right: {
        padding: "15px 0",
        margin: "0",
        fontSize: "14px",
        textAlign: "center",
    },
    footer: {
        bottom: "0",
        borderTop: "1px solid " + grayColor[11],
        padding: "15px 0",
        flex: "0 1 150px",
        ...defaultFont
    },
    container,
    a: {
        color: primaryColor,
        textDecoration: "none",
        backgroundColor: "transparent"
    },
    list: {
        marginBottom: "0",
        padding: "0",
        marginTop: "0"
    },
    inlineBlock: {
        display: "inline-block",
        padding: "0px",
        width: "auto"
    },
    partnerBox: {
        padding: theme.spacing(1),
        margin: theme.spacing(1),
        marginLeft: "auto",
        marginRight: "auto",
        width: 80,
        textAlign: "center",
    }
}));