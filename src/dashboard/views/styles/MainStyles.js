import { makeStyles } from "@material-ui/core";
import { container } from "./variables";

export default makeStyles(theme => ({
    mainPanel: {
        padding: '25px',
        display: "flex",
        flexFlow: "column",
        height: "calc(100% - 85px)"
    },
    content: {
        marginTop: "70px",
        padding: "30px 15px",
        minHeight: "calc(100vh - 123px)"
    },
    container,
    map: {
        marginTop: "70px"
    },
    toolbarSpace: {
        ...theme.mixins.toolbar,
        marginBottom: '15px'
    }
}));