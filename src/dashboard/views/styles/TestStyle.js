import { makeStyles } from "@material-ui/core";
import { green } from '@material-ui/core/colors';

export default makeStyles({
    root: {
        maxWidth: '900px',
        maxHeight: '100vh',
        overflowY: 'auto',
    },
    result: {
        backgroundColor: green[100]
    }
})