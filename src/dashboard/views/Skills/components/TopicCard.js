import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { green } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import LaunchIcon from '@material-ui/icons/Launch';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import TopicLogo from '../../../components/Logos/Topic';
import ShareButton from '../../../components/ShareButton';
import { getTopicOnly } from '../../Discovery/Topics/utils';
import DetailWindow from './DetailWindow';
import TopicDescription from './TopicDescription';

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around"
    },
    actionRight: {
        marginLeft: 'auto'
    },
    avatar: {
        backgroundColor: green[100],
    },
    cardHeaderTitle: {
        width: "95%",
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis"
    },
    doneIcon: {
        cursor: "default",
    },
    title: {
        cursor: "pointer",
        paddingRight: theme.spacing(0.5)
    },
    titleNonAddable: {
        cursor: "pointer",
        paddingRight: theme.spacing(0.5),
        color: "#CCC",
    }
}));

export default function TopicCard({ topic, onAddSkill, addDisabled, basePath }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    const [loading, setLoading] = useState(false);
    const [oers, setOers] = useState(null); // null = not loaded yet
    const [showDescription, setShowDescription] = useState(false);
    const [mouseOver, setMouseOver] = useState(false);

    const myUrl = useMemo(
        () => `${basePath}/topics/${topic.topic}`,
        [topic, basePath]
    );

    const handleMouseEnter = useCallback(
        () => setMouseOver(true),
        []
    );

    const handleMouseOut = useCallback(
        () => setMouseOver(false),
        []
    );

    const loadTopicDetails = useCallback(
        (topicId) => { // load topics
            setLoading(true);
            getTopicOnly(
                topicId,
                res => {
                    setOers(res.oer_groups);
                },
                null,
                () => setLoading(false)
            )
        },
        []);

    const handleShowDetails = useCallback(
        () => {
            setShowDescription(true);
            if (oers === null) {
                loadTopicDetails(topic.topic);
            }
        },
        [oers, loadTopicDetails]
    );

    const handleCloseDescription = useCallback(
        () => setShowDescription(false),
        []
    );

    return (<>
        <Card className={classes.root}
            raised={mouseOver}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseOut}
        >
            <CardHeader
                avatar={<TopicLogo />}
                title={topic.topic_title}
                titleTypographyProps={{
                    title: `${topic.topic_title} ${!topic.topic_learnable && `(${t("Not learnable")})`}`,
                    variant: "h6",
                    className: topic.topic_learnable ? classes.title : classes.titleNonAddable,
                    onClick: handleShowDetails
                }}
            />
            <CardActions disableSpacing>
                <div className={classes.actionRight}>
                    <ShareButton title={topic.topic_title} componentTitle={t("Share this Course")} url={myUrl}>
                        <ShareIcon />
                    </ShareButton>
                    <IconButton title={t("Learn more")} onClick={handleShowDetails}>
                        <LaunchIcon />
                    </IconButton>
                </div>
            </CardActions>
        </Card>
        <DetailWindow
            title={topic.topic_title}
            icon={<TopicLogo />}
            content={<TopicDescription description={topic.topic_description} oers={oers} loading={loading} />}
            open={showDescription}
            onClose={handleCloseDescription}
            topicId={topic.topic}
        />
    </>);
}
