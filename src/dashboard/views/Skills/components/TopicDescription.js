import { Typography } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';

export default function TopicDescription({ description, oers, loading }) {
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    return (<>
        { !description && !loading && oers && oers.length === 0 && <i>{t("No description available.")}</i>}
        { description !== null &&
            <Typography paragraph>
                {description}
            </Typography>
        }
        { loading ?
            <>
              <Skeleton variant="text" />
              <Skeleton variant="rect" />
            </>
        :
            oers && oers.length > 0 && 
            <>
                <i>{t("Sample educational packages on this topic")}</i>
                <ul>
                {
                    oers.map((oer, ind) => (
                    <li key={ind}><Link to={`/${language}/dashboard/oer-groups/${oer.id}`} title={t("View this educational content")}>{oer.title}</Link></li>
                    ))                    
                }
                </ul>
            </>
        }
    </>)
}
