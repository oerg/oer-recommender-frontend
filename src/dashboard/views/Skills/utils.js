import Axios from 'axios';
import { requestAddHandlers } from '../helpers';

export function getJob(id, onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        Axios.get('/jobs/' + id),
        onSuccess,
        onError
    );
}