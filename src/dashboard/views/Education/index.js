import { Backdrop, Button, ButtonGroup, Collapse, Grid, Paper, Snackbar } from "@material-ui/core"
import AddIcon from "@material-ui/icons/Add"
import CancelIcon from "@material-ui/icons/Cancel"
import HeightIcon from "@material-ui/icons/Height"
import { Alert, Skeleton } from "@material-ui/lab"
import Axios from "axios"
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react"
import { useTranslation } from "react-i18next"
import { useHistory } from "react-router-dom"
import { Container, Draggable } from "react-smooth-dnd"
import { useLanguage } from "../../../i18n"
import { parseDate } from "../../../utils"
import Card from "../../components/Card/Card"
import CardBody from "../../components/Card/CardBody"
import CardHeader from "../../components/Card/CardHeader"
import ConfirmDialog from "../../components/ConfirmDialog/ConfirmDialog"
import GridContainer from "../../components/Grid/GridContainer"
import GridItem from "../../components/Grid/GridItem"
import ToggleDiv from "../../components/ToggleDiv"
import TooltipWithHelp from "../../components/TooltipWithHelp/TooltipWithHelp"
import { getSkillOrder, saveDeadline, saveSkill, updateSkillsBatch } from "../Goals/utils"
import { isMyFirstTime, saveVisit } from "../helpers"
import useSearch from "../Search/useSearch"
import useStyles from "../styles/EducationalContentStyles"
import Test from "../Test"
import { skipAssessment } from "../Test/utils"
import ArchivedSkill from "./components/ArchivedSkill"
import ArchivedSkillDialog from "./components/ArchivedSkillDialog"
import DateDialog from "./components/DateDialog"
import SkillConfirmDialog from "./components/SkillConfirmDialog"
import SkillView from "./components/SkillView"
import TopicsConfirmDialog from "./components/TopicsConfirmDialog"
import { doArchive, doUnarchive, loadMyArchivedSkills, loadMySkills, saveSkillOrders, updateTopics } from "./utils"
import IAlert from "../../components/ISys/IAlert"
import CloseIcon from "@material-ui/icons/Close"
import DemoDialog from "../../../landing/components/DemoDialog"
import Cookies from "js-cookie"

const pageCode = "gEducation";
const DemoCookieId = "__learn_demo_viewed"

function EducationalContent({ user, userData }) {
    const classes = useStyles();
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const isFirstTime = isMyFirstTime(pageCode, userData);
    const [openSearch] = useSearch()

    const [skills, setSkills] = useState([])
    const [skillsReloadKeys, setSkillsReloadKeys] = useState([])
    const [openedSkill, setOpenedSkill] = useState('')
    const [loading, setLoading] = useState(false)
    const [showTest, setShowTest] = useState(false);
    const [testSkillId, setTestSkillId] = useState(0);
    const [testTopicId, setTestTopicId] = useState(null);

    const [openDeadlineSelect, setOpenDeadlineSelect] = useState(false)
    const [selectedSkillForDeadline, setSelectedSkillForDeadline] = useState(null)
    const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
    const [selectedSkillForDelete, setSelectedSkillForDelete] = useState(null)
    const [openTopicUpdateConfirm, setOpenTopicUpdateConfirm] = useState(false)
    const [selectedSkillForTopicUpdate, setSelectedSkillForTopicUpdate] = useState(null) // { skill, updates }
    const [reordering, setReordering] = useState(false)
    const [loadingReorder, setLoadingReorder] = useState(false)
    const [, setSkillsToReorder] = useState([])
    const [skillsDialogOpen, setSkillsDialogOpen] = useState(false)
    const [skillsToUpdate, setSkillsToUpdate] = useState([])

    const [showDemoSnack, setShowDemoSnack] = useState(false)
    const [showDemo, setShowDemo] = useState(false)
    const [snackHided, setSnackHided] = useState(false)
    const snackTimeoutId = useRef(null)

    useEffect(() => {
        if (Cookies.get(DemoCookieId) !== "true") {
            snackTimeoutId.current = window.setTimeout(() => setShowDemoSnack(true), 1500)
            return () => window.clearTimeout(snackTimeoutId.current)
        } else {
            setSnackHided(true)
        }
    }, [])

    const handleShowDemo = useCallback(
        () => setShowDemo(true),
        []
    )

    const handleDismissSnack = useCallback(
        () => {
            Cookies.set(DemoCookieId, "true")
            setShowDemoSnack(false)
            setSnackHided(true)
        },
        []
    )

    const handleHideDemo = useCallback(
        () => setShowDemo(false),
        []
    )

    const [archivedSkills, setArchivedSkills] = useState([])
    const [loadingArchivedSkills, setLoadingArchivedSkills] = useState(false)
    const [selectedArchivedSkill, setSelectedArchivedSkill] = useState(null)

    const SkillsWrapperComponent = useMemo(
        () => reordering ?
            Container
            :
            "div",
        [reordering]
    )

    const loadAllMySkills = useCallback(
        (isFirstTime = false) => {
            setLoading(true)
            loadMySkills(
                mySkills => {
                    setSkills(mySkills)
                    if (isFirstTime) {
                        setSkillsReloadKeys(mySkills.map(() => 0))
                    } else {
                        setSkillsReloadKeys(
                            keys => keys.map(k => k + 1)
                        )
                    }
                },
                null,
                () => setLoading(false)
            )
        },
        []
    )

    useEffect(
        () => {
            loadAllMySkills(true)
        },
        [loadAllMySkills]
    )

    const loadAllMyArchivedSkills = useCallback(
        () => {
            setLoadingArchivedSkills(true)
            loadMyArchivedSkills(
                mySkills => {
                    setArchivedSkills(mySkills)
                },
                null,
                () => setLoadingArchivedSkills(false)
            )
        },
        []
    )

    const handleCancelSkillDialog = useCallback(
        () => {
            setSkillsDialogOpen(false);
            setSkillsToUpdate([]);
        },
        []
    );

    const handleConfirmSkillDialog = useCallback(
        (skills, is_add) => {
            updateSkillsBatch(
                skills,
                is_add,
                () => {
                    loadAllMySkills()
                },
                null,
                () => {
                    setSkillsDialogOpen(false);
                    setSkillsToUpdate([]);
                }
            )
        },
        [loadAllMySkills]
    );

    const handleTest = useCallback((skill) => {
        if (window.confirm(t("Do you want to start the test?"))) {
            setTestSkillId(skill.skill.id);
            setTestTopicId(null);
            setShowTest(true);
        }
    }, [t]);

    const handleSkip = useCallback((skill_id, topic_id) => {
        skipAssessment(
            skill_id,
            topic_id,
            () => loadAllMySkills()
        )
    }, []);

    const handleOerChange = useCallback(
        (skill, done) => {
            if (done) {
                loadAllMySkills()
            }
        },
        [loadAllMySkills]
    );

    const handleTestClose = useCallback(
        (done) => {
            setShowTest(false);
            setTestSkillId(0);
            setTestTopicId(null);
            loadAllMySkills()
        },
        [loadAllMySkills]
    );

    const [loadingDirectTest, setLoadingDirectTest] = useState(false);

    const handleDirectTest = useCallback(
        (skill, oer) => {
            const data = {
                oer_id: oer['id'],
                skill_id: skill.skill.id,
                comment: '--- Automatic ---',
                rate: null,
                status: "CH",
                is_exam: true,
            }
            setLoadingDirectTest(true);
            Axios.put('/members/change-oer-status/', data)
                .then(res => {
                    handleTest(skill);
                }).catch(err => {
                }).finally(() => setLoadingDirectTest(false))
        },
        [handleTest]
    );

    // handle active tab for user added oers
    const handleActiveTab = useCallback(
        (tab, expanded) => {
            if (expanded) {
                setOpenedSkill(tab.skill.id);
                // setTutorialStep(1);
            } else {
                setOpenedSkill('');
            }
        },
        []
    );

    const handleChangeDeadline = useCallback(
        (newDeadline) => {
            const parsedDeadline = newDeadline ? parseDate(newDeadline) : null
            saveDeadline(
                selectedSkillForDeadline.skill.id,
                parsedDeadline,
                loadAllMySkills, // reload
                null,
                () => setOpenDeadlineSelect(false)
            )
        },
        [loadAllMySkills, selectedSkillForDeadline]
    )

    const handleStartDeadlineChange = useCallback(
        (skill) => {
            setSelectedSkillForDeadline(skill)
            setOpenDeadlineSelect(true)
        },
        []
    )

    const handleCloseDeadline = useCallback(
        () => setOpenDeadlineSelect(false),
        []
    )

    const handleStartDelete = useCallback(
        (skill) => {
            setSelectedSkillForDelete(skill)
            setOpenDeleteDialog(true)
        },
        []
    )

    const handleDeleteSkill = useCallback(
        () => {
            setOpenDeleteDialog(false)
            saveSkill(
                selectedSkillForDelete.skill.id,
                false,
                selectedSkillForDelete.skill_order,
                () => {
                    loadAllMySkills()
                }
            )
        },
        [selectedSkillForDelete]
    )

    const handleCancelDelete = useCallback(
        () => {
            setOpenDeleteDialog(false)
        },
        []
    )

    const handleStartUpdate = useCallback(
        (skill, updates) => {
            setSelectedSkillForTopicUpdate({
                skill,
                updates
            })
            setOpenTopicUpdateConfirm(true)
        },
        []
    )

    const handleCancelTopicsUpdate = useCallback(
        () => {
            setOpenTopicUpdateConfirm(false)
        },
        []
    )

    const handleConfirmTopicsUpdate = useCallback(
        (toAdd, toRemove) => {
            updateTopics(
                selectedSkillForTopicUpdate.skill.skill.id,
                toAdd,
                toRemove,
                () => {
                    setOpenTopicUpdateConfirm(false)
                    loadAllMySkills()
                }
            )
        },
        [loadAllMySkills, selectedSkillForTopicUpdate]
    )

    const handleDrop = useCallback(
        ({ removedIndex, addedIndex }) => {
            if (removedIndex !== addedIndex) {
                const movedSkill = skills[removedIndex]
                setSkills(
                    skills => {
                        const newSkills = [...skills]
                        const [movedSkill] = newSkills.splice(removedIndex, 1)
                        newSkills.splice(addedIndex, 0, movedSkill)
                        movedSkill.skill_order = getSkillOrder(newSkills, addedIndex)
                        return newSkills
                    }
                )
                setSkillsToReorder(
                    reorderList => {
                        return [...reorderList, movedSkill]
                    }
                )
            }
        },
        [skills]
    )

    const startReordering = useCallback(
        () => {
            setReordering(true)
        },
        []
    )

    const handleReordering = useCallback(
        () => {
            setReordering(false)
            setLoadingReorder(true)
            setSkillsToReorder(
                skills => {
                    saveSkillOrders(
                        skills,
                        null,
                        null,
                        () => setLoadingReorder(false)
                    )
                    return []
                }
            )
        },
        []
    )

    const cancelReordering = useCallback(
        () => {
            setReordering(false)
            setSkillsToReorder([])
        }
    )

    const handleArchive = useCallback(
        skill => {
            if (window.confirm(t("Do you want archive the selected course?"))) {
                doArchive(
                    skill.skill.id,
                    res => {
                        if (res.done) {
                            setSkills(
                                skills => {
                                    const index = skills.findIndex(s => s.skill.id === skill.skill.id)
                                    if (index >= 0) {
                                        const newSkills = [...skills]
                                        newSkills.splice(index, 1)
                                        return newSkills
                                    }
                                    return skills
                                }
                            )
                            setArchivedSkills(
                                archivedSkills => [...archivedSkills, skill]
                            )
                        }
                    }
                )
            }
        },
        [t]
    )

    const handleArchiveOpen = useCallback(
        (isOpen) => {
            if (isOpen) {
                loadAllMyArchivedSkills()
            }
        },
        [loadAllMyArchivedSkills]
    )

    const handleUnarchive = useCallback(
        (skill) => {
            if (window.confirm(t("Do you want to unarchive the selected course?"))) {
                setLoadingArchivedSkills(true)
                doUnarchive(
                    skill.skill.id,
                    res => {
                        if (res.done) {
                            setArchivedSkills(
                                archivedSkills => {
                                    const newSkills = [...archivedSkills]
                                    const index = newSkills.findIndex(s => s.skill.id === skill.skill.id)
                                    newSkills.splice(index, 1)
                                    return newSkills
                                }
                            )
                            setSkills(
                                skills => [...skills, { ...skill, skill_order: res.new_order }]
                            )
                        }
                    },
                    null,
                    () => setLoadingArchivedSkills(false)
                )
            }
        },
        [t]
    )

    const handleArchivedSkillSelection = useCallback(
        (skill) => {
            setSelectedArchivedSkill(skill)
        },
        []
    )

    const handleCloseArchiveDialog = useCallback(
        () => {
            setSelectedArchivedSkill(null)
        },
        []
    )

    useEffect(() => {
        if (isFirstTime && user.data.user.email.split("@")[1] !== "guest.edoer.eu") {
            history.push(`/${language}/dashboard/profile/preferences?reg=1`)
        }
        return () => {
            if (isFirstTime) {
                saveVisit(pageCode, user.setData);
            }
        }
    }, [isFirstTime, user.setData, user.data])

    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("My Personal Curriculum")}
                        </h3>
                        <h4 className={classes.cardCategoryWhite}>
                            {t("Learning towards the target courses")}
                        </h4>
                    </CardHeader>
                    <CardBody id="skills">
                        <Collapse in={loading}>
                            <Skeleton variant='rect' animation="wave" height={80} style={{ marginBottom: 16 }} />
                            <Skeleton variant='rect' animation="wave" height={80} style={{ marginBottom: 16 }} />
                            <Skeleton variant='rect' animation="wave" height={80} style={{ marginBottom: 16 }} />
                        </Collapse>
                        <Collapse in={!loading} unmountOnExit>
                            <div className={classes.topActions}>
                                <Collapse in={snackHided}>
                                    <Button variant='outlined' onClick={handleShowDemo}>
                                        {t("View Demo")}
                                    </Button>
                                </Collapse>
                                <ButtonGroup variant='outlined'>
                                    <Button
                                        startIcon={<HeightIcon />}
                                        onClick={startReordering}
                                        title={t("Reorder the courses in your dashboard.")}
                                        disabled={skills.length === 0 || loadingReorder || reordering}
                                    >
                                        {t("Reorder")}
                                    </Button>
                                    <Button
                                        title={t("Add a new learning item")}
                                        onClick={openSearch}
                                        color="primary"
                                    >
                                        {t("Add Course")}
                                    </Button>
                                </ButtonGroup>
                            </div>
                            <SkillsWrapperComponent lockAxis="y" onDrop={handleDrop}>
                                {skills.length > 0 ?
                                    skills.map(
                                        (skill, index) => (
                                            reordering ?
                                                <Draggable
                                                    key={index}
                                                    render={() => (
                                                        <SkillView
                                                            skill={skill}
                                                            open={openedSkill === skill.skill.id}
                                                            onSelect={handleActiveTab}
                                                            onDirectTest={handleDirectTest}
                                                            loading={loadingDirectTest}
                                                            showSkip
                                                            onSkip={handleSkip}
                                                            onTest={handleTest}
                                                            onOerChange={handleOerChange}
                                                            onOerDone={handleOerChange}
                                                            userData={userData}
                                                            onDeadlineChangeRequest={handleStartDeadlineChange}
                                                            onDeleteRequest={handleStartDelete}
                                                            onUpdateRequest={handleStartUpdate}
                                                            isMoving={reordering}
                                                            reloadKey={skillsReloadKeys[index]}
                                                            onArchive={handleArchive}
                                                        />
                                                    )}
                                                />
                                                :
                                                <SkillView
                                                    key={index}
                                                    skill={skill}
                                                    open={openedSkill === skill.skill.id}
                                                    onSelect={handleActiveTab}
                                                    onDirectTest={handleDirectTest}
                                                    loading={loadingDirectTest}
                                                    showSkip
                                                    onSkip={handleSkip}
                                                    onTest={handleTest}
                                                    onOerChange={handleOerChange}
                                                    onOerDone={handleOerChange}
                                                    userData={userData}
                                                    onDeadlineChangeRequest={handleStartDeadlineChange}
                                                    onDeleteRequest={handleStartDelete}
                                                    onUpdateRequest={handleStartUpdate}
                                                    isMoving={reordering}
                                                    reloadKey={skillsReloadKeys[index]}
                                                    onArchive={handleArchive}
                                                />
                                        )
                                    )
                                    :
                                    <Alert
                                        severity='warning'
                                        action={<Button
                                            startIcon={<AddIcon />}
                                            title={t("Add a new learning item")}
                                            onClick={openSearch}
                                        >
                                            {t("Add Course")}
                                        </Button>}
                                        style={{
                                            margin: "auto",
                                            width: 400
                                        }}
                                    >
                                        {t("No course selected.")}
                                    </Alert>
                                }
                            </SkillsWrapperComponent>
                            <Collapse in={reordering} unmountOnExit>
                                <ButtonGroup
                                    fullWidth
                                    variant='outlined'
                                    style={{
                                        height: 75
                                    }}
                                >
                                    <TooltipWithHelp
                                        title={t("Save your order after all the moves")}
                                        arrow
                                        enterTimeout={1000}
                                    >
                                        <Button
                                            color='primary'
                                            startIcon={<HeightIcon />}
                                            onClick={handleReordering}
                                        >
                                            {t("Save Reordering")}
                                        </Button>
                                    </TooltipWithHelp>
                                    <Button
                                        color='secondary'
                                        style={{
                                            width: 110,
                                        }}
                                        startIcon={<CancelIcon />}
                                        onClick={cancelReordering}
                                    >
                                        {t("Cancel")}
                                    </Button>
                                </ButtonGroup>
                            </Collapse>
                        </Collapse>
                        <Paper style={{
                            marginTop: 24,
                        }}>
                            <ToggleDiv
                                title={<b>{t("Archived Courses")}</b>}
                                onChange={handleArchiveOpen}
                            >
                                <div style={{ padding: 8 }}>
                                    <Collapse in={loadingArchivedSkills}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={4}>
                                                <Skeleton variant='rect' height={80} />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Skeleton variant='rect' height={80} />
                                            </Grid>
                                            <Grid item xs={4}>
                                                <Skeleton variant='rect' height={80} />
                                            </Grid>
                                        </Grid>
                                    </Collapse>
                                    <Collapse in={!loadingArchivedSkills}>
                                        {archivedSkills.length === 0 ?
                                            <Alert color='success' >
                                                {t("You do not have any archived skills.")}
                                            </Alert>
                                            :
                                            <Grid container spacing={1}>
                                                {archivedSkills.map(
                                                    (skill, index) => <Grid item xs={4} key={index}>
                                                        <Paper>
                                                            <ArchivedSkill
                                                                skill={skill}
                                                                fullWidth
                                                                onClick={handleArchivedSkillSelection}
                                                                onUnarchive={handleUnarchive}
                                                            />
                                                        </Paper>
                                                    </Grid>
                                                )}
                                            </Grid>
                                        }
                                    </Collapse>
                                </div>
                            </ToggleDiv>
                        </Paper>
                    </CardBody>
                </Card>
            </GridItem>
            <Backdrop className={classes.backdrop} open={showTest}>
                <Test skillId={testSkillId} onClose={handleTestClose} topicId={testTopicId} open={showTest} />
            </Backdrop>
            <DateDialog
                onChange={handleChangeDeadline}
                open={openDeadlineSelect}
                value={selectedSkillForDeadline ? selectedSkillForDeadline.deadline : null}
                onClose={handleCloseDeadline}
                clearable
            />
            <SkillConfirmDialog
                open={skillsDialogOpen}
                onCancel={handleCancelSkillDialog}
                onAccept={handleConfirmSkillDialog}
                isAdd={false}
                skills={skillsToUpdate}
            />
            <TopicsConfirmDialog
                topicsToAdd={selectedSkillForTopicUpdate ? selectedSkillForTopicUpdate.updates.adding_topics : []}
                topicsToRemove={selectedSkillForTopicUpdate ? selectedSkillForTopicUpdate.updates.removing_topics : []}
                open={openTopicUpdateConfirm}
                onCancel={handleCancelTopicsUpdate}
                onAccept={handleConfirmTopicsUpdate}
            />
            <ConfirmDialog
                title={t("Delete Confirmation")}
                open={openDeleteDialog}
                onAccept={handleDeleteSkill}
                onReject={handleCancelDelete}
                isDangerous
            >
                {t("Are sure you want to remove the following course from your dashboard?")}<br />
                <span>
                    <b><em>{selectedSkillForDelete ? selectedSkillForDelete.skill.title : ""}</em></b>
                </span>
            </ConfirmDialog>
            <Snackbar
                open={showDemoSnack}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
                style={{
                    top: 130,
                    zIndex: 99
                }}
            >
                <IAlert
                    show
                    transition={false}
                    action={
                        <ButtonGroup color='inherit' variant='text'>
                            <Button onClick={handleShowDemo} title={t("Start adding with help")}>
                                {t("View Demo")}
                            </Button>
                            <Button onClick={handleDismissSnack} title={t("Dismiss")}>
                                <CloseIcon />
                            </Button>
                        </ButtonGroup>
                    }
                    style={{
                        border: "1px solid #2196f3",
                    }}
                >
                    {t("Need Help? Watch our demo.")}
                </IAlert>
            </Snackbar>
            <DemoDialog open={showDemo} onClose={handleHideDemo} />
            <ArchivedSkillDialog
                open={Boolean(selectedArchivedSkill)}
                skill={selectedArchivedSkill}
                onClose={handleCloseArchiveDialog}
            />
        </GridContainer>
    )
}

export default EducationalContent
