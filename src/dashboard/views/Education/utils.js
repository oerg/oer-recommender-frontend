import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function loadMySkills(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get('/members/myskills/'),
        onLoad,
        onError,
        onEnd
    )
}

export function loadMyArchivedSkills(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get('/members/myskills-archived/'),
        onLoad,
        onError,
        onEnd
    )
}

export function loadMyGoals(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get('/members/mygoals/'),
        onLoad,
        onError,
        onEnd
    )
}

export function requestRecommendation(skill_id, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post('/members/assign-recommendation/', { skill_id }),
        onLoad,
        onError,
        onEnd
    );
}

export function getReports(skill_id, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.get(`/members/get-skill-report/?skill_id=${skill_id}`),
        onLoad,
        onError,
        onEnd
    );
}

export function reactivateTopic(skill_id, topic_id, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post('/members/reenable-topic/', { skill_id, topic_id }),
        onLoad,
        onError,
        onEnd
    );
}

export function updateTopics(skill_id, topics_to_add, topics_to_remove, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.put('/members/update-skill-topics/',
            {
                skill_id,
                adding_topics: topics_to_add.map(t => t.id),
                removing_topics: topics_to_remove.map(t => t.id)
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}

export function updateTopicsInfo(skill_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/members/update-skill-topics-inf/?skill_id=${skill_id}`),
        onLoad,
        onError,
        onEnd
    )
}

export function saveSkillOrders(skills, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(
            '/members/update-skill/',
            {
                skills: skills.map(
                    skill => ({
                        skill: skill.skill.id,
                        skill_order: skill.skill_order,
                    })
                )
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function doArchive(skill_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(`/members/myskills/${skill_id}/archive/`),
        onLoad,
        onError,
        onEnd
    )
}

export function doUnarchive(skill_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(`/members/myskills/${skill_id}/unarchive/`),
        onLoad,
        onError,
        onEnd
    )
}