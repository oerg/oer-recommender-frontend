import { Box, Collapse, IconButton, makeStyles, TableCell, TableRow } from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../../components/CustomButton/Button';
import TopicLogo from '../../../components/Logos/Topic';
import OerGroup from '../../../components/OerGroup';

const useStyles = makeStyles(
    {
        hidden: {
            display: "none",
        },
        row: {
            cursor: 'pointer',
        }
    }
)

function TopicRow({ topic, onTest, onOerChange, onOerDone, expanded, onExpandedChange, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    const handleTest = useCallback(
        () => {
            onTest && onTest(topic)
        },
        [onTest, topic],
    );

    const handleChange = useCallback(
        (done) => {
            onOerChange && onOerChange(topic, done)
        },
        [topic, onOerChange],
    );

    const handleDone = useCallback(
        (done) => {
            onOerDone && onOerDone(topic, done)
        },
        [topic, onOerChange],
    );

    const handleExpanded = useCallback(
        (e) => {
            onExpandedChange && onExpandedChange(e, !expanded, 'uat_' + topic['id'])
        },
        [expanded],
    );

    return (<>
        <TableRow onClick={handleExpanded} className={classes.row} {...props}>
            <TableCell>
                <div style={{
                    display: "flex",
                    gap: 8
                }}>
                    <TopicLogo picture={topic.picture} />
                    {topic['title']}
                </div>
            </TableCell>
            <TableCell align="right">
                <IconButton>
                    {expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                </IconButton>
            </TableCell>
        </TableRow>
        <TableRow className={!expanded ? classes.hidden : ''}>
            <TableCell colSpan={3}>
                <Collapse in={expanded}>
                    <div>
                        {topic['status'] === "EX" ?
                            <Box textAlign="center" p={5}>
                                <Button color="rose" variant="contained" onClick={handleTest}>
                                    {t("I am ready to Take a Test")}
                                </Button>
                            </Box>
                            :
                            topic['user_oers'].filter(
                                oerg => oerg['status'] === 'AS'
                            ).map(
                                (oerg, ind) => (
                                    <OerGroup topic={topic} group={oerg['oer_group']} status={oerg['status']} topicOnly onChange={handleChange} onDone={handleDone} />
                                )
                            )
                        }
                    </div>
                </Collapse>
            </TableCell>
        </TableRow>
    </>)
}

export default TopicRow
