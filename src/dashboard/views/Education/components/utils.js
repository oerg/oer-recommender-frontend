import axios from "axios";
import { requestAddHandlers } from "../../helpers";

export function loadOerGroup(topicId, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/members/get-passed-topics?topic_id=${topicId}`
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function saveTopicOrder(skillId, topicId, newOrder, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post(
            `/members/change-topic-order/`,
            {
                skill_id: skillId,
                topic_id: topicId,
                topic_order: newOrder
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function calculateOrder(array, newIndex) {
    if (array.length === 1) {
        return 1
    }

    if (array.length - 1 === newIndex) { // last
        return array[newIndex - 1].topic_order + 16
    } else if (newIndex === 0) { // first
        return array[1].topic_order - 16
    }

    return (array[newIndex - 1].topic_order + array[newIndex + 1].topic_order) / 2.0
}

export function loadMySkill(skill_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/members/myskills/${skill_id}/`),
        onLoad,
        onError,
        onEnd
    )
}