import { Accordion, AccordionDetails, AccordionSummary, Badge, Box, Button, CircularProgress, Collapse, Divider, Grid, IconButton, LinearProgress, makeStyles, Paper, Typography, Zoom } from '@material-ui/core';
import AirplanemodeInactiveIcon from '@material-ui/icons/AirplanemodeInactive';
import ArchiveIcon from '@material-ui/icons/Archive';
import AssessmentIcon from '@material-ui/icons/Assessment';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import LaunchIcon from '@material-ui/icons/Launch';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ReceiptIcon from '@material-ui/icons/Receipt';
import SpeedIcon from '@material-ui/icons/Speed';
import UpdateIcon from '@material-ui/icons/Update';
import { Alert, Skeleton } from '@material-ui/lab';
import clsx from 'classnames';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import { parseISOToDate } from '../../../../utils';
import AssignmentIcon from '@material-ui/icons/Assignment';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import SkillLogo from '../../../components/Logos/Skill';
import MenuButton from '../../../components/MenuButton/MenuButton';
import OerGroup from '../../../components/OerGroup';
import ToggleDiv from '../../../components/ToggleDiv';
import MutedText from '../../../components/Typography/Muted';
import RoseText from '../../../components/Typography/Rose';
import { getReports, reactivateTopic, requestRecommendation, updateTopicsInfo } from '../utils';
import GoalReport from './ReportComponents/GoalReport';
import LevelReport from './ReportComponents/LevelReport';
import ReportItem from './ReportComponents/ReportItem';
import TopicList from './TopicList';
import { loadMySkill, loadOerGroup } from './utils';

const useStyles = makeStyles(theme => ({
    listPaper: {
        height: '100%',
        display: 'block',
        overflow: 'auto',
    },
    headerSkill: {
        flexBasis: "33.3%",
        flexShrink: 0,
    },
    skillRow: {
        alignItems: "center",
        gap: 8,
        height: 54,
    },
    wrapper: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    reportItem: {
        padding: theme.spacing(2),
    },
    oerPaper: {

    },
    oerHistoryPaper: {
        overflowY: 'scroll',
        height: 400
    },
    optionsButton: {
        border: "1px solid #CCC",
    },
    ongoingOerGroup: {
        backgroundColor: "#fffdcf",
    },
    finishedOerGroup: {
        backgroundColor: "#dbffdc",
    },
    toggleDiv: {
        marginTop: 24,
        border: '1px solid #9ed1d3',
        padding: 8,
    },
    toggleDivTitle: {
        backgroundColor: '#9ed1d3',
        textAlign: "left"
    },
    isMoving: {
        margin: "24px 0",
        cursor: "grab",
    }
}));

function SkillView({ open, reloadKey, onSelect, onDirectTest, loading, skill: skillData, showSkip, onSkip, onTest, isMoving, className,
    onOerChange, onOerDone, userData, onDeadlineChangeRequest, onDeleteRequest, onUpdateRequest, onArchive, ...props }) {
    const classes = useStyles();
    const [loadingReactivate, setLoadingReactivate] = useState(false);
    const [selectedTopics, setSelectedTopics] = useState([]);
    const [selectedOerGroups, setSelectedOerGroups] = useState(null)
    const [loadingSelectedOerGroups, setLoadingSelectedOerGroups] = useState(false)
    const [reports, setReports] = useState(null)
    const [loadingReports, setLoadingReports] = useState(false)
    const [loadingUpdateTopics, setLoadingUpdateTopics] = useState(false)

    const [loadingTopics, setLoadingTopics] = useState(false)
    const [, setTopicsLoaded] = useState(false)
    const [topics, setTopics] = useState([])

    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();

    const loadMe = useCallback(
        () => {
            setLoadingTopics(true)
            loadMySkill(
                skillData.skill.id,
                skillDetails => {
                    setTopics(skillDetails.user_skill_user_topic)
                },
                null,
                () => {
                    window.setTimeout(() => setLoadingTopics(false), 700)
                }
            )
            setLoadingReports(true)
            getReports(
                skillData.skill.id,
                (res) => {
                    setReports(res)
                },
                null,
                () => setLoadingReports(false)
            )
        },
        [skillData]
    )

    useEffect(
        () => {
            if (reloadKey > 0) {
                loadMe()
            }
        },
        [reloadKey, loadMe]
    )

    useEffect(
        () => {
            setTopicsLoaded(false)
        },
        [skillData]
    )

    useEffect(
        () => {
            setTopicsLoaded(
                loaded => {
                    if (open && !loaded) {
                        loadMe()
                    }
                    return open || loaded // set loaded true after the first open
                }
            )
        },
        [open, loadMe]
    )

    const currentTopics = useMemo(
        () => topics.filter(t => t.is_current),
        [topics]
    );

    const isExam = useMemo(() => currentTopics.some(t => t.user_topic.status === 'EX'), [currentTopics]);

    const isFinished = useMemo(
        () => currentTopics.every(t => t.user_topic.status === "FI"),
        [currentTopics]
    );

    useEffect( // check if it requires re-recommendation
        () => {
            if (!isExam && !isFinished) {
                if (currentTopics.filter(t => t.user_topic.status === 'ON' || t.user_topic.status === 'EX').length === 0) { // request a recom
                    requestRecommendation(
                        skillData.skill.id,
                        () => {
                            loadMe()
                        }
                    )
                }
            }
        },
        [isFinished, skillData, isExam, currentTopics, loadMe]
    )

    const activeOerGroup = useMemo(
        () => {
            if (!isExam) {
                var activeTopics = currentTopics.filter(t => t.user_topic.status === 'ON');
                // if there is an active topic & no exam & it has oers
                if (activeTopics.length > 0 && activeTopics[0].user_topic.user_oers && activeTopics[0].user_topic.user_oers.length > 0) {
                    var assignedOer = activeTopics[0].user_topic.user_oers.find(uo => uo.status === 'AS'); // find first assigned OERGroup
                    return assignedOer ? assignedOer.oer_group : null;
                }
            }
            return null;
        },
        [currentTopics, isExam]
    );

    const handleSelect = useCallback(
        (e, expanded) => !isMoving && onSelect && onSelect(skillData, expanded),
        [onSelect, skillData, isMoving]
    );

    const handleDirectTest = useCallback(
        () => onDirectTest && onDirectTest(skillData, activeOerGroup),
        [onDirectTest, skillData, activeOerGroup]
    );

    const handleSkip = useCallback(
        (topic_id) => {
            onSkip && onSkip(skillData.skill.id, topic_id)
        },
        [onSkip, skillData]
    );

    const handleTest = useCallback(
        () => onTest && onTest(skillData),
        [onTest, skillData]
    );

    const handleOerChange = useCallback(
        (done) => onOerChange && onOerChange(skillData, done),
        [onOerChange, skillData]
    );

    const handleOerDone = useCallback(
        (done) => onOerDone && onOerDone(skillData, done),
        [onOerDone, skillData]
    );

    const handleReactivate = useCallback(
        (topic) => {
            setLoadingReactivate(true);
            reactivateTopic(
                skillData.skill.id,
                topic.user_topic.topic_id,
                () => {
                    setLoadingReactivate(false)
                    loadMe()
                },
                () => setLoadingReactivate(false)
            )
        },
        [skillData, loadMe]
    )

    const handleUpdateTopics = useCallback(
        (e) => {
            e.stopPropagation();
            setLoadingUpdateTopics(true);

            updateTopicsInfo(
                skillData.skill.id,
                (res) => {
                    onUpdateRequest(
                        skillData,
                        res // { adding_topics, removing_topics }
                    )
                },
                null,
                () => setLoadingUpdateTopics(false)
            )
        },
        [skillData, onUpdateRequest]
    )

    const handleSelectedTopics = useCallback(
        topics => {
            if (topics.length > 0) {
                setLoadingSelectedOerGroups(true)
                loadOerGroup(
                    topics[0].user_topic.topic_id,
                    res => {
                        if (res.length > 0)
                            setSelectedOerGroups(res.map(o => o.oer_group))
                        else
                            setSelectedOerGroups(null)
                    },
                    null,
                    () => setLoadingSelectedOerGroups(false)
                )
            }
            setSelectedTopics(topics)
        },
        []
    )

    const handleDeadline = useCallback(
        () => {
            onDeadlineChangeRequest(skillData)
        },
        [onDeadlineChangeRequest, skillData]
    )
    const handleDelete = useCallback(
        () => {
            onDeleteRequest(skillData)
        },
        [onDeleteRequest, skillData]
    )

    const handleArchive = useCallback(
        () => {
            onArchive(skillData)
        },
        [onArchive, skillData]
    )

    const menus = useMemo(
        () => [
            {
                label: t("Course Description"),
                icon: <LaunchIcon fontSize='small' />,
                component: Link,
                to: `/${systemLanguage}/dashboard/skills/${skillData.skill.id}`,
                target: "_blank",
            },
            {
                label: "-",
            },
            {
                label: t("Take an Exam"),
                component: Link,
                icon: <ReceiptIcon />,
                to: {
                    pathname: `/${systemLanguage}/dashboard/assess`,
                    state: {
                        exam_skill_id: skillData.skill.id,
                    },
                },
                disabled: !skillData.skill_assessmentable,
                inset: true,
            },
            {
                label: t("Take a Self-Assessment"),
                component: Link,
                icon: <AssessmentIcon />,
                to: {
                    pathname: `/${systemLanguage}/dashboard/assess`,
                    state: {
                        sa_skill_id: skillData.skill.id,
                    }
                },
                TextProps: {
                    inset: true,
                },
                disabled: !skillData.skill_self_assessmentable,
            },
            {
                label: "-"
            },
            {
                label: t("Update the topic list"),
                icon: loadingUpdateTopics ? <CircularProgress size={16} /> : <UpdateIcon fontSize='small' />,
                onClick: handleUpdateTopics,
                disabled: !skillData.skill_needs_update || loadingUpdateTopics,
                showBadge: skillData.skill_needs_update,
            },
            {
                label: skillData.deadline ? t("Change Deadline") : t("Set Deadline"),
                icon: <CalendarTodayIcon fontSize='small' />,
                onClick: handleDeadline // open deadline dialog
            },
            {
                label: t("Archive"),
                icon: <ArchiveIcon fontSize='small' />,
                title: t("Archive this course. You will be able to unarchive it later."),
                onClick: handleArchive,
            },
            {
                label: '-'
            },
            {
                label: t("Delete"),
                icon: <DeleteForeverIcon fontSize='small' />,
                onClick: handleDelete,
                title: t("Remove this course"),
            },
        ],
        [t, systemLanguage, skillData, loadingUpdateTopics, handleUpdateTopics, handleDeadline, handleArchive, handleDelete]
    )

    return (<>
        <Accordion expanded={open && !isMoving} onChange={handleSelect} disabled={isMoving} {...props}
            className={clsx(className, { [classes.isMoving]: isMoving })} TransitionProps={{ unmountOnExit: true }}
        >
            <AccordionSummary
                classes={{
                    content: classes.skillRow
                }}
            >
                <Zoom in={isMoving} unmountOnExit>
                    <DragHandleIcon />
                </Zoom>
                <SkillLogo picture={skillData.skill.picture} size="large" />
                <div className={classes.headerSkill}>
                    <RoseText style={{ fontSize: "1.2em", marginTop: 6 }}><b>{skillData.skill.title}</b></RoseText>
                    {skillData.deadline && <MutedText title={t("Deadline")} style={{ marginTop: 6 }}>{t("Deadline")}: <em>{parseISOToDate(skillData.deadline)}</em></MutedText>}
                </div>

                <Box display="flex" alignItems="center" width="100%" flex={1}>
                    <Box width="100%" m={1}>
                        <LinearProgress value={skillData.progress * 100} variant="determinate" style={{ height: 25, borderRadius: 5 }} />
                    </Box>
                    <MutedText><small>{Math.round(skillData.progress * 100)}%</small></MutedText>
                </Box>
                <Badge
                    variant="dot"
                    invisible={!skillData.skill_needs_update}
                    color="secondary"
                    overlap="circular"
                >
                    <MenuButton
                        component={IconButton}
                        onClick={e => e.stopPropagation()}
                        MenuListProps={{
                            onClick: e => e.stopPropagation()
                        }}
                        disabled={isMoving}
                        className={classes.optionsButton}
                        size="small"
                        variant="outlined"
                        menus={menus}
                    >
                        <MoreHorizIcon />
                    </MenuButton>
                </Badge>
            </AccordionSummary>
            <AccordionDetails>
                <Grid container spacing={3} alignItems="stretch">
                    <Grid item md={5}>
                        <Paper className={classes.listPaper}>
                            <Collapse in={loadingTopics}>
                                <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={60} />
                                <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={60} />
                                <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={60} />
                                <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={60} />
                                <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={60} />
                            </Collapse>
                            <Collapse in={!loadingTopics} unmountOnExit>
                                <TopicList topics={currentTopics} onDirectTest={handleDirectTest} onSkip={handleSkip} disableReactivate={loadingReactivate}
                                    disableDirectTest={loading} showSkip={showSkip} onReactivate={handleReactivate}
                                    selectedTopics={selectedTopics} onSelectedTopicsChange={handleSelectedTopics}
                                    skill={skillData}
                                />
                            </Collapse>
                        </Paper>
                    </Grid>
                    <Grid item md={7} xs={12}>
                        <Box textAlign="center">
                            {loadingTopics ?
                                <>
                                    <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={200} />
                                    <Skeleton animation="wave" variant='rect' style={{ marginBottom: 10 }} height={180} />
                                </>
                                :
                                selectedTopics.length > 0 ?
                                    loadingSelectedOerGroups ?
                                        <CircularProgress />
                                        :
                                        <>
                                            <Paper elevation={2} className={classes.oerHistoryPaper}>
                                                {selectedOerGroups === null ?
                                                    <Alert
                                                        severity='warning'
                                                    >
                                                        <em>{t("You have not finished any Educational Package for this topic.")}</em>
                                                    </Alert>
                                                    :
                                                    <>
                                                        <div
                                                            style={{
                                                                fontSize: "1.3rem",
                                                                padding: 16,
                                                                fontWeight: "bold"
                                                            }}
                                                        >
                                                            {t("Finished Educational Contents")}
                                                        </div>
                                                        <Divider />
                                                        {selectedOerGroups.map((oerg, index) =>
                                                            <React.Fragment key={index}>
                                                                <OerGroup
                                                                    elevation={0}
                                                                    skill={skillData}
                                                                    group={oerg}
                                                                    onDone={handleOerDone}
                                                                    onChange={handleOerChange}
                                                                    readOnly
                                                                    style={{ backgroundColor: "inherit" }}
                                                                    className={classes.finishedOerGroup}
                                                                />
                                                                <Divider
                                                                    style={{
                                                                        backgroundColor: "black",
                                                                        marginBottom: 26
                                                                    }}
                                                                />
                                                            </React.Fragment>
                                                        )}
                                                    </>
                                                }
                                            </Paper>
                                        </>
                                    :
                                    <Paper elevation={2} className={classes.oerPaper}>
                                        {isFinished ?
                                            <Box textAlign="center" p={5}>
                                                {t("Congradulations! This course is finished!")}
                                            </Box>
                                            : isExam ?
                                                <div style={{padding: 28}}>
                                                    <Paper
                                                        style={{
                                                            marginBottom: 12
                                                        }}
                                                    >
                                                        <Grid
                                                            container
                                                            style={{
                                                                alignItems: "center",
                                                                padding: "12px 2px"
                                                            }}
                                                        >
                                                            <Grid item xs={12} sm={10} style={{textAlign: "left", paddingLeft: 12}}>
                                                                <Typography>
                                                                    {t("I want to test myself and continue this topic in case of wrong answer.")}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={12} sm={2}>
                                                                <Button
                                                                    onClick={handleTest}
                                                                    color="primary"
                                                                    variant="outlined"
                                                                    startIcon={<AssignmentIcon />}
                                                                    style={{
                                                                        backgroundColor: "white",
                                                                        height: "100%",
                                                                    }}
                                                                >
                                                                    {t("Test")}
                                                                </Button>
                                                            </Grid>
                                                        </Grid>
                                                    </Paper>
                                                    <Paper>
                                                        <Grid
                                                            container
                                                            style={{
                                                                alignItems: "center",
                                                                padding: "12px 2px"
                                                            }}
                                                        >
                                                            <Grid item xs={12} sm={10} style={{textAlign: "left", paddingLeft: 12}}>
                                                                <Typography>
                                                                    {t("I want to skip the test and go to the next topic.")}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={12} sm={2}>
                                                                <Button
                                                                    onClick={() => handleSkip(null)}
                                                                    color="secondary"
                                                                    variant="outlined"
                                                                    startIcon={<SkipNextIcon />}
                                                                    style={{
                                                                        backgroundColor: "white",
                                                                        height: "100%",
                                                                    }}
                                                                >
                                                                    {t("Skip")}
                                                                </Button>
                                                            </Grid>
                                                        </Grid>
                                                    </Paper>
                                                </div>

                                                :
                                                activeOerGroup ?
                                                    <OerGroup
                                                        elevation={0}
                                                        skill={skillData}
                                                        group={activeOerGroup}
                                                        onDone={handleOerDone}
                                                        onChange={handleOerChange}
                                                        className={classes.ongoingOerGroup}
                                                        style={{ backgroundColor: "inherit" }}
                                                    />
                                                    :
                                                    <em>{t("Unfortunately there is no educational content to offer for this topic.")}</em>
                                        }
                                    </Paper>
                            }
                            {reports && !selectedTopics.length > 0 &&
                                <ToggleDiv
                                    title={t("Course Report")}
                                    className={classes.toggleDiv}
                                    titleClassName={classes.toggleDivTitle}
                                >
                                    <Grid container justifyContent="center" spacing={1} className={classes.wrapper}>
                                        <Grid item container className={classes.reportLine} spacing={1}>
                                            {reports.level_report && userData.settings.report_level &&
                                                <Grid item xs={12} md={6} lg={4}>
                                                    <Paper className={classes.reportItem}>
                                                        <ReportItem
                                                            image={`level${reports.level_report.progress_level}.jpg`}
                                                            tooltip={"Newbie -> Freshman -> Junior -> Senior -> Specialist -> Master"}
                                                            title={t("Your current level")}
                                                            alt={t("Your current level")}
                                                            value={reports.level_report.progress_level_title}
                                                        />
                                                    </Paper>
                                                </Grid>
                                            }
                                            {userData.settings.report_speed &&
                                                <Grid item xs={12} md={6} lg={4}>
                                                    <Paper className={classes.reportItem}>
                                                        <ReportItem
                                                            icon={SpeedIcon}
                                                            title={t("Your speed")}
                                                            alt={t("Your speed")}
                                                            tooltip={t("Number of topics you have passed per week")}
                                                            value={`${reports.topics_per_week} ${t("topics/week")}`}
                                                        />
                                                    </Paper>
                                                </Grid>
                                            }
                                            {userData.settings.report_inactivity &&
                                                <Grid item xs={12} md={6} lg={4}>
                                                    <Paper className={classes.reportItem}>
                                                        <ReportItem
                                                            icon={AirplanemodeInactiveIcon}
                                                            tooltip={t("Number of days from your last passed topic")}
                                                            title={t("Inactive since")}
                                                            alt={t("Inactive since")}
                                                            value={`${reports.inactive_days} ${t("days ago")}`}
                                                        />
                                                    </Paper>
                                                </Grid>
                                            }

                                        </Grid>
                                        {reports && reports.level_report && userData.settings.report_next_level &&
                                            <Grid item xs={12} className={classes.reportLine}>
                                                <Paper className={classes.reportItem}>
                                                    <LevelReport
                                                        reports={reports.level_report}
                                                        loading={loadingReports}
                                                        title={t("Your next level in this course")}
                                                        alt={t("Your next level in this course")}
                                                    />
                                                </Paper>
                                            </Grid>
                                        }
                                        {reports && reports.goal_report && userData.settings.report_deadline &&
                                            <Grid item xs={12} className={classes.reportLine}>
                                                <Paper className={classes.reportItem}>
                                                    <GoalReport reports={reports.goal_report} />
                                                </Paper>
                                            </Grid>
                                        }
                                    </Grid>
                                </ToggleDiv>
                            }
                        </Box>
                    </Grid>
                </Grid>
            </AccordionDetails>
        </Accordion>
    </>
    )
}

export default SkillView
