import { Button, LinearProgress, makeStyles, Tooltip } from '@material-ui/core';
import UnarchiveIcon from '@material-ui/icons/Unarchive';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { parseISOToDate } from '../../../../utils';
import SkillLogo from '../../../components/Logos/Skill';
import MutedText from '../../../components/Typography/Muted';
import RoseText from '../../../components/Typography/Rose';

const useStyles = makeStyles({
    root: {
        display: "flex",
        gap: 8,
        justifyContent: "flex-start",
        textAlign: 'left',
        textTransform: "none"
    },
    header: {
        flex: 1,
        width: 182,
    },
    title: {
        fontSize: "1.2em",
        marginTop: 6,
        fontWeight: "bold",
        textOverflow: "ellipsis",
        overflow: "hidden",
        whiteSpace: "nowrap",
        width: "100%",
    },
    right: {
        display: "flex",
        flexDirection: "column",
        gap: 4,
        alignItems: "center",
    }
})

function ArchivedSkill({ skill: skillData, component: propComponent, onUnarchive, onClick, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])

    const handleUnarchive = useCallback(
        (e) => {
            e.stopPropagation()
            e.preventDefault()
            if (onUnarchive) {
                onUnarchive(skillData)
            }
        },
        [skillData, onUnarchive]
    )

    const handleClick = useCallback(
        () => {
            if (onClick) {
                onClick(skillData)
            }
        },
        [skillData, onClick]
    )

    return <Tooltip
        title={t("Click to review the topics")}
        placement="top"
        arrow
    >
        <Button
            {...props}
            classes={{
                label: classes.root,
            }}
            onClick={handleClick}
        >
            <SkillLogo picture={skillData.skill.picture} size="large" />
            <div className={classes.header}>
                <RoseText className={classes.title} title={skillData.skill.title}>{skillData.skill.title}</RoseText>
                {skillData.deadline && <MutedText title={t("Deadline")} style={{ marginTop: 6 }}>{t("Deadline")}: <em>{parseISOToDate(skillData.deadline)}</em></MutedText>}
            </div>
            <div className={classes.right}>
                <LinearProgress
                    variant='determinate'
                    color='primary'
                    value={skillData.progress * 100}
                    title={`${t("Your progress in this skill: ")} ${Math.round(skillData.progress * 100)}%`}
                    style={{ height: 15, width: "100%", borderRadius: 5 }}
                />
                <br />
                <Button
                    startIcon={<UnarchiveIcon />}
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={handleUnarchive}
                >
                    {t("Unarchive")}
                </Button>
            </div>
        </Button>
    </Tooltip>
}

export default ArchivedSkill