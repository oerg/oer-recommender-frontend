import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, List, ListItem, ListItemIcon, ListItemText, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab';
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next';

function TopicsConfirmDialog({ topicsToAdd, topicsToRemove, open, onAccept, onCancel, ...props }) {
    const { t } = useTranslation(['dashboard']);

    const [selectedAddTopics, setSelectedAddTopics] = useState([])
    const [selectedRemoveTopics, setSelectedRemoveTopics] = useState([])

    useEffect(
        () => {
            setSelectedAddTopics([...topicsToAdd])
            setSelectedRemoveTopics([...topicsToRemove])
        },
        [topicsToAdd, topicsToRemove]
    );

    const acceptable = useMemo(
        () => (topicsToAdd.length > 0 || topicsToRemove.length > 0) 
            && (selectedRemoveTopics.length > 0 || selectedAddTopics.length > 0),
        [topicsToAdd, topicsToRemove, selectedAddTopics, selectedRemoveTopics]
    )

    const handleClose = useCallback(
        (e, reason) => {
            if (reason !== "backdropClick") {
                onCancel();
            }
        },
        []
    );

    const handleSelect = useCallback(
        (topic, setter) => {
            setter(
                topicsList => {
                    const pos = topicsList.indexOf(topic)
                    if (pos === -1) {
                        return [...topicsList, topic];
                    } else {
                        topicsList.splice(pos, 1);
                        return [...topicsList];
                    }
                }
            );
        },
        []
    );

    const handleAddTopic = useCallback(
        (topic) => () => {
            handleSelect(topic, setSelectedAddTopics)
        },
        [handleSelect]
    );

    const handleRemoveTopic = useCallback(
        (topic) => () => {
            handleSelect(topic, setSelectedRemoveTopics)
        },
        [handleSelect]
    );

    const handleAccept = useCallback(
        () => {
            onAccept(selectedAddTopics, selectedRemoveTopics)
        },
        [selectedAddTopics, selectedRemoveTopics, onAccept]
    )

    return (
        <Dialog {...props} open={open} onClose={handleClose}>
            <DialogTitle>{t("Update topic list")}</DialogTitle>
            <DialogContent dividers style={{ minWidth: 600 }}>
                <DialogContentText>
                    {topicsToAdd.length > 0 || topicsToRemove.length > 0 ?
                        t("Please confirm the changes in the topic list")
                        :
                        <Alert severity='success'>
                            {t("Your topics are already up to date.")}
                        </Alert>
                    }
                </DialogContentText>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>
                            {t("Add List")}
                        </Typography>
                        {topicsToAdd.length === 0 ?
                            <em>{t("No topics will be added")}</em>
                            :
                            <List>
                                {topicsToAdd.map(
                                    (topic, index) => <ListItem key={index} button dense onClick={handleAddTopic(topic)}>
                                        <ListItemIcon>
                                            <Checkbox edge="start" tabIndex={-1} disableRipple checked={selectedAddTopics.includes(topic)} />
                                        </ListItemIcon>
                                        <ListItemText primary={topic.title} secondary={selectedAddTopics.includes(topic) ? t("will be added") : ""} />
                                    </ListItem>
                                )}
                            </List>
                        }
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>
                            {t("Remove List")}
                        </Typography>
                        {topicsToRemove.length === 0 ?
                            <em>{t("No topics will be removed")}</em>
                            :
                            <List>
                                {topicsToRemove.map(
                                    (topic, index) => <ListItem key={index} button dense onClick={handleRemoveTopic(topic)}>
                                        <ListItemIcon>
                                            <Checkbox edge="start" tabIndex={-1} disableRipple checked={selectedRemoveTopics.includes(topic)} />
                                        </ListItemIcon>
                                        <ListItemText primary={topic.title} secondary={selectedRemoveTopics.includes(topic) ? t("will be removed") : ""} />
                                    </ListItem>
                                )}
                            </List>
                        }
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>{t("Cancel")}</Button>
                <Button color="primary" disabled={!acceptable} onClick={handleAccept}>{t("Ok")}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default TopicsConfirmDialog