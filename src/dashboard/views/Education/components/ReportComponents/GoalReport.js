import { Tooltip, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { parseDateTime } from '../../../../../utils';
import ProgressLine from './ProgressLine/ProgressLine';


export default function GoalReport({ reports, alt, loading, enabled=true, width = 500 }) {
    const { t } = useTranslation(['dashboard']);

    const finished_topics_percentage = useMemo(
        () => reports ? reports.finished_topics * 100 / reports.all_topics : null,
        [reports]
    )

    const aligned_progress = useMemo(
        () => reports ? 100 - (reports.progress_at_deadline_creation) : null,
        [reports]
    )

    const prog_at_deadline = useMemo(
        () => reports ? reports.progress_at_deadline_creation : null,
        [reports]
    )

    const expected_progress_percentage = useMemo(
        () => reports.remaining_days > 0 ? (((reports.estimated_days - reports.remaining_days) / reports.estimated_days) * aligned_progress) + prog_at_deadline : 100,
        [reports, aligned_progress]
    )

    return (
        <>
            {loading ?
                <>
                    <Skeleton variant="text" />
                    <Skeleton variant="rect" />
                </>
                :
                <div>
                    <Typography variant="body1" style={{
                        fontSize: "1.2rem",
                        fontWeight: "bold"
                    }}>
                        {t("Your progress based on the deadline")}
                    </Typography>
                    <div style={{ display: "flex", gap: 12, alignItems: "center", justifyContent: "center" }}>
                        <ProgressLine
                            label="Two visual percentages"
                            backgroundColor="lightgrey"
                            visualParts={[
                                {
                                    percentage: `${finished_topics_percentage}%`,
                                    text: `${finished_topics_percentage.toFixed(2)}%`,
                                    tooltip: `${t("Passed topics:")} ${reports.finished_topics}, ${t("All topics:")} ${reports.all_topics}`,
                                    color: enabled ? "#c8e6c9" : "#bcb7b7"
                                }
                            ]}
                            visualLines={[
                                {
                                    percentage: `${expected_progress_percentage}%`,
                                    color: enabled ? "green" : "#bcb7b7",
                                    tooltip: "You are expected to be here based on the deadline",
                                }
                            ]}
                            style={{
                                width: width
                            }}
                        />
                        <Tooltip
                            arrow
                            interactive
                            title={`${t("Start date:")} ${parseDateTime(reports.start_date, "yyyy-MM-dd")}, ${t("Deadline")} ${parseDateTime(reports.deadline, "yyyy-MM-dd")}`}
                        >
                            <img
                                height={55}
                                src={process.env.PUBLIC_URL + `/images/finish.png`}
                                variant="rounded"
                                alt={alt}
                                style={{
                                    margin: "0 8px",
                                    border: "3px solid #00535f",
                                    borderRadius: "50%",
                                    filter: enabled ? 'grayscale(0%)' : 'grayscale(100%)'
                                }}
                            />
                        </Tooltip>

                    </div>
                </div>
            }
        </>
    )
}
