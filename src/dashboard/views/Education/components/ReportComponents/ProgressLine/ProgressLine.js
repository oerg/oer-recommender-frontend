import { Tooltip } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import "./ProgressLine.css";

const ProgressLine = ({
    backgroundColor = "#e5e5e5",
    visualParts = [
        {
            percentage: "0%",
            color: "white"
        }
    ],
    visualLines = [

    ],
    style
}) => {
    const [widths, setWidths] = useState(
        visualParts.map(() => {
            return 0;
        })
    );

    useEffect(() => {
        requestAnimationFrame(() => {
            setWidths(
                visualParts.map(item => {
                    return item.percentage;
                })
            );
        });
    }, [visualParts]);

    return (
        <div
            className="progressRoot"
        >
            <div
                className="progressVisualFull"
                style={{
                    backgroundColor,
                    ...style
                }}
            >
                {visualParts.map((item, index) => (
                    item.percentage !== "0%" &&
                    <Tooltip
                        arrow
                        interactive
                        title={item.tooltip}
                        key={index}
                        style={{
                            width: widths[index],
                            backgroundColor: item.color
                        }}
                        className="progressVisualPart"
                    >
                        <div className="partContent">{item.text}</div>
                    </Tooltip>
                ))}
            </div>
            {visualLines.map(
                (item, index) => (
                    <Tooltip
                        arrow
                        interactive
                        title={item.tooltip}
                        key={index}
                        style={{
                            backgroundColor: item.color,
                            position: "absolute",
                            left: item.percentage,
                        }}
                        className="progressVisualLine"
                    >
                        <div />
                    </Tooltip>
                )
            )}
        </div>
    );
};

export default ProgressLine;
