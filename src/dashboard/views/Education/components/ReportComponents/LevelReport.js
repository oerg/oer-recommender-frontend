import { Tooltip, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import ProgressLine from './ProgressLine/ProgressLine';

export default function LevelReport({ reports, title, alt, loading, enabled=true, width = 500 }) {
    const { t } = useTranslation(['dashboard']);

    const level = useMemo(
        () => reports ? reports.progress_level : null,
        [reports]
    )

    const topics_in_current_level = useMemo(
        () => reports ? reports.topics_in_current_level : null,
        [reports]
    )

    const passedTopics = useMemo(
        () => reports ? reports.topics_in_current_level - reports.topics_to_next_level : null,
        [reports]
    )

    const ongoingTopics = useMemo(
        () => reports ? reports.ongoing_topics : null,
        [reports]
    )

    const futureTopics = useMemo(
        () => reports ? reports.topics_to_next_level - reports.ongoing_topics : null,
        [reports]
    )

    const passedTopicsPercentage = useMemo(
        () => passedTopics * 100 / topics_in_current_level,
        [passedTopics, topics_in_current_level]
    )

    const ongoingTopicsPercentage = useMemo(
        () => ongoingTopics * 100 / topics_in_current_level,
        [ongoingTopics, topics_in_current_level]
    )

    const futureTopicsPercentage = useMemo(
        () => futureTopics * 100 / topics_in_current_level,
        [futureTopics, topics_in_current_level]
    )    

    return (
        <>
            {loading ?
                <>
                    <Skeleton variant="text" />
                    <Skeleton variant="rect" />
                </>
                :
                level !== 5 ?
                    <div>
                        <Typography variant="body1" style={{
                            fontSize: "1.2rem",
                            fontWeight: "bold"
                        }}>
                            {t("Road to your next level")}
                        </Typography>

                        <div style={{ display: "flex", gap: 12, alignItems: "center", justifyContent: "center" }}>
                            <ProgressLine
                                label="Two visual percentages"
                                backgroundColor="lightgrey"
                                visualParts={[
                                    {
                                        percentage: `${passedTopicsPercentage}%`,
                                        tooltip: `${passedTopics} ${t("passed topics")}`,
                                        text: `${passedTopics} ${t("passed topics")}`,
                                        color: enabled ? "#c8e6c9" : "#bcb7b7"
                                    },
                                    {
                                        percentage: `${ongoingTopicsPercentage}%`,
                                        tooltip: `${ongoingTopics} ${t("ongoing topics")}`,
                                        text: `${ongoingTopics} ${t("ongoing topics")}`,
                                        color: enabled ? "#ffee58" : "#bcb7b7"
                                    },
                                    {
                                        percentage: `${futureTopicsPercentage}%`,
                                        tooltip: `${futureTopics} ${t("upcoming topics")}`,
                                        text: `${futureTopics} ${t("upcoming topics")}`,
                                        color: enabled ? "#bbdefb" : "#bcb7b7"
                                    },
                                ]}
                                style={{
                                    width: width
                                }}
                            />

                            <Tooltip
                                arrow
                                interactive
                                title={title}
                            >
                                <img
                                    height={50}
                                    src={process.env.PUBLIC_URL + `/images/levels/level${level + 1}.jpg`}
                                    variant="rounded"
                                    alt={alt}
                                    style={{
                                        filter: enabled ? `grayscale(${100-passedTopicsPercentage}%)` :'grayscale(100%)'
                                    }}
                                />
                            </Tooltip>
                        </div>
                    </div>

                    :
                    <img width={112} height={79} src={process.env.PUBLIC_URL + `/images/levels/level${level}.jpg`} variant="rounded" alt={"Current Level"} />
            }
        </>
    )
}
