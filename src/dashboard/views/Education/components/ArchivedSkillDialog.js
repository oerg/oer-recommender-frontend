import { Button, Collapse, Dialog, DialogActions, DialogContent, DialogTitle, LinearProgress, makeStyles } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import TopicList from './TopicList'
import { loadMySkill } from './utils'

const useStyles = makeStyles({
    body: {
        minWidth: 400,
    }
})

function ArchivedSkillDialog({ skill: skillData, onClose, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])

    const [loading, setLoading] = useState(false)
    const [topics, setTopics] = useState([])

    useEffect(
        () => {
            if (skillData) {
                setLoading(true)
                loadMySkill(
                    skillData.skill.id,
                    skillDetails => {
                        setTopics(skillDetails.user_skill_user_topic)
                    },
                    null,
                    () => setLoading(false)
                )
            }
        },
        [skillData]
    )

    return <Dialog 
        scroll='paper'
        classes={{
            paper: classes.body
        }}
        {...props}
        onClose={onClose}
    >
        <DialogTitle>
            {skillData && skillData.skill.title}
        </DialogTitle>
        <DialogContent
            dividers
            style={{
                paddingTop: 0
            }}
        >
            <Collapse in={loading}>
                <LinearProgress />
            </Collapse>
            <Collapse in={!loading}>
                <TopicList
                    skill={skillData}
                    topics={topics}
                    showSkip={false}
                    noOrderChange
                    allOpen
                />
            </Collapse>
        </DialogContent>
        <DialogActions>
            <Button
                variant='outlined'
                color='primary'
                onClick={onClose}
            >
                {t("Close")}
            </Button>
        </DialogActions>
    </Dialog>
}

export default ArchivedSkillDialog