import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Button, Dialog, DialogActions, DialogContent, Checkbox, List, DialogContentText, DialogTitle, ListItem, ListItemText, ListItemIcon, Typography, FormControlLabel, ListItemSecondaryAction, IconButton, Divider } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { Alert } from '@material-ui/lab';
import Help from '../../../components/Help';
import { preAddress } from '../../../../App';
import { useLanguage } from '../../../../i18n';
import LaunchIcon from '@material-ui/icons/Launch';

function SkillConfirmDialog(rawProps) {
    const {
        skills,
        open,
        onAccept,
        onCancel,
        isAdd,
        goal,
        showFollow = true,
        ...props
    } = rawProps;

    const [selectedSkills, setSelectedSkills] = useState([]);
    const [alreadyAddedSkills, setAlreadyAddedSkills] = useState([])
    const [follow, setFollow] = useState(showFollow)

    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage()

    const handleClose = useCallback(
        (e, reason) => {
            if (reason !== "backdropClick") {
                onCancel();
            }
        },
        [onCancel]
    );

    const handleSelect = useCallback(
        (skill) => () => {
            setSelectedSkills(
                ss => {
                    const pos = ss.indexOf(skill)
                    if (pos === -1) {
                        return [...ss, skill];
                    } else {
                        ss.splice(pos, 1);
                        return [...ss];
                    }
                }
            );
        },
        []
    );

    useEffect(() => {
        setSelectedSkills(skills.filter(skill => skill.is_current))
        setAlreadyAddedSkills(skills.filter(skill => skill.is_current))
    }, [skills])


    const handleAccept = useCallback(
        () => {
            onAccept(selectedSkills, isAdd, follow && goal)
        },
        [selectedSkills, onAccept, isAdd, goal, follow]
    )

    const handleFollowChange = useCallback(
        e => {
            setFollow(e.target.checked)
        },
        []
    )

    const handleSelectAll = useCallback(
        e => {
            if (selectedSkills.length < skills.length) {
                setSelectedSkills([...skills])
            } else {
                setSelectedSkills([...alreadyAddedSkills])
            }
        },
        [skills, selectedSkills, alreadyAddedSkills]
    )

    return (
        <Dialog {...props} open={open} onClose={handleClose}>
            <DialogTitle>{goal && goal.title}</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    {
                        isAdd ?
                            <>
                                <Alert severity="info" variant="standard">
                                    {t("This is a journey")}
                                    <Help style={{ fontSize: 20 }}>
                                        {t("A Journey includes related courses to help you achieve higher-level learning goals like achieving knowledge for a job or a field of study.")}
                                    </Help>
                                </Alert>
                                <Typography style={{ marginTop: 24 }}>
                                    {t("The following courses are part of the journey you are addding, select the courses you want to add to your curriculum.")}
                                </Typography>
                            </>
                            :
                            t("The following courses are part of the journey you are removing, select the courses you want to remove from your curriculum.")
                    }
                </DialogContentText>
                <List>
                    {alreadyAddedSkills.length < skills.length && skills.length > 1 &&
                        <ListItem
                            button
                            onClick={handleSelectAll}
                        >
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    tabIndex={-1}
                                    disableRipple
                                    checked={selectedSkills.length > alreadyAddedSkills.length}
                                    indeterminate={selectedSkills.length > alreadyAddedSkills.length && selectedSkills.length !== skills.length}
                                    title={selectedSkills.length < skills.length ? t("Select all") : t("Deselect all")}
                                />
                            </ListItemIcon>
                            <ListItemText primary={<i>{selectedSkills.length < skills.length ? t("Select all skills") : t("Deselect all skills")}</i>} />
                        </ListItem>
                    }
                    <Divider />
                    {skills.map(
                        (s, index) => <ListItem key={index} button dense onClick={handleSelect(s)} disabled={alreadyAddedSkills.includes(s)}>
                            <ListItemIcon>
                                <Checkbox edge="start" tabIndex={-1} disableRipple indeterminate={s.is_archived} checked={selectedSkills.includes(s)} disabled={alreadyAddedSkills.includes(s)} />
                            </ListItemIcon>
                            <ListItemText
                                primary={isAdd ? s.skill_title : s.title}
                                secondary={
                                    selectedSkills.includes(s) ?
                                        isAdd ?
                                            alreadyAddedSkills.includes(s) ?
                                                s.is_archived ?
                                                    t("archived")
                                                    :
                                                    t("already added")
                                                : t("will be added")
                                            : t("will be removed")
                                        : ""}
                            />
                            <ListItemSecondaryAction>
                                <IconButton
                                    edge="end"
                                    aria-label="open"
                                    href={`${preAddress}/${systemLanguage}/dashboard/skills/${s.skill}`}
                                    target="_blank"
                                >
                                    <LaunchIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
            </DialogContent>
            {isAdd && showFollow &&
                <FormControlLabel
                    style={{
                        marginLeft: 12,
                        marginTop: 8
                    }}
                    control={
                        <Checkbox
                            checked={follow}
                            onChange={handleFollowChange}
                            color="primary"
                        />
                    }
                    label={t("Follow this journey and be norified in case of updates")}
                />
            }
            <DialogActions>
                <Button color="primary" onClick={handleAccept}>{t("Ok")}</Button>
            </DialogActions>
        </Dialog>
    )
}

SkillConfirmDialog.propTypes = {
    skills: PropTypes.array.isRequired,
    open: PropTypes.bool.isRequired,
    onAccept: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
}

export default SkillConfirmDialog

