import { Collapse, IconButton, List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, ListSubheader, makeStyles } from '@material-ui/core';
import { blue, green, yellow } from '@material-ui/core/colors';
import DragHandleIcon from "@material-ui/icons/DragHandle";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ReplayIcon from '@material-ui/icons/Replay';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import arrayMove from 'array-move';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Container, Draggable } from 'react-smooth-dnd';
import TopicStatusIcon from '../../../components/TopicStatusIcon';
import { calculateOrder, saveTopicOrder } from './utils';


const useStyles = makeStyles({
    root: ({ height }) => ({
        maxHeight: `${height}px`,
        "&>li": {
            marginTop: 8,
        },
        "&>li:first-child, > li:nth-child(2)": {
            margin: 0,
        },
    }),
    AS: { //assigned row
        backgroundColor: blue[100],
    },
    FI: { // finished row
        backgroundColor: green[100],
        "&:hover": {
            backgroundColor: green[50],
        }
    },
    selectedFI: {
        backgroundColor: `${green[400]} !important`,
    },
    ON: { // ongoing row
        backgroundColor: yellow[100],
        "&:hover": {
            backgroundColor: yellow[50],
        }
    },
    selectedON: {
        backgroundColor: `${yellow[400]} !important`,
    },
    EX: { // exam row
        backgroundColor: blue[300],
        "&:hover": {
            backgroundColor: blue[200],
        }
    },
    topicGroup: {
        padding: 0,
        listStyle: "none",
    },
    topicGroupTitle: {
        backgroundColor: "#EEE",
        textAlign: "center",
        cursor: "pointer",
        display: "flex",
        alignItems: "center",
        "& svg": {
            marginRight: 8,
        }
    },
    dragHandle: {
        cursor: "grab",
    },
    dragging: {
        cursor: "grabbing"
    }
})

function TopicList({ skill, topics: propTopics, className, height, onDirectTest, onSkip, onReactivate, disableReactivate, disableDirectTest,
    selectedTopics, onSelectedTopicsChange, showSkip, noOrderChange = false, allOpen = false, ...props }) {
    const classes = useStyles({ height });
    const { t } = useTranslation(['dashboard']);
    const [orderedTopics, setOrderedTopics] = useState(null)
    const [alreadyScrolled, setAlreadyScrolled] = useState(false)
    const [collapsed, setCollapsed] = useState({})

    useEffect(
        () => {
            const fi = propTopics.filter(t => t.user_topic.status === 'FI')
            const on = propTopics.filter(t => t.user_topic.status === 'ON')
            const ex = propTopics.filter(t => t.user_topic.status === 'EX')
            const as = propTopics.filter(t => t.user_topic.status === 'AS')

            setCollapsed({
                [t("Passed Topics")]: !allOpen && (on.length > 0 || ex.length > 0 || as.length > 0),
                [t("Ongoing Topics")]: false,
                [t("Upcoming Topics")]: !allOpen && (on.length > 0 || ex.length > 0),
            })

            setOrderedTopics(
                () => {
                    return [
                        {
                            label: t("Passed Topics"),
                            items: fi,
                        },
                        {
                            label: t("Ongoing Topics"),
                            items: [...on, ...ex],
                        },
                        {
                            label: t("Upcoming Topics"),
                            items: as.sort((a, b) => a.topic_order - b.topic_order),
                        }
                    ]
                }
            )
        },
        [propTopics, allOpen, t]
    );

    const handleScroll = useCallback((topic) => (item) => {
        if (!alreadyScrolled && item && topic.user_topic.status === 'ON') {
            item.scrollIntoView({
                block: "end",
                inline: "nearest",
                behavior: "smooth",
            });
            setAlreadyScrolled(true)
        }
    }, [alreadyScrolled]);

    const handleReactivate = useCallback(
        (topic) => () => {
            onReactivate(topic);
        },
        [onReactivate]
    )

    const isSelected = useCallback(
        (topic) => selectedTopics && (
            selectedTopics.length === 0 ?
                topic.user_topic.status === "ON" || topic.user_topic.status === "EX"
                :
                selectedTopics.some(st => st.user_topic.topic_id === topic.user_topic.topic_id)
        ),
        [selectedTopics]
    )

    const handleTopicClick = useCallback(
        (topic) => () => {
            if (onSelectedTopicsChange) {
                if (topic.user_topic.status === "ON" || topic.user_topic.status === "EX") {
                    onSelectedTopicsChange([])
                } else {
                    onSelectedTopicsChange([topic])
                }
            }
        },
        [onSelectedTopicsChange]
    )

    const handleDrop = useCallback(
        ({ removedIndex, addedIndex }) => {
            if (removedIndex !== addedIndex) {
                setOrderedTopics(
                    oldOrderedTopics => {
                        const items = arrayMove(oldOrderedTopics[2].items, removedIndex, addedIndex)
                        const newOrder = calculateOrder(items, addedIndex)
                        items[addedIndex] = {
                            ...items[addedIndex],
                            topic_order: newOrder,
                        }

                        saveTopicOrder(
                            skill.id,
                            items[addedIndex].topic_id,
                            newOrder
                        )

                        return [
                            oldOrderedTopics[0],
                            oldOrderedTopics[1],
                            { ...oldOrderedTopics[2], items }
                        ]
                    }
                )
            }
        },
        [skill]
    )

    const handleHeaderClick = useCallback(
        (topics) => () => {
            setCollapsed(
                collapsed => {
                    const key = topics.label
                    if (collapsed[key]) {
                        collapsed[key] = false
                        return { ...collapsed }
                    }
                    collapsed[key] = true
                    return { ...collapsed }
                }
            )
        },
        []
    )

    return (
        <List className={clsx(classes.root, className)} subheader={<li />} {...props}>
            {orderedTopics && orderedTopics.slice(0, -1).map(
                (topics, index) => (topics.items.length > 0 &&
                    <li key={index}>
                        <ul className={classes.topicGroup}>
                            <ListSubheader className={classes.topicGroupTitle} onClick={handleHeaderClick(topics)}>
                                {collapsed[topics.label] ?
                                    <KeyboardArrowRightIcon />
                                    :
                                    <ExpandMoreIcon />
                                }
                                {topics.label} ({topics.items.length})
                            </ListSubheader>
                            <Collapse in={!collapsed[topics.label]}>
                                {topics.items.map(
                                    (topic, ind) => (
                                        <ListItem key={ind} className={classes[topic.user_topic.status]} ref={handleScroll(topic)}
                                            button={Boolean(onSelectedTopicsChange)}
                                            selected={isSelected(topic)}
                                            onClick={handleTopicClick(topic)}
                                            divider
                                            classes={{
                                                selected: classes[`selected${topic.user_topic.status}`]
                                            }}
                                        >
                                            <ListItemIcon>
                                                <TopicStatusIcon status={topic.user_topic.status} />
                                            </ListItemIcon>
                                            <ListItemText primary={topic.user_topic.topic_title} />
                                            <ListItemSecondaryAction>
                                                {topic.user_topic.status === 'ON' && showSkip &&
                                                    <IconButton size="small" title={t("I want to skip this topic.")} onClick={() => onSkip(topic.user_topic.topic_id)} disabled={disableDirectTest}>
                                                        <SkipNextIcon />
                                                    </IconButton>
                                                }
                                                {topic.user_topic.status === "FI" && Boolean(onReactivate) &&
                                                    <IconButton size="small" title={t("Re-activate this topic")} onClick={handleReactivate(topic)} disabled={disableReactivate}>
                                                        <ReplayIcon />
                                                    </IconButton>
                                                }
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    )
                                )}
                            </Collapse>
                        </ul>
                    </li>
                )
            )}
            {orderedTopics && orderedTopics.slice(-1).map( // only Assigned topics
                (topics, index) => (topics.items.length > 0 &&
                    <li key={index}>
                        <ul className={classes.topicGroup}>
                            <ListSubheader className={classes.topicGroupTitle} onClick={handleHeaderClick(topics)}>
                                {collapsed[topics.label] ?
                                    <KeyboardArrowRightIcon />
                                    :
                                    <ExpandMoreIcon />
                                }
                                {topics.label} ({topics.items.length})
                            </ListSubheader>
                            <Collapse in={!collapsed[topics.label]}>
                                <Container dragHandleSelector=".drag-handle" lockAxis="y" onDrop={handleDrop} dragClass={classes.dragging}>
                                    {topics.items.map(
                                        (topic, ind) => (
                                            <Draggable key={ind}>
                                                <ListItem
                                                    className={classes[topic.user_topic.status]}
                                                    ref={handleScroll(topic)}
                                                    selected={isSelected(topic)}
                                                    divider
                                                    classes={{
                                                        selected: classes[`selected${topic.user_topic.status}`]
                                                    }}
                                                >
                                                    <ListItemIcon>
                                                        <TopicStatusIcon status={topic.user_topic.status} />
                                                    </ListItemIcon>
                                                    <ListItemText primary={topic.user_topic.topic_title} />
                                                    {!noOrderChange && <ListItemSecondaryAction>
                                                        <DragHandleIcon className={clsx('drag-handle', classes.dragHandle)} />
                                                    </ListItemSecondaryAction>}
                                                </ListItem>
                                            </Draggable>
                                        )
                                    )}
                                </Container>
                            </Collapse>
                        </ul>
                    </li>
                )
            )}
        </List>
    )
}

TopicList.propTypes = {
    topics: PropTypes.array.isRequired,
    className: PropTypes.string,
    height: PropTypes.number,
    onDirectTest: PropTypes.func,
    disableDirectTest: PropTypes.bool,
    onReactivate: PropTypes.func,
    disableReactivate: PropTypes.bool,
}

TopicList.defaultProps = {
    height: 400,
    onDirectTest: () => { },
    disableDirectTest: false,
    disableReactivate: false,
}

export default TopicList
