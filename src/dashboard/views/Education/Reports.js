import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { ResponsiveContainer, XAxis, YAxis, Bar, Tooltip, Line, ComposedChart, Legend } from 'recharts';
import { Box, CircularProgress } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import { useTranslation } from 'react-i18next';

function TopicTooltip({active, label, payload, ...props}) {
    return active && (
        <Box bgcolor="white" border={1} borderColor="#AAA" p={1}>
            <p>
                {label}: <em>{payload[0].value}</em>
            </p>
        </Box>
    )
}

function EducationalReports({ loading, reports, loadNow, onTopicClick, ...props }) {
    const r = useMemo(() => reports ? reports.topic_reports.filter(rt => rt.finished_count > 0) : null, [reports]);
    const { t } = useTranslation([ 'dashboard' ]);

    return (
        loading ?
            <Box p={3} mt={2} textAlign="center">
                <CircularProgress />
            </Box>
        :
            r && r.length > 0 ?
            (loadNow && 
            <Box overflow="scroll" height="300px" width="100%" mt={2} {...props}>
                <ResponsiveContainer width="95%" minHeight={250}>
                    <ComposedChart data={r} layout="vertical">
                        <Legend width={150} payload={[{"dataKey": "finished_count", "type": "rect", "color": "#a5d6a7", "value": t("Completed Resources per Topic")}]}
                         wrapperStyle={{ top: 40, right: 20, backgroundColor: '#f5f5f5', border: '1px solid #d5d5d5', borderRadius: 3 }} />
                        <XAxis type="number" dataKey="finished_count" domain={[0, 'dataMax + 1']} 
                        allowDecimals={false} orientation="top" />
                        <Tooltip content={<TopicTooltip />} />
                        <YAxis type="category" dataKey="topic_title" tick={false} />
                        <Bar dataKey="finished_count" fill={green[200]} onClick={onTopicClick} maxBarSize={20} />
                        <Line type="monotone" dataKey="finished_count" stroke={green[900]} />
                    </ComposedChart>
                </ResponsiveContainer>
            </Box>
            )
            :
            <Box height="300px" width="100%" mt={2} p={1} textAlign="center" {...props}>
                <em>{t("You haven't finished any educational content yet.")}</em>
            </Box>
    )
}

EducationalReports.propTypes = {
    loading: PropTypes.bool,
    reports: PropTypes.object,
    loadNow: PropTypes.bool,
    onTopicClick: PropTypes.func,
}

EducationalReports.defaultProps = {
    loadNow: true,
}

export default EducationalReports
