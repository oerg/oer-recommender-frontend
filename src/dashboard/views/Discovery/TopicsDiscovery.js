import { CircularProgress, Grid, IconButton, InputAdornment, ListItem, ListItemText, makeStyles, Paper, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { FixedSizeList } from 'react-window';
import { useLanguage } from '../../../i18n';
import LangIcon from '../../components/LangIcon';
import { cancelLoadPart, loadPart } from './utils';

const useStyles = makeStyles(theme => ({
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    hasPadding: {
        padding: theme.spacing(1),
    },
    loadingPanel: {
        textAlign: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    }
}));


function TopicsDiscovery() {
    const classes = useStyles();
    const [topics, setTopics] = useState(null);
    const [topicsLoading, setTopicsLoading] = useState(false);
    const [searchString, setSearchString] = useState('');
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const renderRow = useCallback(({ index, style }) => {
        return (
            <ListItem button style={style} key={index} to={`/${language}/dashboard/discovery/topics/${topics[index].id}`} component={Link}>
                <ListItemText primary={<span>{topics[index].title} <LangIcon language={topics[index].lang} small /></span>} />
            </ListItem>
        );
    }, [topics, language])

    useEffect(() => { // do the search
        if (searchString.length === 0) {
            cancelLoadPart();
            setTopics(null);
            setTopicsLoading(false);
        } else {
            setTopicsLoading(true);
            loadPart(
                "skills/topics",
                searchString, res => {
                    setTopics(res.data.results);
                    setTopicsLoading(false);
                })
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, [])

    const handleAddTopic = useCallback(() => {
        history.push(`/${language}/dashboard/discovery/topics/add?init=${searchString}`)
    }, [history, searchString])

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} md={8} lg={6}>
                <Paper elevation={1} className={classes.hasPadding}>
                    <TextField label={t("Search topics...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                        InputProps={{
                            endAdornment:
                                <InputAdornment position="end">
                                    <IconButton title={t("Add a new course")} className={classes.newButton} onClick={handleAddTopic}>
                                        <AddIcon />
                                    </IconButton>
                                </InputAdornment>
                        }} autoFocus />
                    {topicsLoading ?
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                        :
                        topics && (
                            topics.length === 0 ?
                                <div className={classes.loadingPanel}>
                                    <i>{t("No match found...")}</i>
                                </div>
                                :
                                <FixedSizeList height={350} width="100%" itemCount={topics.length} itemSize={50}>
                                    {renderRow}
                                </FixedSizeList>
                        )}
                </Paper>
            </Grid>
            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-topic.jpg`} width="70%" alt="" style={{ marginTop: "60px" }} />
        </Grid>
    )
}

export default TopicsDiscovery
