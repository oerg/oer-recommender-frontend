import { Box, Grid, IconButton, LinearProgress, Paper } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import OerInput from '../../../components/OerInput'
import useStyles from '../../styles/SkillsStyle'
import { updateOer } from './utils'

function Edit(rawProps) {
    const { 
        navHistory,
    } = rawProps;
    const { id: oerId } = useParams();

    const classes = useStyles();
    const history = useHistory();
    const language = useLanguage();
    const { t } = useTranslation([ 'dashboard' ]);

    const [oer, setOer] = useState({});
    const [loading, setLoading] = useState(false);

    useEffect(
        () => {
            setOer({ autoload_id: oerId })
        },
        [ oerId ]
    );

    const handleChange = useCallback(
        (oerData, isSave) => {
            setOer(oerData);
            if (isSave) {
                setLoading(true);
                updateOer(
                    oerData.id,
                    oerData,
                    () => history.push(`/${language}/dashboard/discovery/oers/${oerData.id}`)
                )
            }
        },
        [],
    );

    return (
        <Grid container justifyContent="center">
            <Grid item xs={12}>
                <DiscoveryNav navHistory={navHistory} />
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Editing Educational Content")}
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Grid container justifyContent="center" style={{ margin: "auto" }}>
                            <Grid item xs={12} sm={9} md={8} className={classes.formField}>
                                <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                                    <OerInput
                                        alertOnExist={false}
                                        value={oer}
                                        onChange={handleChange}
                                        showCancel={false}
                                        saveLabel={t("Save")}
                                        urlReadOnly
                                        persistentData={{}}
                                    />
                                    { loading && <LinearProgress /> }
                                </Paper>
                            </Grid>
                        </Grid>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Edit.propTypes = {

}

export default Edit

