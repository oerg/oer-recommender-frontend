import { Box, CircularProgress, Grid, IconButton, Paper, Table, TableCell, TableRow } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import clsx from 'classnames'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import { allFormatTypes } from '../../../../data'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import GridContainer from '../../../components/Grid/GridContainer'
import GridItem from '../../../components/Grid/GridItem'
import VotingTable from '../../../components/Vote/VotingTable'
import useStyles from '../../styles/SkillsStyle'
import { formatLength, getOer } from './utils'

function Oers({ onNav, navHistory }) {
    const classes = useStyles();
    const { id: oerId } = useParams();

    const [isLoading, setLoading] = useState(true);  
    const [criticalError, setCriticalError] = useState(null);
    const [oer, setOer] = useState([]);
    const [oerGroups, setOerGroups] = useState([]);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();


    const strategies = useMemo(
        () => ({
            "TE": t("Theory and example"),
            "TH": t("Theory"),
            "EX": t("Example")
        }),
        [ t ]
    );

    const details = useMemo(
        () => ({
            "LO": t("Low"),
            "ME": t("Medium"),
            "HI": t("High"),
        }),
        [t]
    );

    useEffect(() => { // load oerGroup and suggestions
        setLoading(true);
        getOer(oerId, (data, suggestions) => { // load
            setOer(data);
            setOerGroups(data.oer_group);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Oer was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
    }, [oerId]);

    const formatOerGroupLink = useCallback(o => {
        return `/${language}/dashboard/discovery/oer-groups/${o.id}`
    }, [language]);

    const formatType = useMemo(
        () => {
            if (oer['format_type']) {
                return allFormatTypes.find(f => f.code === oer['format_type']);
            }
            return null
        },
        [ oer ]
    );

    const formattedLength = useMemo(
        () => {
            const formatter = new Intl.NumberFormat('en-US',
                        { minimumIntegerDigits: 2 });
            const { hours, minutes, seconds } = formatLength(oer["length"]);
            return `${formatter.format(hours)}:${formatter.format(minutes)}:${formatter.format(seconds)}`
        },
        [ oer ]
    )

    return (
        <>
        {isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
        :
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <>
        <DiscoveryNav onNav={onNav} navHistory={navHistory} oer={oer} />
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {oer['title']}
                            <Box component="span" ml={2}>
                                { (oer.is_contributor || oer.is_admin) &&
                                    <IconButton style={{ color: "white" }} title={t("Edit Metadata")} size="small" component={Link} to={`/${language}/dashboard/discovery/oers/edit/${oer.id}`}>
                                        <EditIcon />
                                    </IconButton>
                                }
                            </Box>
                        </h3>
                        <h4 className={classes.cardCategoryWhite}>
                            {t("Educational Content")}
                        </h4>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
                            <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                <Grid item xs={12} md={6} className={classes.col}>
                                    <Paper className={clsx(classes.hasPadding, classes.hasYMargin)}>
                                        <Table>
                                            <TableRow>
                                                <TableCell component="th">URL</TableCell>
                                                <TableCell><a href={oer['url']} target="_blank" rel="noopener noreferrer">{oer['url']}</a></TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th">{t("Description")}</TableCell>
                                                <TableCell>
                                                    <div className={classes.description}>
                                                        {oer['description'] && oer['description'].length > 0 ? oer['description'] : <i>No Description</i>}
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th">{t("Author")}</TableCell>
                                                <TableCell>
                                                    <div className={classes.description}>
                                                        {oer.author}
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                            { oer['resource'] &&
                                                <TableRow>
                                                    <TableCell component="th">{t("Resource")}</TableCell>
                                                    <TableCell>{oer['resource']}</TableCell>
                                                </TableRow> 
                                            }
                                            { oer["length"] &&
                                                <TableRow>
                                                    <TableCell component="th">{t("Length")}</TableCell>
                                                    <TableCell>{formattedLength}</TableCell>
                                                </TableRow> 
                                            }
                                            { oer['strategy'] && strategies[oer['strategy']] &&
                                            <TableRow>
                                                <TableCell component="th">{t("Strategy")}</TableCell>
                                                <TableCell>{strategies[oer['strategy']]}</TableCell>
                                            </TableRow>
                                            }
                                            { oer['detail'] && details[oer['detail']] &&
                                            <TableRow>
                                                <TableCell component="th">{t("Details")}</TableCell>
                                                <TableCell>{details[oer['detail']]}</TableCell>
                                            </TableRow>
                                            }
                                            { formatType &&
                                            <TableRow>
                                                <TableCell component="th">{t("Format Type")}</TableCell>
                                                <TableCell>{formatType.label}</TableCell>
                                            </TableRow>
                                            }
                                        </Table>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} md={6} className={classes.col}>
                                    <VotingTable data={oerGroups} idField="topic" 
                                        objectType="OG" title={t("Related Educational Package")} height="100%" showVote={false} collapsable={false} headClass={classes.delSugg} 
                                        formatLink={formatOerGroupLink} />
                                </Grid>
                            </Grid>
                        </Paper>

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
        </>
        }
        </>
    )
}

export default Oers