import { CircularProgress, Grid, IconButton, InputAdornment, ListItem, ListItemText, makeStyles, Paper, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { FixedSizeList } from 'react-window';
import { useLanguage } from '../../../i18n';
import { cancelLoadPart, loadPart } from './utils';

const useStyles = makeStyles(theme => ({
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    hasPadding: {
        padding: theme.spacing(1),
    },
    loadingPanel: {
        textAlign: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    }
}));


function OersDiscovery() {
    const classes = useStyles();
    const [oers, setOers] = useState(null);
    const [oersLoading, setOersLoading] = useState(false);
    const [searchString, setSearchString] = useState('');
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const renderRow = useCallback(({ index, style }) => {
        return (
            <ListItem button style={style} key={index} to={`/${language}/dashboard/discovery/oer-groups/${oers[index].id}`} component={Link}>
                <ListItemText primary={oers[index].title} />
            </ListItem>
        );
    }, [oers, language])

    useEffect(() => { // do the search
        if (searchString.length === 0) {
            cancelLoadPart();
            setOers(null);
            setOersLoading(false);
        } else {
            setOersLoading(true);
            loadPart(
                "oers",
                searchString, res => {
                    setOers(res.data.results);
                    setOersLoading(false);
                })
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, [])

    const handleAddOerGroup = useCallback(() => {
        history.push(`/${language}/dashboard/discovery/oer-groups/add`);
    }, [history]);

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} md={8} lg={6}>
                <Paper elevation={1} className={classes.hasPadding}>
                    <TextField label={t("Search educational package...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                        InputProps={{
                            endAdornment:
                                <InputAdornment position="end">
                                    <IconButton title={t("Add a new educational package")} className={classes.newButton} onClick={handleAddOerGroup}>
                                        <AddIcon />
                                    </IconButton>
                                </InputAdornment>
                        }}
                        autoFocus />
                    {oersLoading ?
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                        :
                        oers && (
                            oers.length === 0 ?
                                <div className={classes.loadingPanel}>
                                    <i>{t("No match found...")}</i>
                                </div>
                                :
                                <FixedSizeList height={350} width="100%" itemCount={oers.length} itemSize={50}>
                                    {renderRow}
                                </FixedSizeList>
                        )}
                </Paper>
            </Grid>
            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-pack.jpg`} width="70%" alt="" style={{ marginTop: "60px" }} />
        </Grid>

    )
}

export default OersDiscovery
