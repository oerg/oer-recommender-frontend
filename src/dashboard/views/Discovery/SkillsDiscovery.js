import { Box, CircularProgress, Grid, IconButton, InputAdornment, ListItem, ListItemText, makeStyles, Paper, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { FixedSizeList } from 'react-window';
import { useLanguage } from '../../../i18n';
import LangIcon from '../../components/LangIcon';
import { cancelLoadPart, loadPart } from './utils';

const useStyles = makeStyles(theme => ({
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    hasPadding: {
        padding: theme.spacing(1),
    },
    loadingPanel: {
        textAlign: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    newButton: {

    }
}));


function SkillsDiscovery() {
    const classes = useStyles();
    const [skills, setSkills] = useState(null);
    const [skillsLoading, setSkillsLoading] = useState(false);
    const [searchString, setSearchString] = useState('');
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const renderRow = useCallback(({ index, style }) => {
        return (
            <ListItem button style={style} key={index} to={`/${language}/dashboard/discovery/skills/${skills[index].id}`} component={Link}>
                <ListItemText primary={<span>{skills[index].title} <LangIcon language={skills[index].lang} small /></span>} />
            </ListItem>
        );
    }, [skills, language])

    useEffect(() => { // do the search
        if (searchString.length === 0) {
            cancelLoadPart();
            setSkills(null);
            setSkillsLoading(false);
        } else {
            setSkillsLoading(true);
            loadPart(
                "skills",
                searchString, res => {
                    setSkills(res.data.results);
                    setSkillsLoading(false);
                })
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, [])

    const handleAddSkill = useCallback(() => {
        history.push(`/${language}/dashboard/discovery/skills/add?init=${searchString}`)
    }, [history, searchString])

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} md={8} lg={6}>
                <Paper elevation={1} className={classes.hasPadding}>
                    <Box display="flex">
                        <TextField label={t("Search courses...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                            InputProps={{
                                endAdornment:
                                    <InputAdornment position="end">
                                        <IconButton title={t("Add a new course")} className={classes.newButton} onClick={handleAddSkill}>
                                            <AddIcon />
                                        </IconButton>
                                    </InputAdornment>
                            }}
                            autoFocus />
                    </Box>
                    {skillsLoading ?
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                        :
                        skills && (
                            skills.length === 0 ?
                                <div className={classes.loadingPanel}>
                                    <i>{t("No match found...")}</i>
                                </div>
                                :
                                <FixedSizeList height={350} width="100%" itemCount={skills.length} itemSize={50}>
                                    {renderRow}
                                </FixedSizeList>)
                    }
                </Paper>
            </Grid>
            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-skill.jpg`} width="70%" alt="" style={{ marginTop: 60, marginBottom: 60 }} />
        </Grid>

    )
}

export default SkillsDiscovery
