import { Box, Grid, IconButton, LinearProgress, makeStyles, Paper, TextField } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert } from '@material-ui/lab'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import DiscoveryNav from '../../../components/DiscoveryNav'
import { getSuggestionColor } from '../../helpers'
import { roseColor } from '../../styles/variables'
import { doEditOerGroup, getOerGroup } from './utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > li:nth-child(even)': {
            backgroundColor: '#CCC',
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(0.65),
        color: getSuggestionColor(0.65),
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(0.65),
        borderColor: getSuggestionColor(0.65),
    }
}))

function Edit({ navHistory, ...props}) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [initTitle, setInitTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [isEditable, setIsEditable] = useState(true);
    const [description, setDescription] = useState('');
    const [changed, setChanged] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const history = useHistory();

    const { id } = useParams();

    useEffect(
        () => {
            setLoading(true);
            getOerGroup(
                id,
                res => {
                    setIsEditable(res['is_admin'] || res['is_contributor']);
                    setTitle(res.title);
                    setInitTitle(res.title);
                    setDescription(res.description);
                },
                null,
                () => setLoading(false)
            )
        },
        [ id ]
    )

    const handleTitle = useCallback(e => {
        setTitle(e.target.value);
        setChanged(true);
    }, []);

    const handleSave = useCallback(() => {
        setLoading(true);
        doEditOerGroup(
            id, title, description,
            () => {
                setLoading(false);
                history.push(`/${language}/dashboard/discovery/oer-groups/${id}`)
            },
            () => setLoading(false),
        )
    }, [ id, title, description ])

    return (
        <Grid container>
            <Grid item xs={12}>
                <DiscoveryNav navHistory={navHistory} />
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Editing Educational Package")}: <em>{initTitle}</em>
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} className={classes.formField}>
                                    <TextField value={title} onChange={handleTitle} label={t("Title")} fullWidth />
                                    { loading && <LinearProgress /> }
                                    { !isEditable && 
                                        <Alert severity="error">
                                            {t("You cannot edit this educational package.")} (<Link to={`/${language}/dashboard/discovery/oer-groups/${id}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    <TextField label={t("Description")} multiline fullWidth helperText={t("Description for the educational package.")}
                                        value={description} onChange={e => setDescription(e.target.value) || setChanged(true)} rows={3}
                                        disabled={loading || !isEditable} />
                                </Grid>
                                <Grid item xs={12} className={classes.formField}>
                                    <Button color="rose" onClick={handleSave} disabled={loading || !isEditable || !changed || title.length === 0}
                                        fullWidth>{t("Save")}</Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Edit.propTypes = {

}

export default Edit

