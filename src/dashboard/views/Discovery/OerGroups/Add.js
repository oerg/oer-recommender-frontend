import { Box, Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, IconButton, makeStyles, Typography } from '@material-ui/core'
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import HelpIconOutline from "@material-ui/icons/HelpOutline"
import clsx from 'classnames'
import React, { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import Help from '../../../components/Help'
import HtmlTooltip from '../../../components/HtmlTooltip'
import MovableList from '../../../components/MovableList'
import TopicObjectInput from '../../../components/SystemInputs/TopicObjectInput'
import { roseColor } from '../../styles/variables'
import OerGroupForm from './components/OerGroupForm'
import { saveOerGroup } from './utils'


const useStyles = makeStyles(theme => ({
    addSkillButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: -24,
        zIndex: 999,
    },
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },

    suggestions: {
        border: '1px solid #af1414',
        color: '#af1414',
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
}))

function Add() {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [oers, setOers] = useState([]);
    const [topics, setTopics] = useState([]);
    const [loading, setLoading] = useState(false);
    const [showAddForm, setShowAddForm] = useState(false);
    const [selectedTopic, setSelectedTopic] = useState(null);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const [suggestions, setSuggestions] = useState([]);

    const history = useHistory();

    const handleUpdate = useCallback(
        (title, description, oers) => {
            setTitle(title ? title : '');
            setDescription(description ? description : '');
            setOers(oers ? oers : []);
            setSuggestions(() => {
                const allTopics = [].concat(...oers.map((o) => o.topics ? o.topics : []));
                return allTopics.filter(t => !topics.some(at => at.id === t.id))
            })
        },
        [topics]
    );

    const handleSave = useCallback(() => {
        setLoading(true);
        saveOerGroup(
            title,
            description,
            topics.map(t => t.id),
            oers,
            data => {
                history.push(`/${language}/dashboard/discovery/oer-groups/` + data.oer_group_id);
            },
            null,
            () => setLoading(false)
        );
    }, [oers, topics, title, description, history]);

    const handleOpenTopicForm = useCallback(
        () => setShowAddForm(true),
        []
    );

    const handleCloseTopicForm = useCallback(
        () => setShowAddForm(false),
        []
    );

    const handleAddTopic = useCallback(
        () => {
            setTopics(
                topics => {
                    if (topics.some(t => t.id === selectedTopic.id))
                        return topics;
                    return [...topics, { id: selectedTopic.id, title: selectedTopic.title }]
                }
            );
            setSelectedTopic(null);
            setShowAddForm(false);
        },
        [selectedTopic]
    );

    const handleSelectedTopic = useCallback(
        newValue => setSelectedTopic(newValue),
        []
    );

    const handleTopicRemove = useCallback(
        index => {
            setTopics(
                ts => {
                    ts.splice(index, 1);
                    return [...ts];
                }
            );
        },
        []
    );

    const handleChip = useCallback((index, suggestTopic) => () => {
        setTopics(prevState => ([...prevState, suggestTopic]))
        setSuggestions(prevState => prevState.filter(s => s.title !== suggestTopic.title))
    }, []);

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Add a new educational package")}
                            <Help>
                                {t("Educational packages can include one or more educational contents.\n")}
                                {t("When you add more than one content:")}
                                <ul>
                                    <li>{t("It means that the multiple contents together will cover the topic(s).")}</li>
                                    <li>{t("Learners will be shown the contents in the order you add them here.")}</li>
                                </ul>
                            </Help>
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <OerGroupForm
                            className={classes.smallCenter}
                            onUpdate={handleUpdate}
                        />

                        {
                            (suggestions.length > 0) && (
                                <Grid item xs={12} className={clsx(classes.formField, classes.suggestions, classes.smallCenter)}>
                                    {suggestions.map((s, index) => (
                                        <Chip label={s.title} variant="outlined" key={index} onClick={handleChip(index, s)} className={classes.suggestion} title="Add to topics" />
                                    ))}
                                    <HtmlTooltip arrow placement="left-start" title={
                                        <Box textAlign="justify" p={1} borderRadius={15}>
                                            {t("These are computer generated learning topics for this package. You can add them to this educational package by clicking.")}
                                        </Box>
                                    }>
                                        <HelpIconOutline className={classes.suggestionsHelp} />
                                    </HtmlTooltip>
                                </Grid>
                            )}

                        <Box textAlign="center" position="relative" className={classes.smallCenter}>
                            <Typography component="legend" style={{ textAlign: "left", marginTop: 14 }}>{t("Topics")}</Typography>
                            <MovableList
                                options={topics}
                                emptyText={t("No topics selected!")}
                                movable={false}
                                onRemove={handleTopicRemove}
                            />
                            <IconButton className={classes.addSkillButton} title={t("Add Topic")} onClick={handleOpenTopicForm}>
                                <AddCircleRoundedIcon />
                            </IconButton>
                            <Dialog open={showAddForm} onClose={handleCloseTopicForm}
                                PaperProps={{
                                    style: {
                                        minWidth: 450,
                                    }
                                }}
                            >
                                <DialogTitle>
                                    {t("Search and select a topic")}
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        <TopicObjectInput onSelect={handleSelectedTopic} />
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button
                                        onClick={handleAddTopic}
                                        color="rose"
                                        disabled={!selectedTopic}
                                    >
                                        {t("Add selected topic")}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Box>
                        <div className={classes.smallCenter} style={{ marginTop: 20 }}>
                            <Button color="rose" onClick={handleSave} disabled={loading || topics.length === 0 || title.length === 0}
                                fullWidth>{t("Save")}</Button>
                        </div>

                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

export default Add
