import { Box, CircularProgress, Grid, IconButton, Paper } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import GridContainer from '../../../components/Grid/GridContainer'
import GridItem from '../../../components/Grid/GridItem'
import LoginWarning from '../../../components/LoginWarning'
import VotingTable from '../../../components/Vote/VotingTable'
import useStyles from '../../styles/SkillsStyle'
import OersTable from './OersTable'
import { addDeleteSuggestion, formatTopicTitle, getOerGroup } from './utils'

function OerGroups({ requireLogin, navHistory, onNav, ...props}) {
    const classes = useStyles();
    const { id: oerGroupId } = useParams();

    const [oerGroup, setOerGroup] = useState(null);
    const [title, setTitle] = useState('')
    const [oers, setOers] = useState([]);
    const [topics, setTopics] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const [reloadNeeded, setReloadNeeded] = useState(true);

    const history = useHistory();

    useEffect(() => { // load oerGroup and suggestions
        setLoading(true);
        getOerGroup(oerGroupId, (data) => { // load
            setOerGroup(data);
            setOers(data.oergroupoer_set.sort((a, b) => a.order - b.order));
            setTitle(data.title);
            setTopics(data.topics);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: OerGroup was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
        setReloadNeeded(false);
    }, [oerGroupId, reloadNeeded])

    const handleDelete = useCallback((oerId) => { // deleting suggestions
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addDeleteSuggestion(oerGroupId, oerId, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [oerGroupId, requireLogin])

    const formatTopicLink = useCallback((topic, idField) => {
        return `/${language}/dashboard/discovery/topics/${topic[idField]}`
    }, [language]);

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history]);

    return (
        <>
        { requireLogin &&
            <LoginWarning show={showLoginWarning} onSelect={handleLogin} />
        }
        {isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
        :
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <>
        <DiscoveryNav onNav={onNav} navHistory={navHistory} oerg={oerGroup} />
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {title}
                            <Box component="span" ml={2}>
                                { (oerGroup.is_contributor || oerGroup.is_admin) &&
                                    <IconButton style={{ color: "white" }} title={t("Edit this Educational Package")} size="small" onClick={() => history.push(`/${language}/dashboard/discovery/oer-groups/edit/${oerGroupId}`)}>
                                        <EditIcon />
                                    </IconButton>
                                }
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        { oerGroup.description &&
                        <Paper elevation={2} className={classes.descriptionBox}>
                            {oerGroup.description}
                        </Paper> }
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
                            <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                <Grid item xs={12} md={6} className={classes.col}>
                                    <OersTable oers={oers} onDelete={handleDelete} headClass={classes.addSugg} />
                                </Grid>
                                <Grid item xs={12} md={6} className={classes.col}>
                                    <VotingTable showVote={false} data={topics} formatTitle={formatTopicTitle} idField="id" 
                                        objectType="TO" title={t("Related Topics")} height="100%" collapsable={false} headClass={classes.delSugg} 
                                        formatLink={formatTopicLink} />
                                </Grid>
                            </Grid>
                        </Paper>

                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
        </>
        }
        </>
    )
}

export default OerGroups
