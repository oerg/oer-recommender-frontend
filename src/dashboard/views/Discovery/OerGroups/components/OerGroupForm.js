import { Dialog, DialogActions, DialogContent, DialogTitle, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import Button from '../../../../components/CustomButton/Button'
import { StaticNote } from '../../../../components/Notes'
import OerInput from '../../../../components/OerInput'
import { roseColor } from '../../../styles/variables'
import NewOerButton from './NewOerButton'
import OerGroupSearch from './OerGroupSearch'
import UploadForm from './UploadForm'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    smallCenter: {
        minWidth: '200px',
        margin: 'auto',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '750px',
        overflowY: 'auto',
        overflowX: 'hidden',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestions: {
        border: '1px solid #af1414',
        color: '#af1414',
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    oergItem: {
        margin: `0 ${theme.spacing(2)}px`,
    }
}))

function OerGroupForm({ onUpdate, onError, className }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [oers, setOers] = useState([]);
    const [showNewOergImport, setShowNewOergImport] = useState(false);
    const [showNewOergUpload, setShowNewOergUpload] = useState(false);
    const [showNewOergExisting, setShowNewOergExisting] = useState(false);
    const [focusedOnce, setFocusedOnce] = useState(false);
    const [newUrl, setNewUrl] = useState('');
    const { t } = useTranslation(['dashboard']);

    const hasMoreThanOneOer = useMemo(
        () => oers.length > 1,
        [oers]
    );

    useEffect(() => { // send data back to parent
        if (onUpdate) {
            if (oers.length === 1) {
                onUpdate(oers[0].title, oers[0].description, oers);
            } else {
                onUpdate(title, description, oers)
            }
        }
        if (oers.some(og => (!og.title || og.title.length === 0) || (!og.resource || og.resource.length === 0))) {
            onError && onError(true);
        } else {
            onError && onError(false);
        }
    }, [oers, title, description, onError]);

    const handleRemoveOer = useCallback(index => () => {
        setOers(os => {
            os.splice(index, 1);
            return [...os];
        })
    }, []);

    const handleChangeOer = useCallback(
        index => newOerg => {
            setOers(
                os => {
                    os.splice(index, 1, newOerg);
                    return [...os];
                }
            )
        },
        []
    );

    const handleImportRequest = useCallback(
        () => {
            setShowNewOergImport(true);
            setShowNewOergUpload(false);
            setShowNewOergExisting(false);
        },
        []
    );

    const handleUploadRequest = useCallback(
        () => {
            setShowNewOergImport(false);
            setShowNewOergUpload(true);
            setShowNewOergExisting(false);
        },
        []
    );

    const handleExistingRequest = useCallback(
        () => {
            setShowNewOergImport(false);
            setShowNewOergUpload(false);
            setShowNewOergExisting(true);
        },
        []
    );

    const handleNewImport = useCallback(
        () => {
            setShowNewOergImport(false);
            setOers(oers => [...oers,
            {
                autoload_url: newUrl,
                data: {
                    exists: false,
                    alert: true
                }
            }
            ]);
            setNewUrl('');
        },
        [newUrl]
    );

    const handleExistingAdded = useCallback(
        oer => () => {
            setShowNewOergExisting(false);
            setOers(oers => [...oers,
            {
                ...oer,
                autoload_url: oer.url,
                data: {
                    exists: true,
                    alert: false,
                }
            }
            ])
        },
        []
    );

    const handleUploaded = useCallback(
        uploaded => {
            setOers(oers =>
                [...oers,
                {
                    ...uploaded,
                    defaultOpen: true,
                    data: {
                        exists: false,
                        alert: false,
                    }
                }
                ]
            );
            setShowNewOergUpload(false);
        },
        []
    );

    const handleTitle = useCallback(
        e => setTitle(e.target.value),
        []
    );

    // do scrolling
    const handleInputOpen = useCallback(
        (open, ref) => {
            if (open && ref) {
                ref.scrollIntoView(true);
            }
        },
        []
    );

    return (
        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={className} >
            <Grid container className={classes.smallCenter}>
                <Grid item xs={12}>
                    <Paper elevation={2} style={{
                        margin: 16,
                    }}>
                    <StaticNote
                        title={<b>{t("Important Notes")}</b>}
                        id="discovery_add_oerg"
                    >
                        <ul>
                            <li>
                                {t("Each and every educational package must cover the topic by its own.")}
                            </li>
                            <li>
                                {t("After saving a package, you cannot add/remove content to/from it.")}
                            </li>                            
                            <li>
                                {t("Put contents only if they are free to use.")}
                            </li>
                            <li>
                                {t("If you are uploading a material, you must have the proper rights to publish it. This content will be published under")} <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener noreferrer">CCBYSA 4.0</a>.&nbsp;
                                {t("The uploaded files will be hosted on ")} <a href='https://www.twillo.de/' target="_blank" rel="noopener noreferrer">Twillo Service</a>.
                            </li>
                        </ul>
                    </StaticNote>
                    </Paper>
                </Grid>
                <Grid item xs={12} className={classes.formField}>
                    <div className={classes.listRoot}>
                        <Typography style={{ textAlign: 'left', margin: 15 }}>{t("Educational Content")}</Typography>
                        {oers.map((o, index) => (
                            <Paper
                                key={index}
                                className={classes.oergItem}
                            >
                                <OerInput
                                    value={o}
                                    showRemove
                                    onChange={handleChangeOer(index)}
                                    onRemove={handleRemoveOer(index)}
                                    onOpen={handleInputOpen}
                                    persistentData={o.data}
                                    readOnly={o.data.exists}
                                    alertOnExist={o.data.alert}
                                />
                            </Paper>
                        ))}
                        <NewOerButton onUrlRequest={handleImportRequest} onUploadRequest={handleUploadRequest} onExistingRequest={handleExistingRequest} />
                    </div>
                </Grid>
                {hasMoreThanOneOer && [
                    <Grid key={0} item xs={12} className={classes.formField}>
                        <TextField value={title} onChange={handleTitle} label={t("Package Name")} fullWidth
                            variant="outlined" required error={focusedOnce && title.length === 0} onBlur={() => setFocusedOnce(true)} />
                    </Grid>,
                    <Grid key={1} item xs={12} className={classes.formField}>
                        <TextField value={description} onChange={e => setDescription(e.target.value)} multiline rows={3} label={t("Description")} fullWidth variant="outlined" />
                    </Grid>
                ]}
            </Grid>
            <Dialog open={showNewOergImport} onClose={() => setShowNewOergImport(false)}
                PaperProps={{
                    style: {
                        minWidth: 450
                    }
                }}>
                <DialogTitle>{t("Import")}</DialogTitle>
                <DialogContent>
                    <TextField
                        value={newUrl}
                        onChange={e => setNewUrl(e.target.value)}
                        fullWidth
                        label="URL"
                        placeholder="https://..."
                        autoFocus
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="rose" onClick={handleNewImport}>
                        {t("Import")}
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={showNewOergUpload} onClose={() => setShowNewOergUpload(false)}>
                <DialogTitle>{t("Upload")}</DialogTitle>
                <DialogContent>
                    <UploadForm afterUpload={handleUploaded} />
                </DialogContent>
            </Dialog>
            <Dialog open={showNewOergExisting} onClose={() => setShowNewOergExisting(false)}
                PaperProps={{
                    style: {
                        minWidth: 450
                    }
                }}>
                <DialogTitle>{t("Search for existing contents")}</DialogTitle>
                <DialogContent>
                    <OerGroupSearch afterAdd={handleExistingAdded} />
                </DialogContent>
            </Dialog>

        </Paper>
    )
}

OerGroupForm.defaultProps = {
    showTopics: true,
}

export default OerGroupForm
