import axios from "axios";


export function doUpload(data, onProgress, onLoad, onError, onEnd) {
    const config = {
        onUploadProgress: (progressEvent) => onProgress && onProgress(Math.round( (progressEvent.loaded * 100) / progressEvent.total ))
    };

    axios.post(
        '/oers/upload-oer/',
        data,
        config
    )
    .then(res => {
        onLoad && onLoad(res.data);
    })
    .catch(err => {
        onError && onError(err.response);
        err.response && err.response.result && alert(err.response.result)
    })
    .finally(() => onEnd && onEnd());
}