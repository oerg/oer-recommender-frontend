import { MenuItem, MenuList, Popover } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import PropTypes from 'prop-types';
import React, { useCallback, useRef, useState } from 'react';
import Button from '../../../../components/CustomButton/Button';
import AddIcon from '@material-ui/icons/Add';
import { useTranslation } from 'react-i18next';

function NewOerButton({ disabled, onUrlRequest, onUploadRequest, onExistingRequest, ...props }) {
    const myRef = useRef(null);
    const [open, setOpen] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);

    const handleClose = useCallback(
        () => setOpen(false),
        []
    );

    const handleOpen = useCallback(
        () => setOpen(true),
        []
    );

    const handleMenuClick = useCallback(
        func =>
            () => {
                setOpen(false);
                func();
            },
        []
    );

    return (<>
        <Button
            ref={myRef}
            variant="contained"
            color="info"
            endIcon={<ArrowDropDownIcon />}
            disabled={disabled}
            onClick={handleOpen}
            startIcon={<AddIcon />}
            {...props}
        >
            {t("New educational content")}
        </Button>
        <Popover
            open={open}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            anchorEl={myRef.current}
        >
            <MenuList>
                <MenuItem onClick={handleMenuClick(onUrlRequest)}>
                    {t("Import from URL")}
                </MenuItem>
                {/*<MenuItem onClick={handleMenuClick(onUploadRequest)}>*/}
                <MenuItem disabled>
                    {t("Upload data")}
                </MenuItem>
                <MenuItem onClick={handleMenuClick(onExistingRequest)}>
                    {t("Add from existing contents")}
                </MenuItem>
            </MenuList>
        </Popover>
    </>)
}

NewOerButton.propTypes = {
    disabled: PropTypes.bool,
}

NewOerButton.defaultProps = {
    disabled: false,
}

export default NewOerButton

