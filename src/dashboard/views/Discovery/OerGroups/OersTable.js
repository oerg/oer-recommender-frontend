import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, withStyles } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../../i18n';
import Help from '../../../components/Help';

const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
    },
    buttonRoot: {
        display: 'inline-block',
    },
    button: {
    },
    hidden: {
        display: 'none',
    },
    stickyFooter: {
        left: 0,
        bottom: -20,
        zIndex: 2,
        position: 'sticky',
        textAlign: 'center',
    },
    oerGreen: {
        backgroundColor: '#005f5f',
        color: '#FFF',
        opacity: 0.5,
        transition: 'margin-bottom 0.3s',
        '&:hover': {
            backgroundColor: "#004555",
            color: '#AAA',
            opacity: 1,
            marginBottom: 10
        }
    },
    headRow: {
        height: 60,
    }
})

function AddOerDialog({show, onClose, topics, ...props}) {
    return(<div></div>)
}

const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

function OersTable({ className, oers, onDelete, onNew, headClass, ...props }) {
    const classes = useStyles();
    const [showNew, setShowNew] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const handleNewDialog = useCallback((save, data) => {
        setShowNew(false);
        if (save) onNew(data);
    }, [onNew]);

    return (<>
        <AddOerDialog show={showNew} onClose={handleNewDialog} oers={oers} />

        <TableContainer component={Paper} className={className} style={{ height: '400px'}} {...props}>
            <Table size="small" stickyHeader>
                <TableHead>
                    <TableRow className={classes.headRow}>
                        <TableCell component="th" className={headClass}>
                            {t("Contents in this educational package")}
                            <Help>
                                {t("The following contents will be shown to learners in this order. The Related Topics of this educational package should be covered after finishing all these contents by learners.")}
                            </Help>                            
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                { oers.length === 0 ?
                    <TableRow>
                        <TableCell colSpan={2}>
                            <em>{t("No resources found.")}</em>
                        </TableCell>
                    </TableRow>
                :
                oers.map((i, index) => (
                    <StyledTableRow key={index}>
                        <TableCell>
                            <Link to={`/${language}/dashboard/discovery/oers/${i.oer.id}`} title={t("Go to this oer")}>{i.oer.title}</Link>
                        </TableCell>
                    </StyledTableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    </>)
}

export default OersTable
