import Axios from 'axios';
import React from 'react';
import { requestAddHandlers } from '../../helpers';

export function getOerGroup(id, onLoad, onError, onEnd) { // get topic from the server
    requestAddHandlers(
        Axios.get('/oers/' + id),
        onLoad,
        onError,
        onEnd
    )
}

export function doEditOerGroup(oer_group_id, title, description, onLoad, onError, onEnd) { // get topic from the server
    requestAddHandlers(
        Axios.put('/oers/update-oer-group-information/', {
            oer_group_id, title, description, note: ""
        }),
        onLoad,
        onError,
        onEnd
    )
}

export function doOerVote(skillId, topicId, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/skills/add-skill-topic-vote/', {
        skill: skillId,
        topic: topicId,
        vote: vote,
    })
    .then(res => {
        onSuccess && onSuccess(res.data);
    })
    .catch(res => {
        onError ? onError(res.response.data) : alert(res.response.data.result);
    })
}

export function doSuggestionVote(id, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/suggestions/add-suggestion-vote/', {
        suggestion: id,
        vote: vote,
    })
    .then(res => {
        onSuccess && onSuccess(res.data);
    })
    .catch(res => {
        onError ? onError(res.response.data) : alert(res.response.data.result);
    })
}

export function doTopicVote(skillId, jobId, vote, onSuccess, onError) { // send jobs' vote to server.
    Axios.post('/jobs/add-job-skill-vote/', {
        skill: skillId,
        job: jobId,
        vote: vote,
    })
    .then(res => {
        onSuccess && onSuccess(res.data);
    })
    .catch(res => {
        onError ? onError(res.response.data) : alert(res.response.data.result);
    })
}

export function processVote(list, id, up, down, selfVote, idField = 'id') { // update local list of votes
    let data = list.find(t => t[idField] === id)
    data.vote = selfVote;
    data.upvote_count = up;
    data.downvote_count = down;
    return [...list];
}

export function addDeleteSuggestion(oerGroupId, oerId, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'OG',
        object_id: oerGroupId,
        payload: {
            suggestion_type: "DE",
            oer_id: oerId,
        }
    })
    .then(res => {
        onSuccess && onSuccess(res.data);
    })
}

export function addAddOerSuggestion(oerGroupId, oerId, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'OG',
        object_id: oerGroupId,
        payload: {
            suggestion_type: "AD",
            oer_id: oerId,
        }
    })
    .then(res => {
        onSuccess && onSuccess(res.data);
    })
}

export function getUrlInfo(url, onSuccess, onError) {
    Axios.get('/oers/get-oer-info/?url=' + encodeURIComponent(url))
    .then(res => {
        onSuccess && onSuccess(res);
    })
    .catch(err => {
        if (err && err.response) {
            onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
        }
    })
}

export function getAllTopics(onSuccess, onError) {
    Axios.get('/skills/topics/get-only-titles/')
    .then(res => {
        onSuccess && onSuccess(res);
    })
    .catch(err => {
        err &&
        onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
    })
}

export function saveOerGroup(title, description, topics, oers, onSuccess, onError, onEnd) {
    return requestAddHandlers(
        Axios.post('/oers/add-oer-group/', {
            title, description, topics, oers
        }),
        onSuccess,
        onError,
        onEnd
    )
}

export function filterSuggestions(suggestions) {
    const add = [] // add
    const edit = [] // edit
    const reorder = [] // reorder
    const remove = [] // delete
    suggestions.forEach(s => {
        if (s.decision === 'PE') { // only pendings
            switch (s.payload.suggestion_type) {
                case 'AD':
                    add.push(s)
                    break;
                case 'DE':
                    remove.push(s);
                    break;
                default:
            }
        }
        return {
            add, edit, reorder, remove
        }
    });

    return { add, edit, reorder, remove }
}

export function formatTopicTitle(t) {
    return (
        <span>
            {t.title}
        </span>
    )
}