import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { loadDescription } from './utils'

function DescriptionDialog({ keyword, lang, onAccept, onCancel, open }) {
    const { t } = useTranslation(['dashboard'])
    const [loading, setLoading] = useState(false)
    const [text, setText] = useState("")

    useEffect(
        () => {
            setLoading(true)
            loadDescription(
                keyword,
                lang,
                res => {
                    if (res.description) {
                        setText(res.description)
                    }
                },
                null,
                () => setLoading(false)
            )
        },
        [keyword, lang]
    )

    const handleClose = useCallback(
        (e, reason) => {
            if (reason === "backdropClick") {
                return
            }
            onCancel()
        },
        [onCancel]
    )

    const handleAccept = useCallback(
        () => {
            onAccept(text)
        },
        [onAccept, text]
    )

    return (
        <Dialog scroll='paper' open={open} onClose={handleClose}>
            <DialogContent dividers>
                {loading ?
                    <div style={{ textAlign: "center" }}>
                        <CircularProgress />
                    </div>
                    :
                    <DialogContentText>
                        {text}
                    </DialogContentText>
                }
            </DialogContent>
            <DialogActions>
                <Button color='secondary' title={t("The description is not valid.")} onClick={onCancel}>
                    {t("Reject")}
                </Button>
                <Button color='primary' title={t("Import the description.")} onClick={handleAccept}>
                    {t("Accept")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DescriptionDialog