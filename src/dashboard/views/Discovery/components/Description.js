import { Button, ButtonGroup, Paper, TextField, Typography } from '@material-ui/core'
import React, { useCallback, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import IAlert from '../../../components/ISys/IAlert'
import CloseIcon from '@material-ui/icons/Close';
import Help from '../../../components/Help';
import DescriptionDialog from './DescriptionDialog';

function Description({ label, help, placeholder, text: propText, value, onChange, loading: propLoading, TextFieldProps, TypographyProps, IAlertProps, keyword, lang, ...props }) {
    const [dismissed, setDismissed] = useState(false)
    const [showDialog, setShowDialog] = useState(false)

    const { t } = useTranslation(['dashboard'])

    const text = useMemo(
        () => propText ? propText : t("System can provide you with a description from Wikipedia."),
        [t, propText]
    )
    
    const showSystemHelp = useMemo(
        () => !dismissed && !Boolean(value),
        [dismissed, value]
    )

    const handleDismiss = useCallback(
        () => {
            setDismissed(true)
            setShowDialog(false)   
        },
        []
    )

    const handleAccept = useCallback(
        (text) => {
            onChange({
                target: {
                    value: text
                }
            })
            setDismissed(true)
            setShowDialog(false)
        },
        [onChange]
    )

    const handleShow = useCallback(
        () => {
            setShowDialog(true)
        },
        []
    )

    return (
        <Paper {...props}>
            {showSystemHelp && <DescriptionDialog
                open={showDialog}
                keyword={keyword}
                lang={lang}
                onCancel={handleDismiss}
                onAccept={handleAccept}
            />}
            <Typography style={{ textAlign: 'left', padding: 10 }} {...TypographyProps}>
                {label}
                <Help>
                    {help}
                </Help>
            </Typography>
            <IAlert
                style={{
                    marginBottom: 8
                }}
                {...IAlertProps}
                show={showSystemHelp}
                action={
                    <ButtonGroup color='inherit' variant='text'>
                        <Button onClick={handleShow}>
                            {t("Show")}
                        </Button>
                        <Button onClick={handleDismiss}>
                            <CloseIcon />
                        </Button>
                    </ButtonGroup>
                }
            >
                {text}
            </IAlert>

            <TextField multiline fullWidth variant='outlined' placeholder={placeholder}
                rows={4} {...TextFieldProps} value={value} onChange={onChange} disabled={propLoading || (TextFieldProps && TextFieldProps.disabled)} />
        </Paper>)
}

export default Description