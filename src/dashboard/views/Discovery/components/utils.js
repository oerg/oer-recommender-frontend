import axios from "axios";
import { requestAddHandlers } from "../../helpers";

export function loadDescription(keyword, lang, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/skills/get-possible-description/?title=${keyword}&lang=${lang}`),
        onLoad,
        onError,
        onEnd
    )
}