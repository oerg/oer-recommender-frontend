import axios from "axios";
import { MultiCall, requestAddHandlers } from "../helpers";

function loadTopicQuestions(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/assessments/questions/?topics__id=${id}`
        ),
        onLoad,
        onError,
        onEnd
    )
}

function loadSkillQuestions(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/assessments/questions/get-exam-questions?skill_id=${id}`
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function loadTopContributors(objectType, objectId, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/members/get-top-contributors?object_type=${objectType}&object_id=${objectId}`
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function loadQuestions(item, onLoad, onError, onEnd) {
    if (item.object_type === "SK") {
        return loadSkillQuestions(item.object_id, onLoad, onError, onEnd);
    }
    return loadTopicQuestions(item.object_id, onLoad, onError, onEnd);
}

export function loadMyQuestions(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get('/assessments/questions/only-me/'),
        onLoad,
        onError,
        onEnd
    )
};


const mcLoadCode = "DiscoverLoadPart"

export function loadPart(part, searchStr, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(mcLoadCode)
    mc.call(`/${part}/search/?search-term=${searchStr}&sensitive=False&page=1`, onLoad, onError, onEnd);
}

export function cancelLoadPart() {
    MultiCall.getInstance(mcLoadCode).cancel();
}

export function addWordOpinion(id, opinion, onLoad, onError, onEnd, topics = []) {
    const data = {
        "recommendation_id": id,
        "opinion": opinion,
    }

    if (topics.length > 0) {
        data.topics = topics;
    }

    requestAddHandlers(
        axios.post(
            '/intelligent-services/add-opinion-on-recommendation/',
            data
        ),
        onLoad,
        onError,
        onEnd
    );
}