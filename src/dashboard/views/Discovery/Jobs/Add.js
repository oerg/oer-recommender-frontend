import { Box, Button as MuiButton, Chip, Collapse, FormControl, FormHelperText, Grid, IconButton, InputLabel, LinearProgress, makeStyles, MenuItem, Paper, Select, TextField, Typography } from '@material-ui/core'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import { LaunchOutlined } from '@material-ui/icons'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert, Autocomplete } from '@material-ui/lab'
import arrayMove from 'array-move'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory } from 'react-router-dom'
import { allIndustries } from '../../../../data'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import CitySelect from '../../../components/CitySelect'
import CountrySelect from '../../../components/CountrySelect'
import Button from '../../../components/CustomButton/Button'
import Help from '../../../components/Help'
import IDiv from '../../../components/ISys/IDiv'
import LangTextField from '../../../components/LangTextField'
import ListDnd from '../../../components/ListDnd/ListDnd'
import { StaticNote } from '../../../components/Notes'
import TitleConfirmDialog from '../../../components/TitleConfirmDialog'
import { getSuggestionColor, useParam } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { getIsEditableSkill } from '../Skills/utils'
import AddSkillWindow from './components/AddSkillWindow'
import GoalChip from './components/GoalChip'
import Suggestions from './components/Suggestions'
import { cancelIsEditableJob, cancelJobRecommendations, cancelLoadSimilarJobs, doAddJob, getIsEditableJob, getJobRecommendations, loadSimilarJobs as getSimilarJobs } from './utils'


const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > div > div:nth-child(odd)': {
            backgroundColor: '#CCC',
        },
    },
    dragClass: {
        backgroundColor: "#EEE"
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addSkillButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addSkillForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(1),
        color: getSuggestionColor(0.8),
        margin: theme.spacing(2),
        position: 'relative',
        marginTop: 0,
        maxHeight: 200,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(1),
        borderColor: getSuggestionColor(1),
        "&:focus": {
            backgroundColor: `${getSuggestionColor(1)} !important`,
        }
    },
}))

function Add({ navHistory, ...props }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [company, setCompany] = useState('');
    const [country, setCountry] = useState('');
    const [city, setCity] = useState('');
    const [selectedSkills, setSelectedSkills] = useState([]);
    const [loading, setLoading] = useState(false);
    const [showForm, setShowForm] = useState(false);
    const [jobRecommendations, setJobRecommendations] = useState([]);
    const [jobRecommendationsLoading, setJobRecommendationsLoading] = useState(false);
    const [editableLoading, setEditableLoading] = useState(false);
    const [isEditable, setIsEditable] = useState(null);
    const [editableId, setEditableId] = useState(null);
    const [industry, setIndustry] = useState('GE'); // general
    const [description, setDescription] = useState('');
    const history = useHistory();
    const [idChanged, setIdChanged] = useState(false);
    const [isPrivate, setIsPrivate] = useState(false);
    const [skillTitleConfirmText, setSkillTitleConfirmText] = useState('');
    const [skillTitleConfirmIndex, setSkillTitleConfirmIndex] = useState(-1);
    const [skillTitleIsRename, setSkillTitleIsRename] = useState(false);
    const [showTitleConfirm, setShowTitleConfirm] = useState(false);
    const [similarJobs, setSimilarJobs] = useState([]);
    const [loadingSimilarJobs, setLoadingSimilarJobs] = useState(false);
    const [showSimilarJobs, setShowSimilarJobs] = useState(true);
    const [selectedSuggestedGoal, setSelectedSuggestedGoal] = useState(null);
    const [skillIsEditableLoading, setSkillIsEditableLoading] = useState(false)

    const initTitle = useParam('init')
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);

    const formatSkill = useCallback(
      (s) => {
        if (s.isEditable)
            return s.title
        else
            return <>
                    {s.title} &nbsp;
                    <Chip
                        icon={<LaunchOutlined />}
                        style={{
                            backgroundColor: "#e8f4fd"
                        }}
                        clickable
                        component={Link}
                        target="_blank"
                        onClick={(e) => e.stopPropagation()}
                        to={`/${language}/dashboard/discovery/skills/${s.skillId}`}
                        label={"already exists"}
                    />
                </>
      },
      [],
    )
    

    const loadSimilarJobs = useCallback(
        (title, language) => {
            setSimilarJobs([]);
            cancelLoadSimilarJobs();
            if (title.length > 0 && language) {
                setLoadingSimilarJobs(true);
                getSimilarJobs(
                    title,
                    language,
                    res => setSimilarJobs(res.data.results),
                    null,
                    () => setLoadingSimilarJobs(false)
                );
            }
        },
        []
    );

    useEffect(
        () => {
            if (idChanged) {
                setSelectedSkills([]);
            }
        },
        [idChanged]
    )


    useEffect(
        () => { // load init title
            if (initTitle) {
                setTitle(initTitle);
                setIdChanged(true);
            }
        },
        [initTitle]
    );

    useEffect(
        () => {
            setShowSimilarJobs(true);
            loadSimilarJobs(title, language);
        },
        [title, language]
    );

    const checkEditable = useCallback(
        (title, language, industry, company, country, city, isPrivate) => {
            cancelIsEditableJob();
            setEditableLoading(false);
            setIsEditable(null);
            if (title.length > 0) {
                setEditableLoading(true);
                getIsEditableJob(
                    title,
                    language,
                    industry,
                    company,
                    country,
                    city,
                    isPrivate,
                    res => {
                        var data = res.data;
                        setIsEditable(data.editable);
                        setEditableId(data.id);
                        setEditableLoading(false);
                        setIdChanged(false);
                        if (data.editable) {
                            setShowSimilarJobs(false);
                        }
                    }
                )
            }
        },
        []
    );

    const handleIndustry = useCallback((e) => {
        setIndustry(e.target.value);
        setIdChanged(true);
    }, []);

    const handleChangePrivate = useCallback(
        (e) => {
            setIsPrivate(e.target.checked);
            setIdChanged(true);
        },
        [],
    )

    const handleTitleInput = useCallback((e, value) => {
        setTitle(value);
        setIdChanged(true);
        cancelJobRecommendations();

        if (value.length === 0) {
            setJobRecommendationsLoading(false);
            setJobRecommendations([]);
            cancelJobRecommendations();
        } else {
            setJobRecommendationsLoading(true);
            getJobRecommendations(value,
                res => {
                    setJobRecommendations(res.data);
                    setJobRecommendationsLoading(false);
                }
            );
        }

    }, []);

    const handleCheckJob = useCallback(
        () => {
            setDescription("");
            checkEditable(title, language, industry, company, country, city, isPrivate);
        },
        [title, language, industry, company, country, city, isPrivate],
    )

    const handleAddSkill = useCallback(
        (skill) => {
            setSkillIsEditableLoading(true)
            getIsEditableSkill(
                skill,
                language,
                res => {
                    setSelectedSkills(l => {
                        if (l.some(s => s.title === skill))
                            return l;
                        return [...l, {title: skill, isEditable: res.data.editable, skillId: res.data.skill_id}];
                    })
                    setSkillIsEditableLoading(false)
                }
            )
            setShowForm(false);
        },
        [language]);

    const handleCancelAddSkill = useCallback(
        () => setShowForm(false),
        []
    )

    const handleRemove = useCallback(
        (index) => {
            setSelectedSkills(skills => {
                skills.splice(index, 1);
                return [...skills];
            })
        },
        []
    )

    const handleSkillMove = useCallback(
        (fromIndex, toIndex) => {
            setSelectedSkills(
                skills => arrayMove(skills, fromIndex, toIndex)
            )
        },
        []
    )

    const handleSave = useCallback(() => {
        setLoading(true);
        doAddJob(
            title, 
            language, 
            description, 
            selectedSkills.map(s => s.title), 
            industry, 
            company, 
            country, 
            city, 
            isPrivate,  
            selectedSuggestedGoal, 
            res => {
                history.replace(`/${systemLanguage}/dashboard/discovery/jobs/` + res.id)
            },
            null,
            () => setLoading(false))
    }, [title, selectedSkills, language, industry, description, company, country, city, isPrivate, selectedSuggestedGoal])

    const handleSkillWindow = useCallback(() => {
        setShowForm(s => {
            return !s;
        });
    }, []);

    const handleTitleConfirmCancel = useCallback(
        () => setShowTitleConfirm(false),
        []
    );

    const handleTitleConfirmSelect = useCallback(
        skillTitle => {
            setSkillIsEditableLoading(true)
            getIsEditableSkill(
                skillTitle,
                language,
                res => {
                    if (skillTitleIsRename) {
                        setSelectedSkills(
                            skills => skills.map(
                                (s, index) => 
                                    index === skillTitleConfirmIndex ? 
                                        {
                                            title: skillTitle,
                                            isEditable: res.data.editable,
                                            skillId: res.data.skill_id
                                        } 
                                        :
                                        s
                                )
                        )
                    } else {
                        setSelectedSkills(
                            skills => [...skills, {title: skillTitle, isEditable: res.data.editable, skillId: res.data.skill_id}]
                        );
                    }
                    setSkillIsEditableLoading(false)
                }
            )
            setSkillTitleIsRename(false)
            setShowTitleConfirm(false);
        },
        [skillTitleConfirmIndex, skillTitleConfirmText, skillTitleIsRename, language]
    );

    const handleSkillConfirm = useCallback(
        skill => {
            setSkillIsEditableLoading(true)
            getIsEditableSkill(
                skill,
                language,
                res => {
                    setSelectedSkills(skills => (
                        [...skills, {title: skill, isEditable: res.data.editable, skillId: res.data.skill_id}]
                    ));
                    setSkillIsEditableLoading(false)
                }
            )
        },
        [language]
    )

    const handleLanguage = useCallback(
        (lang) => {
            setLanguage(lang);
            setIdChanged(true);
        },
        []
    );

    const handleSkillClick = useCallback( // rename topic
        (index, item) => {
            setSkillTitleConfirmIndex(index)
            setSkillTitleConfirmText(item.title)
            setShowTitleConfirm(true)
            setSkillTitleIsRename(true)
        },
        []
    );

    const onSelectedGoalChange = useCallback(
        (newGoal) => {
            setSelectedSuggestedGoal(newGoal)
        },
        []
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Add a new journey")}
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title="Go back" onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <StaticNote
                                title={<b>{t("Important Notes")}</b>}
                                id="discovery_add_goal"
                            >
                                <ul>
                                    <li>
                                        {t("A journey includes its required courses, every course has its own topics, and each topic will be covered by educational packages.")}
                                    </li>
                                    <li>
                                        {t("A course covers a knowledge area that is important for the journey.")}
                                    </li>
                                </ul>
                            </StaticNote>
                        </Paper>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} md className={classes.formField}>
                                    <Autocomplete options={jobRecommendations} value={title} disabled={loading} openOnFocus
                                        freeSolo selectOnFocus handleHomeEndKeys disableClearable loading={jobRecommendationsLoading}
                                        renderInput={(params) =>
                                            <LangTextField
                                                label={t("Title")}
                                                margin="normal"
                                                fullWidth
                                                required
                                                insideAutocomplete
                                                {...params}
                                                error={isEditable === false}
                                                language={language}
                                                onLanguageChange={handleLanguage}
                                            />
                                        }
                                        onInputChange={handleTitleInput} inputValue={title}
                                    />
                                </Grid>
                                <Grid item xs={12} md={4} className={classes.formField}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={isPrivate}
                                                onChange={handleChangePrivate}
                                                color="primary"
                                            />
                                        }
                                        label={t("Private Journey")}
                                        style={{ marginTop: "24px" }}
                                    />
                                </Grid>
                                {(loadingSimilarJobs || similarJobs.length > 0) &&
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', padding: "0 16px" }}>
                                        <IDiv
                                            show={showSimilarJobs}
                                            loading={loadingSimilarJobs}
                                            loadingTexts={[
                                                t('Searching in eDoer...'),
                                            ]}
                                            title={t("We have similar items in the system.")}
                                            help={t("If the item you are going to add is among them, define it, or contribute to it if it has been already defined.")}
                                            items={similarJobs}
                                            formatItem={(sj, index) => (
                                                <GoalChip goal={sj} key={index} />
                                            )}
                                        />
                                    </Grid>
                                }
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    <Paper>
                                        <Grid container>
                                            <Grid item xs={12} className={classes.formField} style={{ textAlign: "left" }}>
                                                <FormControl fullWidth variant="standard" required={isPrivate}>
                                                    <InputLabel id="industry-label">{t("Industry")}</InputLabel>
                                                    <Select value={industry} onChange={handleIndustry} label={t("Industry")} disabled={loading}>
                                                        {allIndustries.map((i, index) => (
                                                            <MenuItem value={i.code} key={index}>{t(i.label)}</MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <TextField label={t("Company")} required={isPrivate} value={company} onChange={e => setCompany(e.target.value) || setIdChanged(true)} fullWidth
                                                    helperText={t("Leave empty to make it general.")} disabled={loading} placeholder={t("General")} InputLabelProps={{
                                                        shrink: true
                                                    }} />
                                            </Grid>
                                            <Grid item xs={12} md={6} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <CountrySelect required={isPrivate} onChange={(e, v) => setCity('') || setCountry(v) || setIdChanged(true)} value={country}
                                                    disabled={loading} showGeneral />
                                                <FormHelperText>{t("Leave empty to make it general.")}</FormHelperText>
                                            </Grid>
                                            <Grid item xs={12} md={6} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <CitySelect required={isPrivate} value={city} countryCode={country} showGeneral onChange={(e, v) => setCity(v) || setIdChanged(true)} disabled={loading} />
                                                <FormHelperText>{t("Leave empty to make it general.")}</FormHelperText>
                                            </Grid>
                                        </Grid>

                                    </Paper>
                                </Grid>
                                <Collapse className={classes.formField} in={idChanged || isEditable === false} style={{ width: "100%" }}>
                                    <Button color="rose" onClick={handleCheckJob} disabled={loading || editableLoading || title.length === 0 || !language}>
                                        {t("Check Existence")}
                                    </Button>
                                    {editableLoading && <LinearProgress />}
                                    {isEditable === false &&
                                        <Alert severity="error">
                                            {t("This journey already exists in the system.")} (<Link to={`/${systemLanguage}/dashboard/discovery/jobs/${editableId}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Collapse>
                                {!idChanged && isEditable && (<>
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                        <Description
                                            style={{ padding: "10px" }}
                                            label={t("Journey Description")}
                                            help={t("You can describe the journey here. For example, you may explain the objective, importance, etc. of this journey.")}
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                            loading={loading}
                                            placeholder={t('Describe the journey')}
                                            lang={language}
                                            keyword={title}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                        <Paper>
                                            <Typography style={{ textAlign: 'left', padding: 10 }}>
                                                {t("Required Courses")}
                                                <Help>
                                                    {t("Here you can define the courses that needs to be covered for this journey.")} <br />
                                                    {t("In order to define them, you are able to use the Related Courses below, or directly type by pressing Add Course button.")}
                                                </Help>
                                            </Typography>
                                            <div style={{
                                                padding: "0 16px"
                                            }}
                                            >
                                                <Suggestions
                                                    goal={title}
                                                    lang={language}
                                                    onSkillAdd={handleSkillConfirm}
                                                    onChange={onSelectedGoalChange}
                                                />
                                            </div>
                                            <ListDnd
                                                className={classes.listRoot}
                                                items={selectedSkills}
                                                onRemove={handleRemove}
                                                onMove={handleSkillMove}
                                                dragClass={classes.dragClass}
                                                emptyText={t("No courses have been added to this journey yet.")}
                                                onClick={handleSkillClick}
                                                itemTitle={t("Click to rename")}
                                                formatLabel={formatSkill}
                                                loadingLastItem={skillIsEditableLoading}
                                            />
                                            <MuiButton
                                                fullWidth
                                                style={{
                                                    marginTop: -10,
                                                }}
                                                onClick={handleSkillWindow}
                                                startIcon={<AddIcon />}
                                            >
                                                {t("Add Course")}
                                            </MuiButton>
                                            <AddSkillWindow
                                                open={showForm}
                                                loading={loading}
                                                onAdd={handleAddSkill}
                                                onCancel={handleCancelAddSkill}
                                                disabledSave={loading}
                                            />
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} className={classes.formField}>
                                        <Button color="rose" onClick={handleSave} disabled={isEditable === false || loading || selectedSkills.length === 0 || title.length === 0 || industry.length === 0}
                                            fullWidth>{t("Save")}</Button>
                                    </Grid>
                                </>)}
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
                <TitleConfirmDialog
                    title={skillTitleConfirmText}
                    open={showTitleConfirm}
                    onCancel={handleTitleConfirmCancel}
                    onSelect={handleTitleConfirmSelect}
                    label={t("Confirm course's title:")}
                    okButtonLabel={skillTitleIsRename ? t("Rename") : t("Add")}
                    isCourse
                />
            </Grid>
        </Grid>
    )
}

Add.propTypes = {

}

export default Add
