import { Box, CircularProgress, Grid, IconButton, Paper, Typography, Link as MuiLink } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import { backend_base_url } from '../../../../settings'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import GridContainer from '../../../components/Grid/GridContainer'
import GridItem from '../../../components/Grid/GridItem'
import Help from '../../../components/Help'
import { getLocation } from '../../../components/JobItem/utils'
import LangIcon from '../../../components/LangIcon'
import LoginWarning from '../../../components/LoginWarning'
import GoalLogo from '../../../components/Logos/Goal'
import ToggleDiv from '../../../components/ToggleDiv'
import VotingTable from '../../../components/Vote/VotingTable'
import WordSuggestion from '../../../components/WordSuggestion'
import useStyles from '../../styles/SkillsStyle'
import { addWordOpinion } from '../utils'
import SkillsTable from './SkillsTable'
import { addDeleteSuggestion, addEditSuggestion, addNewSuggestion, doSkillVote, doSuggestionVote, filterSuggestions, formatWord, getJob, processVote } from './utils'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import IAlert from '../../../components/ISys/IAlert'


function Jobs({ requireLogin, navHistory, onNav, enqueueSnackbar, ...props }) {
    const classes = useStyles();
    const { id: strJobId } = useParams();

    const jobId = useMemo(
        () => {
            if (strJobId) {
                return parseInt(strJobId);
            }
        },
        [strJobId]
    )

    const [job, setJob] = useState(null);
    const [skills, setSkills] = useState([]);
    const [addSuggestions, setAddSuggestions] = useState([]);
    const [delSuggestions, setDelSuggestions] = useState([]);
    const [movSuggestions, setMovSuggestions] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);
    const [votingSkills, setVotingSkills] = useState(false);
    const [votingSuggestions, setVotingSuggestions] = useState(false);
    const [suggestionsOpen, setSuggestionsOpen] = useState(false);
    const [edoerSuggestionsOpen, setEdoerSuggestionsOpen] = useState(false)
    const [words, setWords] = useState([]);
    const [wordsLoading, setWordsLoading] = useState(false);
    const [addWordObject, setAddWordObject] = useState(null);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [reloadNeeded, setReloadNeeded] = useState(true);

    const history = useHistory();

    const location = useMemo(
        () => job ? getLocation(job['country'], job['city']) : "",
        [job]
    );

    useEffect(() => { // load job and suggestions
        setLoading(true);
        getJob(jobId, (data, suggestions) => { // load
            const suggs = filterSuggestions(suggestions.results);
            setSkills(data.jobskill_set.sort((a, b) => a.order - b.order));
            setJob(data);
            setAddSuggestions(suggs.add);
            setDelSuggestions(suggs.remove);
            setMovSuggestions(suggs.reorder);
            setWords(data.extracted_skills);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Journey was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
        setReloadNeeded(false);
    }, [jobId, reloadNeeded]);

    const handleSkillVote = useCallback((id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingSkills(true);
        doSkillVote(jobId, id, selfVote, () => {
            setVotingSkills(false);
            setSkills(t => processVote(t, id, up, down, selfVote, 'skill'))
        });
    }, [jobId, requireLogin]);

    const handleSuggestionVote = useCallback((setter) => (id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingSuggestions(true);
        doSuggestionVote(id, selfVote, () => {
            setVotingSuggestions(false);
            setter(s => processVote(s, id, up, down, selfVote))
        })
    }, [requireLogin]);

    const handleDelete = useCallback((skillId) => { // deleting suggestions
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addDeleteSuggestion(jobId, skillId, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [jobId, requireLogin]);

    const handleNew = useCallback((data) => {
        if (requireLogin) return setShowLoginWarning(true);

        setAddWordObject(null); // clear add word
        setLoading(true);
        addNewSuggestion(jobId, data, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [jobId, requireLogin])

    const handleEdit = useCallback((skillId, data) => {
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addEditSuggestion(jobId, skillId, data, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [jobId, requireLogin])

    const formatDelTitle = useCallback(s => {
        const skill = skills.find(t => t.skill === s.payload.skill_id)
        return <span>
            <Link to={`/${language}/dashboard/discovery/skills/${skill.skill}`} target="_blank"><i><b>{skill.skill_title}</b></i></Link>
        </span>
    }, [skills, t])

    const formatMovTitle = useCallback(s => {
        const skill = skills.find(t => t.skill === s.payload.skill_id);
        const prevSkillIndex = skills.findIndex(t => t.order > s.payload.skill_order);
        const prevSkill = skills[prevSkillIndex === -1 ? skills.length - 1 : prevSkillIndex - 1];

        return <span>
            <Link to={`/${language}/dashboard/discovery/skills/${skill.skill}`} target="_blank"><i><b>{skill.skill_title}</b></i></Link>:
            {
                prevSkillIndex === 0 ?
                    <b>&nbsp;{t("Move to the top.")}</b>
                    :
                    <>&nbsp;{t("Move after ")}<i><b><Link to={`/${language}/dashboard/discovery/skills/${prevSkill.skill}`} target="_blank">{prevSkill.skill_title}</Link></b></i></>
            }
        </span>
    }, [skills, t])

    const formatAddTitle = useCallback(s => {
        const prevSkillIndex = skills.findIndex(t => t.order > s.payload.skill_order);
        const prevSkill = skills[prevSkillIndex === -1 ? skills.length - 1 : prevSkillIndex - 1]

        return <span>
            <i><b>
                {s.payload.skill_id ?
                    <Link to={`/${language}/dashboard/discovery/skills/${s.payload.skill_id}`} target="_blank">{s.payload.skill_title}</Link>
                    :
                    s.payload.skill_title
                }
            </b></i>:
            {
                prevSkillIndex <= 0 ?
                    <b>&nbsp;{t("to the top")}</b>
                    :
                    <>&nbsp;{t("after")} <i><b><Link to={`/${language}/dashboard/discovery/skills/${prevSkill.skill}`} target="_blank">{prevSkill.skill_title}</Link></b></i></>
            }
        </span>

    }, [skills, t]);

    const handleAddWord = useCallback(
        (id, word) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "AD",
                () => {
                    setAddWordObject(word);
                    setWords(
                        words => words.filter(w => w.id !== id)
                    );
                },
                null,
                () => setWordsLoading(false)
            )
        },
        []
    );

    const handleNeutral = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "NE",
                null,
                null,
                () => setWordsLoading(false)
            )
        },
        []
    );

    const handleAddedWord = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "AA",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: "info",
                        }
                    );
                    setWords(
                        words => words.filter(w => w.id !== id)
                    )
                },
                null,
                () => setWordsLoading(false)
            )
        },
        [t, enqueueSnackbar]
    );

    const handleIrrelaventWord = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "IR",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: "info",
                        }
                    );
                    setWords(
                        words => words.filter(w => w.id !== id)
                    )
                },
                null,
                () => setWordsLoading(false)
            )
        },
        [t, enqueueSnackbar]
    )

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history]);

    return (
        <>
            {requireLogin &&
                <LoginWarning show={showLoginWarning} onSelect={handleLogin} />
            }
            {isLoading ?
                <Box p={10} textAlign="center">
                    <CircularProgress />
                </Box>
                :
                criticalError ?
                    <Box p={10} textAlign="center">
                        {criticalError}
                    </Box>
                    :
                    <>
                        <DiscoveryNav job={job} navHistory={navHistory} onNav={onNav} />
                        <GridContainer>
                            <GridItem xs={12}>
                                <Card>
                                    <CardHeader color="rose" style={{ display: 'flex', alignItems: 'center' }}>
                                        <GoalLogo picture={job.picture} style={{ marginRight: 10 }} size='large' />
                                        <div style={{ flexGrow: 1 }}>
                                            <h3 className={classes.cardTitleWhite}>
                                                {job['title']} <LangIcon language={job.lang} small />
                                                {job.company &&
                                                    <Typography variant="caption" title={t("Company")} className={classes.company}>
                                                        {t("by")} {job.company}
                                                    </Typography>
                                                }
                                                <Box component="span" ml={2}>
                                                    {(job.is_contributor || job.is_admin) &&
                                                        <IconButton style={{ color: "white" }} title={t("Edit the information of this journey")} size="small" onClick={() => history.push(`/${language}/dashboard/discovery/jobs/edit/${job['id']}`)}>
                                                            <EditIcon />
                                                        </IconButton>
                                                    }
                                                </Box>
                                            </h3>
                                            <h4 className={classes.cardCategoryWhite} title={t("Industry and Location")}>
                                                <em>{t(job['industry_label'])}</em>
                                                {location &&
                                                    (
                                                        <> in
                                                            <em>
                                                                {" " + location}
                                                            </em>
                                                        </>
                                                    )
                                                }
                                            </h4>
                                        </div>
                                        <IconButton
                                            style={{ color: "white", marginRight: 20 }}
                                            component={MuiLink}
                                            target="_blank"
                                            href={`${backend_base_url}jobs/download-json-export/?job_id=${job.id}`}
                                            title={t("Download the structure of this item as JSON")}
                                            size="small"
                                        >
                                            <CloudDownloadIcon />
                                        </IconButton>
                                    </CardHeader>
                                    <CardBody>
                                        {job.description &&
                                            <Paper elevation={2} className={classes.descriptionBox}>
                                                <div>
                                                    {job.description}
                                                </div>
                                            </Paper>}
                                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
                                            <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <SkillsTable onVote={handleSkillVote} skills={skills} onDelete={handleDelete} onEdit={handleEdit}
                                                        disabled={votingSkills} onNew={handleNew} headClass={classes.addSugg}
                                                        addObject={addWordObject} />
                                                </Grid>
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <ToggleDiv
                                                        title={<b>{t("Suggestions")}</b>}
                                                        titleClassName={classes.addSugg}
                                                        open={suggestionsOpen}
                                                        onChange={o => setSuggestionsOpen(o)}
                                                        help={
                                                            <Help>
                                                                {t("Here you can find:")}
                                                                <ul>
                                                                    <li>{t("eDoer Intelligence which you can validate, and add if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}</li>
                                                                    <li>{t("Up-vote/Down-vote existing sugestions (i.e. Add, Delete, or Reorder). eDoer automatically decides to accept or reject the suggestions based on your votes.")}</li>
                                                                </ul>
                                                            </Help>
                                                        }
                                                        style={{
                                                            marginBottom: 8
                                                        }}>
                                                        <Paper
                                                            style={{
                                                                padding: 8
                                                            }}>
                                                            <ToggleDiv
                                                                title={<b>{t("eDoer Intelligence")}</b>}
                                                                titleClassName={classes.edoerSugg}
                                                                open={edoerSuggestionsOpen}
                                                                onChange={o => setEdoerSuggestionsOpen(o)}
                                                                help={
                                                                    <Help>
                                                                        {t("You can validate the eDoer's intelligent suggestions and add them if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}
                                                                    </Help>
                                                                }
                                                                style={{
                                                                    marginBottom: 8
                                                                }}
                                                            >    
                                                                {words.length > 0 ?
                                                                    <WordSuggestion
                                                                        words={words}
                                                                        pause={wordsLoading || !suggestionsOpen}
                                                                        disabled={wordsLoading}
                                                                        onAdd={handleAddWord}
                                                                        onIrrelavent={handleIrrelaventWord}
                                                                        onAlreadyAdded={handleAddedWord}
                                                                        onNeutral={handleNeutral}
                                                                        formatWord={formatWord}
                                                                        question={t("Is this a course of this journey?")}
                                                                    />
                                                                    :
                                                                    <>
                                                                        {new Date().getTime() - new Date(job.created_at) < (30 * 60 * 1000) ?
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("We are searching for suggestions to help you develop this page. Please check again this part in a few minutes.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                            :
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("Currently, there is no eDoer-generated suggestion for this page.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                        }
                                                                    </>

                                                                }
                                                            </ToggleDiv>
                                                            <VotingTable onVote={handleSuggestionVote(setAddSuggestions)} data={addSuggestions} disabled={votingSuggestions}
                                                                objectType="SU" title={t("ADD")} className={classes.hasYMargin} formatTitle={formatAddTitle}
                                                                headClass={classes.movSugg} />
                                                            <VotingTable onVote={handleSuggestionVote(setDelSuggestions)} data={delSuggestions} disabled={votingSuggestions}
                                                                objectType="SU" title={t("DELETE")} className={classes.hasYMargin} formatTitle={formatDelTitle}
                                                                headClass={classes.movSugg} />
                                                            <VotingTable onVote={handleSuggestionVote(setMovSuggestions)} data={movSuggestions} disabled={votingSuggestions}
                                                                objectType="SU" title={t("REORDER")} className={classes.hasYMargin} formatTitle={formatMovTitle}
                                                                headClass={classes.movSugg} />
                                                        </Paper>
                                                    </ToggleDiv>
                                                </Grid>
                                            </Grid>
                                        </Paper>

                                    </CardBody>
                                </Card>
                            </GridItem>
                        </GridContainer>
                    </>
            }
        </>
    )
}

Jobs.defaultProps = {
    requireLogin: false,
}

export default withSnackbar(Jobs)
