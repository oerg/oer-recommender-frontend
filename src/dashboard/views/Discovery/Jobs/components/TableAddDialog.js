import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import SkillInput from '../../../../components/SystemInputs/SkillInput';

function TableAddDialog({ show, onClose, skills, defaultTitle, ...props }) {
    const [orderValue, setOrderValue] = useState(-1);
    const [title, setTitle] = useState('');
    const { t } = useTranslation(['dashboard']);

    useEffect(
        () => {
            if (defaultTitle)
                setTitle(defaultTitle);
        },
        [defaultTitle]
    );

    const getOrder = useCallback(
        after => { // gets after index and gives a valid order number
            if (after === -1) { // wants to be the first
                if (skills.length > 0)
                    return skills[0].order - 16.0;
                return 1; // the only skill
            } else if (after === skills.length - 1) { // wants to be the last
                return skills[after].order + 16.0;
            }
            return (skills[after + 1].order + skills[after].order) / 2.0;
        },
        [skills]
    );

    const handleDialog = useCallback(
        (save, title, order) => () => {
            if (save) {
                onClose(
                    true,
                    {
                        skill_title: title,
                        skill_order: getOrder(order),
                    }
                )
            } else {
                onClose(false, {});
            }
        },
        [getOrder, onClose]
    );

    const handleOrderSelect = useCallback(e => {
        setOrderValue(e.target.value);
    }, [])

    const handleTitleChange = useCallback(value => {
        setTitle(value);
    }, [])

    return (
        <Dialog open={show} onClose={onClose} disableEscapeKeyDown disableBackdropClick {...props}>
            <DialogTitle>{t("Suggest adding a new course")}</DialogTitle>
            <DialogContent style={{ minWidth: 400 }}>
                <DialogContentText component="div">
                    <SkillInput label={t("Title")} onSelect={handleTitleChange} newValue={defaultTitle} clearOnSelect={false} />
                    <FormControl fullWidth>
                        <InputLabel>{t("Place new course after")}</InputLabel>
                        <Select value={orderValue} onChange={handleOrderSelect}>
                            <MenuItem value={-1}><i>- ({t("Set as first course")})</i></MenuItem>
                            {skills.map((ts, index) => (
                                <MenuItem value={index} key={index}>{t("After ")}&nbsp;<b><i>{ts.skill_title}</i></b></MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </DialogContentText>
                <Alert severity='warning'>
                    {t("Suggestion will be accepted automatically if you are an epxert in the course.")}
                </Alert>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(false)}>
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(true, title, orderValue)} disabled={title.length === 0} color="primary">
                    {t("Save")}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default TableAddDialog