import { Dialog, DialogActions, DialogContent } from '@material-ui/core'
import React, { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import Button from '../../../../components/CustomButton/Button'
import SkillInput from '../../../../components/SystemInputs/SkillInput'

function AddSkillWindow({ loading, disabledSave, onCancel, onAdd, ...props }) {
    const { t } = useTranslation(['dashboard'])
    const [selectedTopic, setSelectedTopic] = useState("")

    const handleSelect = useCallback(
        (topic) => setSelectedTopic(topic),
        []
    )

    const handleAdd = useCallback(
        () => {
            onAdd && onAdd(selectedTopic)
        },
        [selectedTopic, onAdd]
    )

    return (
        <Dialog
            keepMounted={false}
            onClose={onCancel}
            {...props}
        >
            <DialogContent dividers style={{ minWidth: 400 }}>
                <SkillInput
                    loading={loading}
                    fullWidth
                    onSelect={handleSelect}
                    label={t("Search/Add Course")}
                />
            </DialogContent>
            <DialogActions>
                <Button color="danger" onClick={onCancel}>
                    {t("Cancel")}
                </Button>
                <Button color="rose" onClick={handleAdd} disabled={disabledSave || !selectedTopic} fullWidth>
                    {t("Add course to the journey")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddSkillWindow