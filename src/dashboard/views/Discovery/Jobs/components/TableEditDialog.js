import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';

function TableEditDialog({ id, title, show, onClose, skills }) {
    const [reorderValue, setReorderValue] = useState(0);
    const [orderChanged, setOrderChanged] = useState(false);
    const [myIndex, setMyIndex] = useState(-1);
    const { t } = useTranslation(['dashboard']);

    useEffect(() => { // update form in a new window
        let mi = skills.findIndex(t => t.skill === id);
        setMyIndex(mi)
        setReorderValue(mi - 1);
        setOrderChanged(false);
    }, [id, skills])

    const getOrder = useCallback(
        after => { // gets after index and gives a valid order number
            if (after === -1) { // wants to be the first
                if (skills.length > 0)
                    return skills[0].order - 16.0;
                return 1; // the only skill
            } else if (after === skills.length - 1) { // wants to be the last
                return skills[after].order + 16.0;
            }
            return (skills[after + 1].order + skills[after].order) / 2.0;
        },
        [skills]
    );

    const handleDialog = useCallback((id, save) => () => {
        if (save) {
            onClose(id, true, { skill_order: getOrder(reorderValue) });
        }
        onClose(id, false);
    }, [onClose, getOrder, reorderValue]);

    const handleOrderSelect = useCallback(e => {
        setReorderValue(e.target.value);
        setOrderChanged(true);
    }, [])

    return (
        <Dialog open={show} onClose={onClose} disableBackdropClick disableEscapeKeyDown>
            <DialogTitle>
                {t("Reorder suggestion for")} '{title}'
            </DialogTitle>
            <DialogContent>
                <DialogContentText component="div">
                    <FormControl fullWidth>
                        <InputLabel>Place '<i>{title}</i>' after</InputLabel>
                        <Select value={reorderValue} onChange={handleOrderSelect}>
                            <MenuItem value={-1} disabled={myIndex === 0}><i>- ({t("Set as first course")})</i></MenuItem>
                            {skills.map((ts, index) => (
                                <MenuItem value={index} key={index} disabled={myIndex === index}>
                                    {myIndex === index ? '=> ' : t("After ")}
                                    &nbsp;<b><i>{ts.skill_title}</i></b>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(id, false)}>
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(id, true)} disabled={!orderChanged} color="primary">
                    {t("Send")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default TableEditDialog