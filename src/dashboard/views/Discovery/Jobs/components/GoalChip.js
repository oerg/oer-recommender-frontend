import { Chip, Typography } from '@material-ui/core'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { useLanguage } from '../../../../../i18n'
import { getLocation } from '../../../../components/JobItem/utils'
import { getIndustryLabel } from '../../../helpers'

function GoalChip({ goal, ...props }) {
    const { t } = useTranslation(['dashboard'])
    const systemLanguage = useLanguage()

    return (
        <Chip
            style={{ margin: 4, height: "100%" }}
            label={
                <>
                    <Typography variant="caption" title={t("Company")}>
                        {goal.company ? `${t("by")} ${goal.company}` : <>&nbsp;</>}
                    </Typography>
                    <Typography>
                        {goal['title']}
                    </Typography>
                    <Typography variant="caption" title={t("Industry and Location")}>
                        <em>{t(getIndustryLabel(goal.industry))}</em>
                        {getLocation(goal['country'], goal['city']) &&
                            (
                                <> in
                                    <em>
                                        {" " + getLocation(goal['country'], goal['city'])}
                                    </em>
                                </>
                            )
                        }
                    </Typography>
                </>
            }
            variant="outlined"
            color="primary"
            title={goal.title}
            component={Link}
            target="_blank"
            to={`/${systemLanguage}/dashboard/discovery/jobs/${goal.id}`}
            clickable
            {...props}
        />
    )
}

export default GoalChip