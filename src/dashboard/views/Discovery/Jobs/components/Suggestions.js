import { Box, Chip, Collapse, makeStyles, Paper, Tab, Tabs, Typography } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import IDiv from '../../../../components/ISys/IDiv'
import TitleConfirmDialog from '../../../../components/TitleConfirmDialog'
import { getRelatedJobs, getRelatedSkills } from '../utils'

const useStyles = makeStyles({
    suggestionChip: {
        margin: 4,
    },
    innerDiv: {
        maxHeight: "none",
    }
})

function Suggestions({ goal, lang, onSkillAdd, onChange, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const [isGoalsLoaded, setIsGoalsLoaded] = useState(false)
    const [goals, setGoals] = useState([])
    const [suggestions, setSuggestions] = useState([])
    const [goalsLoading, setGoalsLoading] = useState(false)
    const [selectedGoal, setSelectedGoal] = useState(null)
    const [suggestionsLoading, setSuggestionsLoading] = useState(false)

    const [titleConfirm, setTitleConfirm] = useState('')
    const [showConfirm, setShowConfirm] = useState(false)
    const [titleConfirmIndex, setTitleConfirmIndex] = useState(0)

    const handleGoalSelect = useCallback(
        (e, value) => {
            setSelectedGoal(value)
            onChange && onChange(value)
            setSuggestionsLoading(true)
            getRelatedSkills(
                value,
                lang,
                (res) => {
                    setSuggestions(res.data)
                },
                null,
                () => setSuggestionsLoading(false)
            )
        },
        [lang, onChange]
    )

    useEffect(
        () => {
            setIsGoalsLoaded(false)
            setGoals([])
            setSuggestions([])
            setGoalsLoading(true)
            getRelatedJobs(
                goal,
                lang,
                (res) => {
                    setGoals(res.data)
                    setIsGoalsLoaded(true)
                    if (res.data.length > 0) {
                        setSelectedGoal(res.data[0].value)
                        handleGoalSelect(null, res.data[0].value)
                    }
                },
                null,
                () => setGoalsLoading(false)
            )
        },
        [goal, lang, handleGoalSelect]
    )

    const handleChip = useCallback(
        (index, suggestion) => () => {
            setTitleConfirm(suggestion)
            setTitleConfirmIndex(index)
            setShowConfirm(true)
        },
        []
    )

    const handleTitleConfirmCancel = useCallback(
        () => {
            setShowConfirm(false)
        },
        []
    )

    const handleTitleConfirmSelect = useCallback(
        (title) => {
            onSkillAdd(title)
            setShowConfirm(false)
            setSuggestions(
                suggestions => {
                    suggestions.splice(titleConfirmIndex, 1)
                    return [...suggestions]
                }
            );
        },
        [titleConfirm, titleConfirmIndex, onSkillAdd]
    )

    return (
        <IDiv
            loading={goalsLoading || suggestionsLoading}
            loadingTexts={!isGoalsLoaded ? [
                t('Finding similar items on ESCO...')
            ] : [
                t('Checking the ESCO standard...'),
                t('Finding relatred courses...'),
            ]}
            title={(isGoalsLoaded && goals.length > 0) ?
                t('System has found related courses.')
                :
                t('No similar journeys was found.')
            }
            innerClassName={classes.innerDiv}
            {...props}
        >
            <Typography variant='body' style={{ display: "block", padding: 16 }}>
                {t("To receive the most related courses, please select the closest journey from the list below.")}
            </Typography>
            <Paper
                style={{
                    backgroundColor: 'rgb(232, 244, 253)',
                }}
            >
                <Tabs
                    value={selectedGoal}
                    onChange={handleGoalSelect}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="on"
                    centered
                >
                    {goals.map(
                        (goal, index) => <Tab
                            label={goal.key}
                            value={goal.value}
                            key={index}
                            disabled={suggestionsLoading}
                        />
                    )}
                </Tabs>
            </Paper>
            <Collapse
                in={isGoalsLoaded && !suggestionsLoading}
            >
                <Box
                    padding={1}
                    maxHeight={250}
                    style={{
                        overflowY: "auto"
                    }}
                >
                    {suggestions.map(
                        (sugg, index) => (
                            <Chip
                                label={sugg}
                                tabIndex="-1"
                                variant="outlined"
                                key={index}
                                onClick={handleChip(index, sugg)}
                                className={classes.suggestionChip}
                                clickable
                                title={t("Add to courses")}
                                color="primary"
                            />
                        )
                    )}
                </Box>
            </Collapse>
            <TitleConfirmDialog
                title={titleConfirm}
                open={showConfirm}
                onCancel={handleTitleConfirmCancel}
                onSelect={handleTitleConfirmSelect}
                label={t("Confirm course's title:")}
                okButtonLabel={t("Add")}
                isCourse
            />
        </IDiv>
    )
}

export default Suggestions