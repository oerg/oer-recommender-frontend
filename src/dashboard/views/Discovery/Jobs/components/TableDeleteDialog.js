import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next';
import DeleteIcon from '@material-ui/icons/Delete';

function TableDeleteDialog({ id, title, show, onClose }) {
    const { t } = useTranslation(['dashboard']);

    const handleDialog = useCallback((id, save) => () => {
        onClose(id, save);
    }, [onClose]);

    return (
        <Dialog open={show} onClose={onClose} disableBackdropClick disableEscapeKeyDown>
            <DialogTitle>
                {t("Delete Suggestion")}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {t("Are you sure you want to issue a delete suggestion for the following course?")}<br />
                    '<em>{title}</em>'
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(id, false)}>
                    {t("No")}
                </Button>
                <Button startIcon={<DeleteIcon />} onClick={handleDialog(id, true)} color="primary">
                    {t("Yes")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default TableDeleteDialog