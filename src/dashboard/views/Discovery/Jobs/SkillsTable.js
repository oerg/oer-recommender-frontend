import { ButtonGroup, ClickAwayListener, Fab, Grow, IconButton, makeStyles, Paper, Popper, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow, withStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteSweepIcon from '@material-ui/icons/DeleteSweep';
import EditIcon from '@material-ui/icons/Edit';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import Help from '../../../components/Help';
import Icons from '../../../components/Icons';
import Vote from '../../../components/Vote';
import TableAddDialog from './components/TableAddDialog';
import TableDeleteDialog from './components/TableDeleteDialog';
import TableEditDialog from './components/TableEditDialog';

const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
    },
    buttonRoot: {
        display: 'inline-block',
        marginRight: 8,
        marginTop: 15,
    },
    button: {
    },
    hidden: {
        display: 'none',
    },
    stickyFooter: {
        left: 0,
        bottom: -20,
        zIndex: 2,
        position: 'sticky',
        textAlign: 'center',
    },
    oerGreen: {
        backgroundColor: '#005f5f',
        color: '#FFF',
        opacity: 0.5,
        transition: 'margin-bottom 0.3s',
        '&:hover': {
            backgroundColor: "#004555",
            color: '#AAA',
            opacity: 1,
            marginBottom: 10
        }
    },
    headRow: {
        height: 60,
    },
    dialog: {
        minWidth: 400
    },
    learnableIcon: {
        marginLeft: 6,
    }
})

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function SkillsTable({ className, skills, onVote, onDelete, onEdit, onNew, disabled, headClass, addObject, ...props }) {
    const classes = useStyles();
    const [showDelete, setShowDelete] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    const [editId, setEditId] = useState(0); // used for both edit and delete
    const [editTitle, setEditTitle] = useState(''); // used for both edit and delete
    const [showNew, setShowNew] = useState(false);
    const [editMenuOpen, setEditMenuOpen] = useState(false);
    const [editRef, setEditRef] = useState(null);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const handleEditMenuClose = useCallback(() => {
        setEditMenuOpen(false);
    }, []);

    const handleEditMenuOpen = useCallback((id, title) => e => {
        setEditTitle(title);
        setEditId(id);

        let newRef = e.target
        setEditRef(ref => {
            if (ref === newRef)
                setEditMenuOpen(p => !p)
            else
                setEditMenuOpen(true);
            return newRef;
        });
    }, []);

    const handleEdit = useCallback(() => {
        setShowEdit(true);
    });

    const handleDelete = useCallback(() => {
        setShowDelete(true);
    });

    const handleDeleteDialog = useCallback((id, del) => {
        setShowDelete(false);
        if (!id) return;
        if (del) onDelete(id);
    }, [onDelete]);

    const handleEditDialog = useCallback((id, edit, data) => {
        if (edit) onEdit(id, data);
        setShowEdit(false);
    }, [onEdit]);

    const handleShowNew = useCallback(() => {
        setShowNew(true);
    }, [])

    const handleNewDialog = useCallback((save, data) => {
        setShowNew(false);
        if (save) onNew(data);
    }, [onNew]);

    useEffect(
        () => {
            if (addObject) {
                setShowNew(true);
            }
        },
        [addObject]
    )

    return (<>
        <TableAddDialog show={showNew} onClose={handleNewDialog} skills={skills} PaperProps={{ className: classes.dialog }} defaultTitle={addObject} />
        <TableDeleteDialog id={editId} show={showDelete} title={editTitle} onClose={handleDeleteDialog} />
        <TableEditDialog id={editId} show={showEdit} title={editTitle} onClose={handleEditDialog} skills={skills} />
        <Popper open={editMenuOpen} anchorEl={editRef} transition placement="left">
            {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                    <ClickAwayListener onClickAway={handleEditMenuClose}>
                        <ButtonGroup size="small" color="default">
                            <IconButton title={t("Suggest a re-order")} onClick={handleEdit}><ImportExportIcon /></IconButton>
                            <IconButton title={t("Suggest a delete")} onClick={handleDelete}><DeleteSweepIcon /></IconButton>
                        </ButtonGroup>
                    </ClickAwayListener>
                </Grow>
            )}
        </Popper>

        <TableContainer component={Paper} className={className} style={{ height: '400px' }} onScroll={handleEditMenuClose} {...props}>
            <Table size="small" stickyHeader>
                <TableHead>
                    <TableRow className={classes.headRow}>
                        <TableCell component="th" className={headClass}>{t("Including Courses")}</TableCell>
                        <TableCell component="th" align="right" className={headClass}>{t("Actions")}
                            <Help>
                                {t("Here you can:")}
                                <ul>
                                    <li>{t("suggest to Delete/Reorder a course for this journey.")}</li>
                                    <li>{t("Up-vote/Down-vote each course which helps users find the most important courses in this journey.")}</li>
                                </ul>
                            </Help>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {skills.length === 0 ?
                        <TableRow>
                            <TableCell colSpan={3}>
                                <em>{t("No courses.")}</em>
                            </TableCell>
                        </TableRow>
                        :
                        skills.map((i, index) => (
                            <StyledTableRow key={index}>
                                <TableCell>
                                    <Link to={`/${language}/dashboard/discovery/skills/${i.skill}`} title={t("Go to this course")}>
                                        {i.skill_title}
                                    </Link>
                                    <Icons learnable={i.skill_learnable} assessmentable={i.skill_assessmentable} withBorder={false} addable={i.skill_addable}
                                        className={classes.learnableIcon} hideLearnable={i.skill_learnable} hideAssessmentable={i.skill_assessmentable} hideAddable={i.skill_addable} />
                                </TableCell>
                                <TableCell align="right">
                                    <IconButton size="small" onClick={handleEditMenuOpen(i.skill, i.skill_title)}
                                        className={classes.buttonRoot} title={t("Suggest an edit")}>
                                        <EditIcon />
                                    </IconButton>
                                    <Vote selfVote={i.vote} up={i.upvote_count} down={i.downvote_count} onChange={onVote(i.skill)} className={classes.vote} disabled={disabled} />
                                </TableCell>
                            </StyledTableRow>
                        ))}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TableCell colSpan={3} align="right" classes={{
                            footer: classes.stickyFooter
                        }}>
                            <Fab size="small" className={classes.oerGreen} title={t("Suggest adding a course")} disabled={disabled} onClick={handleShowNew}>
                                <AddIcon />
                            </Fab>
                        </TableCell>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    </>)
}

export default SkillsTable
