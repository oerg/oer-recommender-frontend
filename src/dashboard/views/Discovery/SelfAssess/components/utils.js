import axios from "axios";
import { requestAddHandlers } from "../../../helpers";

export function handleUpload(file) {
    return new Promise(
        (resolve, reject) => {
            var data = new FormData();
            data.append('file', file);
            data.append('title', file.name);
            data.append('mime_type', file.type);
            data.append('is_question', true);
            axios.post(
                '/oers/upload-oer/',
                data
            )
            .then(
                res => {
                    resolve({
                        data: {
                            link: res.data.url
                        }
                    })
                }
            )
            .catch(
                err => reject(err)
            )
        }
    )
}

export const toolbar = {
    options: ['inline', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link', 'image', 'remove'],
    image: {
        urlEnabled: false,
        uploadEnabled: true,
        alignmentEnabled: true,
        uploadCallback: handleUpload,
        previewImage: true,
        inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
        alt: { present: true, mandatory: false },
        defaultSize: {
          height: 'auto',
          width: 'auto',
        },
    },
}

export function saveScales(skill_id, options, description, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post(
            '/assessments/questions/add-self-assessment-options/',
            {
                skill_id, options, description,
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function saveAddQuestion(skill_id, question_text, order, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post(
            '/assessments/questions/add-self-assessment-question/',
            {
                skill_id, question_text, order
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function saveUpdateQuestion(question_id, question_text, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(
            '/assessments/questions/update-self-assessment-question/',
            {
                question_id, question_text
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function saveReorderQuestion(question_id, order, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(
            '/assessments/questions/reorder-self-assessment-question/',
            {
                question_id, order
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function saveDeleteQuestion(question_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.delete(
            '/assessments/questions/delete-self-assessment-question/',
            {
                data: { question_id }
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function getSelfAssessment(skill_id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(
            `/skills/get-self-assessment-exam/?skill_id=${skill_id}`
        ),
        onLoad,
        onError,
        onEnd
    )
}