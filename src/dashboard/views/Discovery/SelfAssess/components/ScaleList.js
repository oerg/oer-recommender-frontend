import { Button, Collapse, LinearProgress, makeStyles, Paper } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import SaveIcon from '@material-ui/icons/Save'
import clsx from 'classnames'
import { ContentState, convertToRaw, EditorState } from 'draft-js'
import draftjsToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'
import React, { useCallback, useEffect, useState } from 'react'
import { Editor } from 'react-draft-wysiwyg'
import { useTranslation } from 'react-i18next'
import ScaleItem from './ScaleItem'
import { toolbar } from './utils'

const useStyles = makeStyles({
    root: {
        display: "flex",
        width: "100%",
        height: "auto",
        justifyContent: "center",
        flexWrap: "wrap",
    },
    item: {
        margin: "0.1em",
    },
    actions: {
        width: "100%",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        padding: 6,
    },
    editor: {
        paddingLeft: 8,
        paddingRight: 8,
    }
})

function ScaleList({ scales: propScales, className, onChange, maxScales = 9, loading, description }) {
    const classes = useStyles()
    const [editing, setEditing] = useState(false)
    const [adding, setAdding] = useState(false)
    const [addingIndex, setAddingIndex] = useState(null)
    const [scales, setScales] = useState([])
    const [text, setText] = useState(EditorState.createEmpty())
    const [textChanged, setTextChanged] = useState(false)


    const { t } = useTranslation(['dashboard'])

    useEffect(
        () => {
            setScales([...propScales])
        },
        [propScales]
    )

    useEffect(
        () => {
            if (description) {
                const blocks = htmlToDraft(description);
                setText(
                    EditorState.createWithContent(
                        ContentState.createFromBlockArray(
                            blocks.contentBlocks,
                            blocks.entityMap
                        )
                    )
                )
            }
        },
        [description]
    )

    const handleText = useCallback(
        (newText) => {
            setText(newText)
            setTextChanged(true)
        },
        []
    );

    const handleLabelChange = useCallback(
        (newLabel, order) => {
            setScales(
                scales => {
                    scales[order - 1] = newLabel
                    return [...scales]
                }
            )
        },
        []
    )

    const handleLabelDelete = useCallback(
        (order) => {
            setScales(
                scales => {
                    scales.splice(order - 1, 1)
                    if (addingIndex === scales.length) {
                        setAdding(false)
                        setAddingIndex(null)
                    }
                    return [...scales]
                }
            )
        },
        [addingIndex]
    )

    const handleEdit = useCallback(
        () => {
            setEditing(true)
        },
        []
    )

    const handleAdd = useCallback(
        () => {
            setAdding(true)
            setAddingIndex(
                lastIndex => {
                    if (lastIndex === null) {
                        return scales.length
                    }
                    return lastIndex
                }
            )
            setScales(
                scales => [...scales, ""]
            )
        },
        [scales]
    )

    const handleSave = useCallback(
        () => {
            onChange(scales, draftjsToHtml(convertToRaw(text.getCurrentContent())))
            setEditing(false)
            setAdding(false)
            setTextChanged(false)
            setAddingIndex(null)
        },
        [editing, adding, scales, text]
    )

    return (
        <div className={clsx(classes.root, className)}>
            <Collapse in={loading}>
                <LinearProgress />
            </Collapse>
            <Paper style={{ marginBottom: 24 }}>
                <Editor
                    editorState={text}
                    onEditorStateChange={handleText}
                    editorClassName={classes.editor}
                    toolbar={toolbar}
                />
            </Paper>
            {scales.map(
                (scale, index) => <ScaleItem
                    order={index + 1}
                    label={scale}
                    key={index}
                    onChange={handleLabelChange}
                    editing={editing || (adding && index >= addingIndex)}
                    onDelete={handleLabelDelete}
                    className={classes.item}
                />
            )}
            <div square className={classes.actions}>
                <Button
                    startIcon={<AddIcon />}
                    title={t("Add a new option")}
                    onClick={handleAdd}
                    disabled={scales.length >= maxScales || loading}
                    fullWidth
                >
                    {t("Add Option")}
                </Button>
                <Button
                    startIcon={<EditIcon />}
                    title={t("Edit options")}
                    onClick={handleEdit}
                    disabled={adding || editing || loading}
                    fullWidth
                >
                    {editing ? t("Editing...") : t("Edit Options")}
                </Button>
                <Button
                    startIcon={<SaveIcon />}
                    title={t("Save changes")}
                    onClick={handleSave}
                    disabled={(!editing && !adding && !textChanged) || loading}
                    fullWidth
                >
                    {t("Save")}
                </Button>
            </div>
        </div>
    )
}

export default ScaleList