import { Button, Collapse, LinearProgress, makeStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import arrayMove from 'array-move';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Container } from 'react-smooth-dnd';
import ConfirmDialog from '../../../../components/ConfirmDialog';
import QuestionForm from './QuestionForm';
import { saveAddQuestion, saveDeleteQuestion, saveReorderQuestion, saveUpdateQuestion } from './utils';

const useStyles = makeStyles({
    newButton: {
        textAlign: "right",
        padding: "0 1em 1em 0",
    },
    dragging: {
        backgroundColor: "#CCC",
        cursor: "grabbing"
    }
})

function QuestionList({ skillId, questions: propQuestions, onChange }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const [questions, setQuestions] = useState([])
    const [openConfirm, setOpenConfirm] = useState(false)
    const [toDeleteIndex, setToDeleteIndex] = useState(-1)
    const [loading, setLoading] = useState(false)

    useEffect(
        () => {
            if (propQuestions.length > 0) {
                setQuestions([...propQuestions])
            } else {
                setQuestions([{
                    id: null,
                    text: "",
                    order: 1,
                }])
            }
        },
        [propQuestions]
    )

    const handleSaveQuestion = useCallback(
        (number, old_question, new_text, onDone) => {
            setLoading(true)

            if (old_question.id === null) {
                saveAddQuestion(
                    skillId,
                    new_text,
                    old_question.order,
                    (data) => {
                        setQuestions(
                            questions => {
                                const newQuestions = [...questions]
                                const newQuestion = newQuestions[number - 1]
                                newQuestion.text = new_text
                                newQuestion.order = old_question.order
                                newQuestion.id = data.question_id
                                return newQuestions
                            }
                        )
                        onDone()
                    },
                    null,
                    () => setLoading(false)
                )
            } else {
                saveUpdateQuestion(
                    old_question.id,
                    new_text,
                    (data) => {
                        setQuestions(
                            questions => {
                                const newQuestions = [...questions]
                                const newQuestion = newQuestions[number - 1]
                                newQuestion.text = new_text
                                newQuestion.id = data.new_question_id
                                return newQuestions
                            }
                        )
                        onDone()
                    },
                    null,
                    () => setLoading(false)
                )
            }
        },
        [skillId]
    )

    const handleAdd = useCallback(
        () => {
            setQuestions(questions => [
                ...questions,
                {
                    id: null,
                    text: '',
                    order: questions[questions.length - 1].order + 12
                }
            ])
        },
        [onChange]
    )

    const handleDelete = useCallback(
        (number) => {
            setToDeleteIndex(number - 1)
            setOpenConfirm(true)
        },
        []
    )

    const handleConfirmDelete = useCallback(
        () => {
            setQuestions(
                questions => {
                    const newQuestions = [...questions]
                    const deletedQuestion = newQuestions.splice(toDeleteIndex, 1)[0]
                    if (deletedQuestion.id !== null) {
                        setLoading(true)
                        saveDeleteQuestion(
                            deletedQuestion.id,
                            null,
                            null,
                            () => setLoading(false)
                        )
                    }
                    return newQuestions
                }
            )
            setOpenConfirm(false)
        },
        [toDeleteIndex]
    )


    const handleCancelDelete = useCallback(
        () => setOpenConfirm(false),
        []
    )

    const handleDrop = useCallback(
        ({ removedIndex, addedIndex }) => {
            if (removedIndex !== addedIndex) {
                setQuestions(
                    questions => {
                        const newQuestions = arrayMove(questions, removedIndex, addedIndex)
                        const targetQuestion = newQuestions[addedIndex]
                        if (addedIndex === 0) {
                            targetQuestion.order = newQuestions[1].order - 12.0
                        } else if (addedIndex === newQuestions.length - 1) {
                            targetQuestion.order = newQuestions[questions.length - 2].order + 12.0
                        } else {
                            targetQuestion.order = (newQuestions[addedIndex - 1].order + newQuestions[addedIndex + 1].order) / 2.0
                        }
                        saveReorderQuestion(targetQuestion.id, targetQuestion.order)
                        return newQuestions
                    }
                )
            }
        },
        []
    )


    return <>
        <Collapse in={loading}>
            <LinearProgress />
        </Collapse>
        <Container dragHandleSelector=".drag-handle" lockAxis="y" onDrop={handleDrop} dragClass={classes.dragging}>
            {questions.map(
                (question, index) => (
                    <QuestionForm
                        number={index + 1}
                        question={question}
                        key={index}
                        onDelete={handleDelete}
                        canDelete={questions.length > 1}
                        onSave={handleSaveQuestion}
                        reorderHandleClassName={question.id !== null ? 'drag-handle' : undefined}
                    />
                )
            )}
        </Container>
        <div className={classes.newButton}>
            <Button
                startIcon={<AddIcon />}
                onClick={handleAdd}
                title={t("Add a new question")}
                fullWidth
            >
                {t("Add Question")}
            </Button>
        </div>
        <ConfirmDialog
            open={openConfirm}
            onAccept={handleConfirmDelete}
            onReject={handleCancelDelete}
            title={t("Delete")}
        >
            {t("Please confirm the deletion the following question: ")}<br />
            - <em>{t("Question")} {toDeleteIndex + 1}</em>
        </ConfirmDialog>
    </>
}

export default QuestionList