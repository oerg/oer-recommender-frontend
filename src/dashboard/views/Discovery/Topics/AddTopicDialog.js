import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, makeStyles, Typography } from '@material-ui/core';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../../components/CustomButton/Button';
import Help from '../../../components/Help';
import MovableList from '../../../components/MovableList';
import TopicObjectInput from '../../../components/SystemInputs/TopicObjectInput';
import { roseColor } from '../../styles/variables';

function AddTopicDialog({ show, onClose, oerTitle, mainTopic, ...props }) {
    const classes = makeStyles(theme => ({
        addSkillButton: {
            position: 'absolute',
            color: roseColor[0],
            bottom: -24,
            zIndex: 999,
        },
        content: {
            position: "relative",
            marginBottom: theme.spacing(2),
            textAlign: "center",
            height: "auto",
        }
    }))();
    const [topics, setTopics] = useState([]);
    const [showAddForm, setShowAddForm] = useState(false);
    const [selectedTopic, setSelectedTopic] = useState(null);

    const { t } = useTranslation(['dashboard'])

    useEffect(
        () => setTopics([mainTopic]),
        [mainTopic]
    );

    const handleTopicRemove = useCallback(
        index => {
            setTopics(
                ts => {
                    ts.splice(index, 1);
                    return [...ts];
                }
            );
        },
        []
    );

    const handleOpenTopicForm = useCallback(
        () => setShowAddForm(true),
        []
    );

    const handleCloseTopicForm = useCallback(
        () => setShowAddForm(false),
        []
    );

    const handleSelectedTopic = useCallback(
        newValue => setSelectedTopic(newValue),
        []
    );

    const handleAddTopic = useCallback(
        () => {
            setTopics(
                topics => {
                    if (topics.some(t => t.id === selectedTopic.id))
                        return topics;
                    return [...topics, { id: selectedTopic.id, title: selectedTopic.title }]
                }
            );
            setSelectedTopic(null);
            setShowAddForm(false);
        },
        [selectedTopic]
    );

    const handleDialog = useCallback((save) => () => {
        save ?
            onClose(true, topics)
            :
            onClose(false, []);
    }, [onClose, topics]);


    return (
        <Dialog open={show} onClose={onClose} disableEscapeKeyDown disableBackdropClick PaperProps={{
            style: {
                minWidth: 500,
            }
        }}>
            <DialogTitle>
                {t("Select Additional Topics for:")}<br /><i>{oerTitle}</i>
            </DialogTitle>
            <DialogContent>
                <div className={classes.content}>
                    <Typography component="legend" style={{ textAlign: "left", marginTop: 14 }}>
                        {t("Topics")} <Help>{t("The current topic has been already added to this package. Here you can add more topics to this content.")}</Help>
                    </Typography>
                    <MovableList
                        options={topics}
                        emptyText={t("No topics selected!")}
                        movable={false}
                        onRemove={handleTopicRemove}
                    />

                    <IconButton className={classes.addSkillButton} title={t("Add Topic")} onClick={handleOpenTopicForm}>
                        <AddCircleRoundedIcon />
                    </IconButton>
                    <Dialog open={showAddForm} onClose={handleCloseTopicForm}
                        PaperProps={{
                            style: {
                                minWidth: 450,
                            }
                        }}
                    >
                        <DialogTitle>
                            {t("Select a topic")}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                <TopicObjectInput onSelect={handleSelectedTopic} />
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                onClick={handleAddTopic}
                                color="rose"
                                disabled={!selectedTopic}
                            >
                                {t("Add selected topic")}
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(false)} color="info">
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(true)} disabled={topics.length === 0} color="rose">
                    {t("Save")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddTopicDialog
