import Axios from 'axios';
import React from 'react';
import { MultiCall, requestAddHandlers } from '../../helpers';

export function getTopic(id, onLoad, onError) { // get topic from the server
    const skillReq = Axios.get('/skills/topics/' + id);
    const suggestionsReq = Axios.get('/suggestions?object_type=TO&object_id=' + id + '&ordering=-created_at');
    Axios.all([skillReq, suggestionsReq])
        .then(Axios.spread((...res) => {
            onLoad(res[0].data, res[1].data);
        }))
        .catch(err => {
            onError && onError(err.response);
        });
}

export function getTopicOnly(id, onLoad, onError, onEnd) {
    requestAddHandlers(
        Axios.get('/skills/topics/' + id),
        onLoad,
        onError,
        onEnd
    )
}

export function createSortString(sortBy, sortAD) {
    let sortStr = sortAD ? '+' : '-';
    sortStr += sortBy === 'date' ? 'created_at' : 'upvote_count'
    return sortStr;
}

export function doSuggestionVote(id, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/suggestions/add-suggestion-vote/', {
        suggestion: id,
        vote: vote,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(res => {
            onError ? onError(res.response.data) : alert(res.response.data.result);
        })
}

export function doOergVote(topicId, oergId, vote, onSuccess, onError, onEnd) { // send topics' vote to server.
    requestAddHandlers(
        Axios.post('/oers/add-oer-vote/', {
            oer: oergId,
            topic: topicId,
            vote: vote,
        }),
        onSuccess,
        onError,
        onEnd
    );
}

export function doSkillVote(skillId, topicId, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/skills/add-skill-topic-vote/', {
        skill: skillId,
        topic: topicId,
        vote: vote,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(res => {
            onError ? onError(res.response.data) : alert(res.response.data.result);
        })
}

export function processVote(list, id, up, down, selfVote, idField = 'id') { // update local list of votes
    let data = list.find(t => t[idField] === id)
    data.vote = selfVote;
    data.upvote_count = up;
    data.downvote_count = down;
    return [...list];
}

export function addDeleteSuggestion(topicId, oerId, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'TO',
        object_id: topicId,
        payload: {
            suggestion_type: "DE",
            oer_group_id: oerId,
        },
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addNewOerg(data, onSuccess) { // send request to server
    Axios.post('/oers/add-oer-group/', data)
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function doAddDiscussion(topicId, msg, parent, onSuccess, onError) {
    Axios.post('/discussions/', {
        text: msg,
        object_type: 'TO',
        object_id: topicId,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(err => {
            onError ? onError(err.response.data) : alert(err.response.data.result);
        })
}

export function formatSkillTitle(j) {
    return (
        <span>
            {j.skill_title}
        </span>
    )
}

export function filterSuggestions(suggestions) {
    const add = [] // add
    const edit = [] // edit
    const reorder = [] // reorder
    const remove = [] // delete
    suggestions.forEach(s => {
        if (s.decision === 'PE') { // only pendings
            switch (s.payload.suggestion_type) {
                case 'DE':
                    remove.push(s);
                    break;
                default:
            }
        }
        return {
            add, edit, reorder, remove
        }
    });

    return { add, edit, reorder, remove }
}

export function getAllOergs(onSucces, onError) {
    Axios.get('/oers/')
        .then(res => {
            onSucces && onSucces(res);
        })
        .catch(err => {
            err &&
                onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
        })
}

export function getIsEditableTopic(title, lang, onSuccess, onError) {
    var mc = MultiCall.getInstance("AT_isEditable")
    return mc.call(`/skills/topics/is-topic-editable/?title=${title}&lang=${lang}`, onSuccess, onError)
}

export function cancelIsEditableTopic() {
    return MultiCall.getInstance("AT_isEditable").cancel();
}

export function doAddTopic(title, lang, description, onLoad, onError, onEnd) {
    requestAddHandlers(
        Axios.post('/skills/topics/add-a-topic/', {
            title, description, lang
        }),
        onLoad,
        onError,
        onEnd
    )

}

const SIMILAR_TOPICS_MC = "AT_similarTopicsLoading";
export function loadSimilarTopics(title, lang, onSuccess, onError, onEnd) {
    var mc = MultiCall.getInstance(SIMILAR_TOPICS_MC)
    return mc.call(`/skills/topics/search/?search-term=${title}&lang=${lang}`, onSuccess, onError, onEnd)
}

export function cancelLoadSimilarTopics() {
    return MultiCall.getInstance(SIMILAR_TOPICS_MC).cancel();
}

export function doEditTopic(id, title, lang, description, onLoad, onError, onEnd) {
    requestAddHandlers(
        Axios.put(
            '/skills/topics/edit-topic-properties/',
            {
                topic_id: id,
                title,
                description,
                lang
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function uploadTopicPicture(data, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.post('/skills/topics/upload-item-picture/', data),
        onLoad,
        onError,
        onEnd
    )
}

export function loadOerGroup(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.get(`/oers/${id}`),
        onLoad,
        onError,
        onEnd
    )
}