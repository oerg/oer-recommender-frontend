import { Box, Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Fab, IconButton, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow, Typography, withStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import DeleteIcon from '@material-ui/icons/Delete';
import HelpIconOutline from "@material-ui/icons/HelpOutline";
import clsx from 'classnames';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import Button from '../../../components/CustomButton/Button';
import DetailsPopover from '../../../components/DetailsPopover';
import Help from '../../../components/Help';
import HtmlTooltip from '../../../components/HtmlTooltip';
import MovableList from '../../../components/MovableList';
import TopicObjectInput from '../../../components/SystemInputs/TopicObjectInput';
import Vote from '../../../components/Vote';
import { roseColor } from '../../styles/variables';
import OerGroupForm from '../OerGroups/components/OerGroupForm';

const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
    },
    buttonRoot: {
        display: 'inline-block',
        marginRight: 8,
        marginTop: 15,
    },
    button: {
    },
    hidden: {
        display: 'none',
    },
    stickyFooter: {
        left: 0,
        bottom: -20,
        zIndex: 2,
        position: 'sticky',
        textAlign: 'center',
    },
    oerGreen: {
        backgroundColor: '#005f5f',
        color: '#FFF',
        opacity: 0.5,
        transition: 'margin-bottom 0.3s',
        '&:hover': {
            backgroundColor: "#004555",
            color: '#AAA',
            opacity: 1,
            marginBottom: 10
        }
    },
    headRow: {
        height: 60,
    },
    title: {
        overflow: "hidden",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
    },
    actions: {
        width: 150,
    },

    table: {
        tableLayout: "fixed",
    },
    titleCol: {
        width: "100%",
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
})

function DeleteDialog({ id, title, show, onClose }) {
    const { t } = useTranslation(['dashboard']);

    const handleDialog = useCallback((id, save) => () => {
        onClose(id, save);
    }, [onClose]);
    return (
        <Dialog open={show} onClose={onClose} disableBackdropClick disableEscapeKeyDown>
            <DialogTitle>
                {t("Delete Review")}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {t("Are you sure you want issue a review for deletion for the following item?")}<br />
                    '<em>{title}</em>'
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(id, false)} color="info">
                    {t("No")}
                </Button>
                <Button startIcon={<DeleteIcon />} onClick={handleDialog(id, true)} color="danger">
                    {t("Yes")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

function AddOerDialog({ show, onClose, mainTopic, ...props }) {
    const classes = makeStyles(theme => ({
        addSkillButton: {
            position: 'absolute',
            color: roseColor[0],
            bottom: -24,
            zIndex: 999,
        },
        suggestions: {
            border: '1px solid #af1414',
            color: '#af1414',
            position: 'relative',
            maxHeight: 250,
            minHeight: 80,
            width: '100%',
            overflowY: 'auto',
            '& > *': {
                margin: theme.spacing(0.5),
            }
        },
        suggestionsHelp: {
            position: 'absolute',
            width: 35,
            height: 35,
            right: 12,
            top: 8,
            color: "#888",
            opacity: 0.7,
            cursor: 'help',
            transition: 'opacity 1s, top 0.5s',
            '&:hover': {
                opacity: 0.9
            }
        },    
    }))();
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [oers, setOers] = useState([]);
    const [hasErrors, setHasErrors] = useState(false);
    const [topics, setTopics] = useState([]);
    const [showAddForm, setShowAddForm] = useState(false);
    const [selectedTopic, setSelectedTopic] = useState(null);
    const [suggestions, setSuggestions] = useState([]);
    const { t } = useTranslation(['dashboard']);

    const handleOergError = useCallback(
        error => setHasErrors(error),
        []
    );

    useEffect(
        () => {
            setTopics([ mainTopic ])
        },
        [ mainTopic, show ]
    );

    const handleUpdate = useCallback((title, description, oers) => {
        setTitle(title ? title : '');
        setDescription(description ? description : '');
        setOers(oers ? oers : []);
        setSuggestions(() => {
            const allTopics = [].concat(...oers.map((o) => o.topics ? o.topics : []));
            return allTopics.filter(t => !topics.some(at => at.id === t.id))
        })
    }, [topics]);

    const handleTopicRemove = useCallback(
        index => {
            setTopics(
                ts => {
                    ts.splice(index, 1);
                    return [...ts];
                }
            );
        },
        []
    );

    const handleAddTopic = useCallback(
        () => {
            setTopics(
                topics => {
                    if (topics.some(t => t.id === selectedTopic.id))
                        return topics;
                    return [...topics, { id: selectedTopic.id, title: selectedTopic.title }]
                }
            );
            setSelectedTopic(null);
            setShowAddForm(false);
        },
        [selectedTopic]
    );

    const handleSelectedTopic = useCallback(
        newValue => setSelectedTopic(newValue),
        []
    );

    const handleOpenTopicForm = useCallback(
        () => setShowAddForm(true),
        []
    );

    const handleCloseTopicForm = useCallback(
        () => setShowAddForm(false),
        []
    );

    const handleDialog = useCallback((save) => () => {
        save ?
            onClose(true, {
                title, description, oers, topics
            })
            :
            onClose(false, {});
    }, [onClose, title, description, oers, topics]);

    const handleChip = useCallback((index, suggestTopic) => () => {
        setTopics(prevState => ([...prevState, suggestTopic]))
        setSuggestions(prevState => prevState.filter(s => s.title !== suggestTopic.title))
    }, []);

    return (
        <Dialog open={show} onClose={onClose} disableEscapeKeyDown disableBackdropClick PaperProps={{
            style: {
                minWidth: 500,
            }
        }} scroll='body'>
            <DialogTitle>
                {t("Add a new educational package")}
                <Help>
                    {t("Educational packages can include one or more educational contents.\n")}
                    {t("When you add more than one content:")}
                    <ul>
                        <li>{t("It means that the multiple contents together will cover the topic(s).")}</li>
                        <li>{t("Learners will be shown the contents in the order you add them here.")}</li>
                    </ul>
                </Help>
            </DialogTitle>
            <DialogContent>
                <OerGroupForm onUpdate={handleUpdate} onError={handleOergError} />
                <Box textAlign="center" position="relative" mb={3}>
                    {
                        (suggestions.length > 0) && (
                            <div className={clsx(classes.formField, classes.suggestions, classes.smallCenter)}>
                                {suggestions.map((s, index) => (
                                    <Chip label={s.title} variant="outlined" key={index} onClick={handleChip(index, s)} className={classes.suggestion} title="Add to topics" />
                                ))}
                                <HtmlTooltip arrow placement="left-start" title={
                                    <Box textAlign="justify" p={1} borderRadius={15}>
                                        {t("These are computer generated learning topics for this package. You can add them to this educational package by clicking.")}
                                    </Box>
                                }>
                                    <HelpIconOutline className={classes.suggestionsHelp} />
                                </HtmlTooltip>
                            </div>
                        )}
                    <Typography component="legend" style={{ textAlign: "left", marginTop: 14 }}>
                        Topics <Help>{t("The current topic has been already added to this package. Here you can add more topics to this content.")}</Help>
                    </Typography>
                    <MovableList
                        options={topics}
                        emptyText={t("No additional topics selected!")}
                        movable={false}
                        onRemove={handleTopicRemove}
                    />
                    <IconButton className={classes.addSkillButton} title={t("Add Topic")} onClick={handleOpenTopicForm}>
                        <AddCircleRoundedIcon />
                    </IconButton>
                    <Dialog open={showAddForm} onClose={handleCloseTopicForm}
                        PaperProps={{
                            style: {
                                minWidth: 450,
                            }
                        }}
                    >
                        <DialogTitle>
                            {t("Select a topic")}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                <TopicObjectInput onSelect={handleSelectedTopic} />
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                onClick={handleAddTopic}
                                color="rose"
                                disabled={!selectedTopic}
                            >
                                {t("Add selected topic")}
                            </Button>
                        </DialogActions>
                    </Dialog>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(false)} color="info">
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(true)} disabled={(!title || title.length === 0) || (!oers || oers.length === 0) || hasErrors} color="rose">
                    {t("Save")}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

function OersTable({ className, oers, onVote, onDelete, onEdit, onNew, disabled, headClass, topic, ...props }) {
    const classes = useStyles();
    const [showDelete, setShowDelete] = useState(false);
    const [deleteId, setDeleteId] = useState(0); // used for both edit and delete
    const [deleteTitle, setDeleteTitle] = useState(''); // used for both edit and delete
    const [showNew, setShowNew] = useState(false);
    const [oerGroups, setOerGroups] = useState([]);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [popperRef, setPopperRef] = useState(null);
    const [popperItems, setPopperItems] = useState([]);
    const [popperDescription, setPopperDescription] = useState('');
    const [popperAuthor, setPopperAuthor] = useState('');
    const [enablePopper, setEnablePopper] = useState(false); // don't show popper for first 3 seconds

    useEffect( // don't show popper for first 3 seconds
        () => {
            var popperEnabler = window.setTimeout(
                () => setEnablePopper(true),
                3000
            );
            return () => window.clearTimeout(popperEnabler);
        },
        []
    )

    useEffect(
        () => {
            if (oers) setOerGroups(oers.filter(o => o.status === "PB"));
        },
        [oers]
    )

    const handleDelete = useCallback(
        (id, title) => () => {
            setDeleteId(id);
            setDeleteTitle(title)
            setShowDelete(true);
        },
        []);

    const handleDeleteDialog = useCallback((id, del) => {
        setShowDelete(false);
        if (!id) return;
        if (del) onDelete(id);
    }, [onDelete]);

    const handleShowNew = useCallback(() => {
        setShowNew(true);
    }, [])

    const handleNewDialog = useCallback((save, data) => {
        setShowNew(false);
        if (save) onNew(data);
    }, [onNew]);

    const handleHover = useCallback(
        e => {
            var index = Number(e.currentTarget.id);
            setPopperDescription(oerGroups[index].description);
            setPopperItems(oerGroups[index].oers);
            setPopperAuthor(oerGroups[index].authors);
            setPopperRef(e.currentTarget)
        },
        [oerGroups]
    );

    const handleLeave = useCallback(
        e => {
            setPopperRef(null)
        },
        [oerGroups]
    )

    const handleClosePopper = useCallback(
        () => setPopperRef(null),
        []
    );

    return (<>
        <DetailsPopover
            anchorEl={popperRef}
            description={popperDescription}
            items={popperItems}
            open={enablePopper && popperRef !== null}
            onClose={handleClosePopper}
            author={popperAuthor}
        />
        <AddOerDialog show={showNew} onClose={handleNewDialog} mainTopic={topic} />
        <DeleteDialog id={deleteId} show={showDelete} title={deleteTitle} onClose={handleDeleteDialog} />

        <TableContainer component={Paper} className={className} style={{ height: '400px' }} {...props}>
            <Table size="small" stickyHeader className={classes.table}>
                <TableHead>
                    <TableRow className={classes.headRow}>
                        <TableCell component="th" className={clsx(headClass, classes.titleCol, classes.title)} title={t("Educational packages in this topic")}>
                            {t("Educational packages in this topic")}
                            <Help>
                                {t("Each of the following packages should INDIVIDUALLY cover the topic of this page.")}
                            </Help>
                        </TableCell>
                        <TableCell component="th" align="right" className={clsx(headClass, classes.actions)}>{t("Actions")}
                            <Help>
                                {t("Here you can:")}
                                <ul>
                                    <li>{t("Suggest to Delete an educational package from this topic.")}</li>
                                    <li>{t("Up-vote/Down-vote each educational package which helps users find the most useful educational packages for this topic.")}</li>
                                </ul>
                            </Help>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {oerGroups.length === 0 ?
                        <TableRow>
                            <TableCell colSpan={2}>
                                <em>{t("No educational package.")}</em>
                            </TableCell>
                        </TableRow>
                        :
                        oerGroups.map((i, index) => (
                            <StyledTableRow key={index}>
                                <TableCell className={classes.title}>
                                    <Link to={`/${language}/dashboard/discovery/oer-groups/${i.id}`} id={index} onMouseEnter={handleHover} onMouseLeave={handleLeave} title={i.title}>{i.title}</Link>
                                </TableCell>
                                <TableCell align="right">
                                    <IconButton size="small" title={t("Suggest a delete")} onClick={handleDelete(i.id, i.title)} className={classes.buttonRoot}>
                                        <DeleteIcon />
                                    </IconButton>
                                    <Vote selfVote={i.vote} up={i.upvote_count} down={i.downvote_count} onChange={onVote(i.id)} className={classes.vote} disabled={disabled} />
                                </TableCell>
                            </StyledTableRow>
                        ))}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TableCell colSpan={3} align="right" classes={{
                            footer: classes.stickyFooter
                        }}>
                            <Fab size="small" className={classes.oerGreen} title={t("Add an educational package")} disabled={disabled} onClick={handleShowNew}>
                                <AddIcon />
                            </Fab>
                        </TableCell>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    </>)
}

export default OersTable
