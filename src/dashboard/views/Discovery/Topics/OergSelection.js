import { Accordion, AccordionDetails, AccordionSummary, TextField } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

function OergSelection() {
    const [tab, setTab] = useState(false); // 0 = search, 1 = new Oerg
    const { t } = useTranslation([ 'dashboard' ]);

    const handleTab = useCallback(
        tab =>
            (e, expanded) => {
                setTab(expanded ? tab : false);
            }
        ,
        []
    );

    return (
        <div>
            <Accordion expanded={tab === 0} onChange={handleTab(0)}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                >
                    <em>{t("Search in our educational package")}</em>
                </AccordionSummary>
                <AccordionDetails>
                    <TextField label={t("Search")} fullWidth />
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={tab === 1} onChange={handleTab(1)}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                >
                    <em>{t("Create a new educational package")}</em>
                </AccordionSummary>
                <AccordionDetails>
                    <TextField label={t("Search")} />
                </AccordionDetails>
            </Accordion>
        </div>
    )
}

export default OergSelection
