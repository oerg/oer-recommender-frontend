import { Box, Grid, IconButton, Input, LinearProgress, makeStyles, Paper } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert } from '@material-ui/lab'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import DiscoveryNav from '../../../components/DiscoveryNav'
import LangTextField from '../../../components/LangTextField'
import TopicLogo from '../../../components/Logos/Topic'
import { getSuggestionColor } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { doEditTopic, getTopicOnly, uploadTopicPicture } from './utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > li:nth-child(even)': {
            backgroundColor: '#CCC',
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(0.65),
        color: getSuggestionColor(0.65),
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(0.65),
        borderColor: getSuggestionColor(0.65),
    }
}))

function Edit({ navHistory, enqueueSnackbar, ...props }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [language, setLanguage] = useState('');
    const [initTitle, setInitTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [isEditable, setIsEditable] = useState(true);
    const [description, setDescription] = useState('');
    const [changed, setChanged] = useState(false);
    const [picture, setPicture] = useState(undefined);
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();

    const history = useHistory();

    const { id } = useParams();

    useEffect(
        () => {
            setLoading(true);
            getTopicOnly(
                id,
                res => {
                    var topic = res;
                    setIsEditable(topic['is_admin'] || topic['is_contributor'] || topic.is_expert);
                    setTitle(topic.title);
                    setInitTitle(topic.title);
                    setDescription(topic.description);
                    setLanguage(topic.lang);
                },
                null,
                () => setLoading(false)
            )
        },
        [id]
    )

    const handleTitle = useCallback(e => {
        setTitle(e.target.value);
        setChanged(true);
    }, []);

    const handleSave = useCallback(() => {
        setLoading(true);
        doEditTopic(id, title, language, description, res => {
            setLoading(false);
            history.push(`/${systemLanguage}/dashboard/discovery/topics/` + id)
        })
    }, [id, title, description, language]);

    const handleLanguage = useCallback(
        lang => {
            setLanguage(lang);
            setChanged(true);
        },
        []
    );

    const handleDescription = useCallback(
        e => {
            setDescription(e.target.value)
            setChanged(true)
        },
        []
    );

    const handleFile = useCallback(
        e => {
            if (!e.target.files[0] || e.target.files[0].size > 2097152) { // 2MB
                enqueueSnackbar(
                    t("Please select a file which is not larger than 2 MB"),
                    {
                        varaint: "warning"
                    }
                )
                return;
            }
            var data = new FormData();
            data.append('file', e.target.files[0]);
            data.append('topic_id', id);
            uploadTopicPicture(
                data,
                res => {
                    setPicture(res.picture)
                    enqueueSnackbar(
                        t("Topic picture updated successfully."),
                        {
                            variant: "success"
                        }
                    )
                },
                () => {
                    enqueueSnackbar(
                        t("Upload failed. Please try again."),
                        {
                            variant: "error"
                        }
                    )
                }
            )
        },
        [t, id, enqueueSnackbar]
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <DiscoveryNav navHistory={navHistory} />
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Editing Topic")}: <em>{initTitle}</em>
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title="Go back" onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} className={classes.formField}>
                                    <LangTextField
                                        value={title}
                                        onChange={handleTitle}
                                        label={t("Title")}
                                        fullWidth
                                        language={language}
                                        onLanguageChange={handleLanguage}
                                    />
                                    {loading && <LinearProgress />}
                                    {!isEditable &&
                                        <Alert severity="error">
                                            {t("You cannot edit this topic.")} (<Link to={`/${systemLanguage}/dashboard/discovery/topics/${id}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', display: "flex" }}>
                                    <TopicLogo src={picture} style={{ marginRight: 10 }} />
                                    <Input
                                        id="upload-button"
                                        type="file"
                                        onChange={handleFile}
                                        fullWidth
                                        placeholder={t("Picture")}
                                        inputProps={{
                                            accept: "image/png, image/jpeg"
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    <Description
                                        style={{ padding: "10px" }}
                                        label={t("Topic Description")}
                                        help={t("You can describe the topic here.")}
                                        value={description}
                                        onChange={handleDescription}
                                        loading={loading}
                                        TextFieldProps={{
                                            disabled: !isEditable
                                        }}
                                        placeholder={t('Describe the topic')}
                                        lang={language}
                                        keyword={title}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField}>
                                    <Button color="rose" onClick={handleSave} disabled={loading || !isEditable || !changed || title.length === 0}
                                        fullWidth>{t("Save")}</Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Edit.propTypes = {

}

export default withSnackbar(Edit)

