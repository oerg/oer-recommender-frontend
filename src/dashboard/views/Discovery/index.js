import { Button, ButtonGroup, Snackbar } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import Card from '../../components/Card/Card'
import CardBody from '../../components/Card/CardBody'
import CardHeader from '../../components/Card/CardHeader'
import GridContainer from '../../components/Grid/GridContainer'
import GridItem from '../../components/Grid/GridItem'
import IAlert from '../../components/ISys/IAlert'
import AddWizardDialog from './components/AddWizardDialog'
import JobsDiscovery from './JobsDiscovery'
import OersDiscovery from './OersDiscovery'
import QuestionsDiscovery from './QuestionsDiscovery'
import SkillsDiscovery from './SkillsDiscovery'
import TopicsDiscovery from './TopicsDiscovery'

function Discovery({ onNav, exploreOpen, onExploreOpenChange, ...props }) {
    const { section } = useParams();
    const { t } = useTranslation(['dashboard']);
    const [showHelp, setShowHelp] = useState(false)
    const [showAddWizard, setShowAddWizard] = useState(false)

    useEffect(
        () => {
            const handle = window.setTimeout(
                () => setShowHelp(true),
                750
            )
            return () => window.clearTimeout(handle)
        },
        []
    )

    useEffect(
        () => onNav(null),
        [onNav]
    );

    const items = useMemo(() => ({
        jobs: {
            title: t("Journeys"),
            content: JobsDiscovery
        },
        skills: {
            title: t("Courses"),
            content: SkillsDiscovery,
        },
        topics: {
            title: t("Educational Topics"),
            content: TopicsDiscovery,
        },
        "oer-groups": {
            title: t("Educational Package"),
            content: OersDiscovery,
        },
        questions: {
            title: t("Questions"),
            content: QuestionsDiscovery,
        }
    }),
        [t]
    );

    const item = useMemo(
        () => section ? items[section] : { title: null },
        [items, section]
    )
    const ContentComponent = useMemo(
        () => section ? items[section].content : null,
        [items, section]
    );

    const handleShowWizard = useCallback(
        () => {
            setShowAddWizard(true)
            setShowHelp(false)
        },
        []
    )

    const handleCloseWizard = useCallback(
        () => {
            setShowAddWizard(false)
            setShowHelp(true)

        },
        []
    )

    const handleDismissHelp = useCallback(
        () => {
            setShowHelp(false)
        },
        []
    )

    return (<>
        
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3>
                            {item.title}
                        </h3>
                    </CardHeader>
                    <CardBody>
                        {item.title && <ContentComponent />}
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
        <Snackbar
            open={showHelp}
            anchorOrigin={{
                vertical: "top",
                horizontal: "center"
            }}
            style={{
                top: 130,
                zIndex: 99
            }}
        >
            <IAlert
                show
                transition={false}
                action={
                    <ButtonGroup color='inherit' variant='text'>
                        <Button onClick={handleShowWizard} title={t("Start adding with help")}>
                            {t("Help Me")}
                        </Button>
                        <Button onClick={handleDismissHelp} title={t("Dismiss")}>
                            <CloseIcon />
                        </Button>
                    </ButtonGroup>
                }
                style={{
                    border: "1px solid #2196f3",
                }}
            >
                {t("Not sure what type of item I want to add?")}
            </IAlert>
        </Snackbar>
        <AddWizardDialog
            open={showAddWizard}
            onClose={handleCloseWizard}
        />
    </>)
}

export default Discovery
