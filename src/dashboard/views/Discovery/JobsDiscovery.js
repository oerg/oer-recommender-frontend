import { Box, CircularProgress, Grid, IconButton, InputAdornment, List, makeStyles, Paper, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import JobListItem from '../../components/JobItem/JobListItem';
import { getIndustryLabel } from '../helpers';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../i18n';
import { cancelLoadPart, loadPart } from './utils';

const useStyles = makeStyles(theme => ({
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    hasPadding: {
        padding: theme.spacing(1),
    },
    loadingPanel: {
        textAlign: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    }
}));

function JobsDiscovery() {
    const classes = useStyles();
    const [jobs, setJobs] = useState(null);
    const [jobsLoading, setJobsLoading] = useState(false);
    const [searchString, setSearchString] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const addRef = useRef(null);

    const history = useHistory();

    useEffect(() => { // do the search
        if (searchString.length === 0) {
            cancelLoadPart();
            setJobs(null);
            setJobsLoading(false);
        } else {
            setJobsLoading(true);
            loadPart(
                "jobs",
                searchString,
                res => setJobs(res.data.results),
                null,
                () => setJobsLoading(false)
            )
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, [])

    const handleAddJob = useCallback(() => {
        history.push(`/${language}/dashboard/discovery/jobs/add?init=${searchString}`)
    }, [history, searchString]);

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} md={8} lg={6}>
                <Paper elevation={1} className={classes.hasPadding}>
                    <Box display="flex">
                        <TextField label={t("Search journeys...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                            InputProps={{
                                endAdornment:
                                    <InputAdornment position="end">
                                        <IconButton title={t("Add a journey")} className={classes.newButton} 
                                            onClick={handleAddJob} ref={addRef}>
                                            <AddIcon />
                                        </IconButton>
                                    </InputAdornment>
                            }}
                            autoFocus />
                    </Box>
                    {jobsLoading ?
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                        :
                        jobs !== null && ( // has something to search for
                            jobs.length === 0 ?
                                <div className={classes.loadingPanel}>
                                    <i>{t("No match found...")}</i>
                                </div>
                                :
                                <List>
                                    { jobs.map(
                                        (j, index) => <JobListItem
                                            button
                                            key={index}
                                            jobTitle={j.title}
                                            jobLang={j.lang}
                                            industry={t(getIndustryLabel(j.industry))}
                                            city={j.city}
                                            country={j.country}
                                            company={j.company}
                                            to={`/${language}/dashboard/discovery/jobs/${j.id}`}
                                            component={Link}
                                        />
                                    )}
                                </List>
                        )}
                </Paper>
            </Grid>
            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-goal.jpg`} width="70%" alt="" style={{marginTop: 60, marginBottom: 60}} />                            
        </Grid>

    )
}

export default JobsDiscovery
