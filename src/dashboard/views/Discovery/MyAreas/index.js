import { Grid, Paper, Typography} from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import GridContainer from '../../../components/Grid/GridContainer';
import GridItem from '../../../components/Grid/GridItem';
import Card from '../../../components/Card/Card';
import CardBody from '../../../components/Card/CardBody';
import CardHeader from '../../../components/Card/CardHeader';
import useStyles from '../../styles/SkillsStyle';
import { getNonlearnableItems } from './utils';
import NonlearnableItems from './components/NonlearnableItems';

function MyAreas(props) {
    const classes = useStyles();

    const { t } = useTranslation(['dashboard']);

    const [loading, setLoading] = useState(false);
    const [skills, setSkills] = useState([]);
    const [topics, setTopics] = useState([]);

    const loadData = useCallback(
        () => {
            setLoading(true);
            getNonlearnableItems((data) => {
                setSkills(data.skills);
                setTopics(data.topics)
                setLoading(false);
            },);
        },
        [],
    )    

    useEffect(() => {
        loadData()
    }, [loadData]);    

    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose" className={classes.titleWithButton}>
                        <div className={classes.cardTitleDiv}>
                            <h3 className={classes.cardTitleWhite}>
                                {t("Non-learnable Items")}
                            </h3>
                            <h4 className={classes.cardCategoryWhite}>
                                {t("Contribute to the items (courses or learning topics) which do not have learning material and make them learnable for learners.")}
                            </h4>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Paper>    
                            <Grid item xs={12} className={classes.col} align="center">
                                <Typography variant="h5" component="h2" style={{margin: "20px"}}>
                                    <i> {t("Non-learnable courses based on your area of expertis")} </i>
                                </Typography>
                            </Grid>                                
                            <NonlearnableItems                    
                                items={skills}
                                loading={loading}
                            />
                            <Grid item xs={12} className={classes.col} align="center" style={{marginTop: "20px"}}>
                                <Typography variant="h5" component="h2" style={{margin: "20px"}}>
                                    <i> {t("Non-learnable topics based on your area of expertis")} </i>
                                </Typography>
                            </Grid>                                
                            <NonlearnableItems                    
                                items={topics}
                                loading={loading}
                            />
                        </Paper>
                    </CardBody>
                </Card>                            
            </GridItem>
        </GridContainer>
    )
}

export default MyAreas

