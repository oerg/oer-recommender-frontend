import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DataGrid } from '@mui/x-data-grid';
import Button from '../../../../components/CustomButton/Button';
import { renderDateCell, renderGeneralCell } from '../utils';
import TopicLogo from '../../../../components/Logos/Topic';
import SkillLogo from '../../../../components/Logos/Skill';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../../i18n';

function alwaysFalse() {
    return false;
}


function NonlearnableItems({ items, loading }) {
    const { t } = useTranslation(['dashboard']);
    const [pageSize, setPageSize] = useState(10)
    const language = useLanguage()

    const renderContributeButton = useCallback(
        (params) => <strong>
                <Button
                    variant="contained"
                    color="info"
                    size="sm"
                    style={{ marginLeft: 16 }}
                    component={Link}
                    to={
                        params.row.object_type === "SK"
                            ?
                            `/${language}/dashboard/discovery/skills/${params.row.id}`
                            :
                            `/${language}/dashboard/discovery/topics/${params.row.id}`
                    }
                >
                    {t("Contribute")}
                </Button>
            </strong>,
        [language, t]
    );

    const renderItemLogo = useCallback(
        (params) => (
            <strong>
                {
                    params.row.object_type === "SK"
                        ?
                        <SkillLogo />
                        :
                        <TopicLogo />
                }
            </strong>
        ),
        []
    );

    const columns = useMemo(
        () => [
            {
                field: "item",
                headerName: t("Item"),
                width: 150,
                renderCell: renderItemLogo,
                sortable: false
            },
            {
                field: "title",
                headerName: t("Title"),
                sortable: false,
                width: 350,
                renderCell: renderGeneralCell,
            },
            {
                field: "created_at",
                headerName: t("This item was added to the system at"),
                sortable: true,
                width: 450,
                renderCell: renderDateCell,
            },
            {
                field: 'action',
                headerName: t('Contribute'),
                width: 220,
                renderCell: renderContributeButton,
                disableClickEventBubbling: true,
                sortable: false,
            },
        ],
        [t]
    );

    const handlePageSizeChange = useCallback(
        (pageSize) => {
            setPageSize(pageSize)
        },
        [],
    )

    return (
        <DataGrid
            style={{
                width: "100%",
                minHeight: 400,
            }}
            columns={columns}
            rows={items}
            isCellEditable={alwaysFalse}
            loading={loading}
            onPageSizeChange={handlePageSizeChange}
            pageSize={pageSize}
            rowsPerPageOptions={[10, 25, 50]}
        />
    )
}

export default NonlearnableItems

