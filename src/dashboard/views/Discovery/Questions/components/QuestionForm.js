import { Button, Collapse, FormControl, FormControlLabel, FormLabel, Grid, InputLabel, makeStyles, MenuItem, Paper, Radio, RadioGroup, Select } from '@material-ui/core';
import { ContentState, convertToRaw, EditorState } from 'draft-js';
import convertFromDraftStateToRaw from 'draft-js/lib/convertFromDraftStateToRaw';
import draftjsToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import React, { useCallback, useEffect, useState } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { useTranslation } from 'react-i18next';
import '../../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import MyButton from '../../../../components/CustomButton/Button';
import HtmlDiv from '../../../../components/HtmlDiv';
import { saveQuestion, toolbar } from './utils';

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'left',
        position: 'relative',
    },
    editor: {
        padding: theme.spacing(1),
    }
}))


function QuestionForm({ onSave, readOnly, question, showEditButton, defaultType, onEdit }) {
    const classes = useStyles();
    const [text, setText] = useState(EditorState.createEmpty());
    const [htmlText, setHtmlText] = useState('');
    const [type, setType] = useState('KT');
    const [level, setLevel] = useState('CO');
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation(['dashboard']);

    const handleText = useCallback((newText) => setText(newText), []);
    const handleType = useCallback((e) => setType(e.target.value), []);
    const handleLevel = useCallback((e) => setLevel(e.target.value), []);

    useEffect(
        () => defaultType ? setType(defaultType) : undefined,
        [defaultType]
    );

    const handleSave = useCallback(
        () => {
            var html = draftjsToHtml(convertToRaw(text.getCurrentContent()))
            setLoading(true);
            saveQuestion(
                question ? question.id : null,
                html,
                type,
                level,
                res => onSave({
                    id: question ? question.id : res.question_id,
                    assessing_type: type,
                    level: level,
                    text: html,
                }),
                null,
                () => setLoading(false)
            )
        }, [text, type, level, question]
    );

    useEffect(
        () => {
            if (readOnly) {
                setHtmlText(draftjsToHtml(convertFromDraftStateToRaw(text.getCurrentContent())))
            }
        },
        [readOnly, text]
    );

    useEffect(
        () => {
            if (question) {
                setType(question.assessing_type);
                setLevel(question.level);
                if (readOnly)
                    setHtmlText(question.text)
                else {
                    const blocks = htmlToDraft(question.text);
                    setText(
                        EditorState.createWithContent(
                            ContentState.createFromBlockArray(
                                blocks.contentBlocks,
                                blocks.entityMap
                            )
                        )
                    )
                }
            }
        },
        [question, readOnly]
    )


    return (
        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
            <Grid container>
                <Grid item xs={12} sm={6} className={classes.formField}>
                    <FormControl component="fieldset" disabled={readOnly}>
                        <FormLabel component="legend">{t("Question Type")}</FormLabel>
                        <RadioGroup name="type" value={type} onChange={handleType}>
                            <FormControlLabel value="KT" control={<Radio />} label={t("Knowledge Test for Topics")} />
                            <FormControlLabel value="EX" control={<Radio />} label={t("Exam for Courses")} />
                        </RadioGroup>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} className={classes.formField}>
                    <FormControl fullWidth variant="outlined" disabled={readOnly}>
                        <InputLabel id="level_type-label">{t("Difficulty Level")}</InputLabel>
                        <Select labelId="level_type-label" label={t("Difficulty Level")} value={level} onChange={handleLevel}>
                            <MenuItem value="NO">{t("Novice")}</MenuItem>
                            <MenuItem value="AB">{t("Advanced Beginner")}</MenuItem>
                            <MenuItem value="CO">{t("Competent")}</MenuItem>
                            <MenuItem value="PR">{t("Proficient")}</MenuItem>
                            <MenuItem value="EX">{t("Expert")}</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} className={classes.formField}>
                    <Paper>
                        <FormLabel component="legend" style={{ padding: 14 }}>
                            {t("Question")}
                            {readOnly && showEditButton &&
                                <span style={{ float: "right" }}>
                                    <Button onClick={onEdit}>
                                        {t("Edit")}
                                    </Button>
                                </span>
                            }
                        </FormLabel>
                        {readOnly ?
                            <HtmlDiv
                                html={htmlText}
                                className={classes.editor}
                            />
                            :
                            <Editor
                                editorState={text}
                                onEditorStateChange={handleText}
                                editorClassName={classes.editor}
                                readOnly={readOnly}
                                toolbar={toolbar}
                            />
                        }
                    </Paper>
                </Grid>
                <Grid item xs={12} className={classes.formField}>
                    <Collapse in={!readOnly}>
                        <MyButton color="rose" onClick={handleSave} disabled={!text.getCurrentContent().hasText() || loading}
                            fullWidth>{t("Save Question and Edit Choices")}</MyButton>
                    </Collapse>
                </Grid>
            </Grid>
        </Paper>
    )
}

QuestionForm.defaultProps = {
    readOnly: false,
}

export default QuestionForm
