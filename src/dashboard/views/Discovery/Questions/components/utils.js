import axios from "axios";
import { requestAddHandlers } from "../../../helpers";

export function saveQuestion(question_id, question_text, assessing_type, level, onLoad, onError, onEnd) {
    requestAddHandlers(
        question_id ? // update?
            axios.put('/assessments/questions/update-question/', {
                question_id,
                question_text,
                assessing_type,
                level,
            })
        :
            axios.post('assessments/questions/add-question/', {
                question_text,
                assessing_type,            
                level,
            }),
        onLoad, onError, onEnd
    )
}

export function handleUpload(file) {
    return new Promise(
        (resolve, reject) => {
            var data = new FormData();
            data.append('file', file);
            data.append('title', file.name);
            data.append('mime_type', file.type);
            data.append('is_question', true);
            axios.post(
                '/oers/upload-oer/',
                data
            )
            .then(
                res => {
                    resolve({
                        data: {
                            link: res.data.url
                        }
                    })
                }
            )
            .catch(
                err => reject(err)
            )
        }
    )
}

export function saveTopicInQuestion(question_id, topic_ids, is_add, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post(
            '/assessments/questions/update-topics-of-question/',
            {
                question_id,
                topic_ids,
                is_add,
            }
        ),
        onLoad,
        onError,
        onEnd,
    )
}

export function isAllowedToAddTopic(topic_id, onAllow, onReject) {
    axios.get(
        '/assessments/questions/add-question/is_this_user_allowed?topic_id=' + topic_id
    )
    .then(
        res => {
            if (res.data.allowed)
                onAllow();
            else
                onReject && onReject();
        }
    )
}

export function publishQuestion(question_id, publish, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post(
            '/assessments/questions/publish-question/',
            {
                question_id,
                publish
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export const toolbar = {
    options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link', 'image', 'remove', 'history'],
    image: {
        urlEnabled: false,
        uploadEnabled: true,
        alignmentEnabled: true,
        uploadCallback: handleUpload,
        previewImage: true,
        inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
        alt: { present: true, mandatory: false },
        defaultSize: {
          height: 'auto',
          width: 'auto',
        },
    },
}