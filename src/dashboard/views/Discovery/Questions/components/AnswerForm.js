import { FormControlLabel, FormLabel, Grid, IconButton, makeStyles, Paper, Switch } from '@material-ui/core'
import { ContentState, convertToRaw, EditorState } from 'draft-js'
import draftToHtml from "draftjs-to-html"
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Editor } from 'react-draft-wysiwyg'
import Button from '../../../../components/CustomButton/Button'
import HtmlDiv from '../../../../components/HtmlDiv'
import { toolbar } from './utils'
import htmlToDraft from 'html-to-draftjs';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'left',
        position: 'relative',
    },
    editor: {
        padding: theme.spacing(1),
    },
    isCorrect: {
        marginLeft: theme.spacing(1),
    }
}))

function AnswerForm({ canBeCorrect, label: propLabel, onSave, disabled, readOnly, answer, loading, showRemoveButton, onRemove }) {
    const classes = useStyles();
    const [text, setText] = useState(EditorState.createEmpty());
    const [isCorrect, setIsCorrect] = useState(false);
    const [htmlText, setHtmlText] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);

    const label = useMemo(
        () => propLabel ? propLabel : t("Answer"),
        [t, propLabel]
    );

    const handleText = useCallback(newValue => setText(newValue), []);
    const handleIsCorrect = useCallback(e => setIsCorrect(e.target.checked), []);
    const handleSave = useCallback(
        () => onSave && onSave({
            text: draftToHtml(convertToRaw(text.getCurrentContent())),
            is_answer: isCorrect,
        }),
        [ text, isCorrect, onSave ]
    );

    const handleRemove = useCallback(
        () => onRemove && onRemove(answer),
        [ onRemove, answer ]
    )

    useEffect( // update isAnswer based on canBeAnswer
        () => setIsCorrect(prev => canBeCorrect ? prev : false),
        [ canBeCorrect ]
    );

    useEffect(
        () => {
            if (answer) {
                if (readOnly) {
                    setHtmlText(answer.text);
                } else {
                    const blocks = htmlToDraft(answer.text)
                    setText(
                        EditorState.createWithContent(
                            ContentState.createFromBlockArray(
                                blocks.contentBlocks,
                                blocks.entityMap
                            )
                        )
                    );                    
                }

                setIsCorrect(answer.is_answer);
            }
        },
        [ answer, readOnly ]
    );

    return (
        <Grid container>
            <Grid item xs={12} className={classes.formField}>
                <Paper>
                    <FormLabel component="legend" style={{ padding: 14 }}>
                        {t(label)}
                        {((readOnly && isCorrect) || (!readOnly && canBeCorrect)) &&
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={isCorrect}
                                        onChange={handleIsCorrect}
                                        color="primary"
                                        disabled={readOnly || disabled || !canBeCorrect}
                                    />
                                }
                                label={t("Correct Answer")}
                                className={classes.isCorrect}
                            />
                        }
                        { showRemoveButton &&
                            <span style={{ float: "right" }}>
                                <IconButton onClick={handleRemove} title="Remove">
                                    <DeleteForeverIcon />
                                </IconButton>
                            </span>
                        }
                    </FormLabel>
                    { readOnly ?
                        <HtmlDiv
                            html={htmlText}
                            className={classes.editor}
                        />
                    :
                        <Editor 
                            editorState={text}
                            onEditorStateChange={handleText}
                            editorClassName={classes.editor}
                            readOnly={disabled || readOnly}
                            toolbar={toolbar}
                        />
                    }
                </Paper>
            </Grid>
            { !readOnly &&
            <Grid item xs={12} className={classes.formField}>
                <Button
                    onClick={handleSave}
                    color="rose"
                    disabled={!text.getCurrentContent().hasText() || loading}
                    fullWidth
                >
                    {t("Save Choice")}
                </Button>
            </Grid>
            }
        </Grid>
    );
}

AnswerForm.defaultProps = {
    canBeCorrect: true,
    disabled: false,
    readOnly: false,
    loading: false,
}

export default AnswerForm
