import { Box, Collapse, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, IconButton, makeStyles, Typography } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useLocation } from 'react-router'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import MovableList from '../../../components/MovableList'
import TopicObjectInput from '../../../components/SystemInputs/TopicObjectInput'
import { roseColor } from '../../styles/variables'
import AnswerForm from './components/AnswerForm'
import QuestionForm from './components/QuestionForm'
import { isAllowedToAddTopic, publishQuestion, saveTopicInQuestion } from './components/utils'
import { removeAnswer, saveAnswer } from './utils'

const useStyles = makeStyles(theme => ({
    addSkillButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: -24,
        zIndex: 999,
    },
}))

function Add(rawProps) {
    const {
        id: propId = null,
        question: propQuestion = null,
        choices: propChoices = [],
        topics: propTopics = [],
        enqueueSnackbar,
    } = rawProps;

    const classes = useStyles();
    const [id, setId] = useState(null);
    const [question, setQuestion] = useState(null);
    const [choices, setChoices] = useState([]);
    const [loading, setLoading] = useState(false);
    const [addChoice, setAddChoice] = useState(false);
    const [questionEditing, setQuestionEditing] = useState(true);
    const [topics, setTopics] = useState([]);
    const [selectedTopic, setSelectedTopic] = useState(null);
    const [topicsToSave, setTopicsToSave] = useState([]);
    const [showAddForm, setShowAddForm] = useState(false);
    const [defaultType, setDefaultType] = useState(null);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const { state } = useLocation();

    useEffect(
        () => {
            if (state && state.topicId) {
                const baseTopic =  {
                    id: state.topicId,
                    title: state.topicTitle
                }
                setTopics(topics => [baseTopic, ...topics]);
                setTopicsToSave(topics => [baseTopic.id, ...topics]);
            } else if (state && state.skillId) {
                setDefaultType('EX');
                if (state.topics) {
                    setTopics(topics => [...state.topics, ...topics]);
                    setTopicsToSave(topics => [...state.topics, ...topics]);
                }
            }
        },
        [ state ]
    )

    // update data from props
    useEffect(
        () => setId(propId),
        [propId]
    );
    useEffect(
        () => setQuestion(propQuestion),
        [propQuestion]
    );
    useEffect(
        () => {
            if (propChoices.length > 0) {
                setChoices([...propChoices]);
            }
        },
        [propChoices]
    );
    useEffect(
        () => {
            if (propTopics.length > 0) {
                setTopics(topics => [...topics, ...propTopics]);
            }
        },
        [propTopics]
    );


    const hasCorrectAnswer = useMemo(
        () => choices.some(a => a.is_answer),
        [choices]
    );

    const history = useHistory();

    const handleQuestionAdd = useCallback(
        q => {
            setId(q.id);
            setQuestion(q);
            setQuestionEditing(false);
        },
        []
    );

    const handleAddAnswer = useCallback(
        ans => {
            setLoading(true);
            setAddChoice(false);
            saveAnswer(
                id,
                ans,
                true,
                (res) => setChoices(prev => [...prev, { ...ans, id: res.choice_id }]),
                null,
                () => setLoading(false)
            )
        },
        [id]
    );

    const handleAddChoice = useCallback(
        () => setAddChoice(true),
        []
    );

    const handleQuestionEdit = useCallback(
        () => setQuestionEditing(true),
        []
    );

    const handleRemoveAnswer = useCallback(
        (ans) => {
            if (!window.confirm(t("Do you want to remove this choice from the question?")))
                return;
            setLoading(true);
            removeAnswer(
                id,
                ans.id,
                () => setChoices(prev => prev.filter(c => c.id !== ans.id)),
                null,
                () => setLoading(false)
            );
        },
        [id]
    );

    const handleRemoveNewAnswer = useCallback(
        () => setAddChoice(false),
        []
    );

    const handleOpenTopicForm = useCallback(
        () => setShowAddForm(true),
        []
    );

    const handleCloseTopicForm = useCallback(
        () => setShowAddForm(false),
        []
    );

    const handleAddTopic = useCallback(
        () => {
            setLoading(true);
            isAllowedToAddTopic(
                selectedTopic.id,
                () => {
                    setShowAddForm(false);
                    setLoading(false);
                    setTopics(
                        topics => [...topics, { ...selectedTopic }]
                    );
                    setTopicsToSave(
                        topics => [...topics, selectedTopic.id]
                    );
                },
                () => {
                    enqueueSnackbar(
                        t("You are not an expert in this topic."),
                        {
                            variant: "warning"
                        }
                    );
                    setLoading(false);
                }
            );
        },
        [selectedTopic, t, enqueueSnackbar]
    );

    const handleSelectedTopic = useCallback(
        newValue => setSelectedTopic(newValue),
        []
    );

    const handleTopicRemove = useCallback(
        (index, topics) => {
            setLoading(true);
            saveTopicInQuestion(
                id,
                [topics[index].id],
                false,
                () => {
                    setTopics(
                        ts => {
                            ts.splice(index, 1);
                            return [...ts];
                        }
                    );
                },
                null,
                () => setLoading(false)
            )
        },
        []
    );

    const handlePublish = useCallback(
        () => {
            setLoading(true);
            publishQuestion(
                id,
                true,
                () => history.replace(`/${language}/dashboard/discovery/questions/${id}`),
                null,
                () => setLoading(false)
            )
        },
        [id, history, language]
    );

    useEffect( // add topics after saving question
        () => {
            if (id) {
                setTopicsToSave(
                    topicsToSave => {
                        if (topicsToSave.length > 0) {
                            setLoading(true);

                            saveTopicInQuestion(
                                id,
                                topicsToSave,
                                true,
                                null,
                                null,
                                () => setLoading(false)
                            );
                            return []
                        }
                        return topicsToSave
                    }
                );
            }
        },
        [id, topicsToSave]
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Add a new question")}
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody style={{ maxWidth: 800, margin: 'auto' }}>
                        <Box textAlign="center" position="relative" mb={3}>
                            <Typography component="legend" style={{ textAlign: "left", marginTop: 14 }}>{t("Topics")}</Typography>
                            <MovableList
                                options={topics}
                                emptyText={t("No topics selected!")}
                                movable={false}
                                onRemove={handleTopicRemove}
                            />
                            <IconButton className={classes.addSkillButton} title={t("Add Topic")} onClick={handleOpenTopicForm}>
                                <AddCircleRoundedIcon />
                            </IconButton>
                            <Dialog open={showAddForm} onClose={handleCloseTopicForm}
                                PaperProps={{
                                    style: {
                                        minWidth: 450,
                                    }
                                }}
                            >
                                <DialogTitle>
                                    {t("Select a topic")}
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        <TopicObjectInput onSelect={handleSelectedTopic} />
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button
                                        onClick={handleAddTopic}
                                        color="rose"
                                        disabled={!selectedTopic}
                                    >
                                        {t("Add selected topic")}
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Box>
                        <QuestionForm
                            onSave={handleQuestionAdd}
                            readOnly={!questionEditing || loading}
                            showEditButton={id !== null && !questionEditing}
                            onEdit={handleQuestionEdit}
                            question={question}
                            defaultType={defaultType}
                        />
                        <Collapse in={!questionEditing}>
                            <Typography component="legend" style={{ textAlign: "left", marginTop: 14 }}>{t("Choices")}</Typography>

                            {choices.map(
                                (ans, index) => (
                                    <AnswerForm
                                        readOnly
                                        answer={ans}
                                        key={index}
                                        label={t("Choice") + ` ${index + 1}`}
                                        showRemoveButton
                                        onRemove={handleRemoveAnswer}
                                    />
                                )
                            )}
                            {addChoice ?
                                <AnswerForm
                                    onSave={handleAddAnswer}
                                    disabled={loading}
                                    label={t("Choice") + ` ${choices.length + 1} (New)`}
                                    canBeCorrect={!hasCorrectAnswer}
                                    answer={{
                                        text: '',
                                        is_answer: false,
                                    }}
                                    showRemoveButton
                                    onRemove={handleRemoveNewAnswer}
                                />
                                :
                                <Button
                                    startIcon={<AddIcon />}
                                    color="rose"
                                    onClick={handleAddChoice}
                                    fullWidth
                                >
                                    {t("Add Choice")}
                                </Button>
                            }
                            <Button
                                fullWidth
                                color="rose"
                                onClick={handlePublish}
                                disabled={loading || choices.length === 0 || topics.length === 0 || choices.every(c => !c.is_answer)}
                            >
                                {t("Publish this question")}
                            </Button>
                        </Collapse>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

export default withSnackbar(Add)
