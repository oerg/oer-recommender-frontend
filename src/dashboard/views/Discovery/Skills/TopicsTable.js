import { ButtonGroup, ClickAwayListener, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Fab, FormControl, Grow, IconButton, InputLabel, makeStyles, MenuItem, Paper, Popper, Select, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TableRow, withStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteSweepIcon from '@material-ui/icons/DeleteSweep';
import EditIcon from '@material-ui/icons/Edit';
import ImportExportIcon from '@material-ui/icons/ImportExport';
import { Alert } from '@material-ui/lab';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import Button from '../../../components/CustomButton/Button';
import Help from '../../../components/Help';
import Icons from '../../../components/Icons';
import TopicInput from '../../../components/SystemInputs/TopicInput';
import Vote from '../../../components/Vote';

const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
    },
    buttonRoot: {
        display: 'inline-block',
        marginRight: 8,
        marginTop: 15,
    },
    button: {
    },
    hidden: {
        display: 'none',
    },
    stickyFooter: {
        left: 0,
        bottom: -20,
        zIndex: 2,
        position: 'sticky',
        textAlign: 'center',
    },
    oerGreen: {
        backgroundColor: '#005f5f',
        color: '#FFF',
        opacity: 0.5,
        transition: 'margin-bottom 0.3s',
        '&:hover': {
            backgroundColor: "#004555",
            color: '#AAA',
            opacity: 1,
            marginBottom: 10
        }
    },
    headRow: {
        height: 60,
    },
    learnableIcon: {
        marginLeft: 6,
    }
})

function EditDialog({ id, title, show, onClose, topics }) {
    const [reorderValue, setReorderValue] = useState(0);
    const [orderChanged, setOrderChanged] = useState(false);
    const [myIndex, setMyIndex] = useState(-1);
    const { t } = useTranslation([ 'dashboard' ]);

    useEffect(() => { // update form in a new window
        const mi = topics.findIndex(t => t.topic === id)
        setMyIndex(mi)
        setReorderValue(mi - 1);
        setOrderChanged(false);
    }, [id, topics])

    const getOrder = useCallback(
        after => { // gets after index and gives a valid order number
            if (after === -1) { // wants to be the first
                if (topics.length > 0)
                    return topics[0].order - 16.0;
                return 1; // the only skill
            } else if (after === topics.length - 1) { // wants to be the last
                return topics[after].order + 16.0;
            }
            return (topics[after + 1].order + topics[after].order) / 2.0;
        },
        [topics]
    );

    const handleDialog = useCallback((id, save) => () => {
        if (save) {
            onClose(id, true, { topic_order: getOrder(reorderValue) });
        }
        onClose(id, false);
    }, [ onClose, getOrder, reorderValue ]);

    const handleOrderSelect = useCallback(e => {
        setReorderValue(e.target.value);
        setOrderChanged(true);
    }, [])

    return (
        <Dialog open={show} onClose={onClose} disableBackdropClick disableEscapeKeyDown>
            <DialogTitle>
                {t("Edit suggestion: ")} '{title}'
            </DialogTitle>
            <DialogContent>
                <FormControl fullWidth>
                    <InputLabel>{t("Place it after")}</InputLabel>
                    <Select value={reorderValue} onChange={handleOrderSelect}>
                        <MenuItem value={-1} disabled={myIndex === 0}><i>- ({t("Set as first topic")})</i></MenuItem>
                        {topics.map((ts, index) => (
                            <MenuItem value={index} key={index} disabled={myIndex === index}>
                                {myIndex === index ? `=> `: t("After ")}
                                &nbsp;<b><i>{ts.topic_title}</i></b>
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(id, false)} color="info">
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(id, true)} disabled={!orderChanged} color="rose">
                    {t("Send")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

function DeleteDialog({ id, title, show, onClose }) {
    const { t } = useTranslation([ 'dashboard' ]);
    const handleDialog = useCallback((id, save) => () => {
        onClose(id, save);
    }, [onClose]);
    return (
        <Dialog open={show} onClose={onClose} disableBackdropClick disableEscapeKeyDown>
            <DialogTitle>
                {t("Delete Suggestion")}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {t("Are you sure you want to issue a delete suggestion for the following topic?")}<br />
                    '<em>{title}</em>'
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleDialog(id, false)} color="info">
                    {t("No")}
                </Button>
                <Button startIcon={<DeleteIcon />} onClick={handleDialog(id, true)} color="danger">
                    {t("Yes")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

function AddTopicDialog({show, onClose, topics, defaultTitle, isExpert, ...props}) {
    const [orderValue, setOrderValue] = useState(-1);
    const [title, setTitle] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);

    useEffect(
        () => {
            if (defaultTitle)
                setTitle(defaultTitle);
        },
        [ defaultTitle ]
    );

    const getOrder = useCallback(
        after => { // gets after index and gives a valid order number
            if (after === -1) { // wants to be the first
                if (topics.length > 0)
                    return topics[0].order - 16.0;
                return 1; // the only skill
            } else if (after === topics.length - 1) { // wants to be the last
                return topics[after].order + 16.0;
            }
            return (topics[after + 1].order + topics[after].order) / 2.0;
        },
        [topics]
    );

    const handleDialog = useCallback((save, title, order) => () => {
        save ?
        onClose(true, {
            topic_title: title, topic_order: getOrder(order)
        })
        :
        onClose(false, {});
    }, [getOrder, onClose]);

    const handleOrderSelect = useCallback(e => {
        setOrderValue(e.target.value);
    }, []);

    const handleTitleChange = useCallback(value => {
        setTitle(value);
    }, []);

    return (
    <Dialog open={show} onClose={onClose} disableEscapeKeyDown disableBackdropClick>
        <DialogTitle>{t("Suggest adding a new topic")}</DialogTitle>
        <DialogContent>
            <DialogContentText component="div">
                <TopicInput onSelect={handleTitleChange} newValue={defaultTitle} label={t("Title")} />
                <FormControl fullWidth>
                    <InputLabel>{t("Place new topic after")}</InputLabel>
                    <Select value={orderValue} onChange={handleOrderSelect}>
                        <MenuItem value={-1}><i>- ({t("Set as first topic")})</i></MenuItem>
                        {topics.map((ts, index) => (
                            <MenuItem value={index} key={index}>{t("After ")}&nbsp;<b><i>{ts.topic_title}</i></b></MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </DialogContentText>
            <Alert severity='warning'>
                {isExpert ?
                    t("Your suggestion will be accepted automatically.")
                :
                    t("Your suggestion will be accepted when it recieves enough votes by the community.")
                }
            </Alert>
        </DialogContent>
        <DialogActions>
                <Button onClick={handleDialog(false)} color="info">
                    {t("Cancel")}
                </Button>
                <Button onClick={handleDialog(true, title, orderValue)} disabled={title.length === 0} color="rose">
                    {t("Save")}
                </Button>
            </DialogActions>
    </Dialog>
    );
}

const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

function TopicsTable({ className, topics, onVote, onDelete, onEdit, onNew, disabled, headClass, addObject, isExpert, ...props }) {
    const classes = useStyles();
    const [showDelete, setShowDelete] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    const [editId, setEditId] = useState(0); // used for both edit and delete
    const [editTitle, setEditTitle] = useState(''); // used for both edit and delete
    const [showNew, setShowNew] = useState(false);
    const [editMenuOpen, setEditMenuOpen] = useState(false);
    const [editRef, setEditRef] = useState(null);
    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();

    const handleEditMenuClose = useCallback(() => {
        setEditMenuOpen(false);
    }, []);

    const handleEditMenuOpen = useCallback((id, title) => e => {
        setEditTitle(title);
        setEditId(id);

        let newRef = e.target
        setEditRef(ref => {
            if (ref === newRef)
                setEditMenuOpen(p => !p)
            else
                setEditMenuOpen(true);
            return newRef;
        });
    }, []);

    const handleEdit = useCallback(() => {
        setShowEdit(true);
    });

    const handleDelete = useCallback(() => {
        setShowDelete(true);
    });

    const handleDeleteDialog = useCallback((id, del) => {
        setShowDelete(false);
        if (!id) return;
        if (del) onDelete(id);
    }, [onDelete]);

    const handleEditDialog = useCallback((id, edit, data) => {
        if (edit) onEdit(id, data);
        setShowEdit(false);
    }, [onEdit]);

    const handleShowNew = useCallback(() => {
        setShowNew(true);
    }, [])

    const handleNewDialog = useCallback((save, data) => {
        setShowNew(false);
        if (save) onNew(data);
    }, [onNew]);

    useEffect(
        () => {
            if (addObject) {
                setShowNew(true);
            }
        },
        [ addObject ]
    )

    return (<>
        <AddTopicDialog show={showNew} onClose={handleNewDialog} topics={topics} defaultTitle={addObject} isExpert={isExpert} />
        <DeleteDialog id={editId} show={showDelete} title={editTitle} onClose={handleDeleteDialog} />
        <EditDialog id={editId} show={showEdit} title={editTitle} onClose={handleEditDialog} topics={topics} />
        <Popper open={editMenuOpen} anchorEl={editRef} transition placement="left">
            { ({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                    <ClickAwayListener onClickAway={handleEditMenuClose}>
                        <ButtonGroup color="default" size="small">
                            <IconButton title={t("Suggest a re-order")} onClick={handleEdit}><ImportExportIcon /></IconButton>
                            <IconButton title={t("Suggest a delete")} onClick={handleDelete}><DeleteSweepIcon /></IconButton>
                        </ButtonGroup>
                    </ClickAwayListener>
                </Grow>
            )}
        </Popper>

        <TableContainer component={Paper} className={className} style={{ height: '400px'}} onScroll={handleEditMenuClose} {...props}>
            <Table size="small" stickyHeader>
                <TableHead>
                    <TableRow className={classes.headRow}>
                        <TableCell component="th" className={headClass}>{t("Topics in this course")}</TableCell>
                        <TableCell component="th" align="right" className={headClass}>{t("Actions")}
                            <Help>
                                {t("Here you can:")}
                                <ul>
                                    <li>{t("suggest to Delete/Reorder a topic for this course.")}</li>
                                    <li>{t("Up-vote/Down-vote each topic that helps users find the most important topics in this course.")}</li>
                                </ul>
                            </Help>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                { topics.length === 0 ?
                    <TableRow>
                        <TableCell colSpan={3}>
                            <em>{t("No topics.")}</em>
                        </TableCell>
                    </TableRow>
                :
                topics.map((i, index) => (
                    <StyledTableRow key={index}>
                        <TableCell>
                            <Link to={`/${language}/dashboard/discovery/topics/${i.topic}`} title={t("Go to this topic")}>{i.topic_title}</Link>
                            <Icons learnable={i.topic_learnable} assessmentable={i.topic_assessmentable} withBorder={false} isTopic
                                className={classes.learnableIcon} hideLearnable={i.topic_learnable} hideAssessmentable={i.topic_assessmentable} />
                        </TableCell>
                        <TableCell align="right">
                            <IconButton size="small" onClick={handleEditMenuOpen(i.topic, i.topic_title)} 
                                className={classes.buttonRoot} title={t("Suggest an edit")}>
                                <EditIcon />
                            </IconButton>
                            <Vote selfVote={i.vote} up={i.upvote_count} down={i.downvote_count} onChange={onVote(i.topic)} className={classes.vote} disabled={disabled} />
                        </TableCell>
                    </StyledTableRow>
                ))}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TableCell colSpan={3} align="right" classes={{
                            footer: classes.stickyFooter
                        }}>
                            <Fab size="small" className={classes.oerGreen} title={t("Suggest adding a topic")} disabled={disabled} onClick={handleShowNew}>
                                <AddIcon />
                            </Fab>
                        </TableCell>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    </>)
}

export default TopicsTable
