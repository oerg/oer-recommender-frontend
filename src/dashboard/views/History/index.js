import { Accordion, AccordionDetails, AccordionSummary, Box, CircularProgress, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import RoseText from '../../components/Typography/Rose';
import { extractSkills } from '../Goals/utils';
import useStyles from '../styles/HistoryStyles';
import TopicRow from './components/TopicRow';
import { hasFinishedOers, loadHistory, removeExtraTopics } from './utils';


function History() {
    const classes = useStyles()
    const history = useHistory();

    const [skills, setSkills] = useState([]);
    const [notCurrentSkills, setNotCurrentSkills] = useState([]);
    const [topics, setTopics] = useState([]);
    const [notCurrentTopics, setNotCurrentTopics] = useState([]);
    const [loading, setLoading] = useState(false);
    const { tab: activeTab } = useParams();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const handleOpenTab = useCallback((skill) => (e, expand) => {
        if (expand)
            history.push(`/${language}/dashboard/history/${skill}`);
        else
            history.push(`/${language}/dashboard/history`);
    }, [history]);

    useEffect(
        () => {
            setLoading(true);
            loadHistory(
                res => {
                    const allSkills = extractSkills(res);

                    const skills = allSkills.filter(s => (hasFinishedOers(s) && s.is_current));
                    setSkills(skills.map(removeExtraTopics(false)));
                    const notCurrentOnes = allSkills.filter(s => (hasFinishedOers(s) && s.is_current === false));
                    setNotCurrentSkills(notCurrentOnes.map(removeExtraTopics(false)));
                    
                    const topics = allSkills[0].topics.filter(t => t.is_current && t.user_oers.length > 0);
                    setTopics(topics);
                    const notCurrentTopics = allSkills[0].topics.filter(t => t.is_current === false && t.user_oers.length > 0);
                    setNotCurrentTopics(notCurrentTopics);
                },
                null,
                () => setLoading(false)
            )
        },
        []
    );

    return (loading ?
        <Box m="auto" p={2} textAlign="center" minHeight='150px'>
            <CircularProgress />
        </Box>
        :
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("My History")}
                        </h3>
                        <h4 className={classes.cardCategoryWhite}>
                            {t("What I did so far")}
                        </h4>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 35 }}>
                            <span className={classes.paperTitle}>{t("My current courses")}</span>
                            { skills.length === 0 ?
                                <em>{t("History is empty.")}</em>
                            :
                            skills.map((skill, ind) => (
                                <Accordion key={ind} expanded={skill.title === activeTab} onChange={handleOpenTab(skill.title)}>
                                    <AccordionSummary>
                                        <RoseText className={classes.headerSkill}>{skill.title}</RoseText>
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <TableContainer component={Paper}>
                                            <Table>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell component="th">{t("Status")}</TableCell>
                                                        <TableCell component="th">{t("Topic")}</TableCell>
                                                        <TableCell component="th"></TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {skill.topics.map((topic, ind) => (
                                                        <TopicRow key={`t-${ind}`} topic={topic} classes={classes} />
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </AccordionDetails>
                                </Accordion>
                            ))}
                        </Paper>
                        {notCurrentSkills && notCurrentSkills.length > 0 &&
                            <Paper elevation={2} style={{ marginTop: 35 }}>
                                <span className={classes.paperTitle}>{t("Courses not part of my current curriculum")}</span>
                                {notCurrentSkills.map((skill, ind) => (
                                    <Accordion key={ind} expanded={skill.title === activeTab} onChange={handleOpenTab(skill.title)}>
                                        <AccordionSummary>
                                            <RoseText className={classes.headerSkill}>{skill.title}</RoseText>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <TableContainer component={Paper}>
                                                <Table>
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell component="th">{t("Status")}</TableCell>
                                                            <TableCell component="th">{t("Topic")}</TableCell>
                                                            <TableCell component="th"></TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {skill.topics.map((topic, ind) => (
                                                            <TopicRow key={`t-${ind}`} topic={topic} classes={classes} />
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </AccordionDetails>
                                    </Accordion>
                                ))}
                            </Paper>
                        }
                        {topics && topics.length > 0 &&
                            <Paper elevation={2} style={{ marginTop: 35 }}>
                                <span className={classes.paperTitle}>{t("My current topic targets")}</span>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell component="th">{t("Topic")}</TableCell>
                                                <TableCell component="th"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {topics.map((topic, ind) => (
                                                <TopicRow topic={topic} classes={classes} topicOnly key={ind} />
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Paper>
                        }
                        {notCurrentTopics && notCurrentTopics.length > 0 &&
                            <Paper elevation={2} style={{ marginTop: 35 }}>
                                <span className={classes.paperTitle}>{t("Topics not part of my current curriculum")}</span>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableBody>
                                            {notCurrentTopics.map((topic, ind) => (
                                                <TopicRow topic={topic} classes={classes} topicOnly  key={ind} />
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Paper>
                        }
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    )
}

export default History
