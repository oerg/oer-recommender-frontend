import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function hasFinishedOers(item, isTopic = false) {
    const source = isTopic ? item : item.topics
    for (const index in source) {
        const topic = source[index];
        if (topic.user_oers.length > 0)
            return true;
    }
    return false;
}

export const removeExtraTopics = (isTopic) => (item, index) => {
    const origTopics = isTopic ? item : item.topics;
    const topics = [];
    for (const topicIndex in origTopics) {
        if (origTopics[topicIndex].user_oers.length > 0)
            topics.push(origTopics[topicIndex])
    }
    return {
        ...item,
        topics
    };
}

export function loadHistory(onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.get('/members/get-history/'),
        onLoad,
        onError,
        onEnd
    )
}