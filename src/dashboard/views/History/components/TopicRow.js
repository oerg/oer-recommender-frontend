import { Collapse, IconButton, Table, TableCell, TableRow } from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'classnames';
import React, { useEffect, useRef, useState } from 'react';
import OerGroup from '../../../components/OerGroup';
import TopicStatusIcon from '../../../components/TopicStatusIcon';
import { useParamWatcher } from '../../helpers';

function TopicRow({ topic, classes, topicOnly = false, ...props }) {
    const [expanded, setExpanded] = useState(false);
    const urlTopicId = parseInt(useParamWatcher('topic'));
    const rowRef = useRef();

    useEffect(() => {
        if (urlTopicId === topic.id) {
            setExpanded(true);
            rowRef.current && rowRef.current.scrollIntoView({
                behavior: 'smooth',
            })
        }
    }, [urlTopicId, topic.id]);

    return (
        <>
            <TableRow hover className={clsx(classes.row, classes[topic.status])} ref={rowRef} onClick={() => setExpanded(p => !p)} key={1}>
                {!topicOnly &&
                    <TableCell>
                        <TopicStatusIcon status={topic.status} />
                    </TableCell>
                }
                <TableCell>
                    {topic.title}
                </TableCell>
                <TableCell align="right">
                    <IconButton>
                        {expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow key={2}>
                <TableCell colSpan={topicOnly ? 2 : 3} style={{ padding: 0 }}>
                    <Collapse in={expanded}>
                        <Table>
                            <tbody>
                                {topic.user_oers.filter(oerg => oerg.status !== 'AS').map((oerg, ind) => (
                                    <TableRow key={`og-${ind}`}>
                                        <TableCell colSpan={3}>
                                            <OerGroup readOnly group={oerg.oer_group} status={oerg.status} />
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </tbody>
                        </Table>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

export default TopicRow