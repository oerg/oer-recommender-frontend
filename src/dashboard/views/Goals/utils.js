import { default as Axios, default as axios } from 'axios';
import { parseISOToDate } from "../../../utils";
import { MultiCall, requestAddHandlers } from '../helpers';

export function getJob(id, onSuccess, onError, onEnd) { // get job and suggestions from the server
    requestAddHandlers(
        Axios.get('/jobs/' + id),
        onSuccess,
        onError,
        onEnd
    );
}

function computeProgress(skill) {
    var done = skill['topics'].filter(t => t.status === "FI").length;
    var all = skill['topics'].length;
    return Math.round(done * 100.0 / all); // round up to 2 decimals
}

function addSingleTopicToSkill(skills, skillId, userSkillUserTopic, topic, userSkills) {
    var id = typeof skillId === 'number' ? skillId : 0; // 0 is for solo topics
    if (!(id in skills)) { // new skill
        skills[id] = {
            id: userSkillUserTopic['skill_id'],
            title: userSkillUserTopic['skill_title'],
            is_current: userSkillUserTopic['skill_is_current'],
            order: userSkillUserTopic['skill_order'],
            status: userSkillUserTopic['skill_status'],
            success: userSkillUserTopic['skill_success'],
            picture: userSkillUserTopic.skill_picture,
            needs_update: id === 0 ? false : userSkills.find(s => s.skill_id === id).skill_needs_update,
            // ...userSkillUserTopic,
            topics: [],
        }
    }
    skills[id]['topics'].push({
        id: topic['topic_id'],
        title: topic['topic_title'],
        success: topic['success'],
        status: topic['status'],
        role: topic['role'],
        order: userSkillUserTopic['topic_order'],
        is_current: userSkillUserTopic['is_current'],
        user_oers: topic['user_oers'],
        picture: topic.topic_picture,
    });
    skills[id]['progress'] = computeProgress(skills[id]);
}

// change array of user_skills to new structure of [ <skill1>, <skill2> ]
// with topics inside of each
export function extractSkills(userTopics, userSkills) {
    if (!userTopics) return {}
    var skillsObject = {
        0: { // for user-added topics
            topics: []
        }
    };
    userTopics.forEach(
        ut =>
            ut['user_skill_user_topic'].forEach(
                usut => addSingleTopicToSkill(skillsObject, usut['skill_id'], usut, ut, userSkills)
            )

    )

    var skills = Object.values(skillsObject);
    skills.forEach(
        skill => skill['topics'].sort((a, b) => a['order'] - b['order'])
    );
    return skills;
}

// returns
// [
//      {
//          ...job1,
//          mySkills: [<skill1>, <skill2>,...]
//      }
// ]
export function mapSkillsToMyJobs(myJobs, skills) {
    var jobs = []
    myJobs.forEach(
        job => {
            var skillRow = [];
            job.skills.forEach(
                jobSkill => {
                    var mySkill = skills.find(s => s['id'] === jobSkill['id'])
                    if (mySkill) {
                        skillRow.push(mySkill);
                    }
                }
            );
            jobs.push({
                ...job,
                mySkills: skillRow.sort((a, b) => a['order'] - b['order']),
            })
        }
    );
    return jobs;
}

// returns an array of skills which are not part of a job
export function getExtraSkills(myJobs, skills) {
    var mySkills = []
    skills.filter(s => s['id'] && s['is_current']).forEach(
        skill => {
            for (const job in myJobs) {
                var jobSkill = myJobs[job]['skills'].find(js => js['id'] === skill['id']);
                if (jobSkill) { // skill exists in a job
                    return;
                }
            }
            // skill not found
            mySkills.push(skill);
        }
    )
    return mySkills.sort((a, b) => a['order'] - b['skill']);
}

function getTopicsOfSkill(skill_id, topics) {
    return topics.filter(
        topic => topic.user_skill_user_topic.some(usut => usut.skill_id === skill_id)
    );
}

// calculate order based on prev and next item
export function getSkillOrder(arr, ind) {
    if (arr.length < 2) // skill is alone
        return 1;

    if (ind === 0) // first
        return arr[1].skill_order - 16;

    if (ind === arr.length - 1) // last
        return arr[ind - 1].skill_order + 16;

    return (arr[ind - 1].skill_order + arr[ind + 1].skill_order) * 0.5;
}

function computeSkillProgress(skill_id, topics) {
    if (topics) {
        var skillTopics = getTopicsOfSkill(skill_id, topics);
        var done = skillTopics.filter(t => t.status === "FI").length;
        var all = skillTopics.length;
        if (all > 0)
            return Math.round(done * 100.0 / all);
        else
            return 100.0;
    }
    return 0;
}


export function processUserSkills(skills, topics) {
    return skills
        .filter(s => s.is_current)
        .map(s => ({
            id: s.skill_id,
            title: s.skill_title,
            order: s.skill_order,
            progress: computeSkillProgress(s.skill_id, topics),
            picture: s.skill_picture,
            ...s,
            deadline: s.deadline ? parseISOToDate(s.deadline) : null
        }))
        .sort((a, b) => a.order - b.order)
}

export function processUserTopics(topics) {
    return topics
        .filter(t => t['user_skill_user_topic'].some(ust => ust['is_current'] && !ust['skill_id']))
        .map(
            t => ({
                id: t.topic_id,
                title: t.topic_title,
                is_current: t['user_skill_user_topic']['is_current']
            })
        );
}


export function saveGoalJob(job, isAdd, onLoad, onError, onEnd) {
    requestAddHandlers(axios.put('/members/set-goal', { job_id: job.id, is_add: isAdd }), onLoad, onError, onEnd);
}

export function saveSkill(skillId, isAdd, order, onLoad, onError, onEnd) {
    requestAddHandlers(axios.put('/members/update-skill/',
        {
            skills: [{
                is_add: isAdd,
                skill: skillId,
                skill_order: order
            }]
        }),
        onLoad, onError, onEnd);
}

export function updateSkillsBatch(skills, is_add, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.put('/members/update-skill/', {
            skills: skills.map(
                s => ({ skill: s.id, is_add })
            )
        }),
        onLoad,
        onError,
        onEnd
    );
}

export function saveTopic(skill, isAdd, onLoad, onError, onEnd) {
    requestAddHandlers(axios.post('/members/add-remove-topic/',
        {
            is_add: isAdd,
            topic_id: skill.id,
        }),
        onLoad, onError, onEnd);
}

export function saveMoveSkill(skill, order, onLoad, onError, onEnd) {
    requestAddHandlers(axios.put('/members/update-skill/',
        {
            skills: [{
                skill_order: order,
                skill: skill.id,
            }]
        }),
        onLoad, onError, onEnd);
}


export function getPossibleIndustries(job_title, jobs) {
    let industries = []
    jobs.filter(j => j.title === job_title).forEach(j => industries.push(j.industry));
    return [...new Set(industries)];
}


function removeDuplicates(list) {
    return [...new Set(list)];
}

export function getTitles(list) {
    let titles = []
    list.forEach(i => titles.push(i.title))
    return removeDuplicates(titles);
}

const mcLoadJobs = "PG_loadJobs"

export function loadJobs(title, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(mcLoadJobs);
    mc.call(`/jobs/?search=${title}&page-size=100`, onLoad, onError, onEnd);
}

export function cancelLoadJobs() {
    MultiCall.getInstance(mcLoadJobs).cancel();
}

// used for autocomplete labels
export function getOptionTitle(obj) {
    return obj ? obj.title : '';
}


const mcLoadTopics = "PG_loadTopics"

export function loadTopics(search, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(mcLoadTopics)
    mc.call(`/skills/topics/get-only-titles/?title=${search}&learnable=True`, onLoad, onError, onEnd);
}

const mcLoadSkills = "PG_loadSkills"

export function loadSkills(search, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(mcLoadSkills)
    mc.call(`/skills/?search=${search}&addable=True`, onLoad, onError, onEnd);
}


export function saveDeadline(skill_id, deadline, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.put(
            "/members/change-skill-deadline/",
            {
                skill_id,
                deadline,
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}