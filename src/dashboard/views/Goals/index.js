
import { Box, Button, CircularProgress, Grid, Paper, Typography } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link, useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import { getLocation } from '../../components/JobItem/utils';
import LangIcon from '../../components/LangIcon';
import LoginWarning from '../../components/LoginWarning';
import GoalLogo from '../../components/Logos/Goal';
import ShareButton from '../../components/ShareButton';
import { saveGoalJob, saveSkill, updateSkillsBatch } from './utils';
import useStyles from '../styles/SkillsStyle';
import SkillCard from './components/SkillCard';
import { getJob } from './utils';
import SkillConfirmDialog from '../Education/components/SkillConfirmDialog';
import AddIcon from '@material-ui/icons/Add';
import DoneIcon from '@material-ui/icons/Done';


export default function Goals({ basePath, user, userData }) {
    const classes = useStyles();
    const { id: strJobId } = useParams();

    const goalId = useMemo(
        () => {
            if (strJobId) {
                return parseInt(strJobId);
            }
            return null;
        },
        [strJobId]
    )

    const [goal, setGoal] = useState(null);
    const [skills, setSkills] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [isSaving, setIsSaving] = useState(false)
    const [criticalError, setCriticalError] = useState(null);
    const [userReloadNeeded, setUserReloadNeeded] = useState(false);
    const [skillsDialogOpen, setSkillsDialogOpen] = useState(false);

    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const history = useHistory();

    const location = useMemo(
        () => goal ? getLocation(goal['country'], goal['city']) : "",
        [goal]
    );

    useEffect(
        () => {
            if (userReloadNeeded)
                return () => user.reload()
        },
        [userReloadNeeded]
    )

    const reload = useCallback(
      () => {
        setLoading(true);
        getJob(goalId, (data) => { // load
            setSkills(data.jobskill_set.sort((a, b) => a.order - b.order));
            setGoal(data);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Journey was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
      },
      [goalId],
    )
    

    useEffect(() => { // load job and suggestions
        reload()
    }, [reload]);

    const handleCancelSkillDialog = useCallback(
        () => {
            setSkillsDialogOpen(false);
        },
        []
    );

    const handleConfirmSkillDialog = useCallback(
        (skills, is_add) => {
            updateSkillsBatch(
                skills.filter(skill => !skill.is_current).map(skill => ({"id": skill.skill, "is_add": is_add})),
                is_add,
                () => {
                    reload()
                },
                null,
                () => {
                    setSkillsDialogOpen(false);
                }
            )
        },
        [reload]
    );

    const handleAddSkill = useCallback(
        (skill) => {
            if (!user.loggedIn) {
                setShowLoginWarning(true);
                return;
            }

            if (window.confirm(`${t('Do you want to add the following course into your target courses?')} ${skill.skill_title}`)) {
                setIsSaving(true);
                saveSkill(skill.skill,
                    true,
                    null,
                    null,
                    null,
                    () => {
                        setIsSaving(false);
                        setSkills(skills => {
                            const target_skill = skills.find(s => s.skill === skill.skill)
                            target_skill.is_current = true
                            return [...skills]
                        });
                        setUserReloadNeeded(true);
                    }
                )
            }
        },
        [user, t]
    );

    const handleAddGoal = useCallback(
        () => {
            if (!user.loggedIn) {
                setShowLoginWarning(true);
                return;
            }

            if (window.confirm(`${t("Do you want to add this to your journeys?")} ${goal.title}`)) {
                setIsSaving(true);
                saveGoalJob(
                    goal,
                    true,
                    () => {
                        setSkillsDialogOpen(true)
                        reload()
                    },
                    null,
                    () => {
                        setLoading(false)
                        setIsSaving(false);
                    }
                )
            }
        },
        [goal, user, reload]
    );

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history, goalId]);

    return (
        <>
            {isLoading ?
                <Box p={10} textAlign="center">
                    <CircularProgress />
                </Box>
                :
                criticalError ?
                    <Box p={10} textAlign="center">
                        {criticalError}
                    </Box>
                    :
                    <GridContainer>
                        <GridItem xs={12}>
                            <Card>
                                <CardHeader color="rose" className={classes.titleWithButton}>
                                    <GoalLogo size="large" className={classes.logo} picture={goal.picture} />
                                    <div className={classes.cardTitleDiv}>
                                        <h3 className={classes.cardTitleWhite}>
                                            {goal['title']} <LangIcon language={goal['lang']} small />
                                            {goal.company &&
                                                <Typography variant="caption" title={t("Company")} className={classes.company}>
                                                    {t("by")} {goal.company}
                                                </Typography>
                                            }
                                        </h3>
                                        <h4 className={classes.cardCategoryWhite} title={t("Industry and Location")}>
                                            <em>{t(goal['industry_label'])}</em>
                                            {location &&
                                                (
                                                    <> in
                                                        <em>
                                                            {" " + location}
                                                        </em>
                                                    </>
                                                )
                                            }
                                        </h4>
                                    </div>
                                    <div className={classes.cardButtonDiv}>
                                        {(user.loggedIn && userData.settings.discovery) &&
                                            <Button title={t("Participate in this journey")} variant="outlined"
                                                disabled={isSaving} component={Link} to={`/${language}/dashboard/discovery/jobs/${goalId}`}
                                            >
                                                <PeopleIcon />
                                            </Button>
                                        }
                                        <ShareButton
                                            component={Button}
                                            variant="outlined"
                                            url={window.location.href}
                                            title={goal.title}
                                            componentTitle={t("Share this Journey")}
                                        >
                                            <ShareIcon />
                                        </ShareButton>
                                        <Button title={goal.is_my_goal ? t("This is already set as a journey") : t("Add as my Journey")}
                                            variant="outlined" onClick={handleAddGoal} disabled={isSaving || goal.is_my_goal}
                                            classes={{
                                                disabled: classes.addButtonDisabled
                                            }}>
                                            {goal.is_my_goal ?
                                                <DoneIcon />
                                                :
                                                <AddIcon />
                                            }
                                        </Button>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    {goal.description &&
                                        <Paper elevation={2} style={{ marginTop: 5, marginBottom: 25, padding: 20 }}>
                                            {goal.description}
                                        </Paper>}
                                    <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                        <Grid item xs={12} className={classes.col} align="center">
                                            <Typography variant="h5" component="h2">
                                                {t("Suggested courses for mastering: ")} <i>{goal['title']}</i>
                                            </Typography>
                                        </Grid>
                                        {skills.map((skill, ind) => (
                                            <Grid key={ind} item xs={12} md={6} lg={4}>
                                                <SkillCard skill={skill} onAddSkill={handleAddSkill}
                                                    addDisabled={isSaving} basePath={`${window.location.protocol}//${window.location.host}${basePath}`} />
                                            </Grid>
                                        ))}
                                    </Grid>
                                </CardBody>
                            </Card>
                        </GridItem>
                        <LoginWarning show={!user.loggedIn && showLoginWarning} onSelect={handleLogin} />
                        <SkillConfirmDialog
                            open={skillsDialogOpen}
                            onCancel={handleCancelSkillDialog}
                            onAccept={handleConfirmSkillDialog}
                            isAdd={true}
                            skills={skills}
                            showFollow={false}
                        />
                    </GridContainer>
            }
        </>
    )
}
