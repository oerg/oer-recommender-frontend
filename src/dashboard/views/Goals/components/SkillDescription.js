import { Typography } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import React from 'react'
import { useTranslation } from 'react-i18next'

export default function SkillDescription({ description, topics, loading }) {
    const { t } = useTranslation(['dashboard']);
    return (<>
        { !description && !loading && topics && topics.length === 0 && <i>{t("No description available.")}</i>}
        { description !== null &&
            <Typography paragraph>
                {description}
            </Typography>
        }
        { loading ?
            <>
              <Skeleton variant="text" />
              <Skeleton variant="rect" />
            </>
        :
            topics && topics.length > 0 && 
            <>
                <i>{t("Covered topics in this course")}</i>
                <ul>
                {
                    topics.map((topic, ind) => (
                    <li key={ind}>{topic}</li>
                    ))                    
                }
                </ul>
            </>
        }
    </>)
}
