import { Button } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { green } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import DoneIcon from '@material-ui/icons/Done';
import LaunchIcon from '@material-ui/icons/Launch';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Icons from '../../../components/Icons';
import SkillLogo from '../../../components/Logos/Skill';
import ShareButton from '../../../components/ShareButton';
import { getSkillOnly } from '../../Discovery/Skills/utils';
import DetailWindow from './DetailWindow';
import SkillDescription from './SkillDescription';

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around"
    },
    actionRight: {
        marginLeft: 'auto'
    },
    avatar: {
        backgroundColor: green[100],
    },
    cardHeaderTitle: {
        width: "95%",
        whiteSpace: "nowrap",
        overflow : "hidden",
        textOverflow: "ellipsis"
    },
    doneIcon: {
        cursor: "default",
    },
    title: {
        cursor: "pointer",
        paddingRight: theme.spacing(0.5)
    }
}));

export default function SkillCard({ skill, onAddSkill, addDisabled, basePath }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    const [loading, setLoading] = useState(false);
    const [topics, setTopics] = useState(null); // null = not loaded yet
    const [showDescription, setShowDescription] = useState(false);
    const [mouseOver, setMouseOver] = useState(false);
    const [, setCriticalError] = useState(null);

    const myUrl = useMemo(
        () => `${basePath}/skills/${skill.skill}`,
        [ skill, basePath ]
    );

    const handleMouseEnter = useCallback(
        () => setMouseOver(true),
        []
    );

    const handleMouseOut = useCallback(
        () => setMouseOver(false),
        []
    );

    const handleAddSkill = useCallback(() => {
        onAddSkill(skill)
    },
        [onAddSkill]
    );

    const loadSkillDetails = useCallback(
        (skillId) => { // load skill and suggestions
            setLoading(true);
            getSkillOnly(
                skillId,
                res => {
                    setTopics(
                        res.data.skilltopic_set.sort((a, b) => a.order - b.order).map(t => t.topic_title)
                    );
                    setLoading(false);
                }, error => { // on error
                    if (error) {
                        if (error.status === 404) {
                            setCriticalError("404: Course was not found!");
                        } else if (error.status >= 500) {
                            setCriticalError(error.status + ": Server error!")
                        } else
                            setCriticalError(error.data);
                    }
                    setLoading(false);
                }
            );
        }, 
        []);

    const handleShowDetails = useCallback(
        () => {
            setShowDescription(true);
            if (topics === null) {
                loadSkillDetails(skill.skill);
            }
        },
        [ topics, loadSkillDetails ]
    );

    const handleCloseDescription = useCallback(
        () => setShowDescription(false),
        []
    );

    return (<>
        <Card className={classes.root}
            raised={mouseOver} 
            onMouseEnter={handleMouseEnter} 
            onMouseLeave={handleMouseOut}
            >
            <CardHeader
                avatar={<SkillLogo />}
                action={
                    skill.is_current ?
                        <div 
                            className={`MuiButtonBase-root MuiButton-root MuiButton-outlined ${classes.doneIcon}`}
                            title={t("Already in your learning dashboard.")}
                        >
                            <DoneIcon htmlColor="green" />
                        </div>
                        :
                        skill.skill_addable ?
                        <Button
                            variant="outlined"
                            title={t("Add to my Learning Dashboard")}
                            className={classes.newButton}
                            onClick={handleAddSkill}
                            disabled={addDisabled}
                        >
                            <AddIcon />
                        </Button>
                        :
                        <Icons learnable={skill.skill_learnable} addable={skill.skill_addable} assessmentable={skill.skill_assessmentable} withBorder={true} />
                }
                title={skill.skill_title}
                titleTypographyProps={{
                    title: skill.skill_title,
                    variant: "h6",
                    className: classes.title,
                    onClick: handleShowDetails
                }}
            />
            <CardActions disableSpacing>
                <div className={classes.actionRight}>
                    <ShareButton title={skill.skill_title} componentTitle={t("Share this Course")} url={myUrl}>
                        <ShareIcon />
                    </ShareButton>
                    <IconButton title={t("Learn more")} onClick={handleShowDetails}>
                        <LaunchIcon />
                    </IconButton>
                </div>
            </CardActions>
        </Card>
        <DetailWindow
            title={skill.skill_title}
            icon={<SkillLogo />}
            content={<SkillDescription description={skill.skill_description} topics={topics} loading={loading} />}
            open={showDescription}
            onClose={handleCloseDescription}
            skillId={skill.skill}
        />
    </>);
}
