import { Button, Dialog, DialogActions, DialogContent, DialogContentText, IconButton, makeStyles, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 500,
    },
    titleRoot: {
      padding: theme.spacing(1),
      display: "flex",
      flex: "0 0 auto",
    },
    noGrow: {
      flex: "0 0 auto",
    },
    title: {
        flex: "1 1 auto",
        margin: theme.spacing(1),
    }
}))
  
export default function DetailWindow({ title, content, onClose, icon, skillId, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dahsboard'])
    const language = useLanguage();

    return (
        <Dialog scroll="paper" onClose={onClose} classes={{ paper: classes.root }} {...props}>
            <div className={classes.titleRoot}> 
                { icon && 
                    <div className={classes.noGrow}>{icon}</div>
                }
                <Typography variant="h5" className={classes.title}>{title}</Typography>
                <IconButton onClick={onClose} className={classes.closeButton}>
                    <CloseIcon />
                </IconButton>
            </div>
            <DialogContent dividers>
                <DialogContentText>
                {content}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button fullWidth variant="outlined" component={Link} to={`/${language}/dashboard/skills/${skillId}`}>
                    {t("View This Course")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
