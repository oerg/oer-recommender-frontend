import { Tooltip } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'

function TooltipWithHelp({ enterTimeout, leaveTimeout, ...props }) {
    const [open, setOpen] = useState(false)

    useEffect(
        () => {
            if (enterTimeout) {
                const timeoutHandle = window.setTimeout(
                    () => {
                        setOpen(true)
                        if (leaveTimeout) {
                            window.setTimeout(
                                () => setOpen(false),
                                leaveTimeout
                            )
                        }
                    },
                    enterTimeout
                )
                return () => window.clearTimeout(timeoutHandle)
            }
        },
        [enterTimeout, leaveTimeout]
    )

    const handleOpen = useCallback(() => setOpen(true), [])
    const handleClose = useCallback(() => setOpen(false), [])

    return (
        <Tooltip {...props} open={open} onOpen={handleOpen} onClose={handleClose} />
    )
}

export default TooltipWithHelp