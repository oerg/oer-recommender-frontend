import { MultiCall } from "../../views/helpers";

const MC_CODE1 = "ltf_ll"
export function detectTextLanguage(term, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(MC_CODE1);
    return mc.call(`/jobs/get-langs/?text=${term}`, onLoad, onError, onEnd);
}

export function cancelDetectTextLanguage() {
    MultiCall.getInstance(MC_CODE1).cancel();
}