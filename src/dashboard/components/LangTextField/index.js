import { makeStyles, MenuItem, Select, TextField } from '@material-ui/core';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getAllLanguages } from '../../../data';
import { useLanguage } from '../../../i18n';
import { cancelDetectTextLanguage, detectTextLanguage } from './utils';

const useStyles = makeStyles({
    noPadding: {
        padding: 10,
    },
    wrapper: {
        display: "flex",
        alignItems: "flex-end",
    },
    langSelect: {
        height: 45,
        paddingTop: 0,
        paddingBottom: 12,
    },
    langSelectInsideAutocomplete: {
        marginBottom: 8,
    }
})

const LangTextField = React.forwardRef((rawProps, ref) => {
    const {
        language,
        onLanguageChange,
        onChange,
        autoDetect = true,
        SelectProps,
        insideAutocomplete,
        ...props
    } = rawProps;
    const classes = useStyles();

    const systemLanguage = useLanguage();
    const inputRef = useRef(null);
    const { t } = useTranslation(['dashboard']);
    const [loadingLanguage, setLoadingLanguage] = useState(false);
    const [, setLastInputProps] = useState(null);

    const allLanguages = useMemo(
        () => getAllLanguages(systemLanguage),
        [systemLanguage]
    );

    const handleLanguage = useCallback(
        (e) => {
            cancelDetectTextLanguage();
            setLoadingLanguage(false);
            onLanguageChange(e.target.value)
        },
        [ onLanguageChange ]
    );
    
    const doDetectLanguage = useCallback(
        (text) => {
            cancelDetectTextLanguage();
            if (text) {
                setLoadingLanguage(true);
                onLanguageChange(null);
                detectTextLanguage(
                    text,
                    ({ data }) => {
                        onLanguageChange(data.lang); 
                    },
                    () => {},
                    () => setLoadingLanguage(false)
                );
            }
        },
        [onLanguageChange]
    )

    useEffect(
        () => {
            if (autoDetect) {
                if (props.value) {
                    doDetectLanguage(props.value)
                } else if (props.inputProps && props.inputProps.value) {
                    setLastInputProps(
                        lastInputProps => {
                            if (!lastInputProps || lastInputProps.value !== props.inputProps.value) {
                                doDetectLanguage(props.inputProps.value)
                            }
                            return props.inputProps
                        }
                    )
                }
            }
        },
        [doDetectLanguage, autoDetect, props.value, props.inputProps]
    )

    const handleChange = useCallback(
        (e) => {
            const text = e.target.value;
            if (onChange)
                onChange(e, text);
        },
        [onChange]
    );

    return <div className={classes.wrapper}>
    <TextField
        {...props}
        ref={ref}
        inputRef={inputRef}
        onChange={handleChange}
    />
    <Select
        variant='filled'
        className={clsx(classes.langSelect, {
            [classes.langSelectInsideAutocomplete]: insideAutocomplete
        })}
        {...SelectProps} 
        value={loadingLanguage ? '__loading__' : language} 
        onChange={handleLanguage} 
        title={t("Language")}
    >
        {loadingLanguage &&
            <MenuItem value="__loading__" >
                <i>{t("Loading...")}</i>
            </MenuItem>
        }
        {allLanguages.map(
            (lang, index) => <MenuItem key={index} value={lang.code} title={lang.label}>{lang.label}</MenuItem>
        )}
    </Select>
    </div>;
});

LangTextField.propTypes = {
    autoDetect: PropTypes.bool,
    language: PropTypes.string,
    onLanguageChange: PropTypes.func,
    onChange: PropTypes.func,
};

export default LangTextField;
