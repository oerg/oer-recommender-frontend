import { makeStyles, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import Axios from 'axios';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
export function countryToFlag(isoCode) {
    return typeof String.fromCodePoint !== 'undefined'
        ? isoCode
            .toUpperCase()
            .replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
        : isoCode;
}

const useStyles = makeStyles({
    option: {
        fontSize: 15,
        '& > span': {
            marginRight: 10,
            fontSize: 18,
        },
    },
});

function CountrySelect({ value: titleValue, label: propLabel, onChange, required, showGeneral, ...props }) {
    const classes = useStyles();
    const [countries, setCountries] = useState([]);
    const [loaded, setLoaded] = useState(false);
    const [value, setValue] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);
    const label = useMemo(
        () => propLabel ? propLabel : t('Country'),
        [propLabel, t]
    );

    useEffect(() => {
        setValue(p => !p || p.title !== titleValue ? countries.find(i => i.title === titleValue) : p);
    }, [countries, titleValue]);

    useEffect(() => {
        Axios.get('/jobs/get-countries-cities/?is-country=true')
            .then(res => {
                if (res.status === 200) {
                    setCountries(res.data);
                    setLoaded(true);
                }
            }).catch(e => {
                setCountries([]);
                setLoaded(true);
            })
    }, []);

    const handleChange = useCallback((e, v) => onChange(e, v ? v.title : v), [onChange]);

    if (loaded)
    return (
        <Autocomplete classes={{ option: classes.option }} options={countries} autoHighlight loading={!loaded}
            getOptionLabel={o => o.title} onChange={handleChange} value={value ? value : { code: "", title: "" }}
            getOptionSelected={(o, v) => v.code === "" || o.code === v.code}
            renderOption={o => (
                <>
                    <span>{countryToFlag(o.code)}</span>
                    {o.title}
                </>
            )} renderInput={params => (
                <TextField 
                    {...params}
                    InputLabelProps={{
                        ...params.InputLabelProps,
                        shrink: showGeneral || Boolean(params.inputProps.value),
                    }}
                    placeholder={showGeneral ? t("General") : ""}
                    label={label} 
                    variant="standard"
                    required={required} 
                />
            )} {...props} />
    );
    else
        return null;
}

CountrySelect.propTypes = {
    value: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func,
    required: PropTypes.bool,
    showGeneral: PropTypes.bool,
}

CountrySelect.defaultProps = {
    onChange: () => { },
    required: false,
    showGeneral: false,
}

export default CountrySelect
