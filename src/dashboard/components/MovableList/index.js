import { ButtonGroup, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, makeStyles } from '@material-ui/core';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import RemoveIcon from '@material-ui/icons/Remove';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(
    theme => ({
        listRoot: {
            border: '1px solid #EEE',
            borderRadius: '5px',
            textAlign: 'center',
            margin: `${theme.spacing(1)}px auto`,
            maxHeight: '300px',
            overflowY: 'auto',
            '& > li:nth-child(even)': {
                backgroundColor: '#CCC',
            }
        },
    })
)

function MovableList({ options, className, emptyText, formatTitle, disabled,
    onMoveUp, onMoveDown, onRemove, movable, removable, ...props }) {
    const classes = useStyles();
    const { t: tr } = useTranslation(['dashboard']);

    const handleUp = useCallback(
        (index) =>
            () => onMoveUp(index, options),
        [options]
    );

    const handleDown = useCallback(
        (index) =>
            () => onMoveDown(index, options),
        [options]
    );

    const handleRemove = useCallback(
        (index) =>
            () => onRemove(index, options),
        [options]
    );

    return (
        <List className={clsx(classes.listRoot, className)}>
            {options.length === 0 ?
                <ListItem>
                    <ListItemText primary={<i>{emptyText}</i>} />
                </ListItem>
                :
                options.map((t, index) => (
                    <ListItem key={index}>
                        <ListItemText primary={formatTitle(t)} />
                        <ListItemSecondaryAction>
                            <ButtonGroup size="small">
                                {movable ?
                                    <>
                                        {index > 0 &&
                                            <IconButton title={tr("Move Up")} disabled={disabled} onClick={handleUp(index)}>
                                                <ArrowUpwardIcon />
                                            </IconButton>
                                        }
                                        {index < options.length - 1 &&
                                            <IconButton title={tr("Move Down")} disabled={disabled} onClick={handleDown(index)}>
                                                <ArrowDownwardIcon />
                                            </IconButton>
                                        }
                                    </>
                                    :
                                    <></>
                                }
                                {removable &&
                                    <IconButton title={tr("Remove")} onClick={handleRemove(index)} disabled={disabled}>
                                        <RemoveIcon />
                                    </IconButton>
                                }
                            </ButtonGroup>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))
            }
        </List>
    )
}

MovableList.propTypes = {
    options: PropTypes.array.isRequired,
    className: PropTypes.string,
    emptyText: PropTypes.string,
    disabled: PropTypes.bool,
    formatTitle: PropTypes.func,
    movable: PropTypes.bool,
    removable: PropTypes.bool,
}

MovableList.defaultProps = {
    emptyText: "No options!",
    disabled: false,
    formatTitle: o => o.title,
    movable: true,
    removable: true,
}

export default MovableList

