import { Parser as HtmlParser } from 'html-to-react';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';

const praser = new HtmlParser();

function HtmlDiv({ html, ...props }) {
    const parsedChildren = useMemo(
        () => praser.parse(html),
        [html]
    )
    return (
        <div {...props}>
            {parsedChildren}
        </div>
    )
}

HtmlDiv.propTypes = {
    html: PropTypes.string.isRequired,
}

export default HtmlDiv

