import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import Button from '../../components/CustomButton/Button';

function LoginWarning({ show, onSelect, path, autoBackUrl = true, enqueueSnackbar }) {
    const { t } = useTranslation(['dashboard'])
    const history = useHistory()

    useEffect(
        () => {
            if (history.location.state && history.location.state.backed) {
                enqueueSnackbar(
                    t("You have been logged in. You can continue your actions from here."),
                    {
                        varaint: "success"
                    }
                )
            }
        },
        [history.location, enqueueSnackbar]
    )

    return (
        <Dialog open={show}>
            <DialogTitle>
                {t("Login Required")}
            </DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    {t("This action requires a user.")}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => onSelect(null)} color="warning">
                    {t("Cancel")}
                </Button>
                <Button onClick={() => onSelect('/register')} color="info">
                    {t("Register")}
                </Button>
                <Button onClick={() => onSelect({
                    pathname: "/",
                    state: {
                        login: true,
                        backUrl: autoBackUrl ? 
                            (path ? 
                                {
                                    pathname: path,
                                    state: { backed: true },
                                } 
                                : 
                                {
                                    pathname: history.location.pathname,
                                    state: { backed: true },
                                }
                            )
                            : 
                            {
                                pathname: path,
                                backed: true,
                            }
                    }
                })} color="info">
                    {t("Login")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

LoginWarning.propTypes = {
    show: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
}

export default withSnackbar(LoginWarning)

