import { Alert } from '@material-ui/lab'
import React, { useMemo } from 'react'
import ComputerIcon from '@material-ui/icons/Computer';
import { Collapse } from '@material-ui/core';

const IAlert = React.forwardRef(
    ({ show, transition = true, ...props }, ref) => {
        const Wrapper = useMemo(
            () => transition ? Collapse : React.Fragment,
            [transition]
        )

        return (
            <Wrapper
                {...(transition ? { in: show } : {})}
            >
                <Alert
                    icon={<ComputerIcon fontSize='inherit' />}
                    severity='info'
                    {...props}
                    ref={ref}
                />
            </Wrapper>
        )
    })

export default IAlert