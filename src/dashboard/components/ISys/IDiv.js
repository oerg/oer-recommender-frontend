import { Button, ButtonGroup, Collapse, makeStyles, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ComputerIcon from '@material-ui/icons/Computer';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Help from '../Help';
import TextLoader from '../TextLoader/TextLoader';
import clsx from 'classnames';

const useStyles = makeStyles({
    root: {
        color: 'rgb(13, 60, 97)',
        backgroundColor: 'rgb(232, 244, 253)',
        padding: "6px 16px",
        borderRadius: 4,
        fontSize: "0.875rem",
        fontWeight: 400,
        letterSpacing: "0.01071em",
    },
    title: {
        display: "flex",
        width: "100%",
    },
    titleIcon: {
        display: "flex",
        padding: "7px 0",
        fontSize: 22,
        marginRight: 12,
        color: "#2196f3",
    },
    titleText: {
        flexGrow: 1,
        padding: "8px 0",
        display: "flex",
    },
    actions: {
        display: "flex",
        alignItems: "center",
        marginLeft: "auto",
        marginRight: -8,
        marginTop: 0,
        marginBottom: 0,
        paddingLeft: 16,
    },
    itemsWrapper: {
        backgroundColor: "#f4fafe",
        marginTop: 8,
        maxHeight: 250,
        overflowY: "auto"
    }
})

function IDiv({ show = true, loading, loadingTexts, defaultOpen, children, TextLoaderProps, title, help, items, formatItem, innerClassName, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const [open, setOpen] = useState(Boolean(defaultOpen))
    const [dismissed, setDismissed] = useState(false)

    const toggleOpen = useCallback(
        () => setOpen(o => !o),
        []
    )

    const handleDismiss = useCallback(
        () => setDismissed(true),
        []
    )

    const toRender = useMemo(
        () => children ?
                children
            :
                items.length > 0 ? items.map(formatItem) : <em>{t("No items to show.")}</em>,
        [children, items, formatItem, t]
    )

    return (
        <Collapse in={!dismissed && show}>
            <div className={classes.root} {...props}>
                <div className={classes.title}>
                    <div className={classes.titleIcon}>
                        <ComputerIcon fontSize='inherit' />
                    </div>

                    <div className={classes.titleText}>
                        {loading ?
                            <TextLoader loop={false} {...TextLoaderProps} texts={loadingTexts} />
                            :
                            <>
                                {title}
                                {help &&
                                    <Help style={{ fontSize: 20 }}>
                                        {help}
                                    </Help>
                                }</>
                        }
                    </div>
                    <div className={classes.actions}>
                        <ButtonGroup variant='text' color='inherit'>
                            <Button onClick={toggleOpen} disabled={loading}>
                                {open ? t("Hide") : t("Show")}
                            </Button>
                            <Button title={t("Dismiss")} onClick={handleDismiss}>
                                <CloseIcon color="inherit" />
                            </Button>
                        </ButtonGroup>
                    </div>
                </div>
                <Collapse in={open}>
                    <Paper className={clsx(classes.itemsWrapper, innerClassName)}>
                        {toRender}
                    </Paper>
                </Collapse>
            </div>
        </Collapse>
    )
}

export default IDiv