import { makeStyles } from '@material-ui/core'
import Cookies from 'js-cookie'
import React, { useCallback, useState } from 'react'
import ToggleDiv from '../ToggleDiv'

const useStyles = makeStyles({
    root: {
        backgroundColor: "rgb(253, 236, 234)",
        "& ul": {
            listStyleType: "'- '",
        },
    }
})

const CookieString = '__static_notes_state'

function StaticNote({ id, saveState = true, ...props }) {
    const classes = useStyles()

    const cookieId = id ? `${id}${CookieString}` : ''

    const [open, setOpen] = useState(
        () => {
            const lastValue = Cookies.get(cookieId)
            if (id && lastValue) {
                return lastValue === "true"
            }
            return true
        }
    )

    const handleChange = useCallback(
        (newOpen) => {
            setOpen(newOpen)
            if (saveState && id) {
                Cookies.set(cookieId, newOpen, { expires: 365 })
            }
        },
        [id, saveState]
    )

    return (
        <ToggleDiv
            {...props}
            open={open}
            onChange={handleChange}
            className={classes.root}
        />
    )
}

export default StaticNote