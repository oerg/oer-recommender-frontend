import { makeStyles, Tooltip } from '@material-ui/core'
import React, { useMemo } from 'react'
import clsx from 'classnames'

const useStyles = makeStyles(
    theme => ({
        root: {
            fontSize: "0.90rem",
            minWidth: 500,
        },
        list: {
            paddingLeft: 16,
            listStyleType: "'-'",
        },
        noError: {
            backgroundColor: "rgb(237, 247, 237)",
            color: "rgb(30, 70, 32)",
        },
        error: {
            backgroundColor: "rgb(97, 26, 21)",
            color: "rgb(253, 236, 234)",
        }
    })
)

function Notes({ children, className, error, notes, ...props }) {
    const classes = useStyles()
    const processedNotes = useMemo(
        () => notes ? <ul className={classes.list}>
            {notes.map(
                (note, index) => <li key={index}>{note}</li>
            )}
        </ul>
            : undefined,
        [notes, classes]
    )

    return (
        <Tooltip
            arrow
            disableHoverListener
            disableTouchListener
            interactive
            title={processedNotes}
            placement="bottom-start"
            {...props}
            classes={{
                tooltip: clsx(className, classes.root, {
                    [classes.noError]: !error,
                    [classes.error]: error,
                })
            }}
        >
            {children}
        </Tooltip>
    )
}

export default Notes