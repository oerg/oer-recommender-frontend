import { makeStyles } from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { useTranslation } from 'react-i18next';
import BaseLogo from './Base';

const useStyles = makeStyles({
    topicLogo: {
        backgroundColor: green[400],
    }
})

function OergLogo({ size, className }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    return (
        <BaseLogo size={size} content="EDP" title={t("Educational Package")} className={clsx(classes.topicLogo, className)} />
    )
}

OergLogo.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large'])
}

OergLogo.defaultProps = {
    size: "normal"
}

export default OergLogo
