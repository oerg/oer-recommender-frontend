import { makeStyles } from '@material-ui/core';
import { red } from '@material-ui/core/colors';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { useTranslation } from 'react-i18next';
import BaseLogo from './Base';

const useStyles = makeStyles({
    topicLogo: {
        backgroundColor: red[200],
    }
})

function OerLogo({ size, className }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    return (
        <BaseLogo size={size} content="Con" title={t("Educational Content")} className={clsx(classes.topicLogo, className)} />
    )
}

OerLogo.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large'])
}

OerLogo.defaultProps = {
    size: "normal"
}

export default OerLogo
