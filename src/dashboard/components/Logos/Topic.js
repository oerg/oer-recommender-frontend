import { makeStyles } from '@material-ui/core';
import { blue } from '@material-ui/core/colors';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import BaseLogo from './Base';

const useStyles = makeStyles({
    skillLogo: {
        backgroundColor: blue[200],
    }
})

function TopicLogo({ size, className, title: propTitle, picture, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const title = useMemo(
        () => propTitle ? propTitle : t('Learning Topic'),
        [ t ]
    )

    return (
        <BaseLogo size={size} content="TPC" title={title} className={clsx(classes.skillLogo, className)}
            src={picture} {...props}
        />
    )
}

TopicLogo.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large'])
}

TopicLogo.defaultProps = {
    size: "normal"
}

export default TopicLogo
