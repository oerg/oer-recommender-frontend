import { makeStyles } from '@material-ui/core';
import { lightBlue } from '@material-ui/core/colors';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import BaseLogo from './Base';

const useStyles = makeStyles({
    goalLogo: {
        backgroundColor: lightBlue[400],
    }
})

function GoalLogo({ size, className, title: propTitle, picture, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const title = useMemo(
        () => propTitle ? propTitle : t('Journey'),
        [ t ]
    )
    return (
        <BaseLogo size={size} content="JRN" title={title} className={clsx(classes.goalLogo, className)}
            src={picture} {...props}
        />
    )
}

GoalLogo.propTypes = {
    size: PropTypes.oneOf([ 'small', 'normal', 'large' ])
}

GoalLogo.defaultProps = {
    size: "normal"
}

export default GoalLogo
