import { Accordion, AccordionDetails, AccordionSummary, makeStyles } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { useCallback, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(
    theme => ({
        question: {
            fontWeight: theme.typography.fontWeightBold,
        },
        answer: {
            "& >p": {
                marginTop: 0,
            }
        },
    })
);

function QuestionsList({ questions, onAsk, defaultOpen = false, ...props }) {
    const classes = useStyles();
    const dom = useRef(null);
    const { t } = useTranslation(['dashboard'])


    const linkHanlder = useCallback(
        (e) => {
            const link = e.target;
            if (link.hash) {
                e.preventDefault();
                onAsk && onAsk(link.hash.substring(1), link);
            }
        },
        [onAsk]
    );

    useEffect(
        () => {
            if (dom.current) {
                dom.current.querySelectorAll("a").forEach(
                    link => {
                        link.onclick = linkHanlder;
                        link.style.color = "#333";
                        link.style.textDecoration = "underline";
                        link.style.backgroundColor = null;
                        if (link.hash && !link.title) {
                            link.title = t('Click to learn more');
                        }
                    }
                )
            }
        },
        [questions, linkHanlder, t]
    );

    return questions.length > 0 ?
        <div ref={dom}>
            {questions.map(
                (q, index) => (
                    <Accordion key={index} defaultExpanded={defaultOpen}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <div className={classes.question}>
                                {q.question}
                            </div>
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.answer} dangerouslySetInnerHTML={{ __html: q.answer }} />
                        </AccordionDetails>
                    </Accordion>)
            )}
        </div>
        :
        <div ref={dom}>
            <i>{t("No results.")}</i>
        </div>
}

export default QuestionsList
