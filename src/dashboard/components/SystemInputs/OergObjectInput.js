import React, { useCallback, useState } from 'react'
import PropTypes from 'prop-types'
import { Autocomplete } from '@material-ui/lab'
import { getTopicLabel, loadTopicObjs } from './utils';
import { TextField } from '@material-ui/core';

function OergObjectInput({ onSelect }) {
    const [options, setOptions] = useState([]);
    const [selectedOerg, setSelectedOerg] = useState({ title: '' });
    const [topicText, setTopicText] = useState('');
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);


    const updateOptions = useCallback(
        (search) => {
            setLoading(true);
            loadTopicObjs(
                search,
                res => {
                    setOptions(res.data);
                },
                null,
                () => setLoading(false)
            )
        },
        []
    );

    const handleSelect = useCallback(
        (e, newValue) => {
            onSelect && onSelect(newValue);
            setSelectedOerg(newValue);
        },
        [ onSelect ]
    );

    const handleTopicText = useCallback(
        (e, newValue) => {
            setTopicText(newValue);
            updateOptions(newValue);
        },
        [ updateOptions ]
    );

    return (
        <Autocomplete
            options={options}
            getOptionLabel={getTopicLabel}
            value={selectedOerg}
            onChange={handleSelect}
            inputValue={topicText}
            onInputChange={handleTopicText}
            loading={loading}
            noOptionsText={t("No topics.")}
            renderInput={(params) => <TextField {...params} label={t("Topic")} variant="outlined" />}
        />
    )
}

OergObjectInput.propTypes = {
    onSelect: PropTypes.func,
}

export default OergObjectInput

