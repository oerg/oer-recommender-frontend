import React, { useCallback, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { Autocomplete } from '@material-ui/lab'
import { CircularProgress, TextField } from '@material-ui/core';
import { loadBothSkillsTopics, openFilter } from './utils';
import { useTranslation } from 'react-i18next';



function groupByFunc(t) {
    return (option) => {
    if (option) {
        return option.object_type === "SK" ? t("Course(s)") : t("Topic(s)");
    }
    return "";}
}

function getTitle(option) {
    return option && option.title ?
        option.title : "";
}

function SkillOrTopicInput(props) {
    const { t } = useTranslation(['dashboard']);
    const {
        label: labelProp,
        placeholder: placeholderProp,
        loading: propLoading = false,
        onSelect,
        inputProps,
        defaultValue,
        ...restProps
    } = props;

    const label = useMemo(
        () => labelProp ? labelProp : t("Course or Topic search"),
        [ t, labelProp ]
    );
    const placeholder = useMemo(
        () => placeholderProp ? placeholderProp : t("Type to search..."),
        [ t, placeholderProp ]
    )

    const [options, setOptions] = useState([]);
    const [internalLoading, setInternalLoading] = useState(false);
    const [value, setValue] = useState({});

    const handleChange = useCallback(
        (e, val, reason) => {
            onSelect(val, reason);
            setValue(val);
        },
        [ onSelect ]
    );

    const handleSearch = useCallback(
        (e, val, reason) => {
            if (reason !== "clear") { // not update on clear
                setOptions([])
                loadBothSkillsTopics(
                    val,
                    ({ data }) => {
                        setOptions([ ...data.skills, ...data.topics ]);
                    },
                    null,
                    () => setInternalLoading(false)
                );
            }
        },
        []
    );

    return (
        <Autocomplete
            autoComplete
            autoHighlight
            autoSelect
            groupBy={groupByFunc(t)}
            loading={propLoading || internalLoading}
            getOptionLabel={getTitle}
            options={options}
            value={value}
            onChange={handleChange}
            onInputChange={handleSearch}
            filterOptions={openFilter}
            renderInput={
                params => <TextField
                    {...params}
                    label={label}
                    variant="outlined"
                    placeholder={placeholder}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {internalLoading ? <CircularProgress size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        ),
                    }}            
                    {...inputProps}
                />
            }
            {...restProps}
        />
    )
}

SkillOrTopicInput.propTypes = {
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node
    ]),
    placeholder: PropTypes.string,
    loading: PropTypes.bool,
    inputProps: PropTypes.object,
    onSelect: PropTypes.func.isRequired,
    defaultValue: PropTypes.string,
}

export default SkillOrTopicInput

