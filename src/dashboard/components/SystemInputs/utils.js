import { ListSubheader, useMediaQuery, useTheme } from "@material-ui/core";
import React from "react";
import { VariableSizeList } from 'react-window';
import { MultiCall } from '../../views/helpers';

const loadSkillsCode = "SI_loadSkills";

export function loadSkills(str, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(loadSkillsCode);
    return mc.call('/skills/get-existing-skills/?search=' + str, onLoad, onError, onEnd);
}

export function cancelLoadSkills() {
    return MultiCall.getInstance(loadSkillsCode).cancel();
}

const loadSystemSkillsCode = "SI_loadSSkills";

export function loadSystemSkills(str, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(loadSystemSkillsCode);
    return mc.call('/skills/?search=' + str, onLoad, onError, onEnd);
}

export function cancelLoadSystemSkills() {
    return MultiCall.getInstance(loadSystemSkillsCode).cancel();
}

const loadTopicsCode = "SI_loadTopics";

export function loadTopics(str, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(loadTopicsCode);
    return mc.call('/skills/topics/get-only-titles/?title=' + str, onLoad, onError, onEnd);
}

export function cancelLoadTopics() {
    return MultiCall.getInstance(loadTopicsCode).cancel();
}

const loadTopicsObjCode = "SI_loadTopicsObj";

export function loadTopicObjs(str, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(loadTopicsObjCode);
    return mc.call('/skills/topics/?search=' + str, onLoad, onError, onEnd);
}

export function cancelLoadTopicObjs() {
    return MultiCall.getInstance(loadTopicsObjCode).cancel();
}

const loadBothObjCode = "SI_loadBoth";

export function loadBothSkillsTopics(str, onLoad, onError, onEnd) {
    var mc = MultiCall.getInstance(loadBothObjCode);
    return mc.call(`/mentoring/get-possible-subjects/?search=${str}`, onLoad, onError, onEnd);
}

export function cancelBothSkillsTopics() {
    return MultiCall.getInstance(loadBothObjCode).cancel();
}

export function getTopicLabel(topic) {
    return topic.title;
}

export function openFilter(options, state) {
    return options
}

/*
 * Handler for autocomplete filters
 */
// --------------------------------
// Virtualized components
//
const LISTBOX_PADDING = 8; // px

function renderRow(props) {
    const { data, index, style } = props;
    return React.cloneElement(data[index], {
        style: {
            ...style,
            top: style.top + LISTBOX_PADDING,
        },
    });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
    const outerProps = React.useContext(OuterElementContext);
    return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
    const ref = React.useRef(null);
    React.useEffect(() => {
        if (ref.current != null) {
            ref.current.resetAfterIndex(0, true);
        }
    }, [data]);
    return ref;
}

// Adapter for react-window
export const ListboxComponent = React.forwardRef(function ListboxComponent(props, ref) {
    const { children, ...other } = props;
    const itemData = React.Children.toArray(children);
    const theme = useTheme();
    const smUp = useMediaQuery(theme.breakpoints.up('sm'), { noSsr: true });
    const itemCount = itemData.length;
    const itemSize = smUp ? 36 : 48;

    const getChildSize = (child) => {
        if (React.isValidElement(child) && child.type === ListSubheader) {
            return 48;
        }

        return itemSize;
    };

    const getHeight = () => {
        if (itemCount > 8) {
            return 8 * itemSize;
        }
        return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
    };

    const gridRef = useResetCache(itemCount);

    return (
        <div ref={ref}>
            <OuterElementContext.Provider value={other}>
                <VariableSizeList
                    itemData={itemData}
                    height={getHeight() + 2 * LISTBOX_PADDING}
                    width="100%"
                    ref={gridRef}
                    outerElementType={OuterElementType}
                    innerElementType="ul"
                    itemSize={(index) => getChildSize(itemData[index])}
                    overscanCount={5}
                    itemCount={itemCount}
                >
                    {renderRow}
                </VariableSizeList>
            </OuterElementContext.Provider>
        </div>
    );
});