import { Box, Button, Card, CardActions, CardContent, CircularProgress, IconButton, makeStyles, Typography } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import OpenInNewTwoToneIcon from '@material-ui/icons/OpenInNewTwoTone';
import RedoIcon from '@material-ui/icons/Redo';
import AnimatedNumber from "animated-number-react";
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Carousel from 'react-material-ui-carousel';
import { loadRelevancy } from './utils';

const useStyles = makeStyles(
    theme => ({
        root: {
            width: "100%",
        },
        title: {
            fontSize: 14,
        },
        word: {
            textAlign: "center",
            minHeight: 60
        },
        clickable: {
            cursor: "pointer",
        },
        navButton: {
            opacity: 0.2,
            "&:hover": {
                opacity: 1,
            }
        }
    })
)

function WordSuggestion({ words, onWordClick, formatWord, question: propQuestion, className, onAdd, onIrrelavent, onAlreadyAdded,
    withProgress, showRelevancy = false, disabled, onNeutral, ...props }) {
    const classes = useStyles();
    const [relevancy, setRelevancy] = useState(null);
    const [current, setCurrent] = useState(0);
    const [loadingRelevancy, setLoadingRelevancy] = useState(false);
    const { t } = useTranslation(['dashboard']);


    const question = useMemo(
        () => propQuestion ? propQuestion : t("Is this a topic of this course?"),
        [t, propQuestion]
    );

    // useEffect(
    //     () => {
    //         setCurrent(0);
    //     },
    //     [words]
    // )

    const getRelevancy = useCallback(
        id => {
            if (!showRelevancy) return;
            setLoadingRelevancy(true);
            setRelevancy(null);
            loadRelevancy(
                id,
                data => {
                    setRelevancy(data.relevancy_score);
                },
                null,
                () => setLoadingRelevancy(false)
            )
        },
        [showRelevancy]
    );

    const handleAdd = useCallback(
        () => onAdd && onAdd(words[current].id, formatWord(words[current])),
        [current, formatWord, words, onAdd]
    );

    const handleIrrelavent = useCallback(
        () => onIrrelavent && onIrrelavent(words[current].id, formatWord(words[current])),
        [current, formatWord, words, onIrrelavent]
    );

    const handleWordClick = useCallback(
        () => {
            onWordClick && onWordClick(words[current])
        },
        [onWordClick, words, current]
    );

    const handleChange = useCallback(
        (newCurrent, previous) => {
            // setCurrent(newCurrent);
            console.log("Change called.");
            getRelevancy(words[newCurrent].id);
        },
        [getRelevancy, words]
    );

    const handleNeutral = useCallback(
        () => {
            onNeutral(words[current].id);
            setCurrent(current => current + 1)
        },
        [current, words]
    );

    const formatValue = (value) => value.toFixed(0);

    return (
        <Card className={clsx(classes.root, className)} {...props}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {t(question)}
                </Typography>
                <Carousel
                    index={current}
                    onChange={handleChange}
                    indicators={false}
                    autoPlay={false}
                    animation='slide'
                    changeOnFirstRender
                    strictIndexing
                    navButtonsAlwaysInvisible
                >
                    {words.map(
                        (word, index) => (
                            <div key={index} style={{ textAlign: "center" }}>
                                <Typography
                                    variant="h5"
                                    component="h2"
                                    className={clsx(
                                        classes.word,
                                        {
                                            [classes.clickable]: Boolean(onWordClick)
                                        })}
                                    onClick={handleWordClick}>
                                    {formatWord(word)}
                                </Typography>
                                {showRelevancy &&
                                    <>
                                        <Typography variant="caption">
                                            {t("from")} {props.formatFrom(word)}
                                        </Typography>
                                        <br />
                                        <Button
                                            color="primary"
                                            variant="outlined"
                                            title={t("Open the conent in new tab")}
                                            startIcon={<OpenInNewTwoToneIcon />}
                                            onClick={handleWordClick}
                                        >
                                            {t("View")}
                                        </Button>
                                    </>
                                }
                            </div>

                        )
                    )}
                </Carousel>
            </CardContent>
            <CardActions>
                {showRelevancy &&
                    <span title={t("Relevancy")} style={{ position: "absolute" }}>
                        {loadingRelevancy ?
                            <CircularProgress size={16} />
                            : relevancy ? <>
                                <em><small>{t("Relevancy")}: </small></em>
                                <br />
                                <AnimatedNumber
                                    value={relevancy}
                                    formatValue={formatValue}
                                />
                                <em><small>%</small></em>
                            </> : <em><small>{t("Relevancy is suspicious")}</small></em>}
                    </span>
                }

                <Box flex="1" textAlign="center">
                    <IconButton size="small" title={t("No")} onClick={handleIrrelavent} disabled={disabled}>
                        <NotInterestedIcon />
                    </IconButton>
                    <IconButton size="small" title={t("Yes")} onClick={handleAdd} disabled={disabled}>
                        <CheckIcon />
                    </IconButton>
                </Box>
                <IconButton size="small" title={t("Skip")} onClick={handleNeutral} disabled={disabled} style={{ position: "absolute", right: 40 }}>
                    <RedoIcon />
                </IconButton>
            </CardActions>
        </Card>
    )
}

WordSuggestion.propTypes = {
    words: PropTypes.arrayOf(PropTypes.object).isRequired,
    className: PropTypes.string,
    onAdd: PropTypes.func,
    onAlreadyAdded: PropTypes.func,
    onIrrelavent: PropTypes.func,
    withProgress: PropTypes.bool,
    progressDuration: PropTypes.number,
    pause: PropTypes.bool,
    disabled: PropTypes.bool,
    onWordClick: PropTypes.func,
    question: PropTypes.string,
    formatWord: PropTypes.func,
}

WordSuggestion.defaultProps = {
    withProgress: true,
    progressDuration: 10000,
    hasNext: true,
    hasPrev: true,
    pause: false,
    disabled: false,
    formatWord: word => word.recommendation.word,
}

export default WordSuggestion
