import axios from "axios";
import { requestAddHandlers } from "../../views/helpers";

export function loadRelevancy(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/skills/topics/get-recommendation-relevance/?id=${id}`),
        onLoad,
        onError,
        onEnd
    );
}