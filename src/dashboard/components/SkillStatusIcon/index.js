import React from 'react';
import { yellow, green } from '@material-ui/core/colors'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';

function SkillStatusIcon(props) {
    return props.status === 'FI' ?
            <CheckCircleOutlineIcon style={{ color: green[600] }} />
        :
            <DonutLargeIcon style={{ color: yellow[700] }} />        
}

export default SkillStatusIcon;