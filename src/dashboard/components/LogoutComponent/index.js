import { useContext, useEffect } from 'react';
import { UserContext } from '../../../landing/User';

// on render logs the user out.
function Logout() {
    const user = useContext(UserContext);

    useEffect(() => {
        user.logout();
    }, []);
    return null;
}

export default Logout
