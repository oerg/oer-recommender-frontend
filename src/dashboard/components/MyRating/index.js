import { makeStyles } from '@material-ui/core';
import { green, orange, red, yellow } from '@material-ui/core/colors';
import { Rating } from '@material-ui/lab';
import clsx from 'classnames';
import React, { useCallback, useState } from 'react';

const useStyles = makeStyles({
    r1: {
        color: red[900],
    },
    r2: {
        color: orange[500],
    },
    r3: {
        color: yellow[500],
    },
    r4: {
        color: green[300],
    },
    r5: {
        color: green[600],
    }
})

function MyRating(props) {
    const classes = useStyles();
    const { value } = props;

    const [hoverValue, setHoverValue] = useState(value);

    const handleHover = useCallback((e, v) => setHoverValue(v), []);

    return (
        <Rating onChangeActive={handleHover} classes={{
            iconFilled: clsx({
                [classes.r1]: value === 1,
                [classes.r2]: value === 2,
                [classes.r3]: value === 3,
                [classes.r4]: value === 4,
                [classes.r5]: value === 5,
            }),
            iconHover: clsx({
                [classes.r1]: hoverValue === 1,
                [classes.r2]: hoverValue === 2,
                [classes.r3]: hoverValue === 3,
                [classes.r4]: hoverValue === 4,
                [classes.r5]: hoverValue === 5,
            }),
        }} {...props} />
    );
}

export default MyRating
