import { makeStyles } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import ReactLoading from 'react-loading'

const useStyles = makeStyles(
    {
        wrapper: {
            display: "flex"
        },
        loading: {
            marginRight: 8,
        }
    }
)

function TextLoader({ texts, loop = true, timeout = 3000 }) {
    const classes = useStyles()
    const [currentIndex, setCurrentIndex] = useState(0)

    const selectNextText = useCallback(
        () => {
            if (loop) {
                setCurrentIndex(i => (i + 1) % texts.length)
            } else {
                setCurrentIndex(i => texts.length - 1 > i ? i + 1 : i)
            }
        },
        [texts, loop]
    )

    useEffect(
        () => {
            setCurrentIndex(0)
            const handler = window.setInterval(
                selectNextText,
                timeout
            )
            return () => window.clearInterval(handler)
        },
        [texts, timeout]
    )

    return (
        <div className={classes.wrapper}>
            <ReactLoading type="bars" color='#2196f3' height={24} width={24} className={classes.loading} />
            {texts[currentIndex]}
        </div>
    )
}

export default TextLoader