import { makeStyles } from '@material-ui/core';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import clsx from 'classnames';
import React from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles({
    noCursor: {
        cursor: "default",
    }
})

export default function AssessmentableIcon({ assessmentable, withBorder, className, isTopic, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard'])

    return (
        <div
            {...props}
            className={withBorder ?
                clsx("MuiButtonBase-root MuiButton-root MuiButton-outlined", classes.noCursor, className)
                :
                className
            }
            title={assessmentable ?
                t("Learners can take test as it includes questions.")
                :
                t("Learners cannot take test as it lacks questions.")
            }
        >
            <FindInPageIcon htmlColor={assessmentable ? "#40E0D0" : "#DE3163"} />
        </div>
    )
}