import { makeStyles } from '@material-ui/core';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import AssessmentableIcon from './Assessmentable';
import LearnableIcon from './Learnable';
import AddableIcon from './Addable';

const useStyles = makeStyles(theme => ({
    rootWithBorder: {
        border: "1px solid #EEE",
        borderRadius: 3,
        display: "inline-flex",
        padding: `2px 4px`,
        height: 36,
        verticalAlign: "middle",
    },
    rootWithoutBorder: {
        display: "inline-flex",
        padding: `2px 4px`,
        height: 34,
        verticalAlign: "middle",
    }
}))

function Icons(rawProps) {
    const {
        hideLearnable = false,
        hideAssessmentable = false,
        hideAddable = false,
        learnable = false,
        addable,
        assessmentable = false,
        withBorder = false,
        isTopic = false,
        className,
        learnableClassName,
        assessmentableClassName,
        ...props
    } = rawProps;
    const classes = useStyles();

    return ((!hideLearnable || !hideAssessmentable) &&
        <div
            className={clsx({
                [classes.rootWithBorder]: withBorder,
                [classes.rootWithoutBorder]: !withBorder,
                className,
            })}
        >
            {!hideLearnable && <LearnableIcon learnable={learnable} className={learnableClassName} withBorder={false} isTopic={isTopic} {...props} />}
            {!isTopic && !hideAddable && <AddableIcon addable={addable} className={learnableClassName} withBorder={false} {...props} />}
            {!hideAssessmentable && <AssessmentableIcon assessmentable={assessmentable} isTopic={isTopic} className={assessmentableClassName} withBorder={false} {...props} />}
        </div>
    )
}

Icons.propTypes = {
    learnable: PropTypes.bool,
    assessmentable: PropTypes.bool,
    className: PropTypes.string,
    hideLearnable: PropTypes.bool,
    hideAssessmentable: PropTypes.bool,
    withBorder: PropTypes.bool,
    isTopic: PropTypes.bool,
    learnableClassName: PropTypes.string,
    assessmentableClassName: PropTypes.string,
}

export default Icons

