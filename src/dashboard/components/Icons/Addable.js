import { makeStyles } from '@material-ui/core';
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded';
import clsx from 'classnames';
import React from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles({
    noCursor: {
        cursor: "default",
    },
})

export default function AddableIcon({ addable, withBorder, className, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    return (
        <div
            {...props}
            className={withBorder ?
                clsx("MuiButtonBase-root MuiButton-root MuiButton-outlined", classes.noCursor, className)
                :
                className
            }
            title={addable ?
                t('This course can be added to learner profiles.')
                :
                t('This course can not be added to learner profiles.')
            }
        >
            <AddBoxRoundedIcon htmlColor={addable ? "#40E0D0" : "#DE3163"} />
        </div>
    )
}