import { makeStyles } from '@material-ui/core';
import CastForEducation from '@material-ui/icons/CastForEducation';
import clsx from 'classnames';
import React from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles({
    noCursor: {
        cursor: "default",
    },
})

export default function LearnableIcon({ learnable, withBorder, className, isTopic, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    return (
        <div
            {...props}
            className={withBorder ?
                clsx("MuiButtonBase-root MuiButton-root MuiButton-outlined", classes.noCursor, className)
                :
                className
            }
            title={learnable ?
                isTopic ?
                    t('Learners can learn this topic as it includes educational material.')
                :
                    t('All topics in this course are learnable.')
                :
                isTopic ?
                    t('Learners cannot learn this topic as it lacks educational material.')
                :
                    t('Some topics in this course are not learnable.')
            }
        >
            <CastForEducation htmlColor={learnable ? "#40E0D0" : "#DE3163"} />
        </div>
    )
}