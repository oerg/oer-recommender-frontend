import { Box, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import RemoveIcon from '@material-ui/icons/Remove'
import React, { useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { Container, Draggable } from 'react-smooth-dnd'
import DragHandle from './DragHandle'


function ListDnd({ items, dragClass, onClick, itemTitle, loading = false, formatLabel = e => e, emptyText, onMove, onRemove, dndProps, loadingLastItem, ...props }) {
    const { t } = useTranslation(['dashboard'])

    const handleRemove = useCallback(
        (index, item) => () => {
            onRemove(index, item)
        },
        [onRemove]
    )

    const handleDrop = useCallback(
        ({ removedIndex, addedIndex }) => {
            if (removedIndex !== addedIndex)
                onMove && onMove(removedIndex, addedIndex)
        },
        []
    )

    const handleClick = useCallback(
        (index, item) => () => {
            onClick(index, item)
        },
        [onClick]
    )

    return loadingLastItem && (!items || items.length === 0) ?
        <Box p={2} style={{color: "red"}}>
            <em>{t("Loading new item...")}</em>
        </Box>
        :
        (emptyText && (!items || items.length === 0) ?
            <Box p={2}>
                <em>{emptyText}</em>
            </Box>
        :
        <List {...props}>
            <Container 
                lockAxis='y' 
                {...dndProps} 
                dragHandleSelector=".__ListDnd__drag-handle" 
                onDrop={handleDrop}
                dragClass={dragClass}
            >
                {items.map(
                    (item, index) => <Draggable key={index}>
                        <ListItem disabled={loading} button={Boolean(onClick)} onClick={handleClick(index, item)} title={itemTitle}>
                            <ListItemText
                                primary={formatLabel(item)}
                            />
                            <ListItemSecondaryAction>
                                <DragHandle
                                    dragClassName="__ListDnd__drag-handle"
                                    title={t("Move")}
                                    disabled={loading}
                                />
                                <IconButton onClick={handleRemove(index, item)} size="small" disabled={loading} title={t("Remove")}>
                                    <RemoveIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </Draggable>
                )}
            </Container>
            { loadingLastItem &&
                <ListItem>
                    <ListItemText style={{color: "red"}} primary={t("Loading new item...")} />
                </ListItem>
            }
        </List>
    )
}

export default ListDnd