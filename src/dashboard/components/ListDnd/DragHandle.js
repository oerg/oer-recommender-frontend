import { IconButton, makeStyles } from '@material-ui/core';
import DragHandleIcon from "@material-ui/icons/DragHandle";
import clsx from 'classnames';
import React from 'react';

const useStyles = makeStyles(
    {
        dragHandle: {
            cursor: "drag"
        },
    }
)
function DragHandle({ disabled, dragClassName, className, ...props }) {
    const classes = useStyles()

    return (
        <IconButton
            size="small"
            disableRipple
            {...props}
            className={clsx({ [classes.dragHandle]: !disabled, [dragClassName]: !disabled, className })}
            disabled={disabled}
        >
            <DragHandleIcon />
        </IconButton>
    )
}

export default DragHandle