import { Breadcrumbs, makeStyles } from '@material-ui/core';
import AppsIcon from '@material-ui/icons/Apps';
// import OergIcon from '@material-ui/icons/CastForEducation';
import CastForEducationIcon from '@material-ui/icons/CastForEducation';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';

const useStyles = makeStyles((theme) => ({
    discoveryNav: {
        paddingLeft: theme.spacing(2),
        marginTop: theme.spacing(2),
    },
    linkIcon: {
        // display: 'flex',
        marginTop: -10,
        width: 20,
        height: 20,
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(-0.5)
    },
    icon: {
        width: 20,
        height: 20,
        marginRight: theme.spacing(1),
        marginTop: theme.spacing(1),
    },
    addIcon: {
        width: 17,
        height: 17,
        color: "gray",
        paddingBottom: theme.spacing(0.7),
    },
}));

function createHistory(job, skill, topic, oerg, oer) {
    if (job)
        return {
            job,
            skill: null,
            topic: null,
            oerg: null,
            oer: null,
        }
    if (skill)
        return {
            skill,
            topic: null,
            oerg: null,
            oer: null,
        }
    if (topic)
        return {
            topic,
            oerg: null,
            oer: null,
        }
    if (oerg)
        return {
            oerg,
            oer: null,
        }
    if (oer)
        return {
            oer
        }
    return {};
}

function DiscoveryNav({ job, skill, topic, oerg, oer, navHistory, onNav }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    useEffect(
        () => {
            onNav && onNav(createHistory(job, skill, topic, oerg, oer))
        },
        [job, skill, topic, oerg, oer, onNav]
    )

    return (navHistory &&
        <Breadcrumbs className={classes.discoveryNav}>
            {navHistory['job'] &&
                <Link to={`/${language}/dashboard/discovery/jobs/${navHistory['job']['id']}`} title={t("Journey")}>
                    <WorkOutlineIcon className={classes.linkIcon} />
                    <em>{navHistory['job']['title']}</em>
                </Link>
            }
            {navHistory['skill'] &&
                <Link to={`/${language}/dashboard/discovery/skills/${navHistory['skill']['id']}`} title={t("Skill")}>
                    <AppsIcon className={classes.linkIcon} />
                    <em>{navHistory['skill']['title']}</em>
                </Link>
            }
            {navHistory['topic'] &&
                <Link to={`/${language}/dashboard/discovery/topics/${navHistory['topic']['id']}`} title={t("Topic")}>
                    <ChatBubbleOutlineIcon className={classes.linkIcon} />
                    <em>{navHistory['topic']['title']}</em>
                </Link>
            }
            {navHistory['oerg'] &&
                <Link to={`/${language}/dashboard/discovery/oer-groups/${navHistory['oerg']['id']}`}>
                    <CastForEducationIcon className={classes.linkIcon} />
                    <em>{navHistory['oerg']['title']}</em>
                </Link>
            }
            {navHistory['oer'] &&
                <Link to={`/${language}/dashboard/discovery/oers/${navHistory['oer']['id']}`}>
                    <em>{navHistory['oer']['title']}</em>
                </Link>
            }
        </Breadcrumbs>
    )
}

export default DiscoveryNav
