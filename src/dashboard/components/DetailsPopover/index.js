import { Box, Button, Fade, List, ListItem, ListItemText, makeStyles, Paper, Popper, TextField, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    hasMargin: {
        marginBottom: theme.spacing(1),
    },
}))

function DetailsPopover({ description, author, items, emptyText, closeDelay, open: propOpen, anchorEl, ...props }) {
    const classes = useStyles();
    const [, setClosingTimeout] = useState(null);
    const [isMouseOn, setIsMouseOn] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);
    const open = useMemo(
        () => propOpen || isMouseOn,
        [ propOpen, isMouseOn ]
    );

    const handleClick = useCallback(
        (e) => {
            var index = Number(e.currentTarget.id);
            window.open(
                items[index].url,
                '_blank'
            )
        },
        [ items ]
    );

    const handleClose = useCallback(
        () => {
            setIsMouseOn(false);
            if (props.onClose)
                props.onClose();
        },
        [ props.onClose ]
    )

    const startCloseTimer = useCallback(
        () => {
            setClosingTimeout(
                window.setTimeout(
                    handleClose,
                    closeDelay
                )
            );
        },
        [ closeDelay, handleClose ]
    );

    // useEffect(
    //     () => {
    //         if (anchorEl) startCloseTimer();
    //     },
    //     [ anchorEl ]
    // );

    const killCloseTimer = useCallback(
        () => {
            setIsMouseOn(true);
            setClosingTimeout(
                timeout => {
                    if (timeout)
                        window.clearTimeout(timeout);
                    return null;
                }
            );
        },
        []
    )

    return (
        <Popper
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
            }}
            style={{
                zIndex: 1001
            }}
            anchorEl={anchorEl}
            open={open}
            transition
            {...props}>
            {({ TransitionProps }) => (
                <Fade {...TransitionProps} timeout={300}>
                    <Paper
                        style={{
                            minWidth: 350,
                            padding: 16,
                            maxWidth: 500,
                            maxHeight: 300,
                            overflow: "auto",
                        }}
                        onMouseLeave={startCloseTimer}
                        onMouseEnter={killCloseTimer}>
                        <Box
                            bgcolor="#CCCCCC"
                            mb={2}
                            p={1}>
                            <Typography variant="h5">
                                {t("Info about this package")}
                            </Typography>
                        </Box>
                        {description && description !== 'nan' &&
                            <TextField
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={description}
                                label={t("Description")}
                                fullWidth
                                multiline
                                className={classes.hasMargin}
                            />
                        }
                        {author && author !== 'nan' &&
                            <TextField
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }}
                                value={author}
                                label={t("Authors")}
                                fullWidth
                                className={classes.hasMargin}
                            />
                        }
                        { items && items.length > 0 ? 
                        <div className="MuiFormControl-root MuiTextField-root MuiFormControl-fullWidth">
                            <Typography>
                                {t("Materials")}
                            </Typography>
                            <List>
                                {items.map(
                                    (item, index) => (
                                        <ListItem 
                                            key={index}
                                            id={index}
                                            button
                                            onClick={handleClick}
                                        >
                                            <ListItemText
                                                primary={item.title}
                                            />
                                        </ListItem>
                                    )
                                )}
                            </List>
                            <Button fullWidth startIcon={<CloseIcon />} onClick={handleClose}>Close</Button>
                        </div>
                        :
                            <em>{emptyText}</em>
                        }
                    </Paper>
                </Fade>
            )}
        </Popper>
    )
}

DetailsPopover.propTypes = {
    emptyText: PropTypes.string,
    items: PropTypes.array.isRequired,
    description: PropTypes.string,
    author: PropTypes.string,
    closeDelay: PropTypes.number,
}

DetailsPopover.defaultProps = {
    emptyText: 'No items.',
    closeDelay: 300,
}

export default DetailsPopover

