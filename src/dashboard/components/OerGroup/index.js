import { Box, Button, Card, CardActions, CardContent, CardHeader, FormControl, InputLabel, LinearProgress, makeStyles, Paper, Popover, Select, TextField, Typography } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import OpenInNewTwoToneIcon from '@material-ui/icons/OpenInNewTwoTone';
import RefreshIcon from '@material-ui/icons/Refresh';
import ReportIcon from '@material-ui/icons/Report';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import Pagination from '@material-ui/lab/Pagination';
import Axios from 'axios';
import clsx from 'classnames';
import PropTypes from "prop-types";
import React, { Fragment, useCallback, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MyButton from '../CustomButton/Button';
import MyRating from '../MyRating';
import ReportDialog from './ReportDialog';
import { SatisfactionIcons } from './utils';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        textAlign: 'center',
    },
    carouselRoot: ({ maxWidth }) => ({
        margin: 'auto',
        maxWidth: maxWidth ? `${maxWidth}px` : '100%',
        textAlign: 'left'
    }),
    actions: {
        margin: 'auto',
        width: '85%',
        marginBottom: 12
    },
    carousel: {
        width: '100%',
    },
    header: {
        minHeight: '100px',
        paddingBottom: 0,
        position: "relative"
    },
    myNav: {
        position: 'relative',
    },
    navWrapper: {
        position: 'absolute',
        backgroundColor: "transparent",
        top: "-50px",
        '& $button': {
            opacity: 0.6,
        },
        '&:hover': {
            '&button': {
                filter: "brightness(120%)",
                opacity: 1,
            }
        }
    },
    next: {
        right: 0,
    },
    back: {
        left: 0,
    },
    oerTitle: {
        '& a': {
            color: theme.palette.text.primary,
        },
        margin: "12px 2px 4px 2px"
    },
    oerActions: {
        position: "absolute",
        right: 15,
        top: 15
    },
    tooltip: {
        fontSize: 14
    },
    npButtons: {
        width: 40,
        height: "100%",
    },
    descriptionPaper: {
        height: 75,
        overflowY: "auto",
        backgroundColor: "inherit",
        padding: "8px 16px",
        margin: "0 72px",
    },
}));

function OerChangeForm({ group, skillId, topicId, onChange }) {
    const { t } = useTranslation(['dashboard']);
    const [isLoading, setIsLoading] = useState(false);
    const [feedback, setFeedback] = useState('');

    const changeFeedbackTextRef = useRef(null);

    const updateChangeFeedback = useCallback(text => {
        setFeedback(text);
        if (changeFeedbackTextRef.current)
            changeFeedbackTextRef.current.focus();
    }, []);

    const handleChangeReason = useCallback(e => {
        updateChangeFeedback(e.target.value);
    }, [updateChangeFeedback]);

    const handleChange = useCallback(() => {
        setIsLoading(true);
        const data = {
            oer_id: group.id,
            skill_id: skillId,
            comment: feedback,
            status: "CH",
            rate: 0,
            is_exam: false,
        }
        if (topicId) {
            data.topic_id = topicId;
        }
        Axios.put('/members/change-oer-status/', data)
            .then(res => {
                onChange(res.data.done);
                setIsLoading(false);
            }).catch(err => {
                setIsLoading(false);
            })
    }, [group.id, feedback, onChange, skillId, topicId]);

    return (
        <FormControl component="fieldset" disabled={isLoading} fullWidth>
            <Box my={1}>
                <FormControl fullWidth>
                    <InputLabel htmlFor='reason-select'>{t("Reason")}</InputLabel>
                    <Select name="changeReason" defaultValue='' onChange={handleChangeReason} inputProps={{ id: 'reason-select' }} native>
                        <option value='' style={{ display: 'none' }}></option>
                        <option value={t("I don't like this type of resource.")}>{t("I don't like the type of resource.")}</option>
                        <option value={t("I don't like the instructor.")}>{t("I don't like the instructor.")}</option>
                        <option value={t("I don't like the teaching method.")}>{t("I don't like the teaching method.")}</option>
                        <option value={t("The resource quality was low.")}>{t("The resource quality was low.")}</option>
                        <option value=" ">{t("Other reasons...")}</option>
                    </Select>
                </FormControl>
            </Box>
            <Box my={1}>
                <TextField variant="outlined" label="Feedback" multiline value={feedback} disabled={isLoading}
                    onChange={e => setFeedback(e.target.value)} fullWidth rows={2} rowsMax={3}
                    inputProps={{ ref: changeFeedbackTextRef }} />
            </Box>
            <Box my={1}>
                {isLoading && <LinearProgress />}
                <MyButton variant="contained" color="rose" onClick={handleChange} fullWidth disabled={isLoading}>{t("Send")}</MyButton>
            </Box>
        </FormControl>
    )
}


function OerGroup({ skill, topic, group, onDone, onChange, readOnly, topicOnly, status, withAdd, onAdd, disableAdd, noChange,
    maxWidth, className, noStatus, ...props }) {
    const classes = useStyles({ maxWidth });
    const { t } = useTranslation(['dashboard']);
    const [showForm, setShowForm] = useState('');
    const [showReport, setShowReport] = useState(false);
    const [selectedOerPage, setSelectedOerPage] = useState(1)
    const [showNext, setShowNext] = useState(false)
    const [anchorEl, setAnchorEl] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [rating, setRating] = useState(0);
    const oers = useMemo(
        () => group.oergroupoer_set.map(ogos => ogos.oer),
        [group]
    );

    const selectedOerObject = useMemo(
        () => oers[selectedOerPage - 1],
        [oers, selectedOerPage]
    );

    const hasDescription = useMemo(() => (
        oers[selectedOerPage - 1] ? !!oers[selectedOerPage - 1].description : false
    ), [selectedOerPage, oers]);


    const handleOerPage = useCallback((event, value) => {
        setSelectedOerPage(value);
    }, []);

    const handleOpenOer = useCallback((oer) => (e) => {
        if (e) e.preventDefault();
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "GO",
            related_data: {
                url: oer.url,
                oer: oer.id,
            }
        })
            .then(() => { })
            .catch(() => { })
        window.open(oer.url, '_blank', '', false);
    }, []);

    const handleShowForm = useCallback((form) => (e) => {
        setShowForm(p => {
            if (p === form) {
                setAnchorEl(null);
                return '';
            }
            setAnchorEl(e.currentTarget);
            return form
        });
    }, []);

    const handleCloseForm = useCallback(() => {
        setShowForm('');
        setAnchorEl(null);
    }, []);

    const handleDone = useCallback(
        () => {
            if (selectedOerPage < oers.length) {
                setSelectedOerPage(page => page + 1)
                setShowNext(false)
            }
            else if (selectedOerPage === oers.length)
                setShowNext(true)
        },
        [oers, selectedOerPage],
    )

    const handleSubmitDone = useCallback((exam) => {
        setIsLoading(true);
        const data = {
            oer_id: group.id,
            skill_id: skill.skill.id,
            comment: "",
            rate: rating / 5,
            status: "FI",
            is_exam: exam,
        }

        Axios.put('/members/change-oer-status/', data)
            .then(res => {
                onDone(res.data.done);
                setIsLoading(false);
            }).catch(err => {
                setIsLoading(false);
            })
    }, [group.id, rating, skill, onDone]);

    const subheader = useMemo(
        () => oers.length > 1 &&
            <span title={`Content ${selectedOerPage} out of ${oers.length}`}>Package: <i>{group.title}</i> <small>(Part {selectedOerPage} out of {oers.length})</small></span>
        ,
        [selectedOerPage, oers, group]
    );

    const handleReport = useCallback(
        () => setShowReport(s => !s),
        []
    );

    return (
        <>
            <div className={clsx(classes.root, className)}>
                <ReportDialog open={showReport} onClose={handleReport} group={group} oer={oers[selectedOerPage - 1]} />
                <Box className={classes.carouselRoot} elevation={2} display="flex" alignItems="center">
                    {group && <>
                        <Card {...props} style={{
                            ...props.style,
                            flexBasis: "100%"
                        }}>
                            <div className={classes.carousel}>
                                {selectedOerObject &&
                                    <Fragment>
                                        <CardHeader
                                            title={
                                                <a href={selectedOerObject.url} onClick={handleOpenOer(selectedOerObject)}>
                                                    {selectedOerObject.title}
                                                </a>
                                            }
                                            action={!readOnly &&
                                                <>
                                                    <Button
                                                        title={t("Report this content")}
                                                        onClick={handleReport}
                                                        style={{
                                                            color: "#FF0000",
                                                            minWidth: 0,
                                                            padding: "0 0"
                                                        }}
                                                    >
                                                        <ReportIcon />
                                                    </Button>
                                                    <Button
                                                        title={t("I do not ike this content, give me another one.")}
                                                        onClick={handleShowForm('change')}
                                                        style={{
                                                            color: "#00525f",
                                                            minWidth: 0,
                                                            padding: "0 0",
                                                        }}
                                                    >
                                                        <RefreshIcon />
                                                    </Button>
                                                </>
                                            }
                                            subheader={subheader}
                                            className={clsx(classes.header)}
                                            classes={{
                                                title: classes.oerTitle,
                                                action: classes.oerActions
                                            }}
                                            titleTypographyProps={{
                                                style: {
                                                    textAlign: "center"
                                                }
                                            }}
                                            subheaderTypographyProps={{
                                                style: {
                                                    textAlign: "center"
                                                }
                                            }}
                                        />
                                        {hasDescription &&
                                            <CardContent>
                                                <Paper className={classes.descriptionPaper}>
                                                    <Typography variant="body2" color="textSecondary" className={classes.description}>
                                                        {selectedOerObject.description}
                                                    </Typography>
                                                </Paper>
                                            </CardContent>
                                        }
                                        <div style={{
                                            textAlign: "center"
                                        }}>
                                            <a href={selectedOerObject.url} onClick={handleOpenOer(selectedOerObject)}>
                                                <Button
                                                    color="primary"
                                                    variant="outlined"
                                                    title={t("Open the conent in new tab")}
                                                    startIcon={<OpenInNewTwoToneIcon />}
                                                >
                                                    {t("View")}
                                                </Button>
                                            </a>
                                            {!readOnly &&
                                                <Button
                                                    color="primary"
                                                    variant="outlined"
                                                    title={t("I am done with the content")}
                                                    startIcon={<DoneIcon />}
                                                    style={{
                                                        marginLeft: 8
                                                    }}
                                                    onClick={handleDone}
                                                    disabled={showNext}
                                                >
                                                    {t("Done")}
                                                </Button>
                                            }
                                        </div>
                                    </Fragment>
                                }
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: 'center',
                                        marginTop: 24
                                    }}
                                >
                                    {oers.length > 1 &&
                                        <Pagination
                                            hidePrevButton
                                            count={oers.length}
                                            variant="outlined"
                                            page={selectedOerPage}
                                            onChange={handleOerPage}
                                        />
                                    }
                                </div>
                            </div>
                            {!readOnly && (<>
                                <Popover open={showForm === 'change'} onClose={handleCloseForm} anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'center',
                                    }} transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'center',
                                    }}>
                                    <Box p={2}>
                                        <OerChangeForm group={group} skillId={skill ? skill.skill.id : null} onChange={onChange} topicOnly={topicOnly} topicId={topic ? topic.id : null} />
                                    </Box>
                                </Popover>
                            </>)}
                            <CardActions disableSpacing className={classes.actions}>
                                {!readOnly && !noChange && showNext &&
                                    <div style={{
                                        margin: "auto"
                                    }}
                                    >
                                        <Box
                                            component="fieldset"
                                            m='auto'
                                            borderColor="transparent"
                                            style={{
                                                display: 'flex',
                                                justifyContent: "center",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Typography
                                                // variant='caption'
                                                style={{
                                                    marginRight: 6
                                                }}
                                            >
                                                {t("How did you like the content?")}
                                            </Typography>
                                            <MyRating value={rating} onChange={(e, v) => setRating(v)} getLabelText={(v) => <SatisfactionIcons value={v} label />}
                                                size="large" name={`rate_${group.id}`} />
                                        </Box>
                                        <div
                                            style={{
                                                display: "flex"
                                            }}
                                        >
                                            <Button
                                                color="secondary"
                                                variant="outlined"
                                                style={{
                                                    backgroundColor: "white",
                                                    height: "100%",
                                                    margin: 4
                                                }}
                                                onClick={() => handleSubmitDone(false)}
                                                startIcon={<RefreshIcon />}
                                                title={t("Request for more content in case you need.")}
                                                disabled={isLoading}
                                            >
                                                {t("I need another content.")}
                                            </Button>
                                            <Button
                                                color="primary"
                                                variant="outlined"
                                                style={{
                                                    backgroundColor: "white",
                                                    height: "100%",
                                                    margin: 4
                                                }}
                                                onClick={() => handleSubmitDone(true)}
                                                startIcon={<SkipNextIcon />}
                                                title={t("Go to the next topic if you have learnt this one")}
                                                disabled={isLoading}
                                            >
                                                {t("I have learnt the topic.")}
                                            </Button>
                                        </div>
                                    </div>
                                }
                            </CardActions>
                        </Card>
                    </>}
                </Box>
            </div>
        </>
    )
}

OerGroup.propTypes = {
    group: PropTypes.shape({
        id: PropTypes.number.isRequired,
        oers: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            note: PropTypes.string,
            url: PropTypes.string.isRequired,
            format_types: PropTypes.shape({
                title: PropTypes.string.isRequired,
            })
        }))
    }),
    skillId: PropTypes.number,
    readOnly: PropTypes.bool,
    withAdd: PropTypes.bool,
    disableAdd: PropTypes.bool,
    onAdd: PropTypes.func,
    noChange: PropTypes.bool,
    maxWidth: PropTypes.number,
    noStatus: PropTypes.bool,
}

OerGroup.defaultProps = {
    readOnly: false,
    withAdd: false,
    disableAdd: false,
    noChange: false,
    noStatus: false,
}

export default OerGroup
