import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import axios from "axios";
import React, { useMemo } from 'react';
import { useTranslation } from "react-i18next";
import { requestAddHandlers } from "../../views/helpers";

export function sendOerReport(oer, text, report_type, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post(
            '/oers/reports/',
            {
                oer,
                text,
                report_type
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}

export function SatisfactionIcons({ label, value }) {
    const { t } = useTranslation(['dashboard']);
    const data = useMemo(
        () => ({
            1: {
                icon: <SentimentVeryDissatisfiedIcon />,
                label: t('Very Dissatisfied'),
            },
            2: {
                icon: <SentimentDissatisfiedIcon />,
                label: t('Dissatisfied'),
            },
            3: {
                icon: <SentimentSatisfiedIcon />,
                label: t('Neutral'),
            },
            4: {
                icon: <SentimentSatisfiedAltIcon />,
                label: t('Satisfied'),
            },
            5: {
                icon: <SentimentVerySatisfiedIcon />,
                label: t('Very Satisfied'),
            },
        }),
        [t]
    );
    return label ?
        data[value].label
        :
        data[value].icon
};