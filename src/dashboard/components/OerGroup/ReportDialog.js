import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, InputLabel, makeStyles, MenuItem, Select, TextField } from '@material-ui/core'
import { withSnackbar } from 'notistack';
import React, { useCallback, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { addDeleteSuggestion } from '../../views/Discovery/Topics/utils';
import MyButton from '../CustomButton/Button';
import { sendOerReport } from './utils';

const useStyles = makeStyles(
    theme => ({
    field: {
        marginBottom: theme.spacing(1),
    },
}))

function ReportDialog(rawProps) {
    const {
        oer,
        group,
        enqueueSnackbar,
        ...props
    } = rawProps;
    const classes = useStyles();
    const [reason, setReason] = useState()
    const [loading, setLoading] = useState(false)
    const text = useRef(null)
    const { t } = useTranslation(['dashboard']);
    const handleSend = useCallback(
        () => {
            setLoading(true)            
            if(reason === "IR") {
                group.topics.forEach(topic => {
                    addDeleteSuggestion(topic.id,
                        group.id
                    )                    
                });
                enqueueSnackbar(
                    t("Thanks, your feedback will be checked by our staff. To get a new content, please press the refresh icon"),
                    {
                        variant: "info"
                    }
                )
                props.onClose && props.onClose()                
            }
            else {
                sendOerReport(oer.id,
                    text.current.value,
                    reason,
                    () => {
                        enqueueSnackbar(
                            t("Thanks, your feedback will be checked by our staff. To get a new content, please press the refresh icon."),
                            {
                                variant: "info"
                            }
                        );
                        props.onClose && props.onClose()
                    }
                )
            }
        },
        [ props.onClose, reason, t, enqueueSnackbar ]
    )

    const handleChange = useCallback(
        (e) => {
            setReason(e.target.value)
        },
        []
    )

    return (
        <Dialog {...props}>
            <DialogTitle>{t("Report Content")}</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>{t("Report")}: <i>{oer.title}</i><br />
                    {t("Please select a reason and describe the situation:")}
                </DialogContentText>

                <FormControl fullWidth variant='outlined' className={classes.field}>
                    <InputLabel id="reason-label" variant='outlined'>{t("Reason")}</InputLabel>
                    <Select variant='outlined' labelId='reason-label' label={t("Reason")} value={reason} onChange={handleChange}>
                        <MenuItem value="IL">{t("Illegal Content (Copyright violation, terms violation, etc.)")}</MenuItem>
                        <MenuItem value="NA">{t("Item is not available")}</MenuItem>
                        <MenuItem value="IR">{t("This content is not related to the topics")}</MenuItem>
                    </Select>
                </FormControl>
                <TextField
                    className={classes.field}
                    fullWidth
                    multiline
                    minRows={3} maxRows={3}
                    variant='outlined'
                    label={t("Description")}
                    inputRef={text}
                />
            </DialogContent>
            <DialogActions>
                <MyButton color="danger" onClick={props.onClose}>{t("Cancel")}</MyButton>
                <MyButton disabled={loading} color="rose" onClick={handleSend}>{t("Send")}</MyButton>
            </DialogActions>
        </Dialog>
    )
}

export default withSnackbar(ReportDialog)
