import { Checkbox, makeStyles, Paper } from '@material-ui/core'
import React, { useCallback } from 'react'
import clsx from "classnames"

const useStyles = makeStyles({
    root: {
        cursor: "pointer",
        position: "relative",
    },
    checkbox: {
        position: 'absolute',
        top: 0,
        right: 0
    }
})

function InteractivePaper({ checked, children, className, onChange, ...props }) {

    const classes = useStyles()

    const handleClick = useCallback(
        () => {
            onChange(!checked)
        },
        [checked],
    )

    return (
        <Paper {...props} onClick={handleClick} className={clsx(classes.root, className)}>
            <div className={classes.checkbox}>
                <Checkbox size='small' onClick={handleClick} checked={checked} />
            </div>
            {children}
        </Paper>
    )
}

export default InteractivePaper