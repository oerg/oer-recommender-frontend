import axios from "axios";
import { requestAddHandlers } from "../../views/helpers";

export function sendFeedback(text, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post('/trackings/bug-reports/', {
            text
        }),
        onLoad,
        onError,
        onEnd
    );
};