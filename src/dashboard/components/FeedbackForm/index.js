import { CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles, Snackbar, TextField } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import PropTypes from 'prop-types';
import React, { useCallback, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../CustomButton/Button';
import { sendFeedback } from './utils';

const useStyles = makeStyles(theme => ({
    feedbackDialog: {
        width: 500,
    }
}));

function FeedbackForm(rawProps) {
    const {
        open,
        onClose,
        ...props
    } = rawProps;
    const classes = useStyles();

    const [loading, setLoading] = useState(false);
    const [showSnack, setShowSnack] = useState(false);
    const textFieldRef = useRef(null);
    const { t } = useTranslation(['dashboard']);

    const handleClose = useCallback(
        () => onClose("close"),
        [onClose]
    );

    const handleCancel = useCallback(
        () => onClose("cancel"),
        [onClose]
    );

    const handleSend = useCallback(
        () => {
            setLoading(true);
            sendFeedback(
                textFieldRef.current.value,
                res => {
                    setLoading(false);
                    setShowSnack(true);
                    onClose("sent");
                }
            );
        },
        [onClose]
    );

    const handleCloseSnack = useCallback(
        () => {
            setShowSnack(false)
        },
        []
    )

    return (<>
        <Dialog
            open={open}
            maxWidth="sm"
            onClose={handleClose}
            classes={{
                paper: classes.feedbackDialog
            }}
            {...props}
        >
            <DialogTitle>{t("Send Us Feedback")}</DialogTitle>
            <DialogContent style={{ width: "100%" }}>
                {loading ?
                    <div>
                        <CircularProgress />
                    </div>
                    :
                    <TextField
                        inputRef={textFieldRef}
                        autoFocus
                        multiline
                        label={t("Your Opinion")}
                        fullWidth
                        placeholder={t("Tell us about our product...")}
                        rows={3}
                        margin="dense"
                        required
                        variant="outlined"
                    />
                }
            </DialogContent>
            <DialogActions>
                <Button color="danger" onClick={handleCancel}>{t("Cancel")}</Button>
                <Button color="success" onClick={handleSend}>{t("Send")}</Button>
            </DialogActions>
        </Dialog>
        <Snackbar
            open={showSnack}
            onClose={handleCloseSnack}
            autoHideDuration={4000}
        >
            <Alert
                severity="success"
                onClose={handleCloseSnack}
            >
                {t("We've received your feedback. Thanks for your contribution.")}
            </Alert>
        </Snackbar>
    </>)
}

FeedbackForm.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default FeedbackForm

