import { Badge, Button, Divider, Menu, MenuItem, Portal } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';

function ParticipateButton(props) {
    const { 
        children = <PeopleIcon />,
        showMentoringBadge = false,
        showLearnabilityBadge = false,
        showMentoring = true,
        showParticipation = true,
        userData,
        ...restProps 
    } = props;

    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleOpen = useCallback(
        e => {
            e.stopPropagation();
            setAnchorEl(e.currentTarget);
        },
        []
    );

    const handleClose = useCallback(
        () => setAnchorEl(null),
        []
    );


    return (<>
        <Button
            variant="outlined"
            color="primary"
            title={t("Mentoring and Content Curation")}
            {...restProps}
            onClick={handleOpen}
        >
            { showLearnabilityBadge || showMentoringBadge ? 
                <Badge color="secondary" variant="dot">
                    {children}
                </Badge>
            :
                children
            }
        </Button>
        <Portal>
            <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                onClick={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                {showParticipation && [
                    <MenuItem 
                        key={0}
                        component={Link}
                        to={`/${language}/dashboard/discovery`}
                    >
                        {t("Participate in Content Curation")}
                    </MenuItem>,
                    <MenuItem
                        key={1}
                        component={Link}
                        to={`/${language}/dashboard/my-area`}
                        onClick={handleClose}                
                    >
                        { showLearnabilityBadge ?
                            <Badge color="secondary" variant="dot">
                                {t("Show Areas Lacking Content")}
                            </Badge>
                            :
                                t("Show Areas Lacking Content")
                        }
                    </MenuItem>
                ]}
                { showMentoring && showParticipation && <Divider /> }
                { showMentoring &&
                    <MenuItem
                        component={Link}
                        to={`/${language}/dashboard/mentoring-requests`}
                        onClick={handleClose}
                    >
                        { showMentoringBadge ?
                            <Badge color="secondary" variant="dot">
                                {t("Mentoring Requests")}
                            </Badge>
                            :
                                t("Mentoring Requests")
                        }
                    </MenuItem>
                }
            </Menu>
        </Portal>
    </>)
}

export default ParticipateButton
