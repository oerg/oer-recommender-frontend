import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import styles from "./styles";

const useStyles = makeStyles(styles);

export default function Quote(props) {
  const classes = useStyles();
  const { text, author, className } = props;
  return (
    <blockquote className={clsx(classes.defaultFontStyle, classes.quote, className)}>
      <p className={classes.quoteText}>{text}</p>
      <small className={classes.quoteAuthor}>{author}</small>
    </blockquote>
  );
}

Quote.propTypes = {
  text: PropTypes.node,
  author: PropTypes.node,
  className: PropTypes.string,
};

Quote.defaultProps = {
  className: '',
}
