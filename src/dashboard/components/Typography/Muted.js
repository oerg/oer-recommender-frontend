import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import styles from "./styles";

const useStyles = makeStyles(styles);

export default function Muted(props) {
  const classes = useStyles();
  const { children, className, ...restProps } = props;
  return (
    <div className={clsx(classes.defaultFontStyle, classes.mutedText, className)} {...restProps}>
      {children}
    </div>
  );
}

Muted.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

Muted.defaultProps = {
  className: '',
}
