import { Collapse, IconButton, makeStyles } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import clsx from 'classnames';
import React, { useCallback, useEffect, useMemo, useState } from 'react';


const useStyles = makeStyles(
    theme => ({
        root: {
            borderRadius: theme.spacing(0.5),
        },
        title: {
            display: "flex",
            height: 55,
            alignItems: "center",
            padding: `${theme.spacing(0.5)}px ${theme.spacing(1)}px`,
            borderTopLeftRadius: theme.spacing(0.5),
            borderTopRightRadius: theme.spacing(0.5),
        },
        titleContent: {
            flex: 1,
            display: "inline-block",
            verticalAlign: "middle",
            cursor: "pointer",
        },
        titleCollapsed: {
            borderBottomLeftRadius: theme.spacing(0.5),
            borderBottomRightRadius: theme.spacing(0.5),
        }
    })
)

function ToggleDiv({ children, title, titleClassName, className, open: propOpen, onChange, defaultOpen, alwaysOpen, help, ...rawProps }) {
    const {
        component: Component = "div",
        ...props
    } = rawProps;
    const classes = useStyles();
    const [open, setOpen] = useState(Boolean(defaultOpen || alwaysOpen));

    const isControlled = useMemo(
        () => { // check value existance
            return propOpen !== undefined
        },
        [ propOpen ]
    )

    useEffect( // update
        () => {
            if (isControlled) {
                setOpen(propOpen);
            }
        },
        [ isControlled, propOpen ]
    )

    const handleToggle = useCallback(
        () => {
            setOpen(
                open => {
                    if (onChange)
                        onChange(!open);
                    if (isControlled) {
                        return open;
                    }
                    return !open;
                }
            )
        },
        [ isControlled, onChange ]
    )

    return (
        <Component className={clsx(className, classes.root)} {...props}>
            <div 
                className={
                    clsx(
                        classes.title,
                        {
                            [classes.titleCollapsed]: !open
                        },
                        titleClassName)}
            >
                {!alwaysOpen && <IconButton onClick={handleToggle} size="small">
                    { open ?
                        <KeyboardArrowDownIcon />
                    :
                        <KeyboardArrowRightIcon />
                    }
                </IconButton>}
                <span className={alwaysOpen ? "" : classes.titleContent} onClick={() => !alwaysOpen && handleToggle()}>
                    {title}
                    {help}
                </span>
            </div>
            <Collapse in={open}>
                {children}    
            </Collapse> 
        </Component>
    )
}

ToggleDiv.propTypes = {

}

export default ToggleDiv

