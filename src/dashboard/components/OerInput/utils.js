import axios from "axios";
import { requestAddHandlers } from "../../views/helpers";

export function processLength(rawLength) {
    let hours = 0
    let mins = 0
    let seconds = 0
    if (rawLength) {
        hours = Math.floor(rawLength / 3600);
        mins = Math.floor((rawLength - (hours * 3600)) / 60)
        seconds = rawLength - (hours * 3600) - (mins * 60)
    }

    return new Date(1970, 0, 1, hours, mins, seconds);
}

export function unprocessLength(length) {
    const baseDate = new Date(1970, 0, 1);
    length.setFullYear(1970)
    length.setMonth(0)
    length.setDate(1)
    return (Math.abs(length - baseDate) / 1000);
}

export function getUrlInfo(url, onSuccess, onError, onEnd) {
    requestAddHandlers(
        axios.get('/oers/get-oer-info/?url=' + encodeURIComponent(url)),
        onSuccess,
        onError,
        onEnd
    );

}

export function getOer(id, onLoad, onError, onEnd) { // get topic from the server
    return requestAddHandlers(
        axios.get('/oers/oers/' + id),
        onLoad,
        onError,
        onEnd
    );
}
