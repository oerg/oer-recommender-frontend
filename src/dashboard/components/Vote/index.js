import React from 'react'
import PropTypes from 'prop-types'
import { Button, ButtonGroup, makeStyles, Paper } from '@material-ui/core'
import clsx from 'classnames'

import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'inline-block',
    },
    upButton: {
        color: 'green',
        justifyContent: 'flex-start'
    },
    downButton: {
        color: 'red',
        justifyContent: 'flex-start'
    },
    selectedVote: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        backgroundColor: '#CCC',
        '&:hover': {
            backgroundColor: '#DDD'
        }
    }
}));

function Vote({ up, down, onChange, selfVote, className, disabled, ...props }) {
    const classes = useStyles();
    const { t } = useTranslation([ 'dashboard' ]);

    // calls onUp(new up value, new down value, new value for selfVote)
    const handleUp = useCallback(() => {
        if (selfVote === true) {
            onChange(up - 1, down, null) // no vote
        } else if (selfVote === false) {
            onChange(up + 1, down - 1, true)
        } else {
            onChange(up + 1, down, true)
        }
    }, [up, down, onChange, selfVote])

    const handleDown = useCallback(() => {
        if (selfVote === true) {
            onChange(up - 1, down + 1, false)
        } else if (selfVote === false) {
            onChange(up, down - 1, null) // no vote
        } else {
            onChange(up, down + 1, false)
        }    
    }, [up, down, onChange, selfVote])

    return (
        <Paper elevation={1} className={clsx(classes.root, className)} {...props}>
            <ButtonGroup orientation="vertical" variant="text" fullWidth size="small">
                <Button title={selfVote ? t('Withdraw My Vote') : t('Vote Up')} className={clsx(classes.upButton, {
                    [classes.selectedVote]: selfVote
                })} onClick={handleUp} classes={{
                    disabled: clsx(classes.upButton, {
                        [classes.selectedVote]: selfVote
                    })
                }} startIcon={<KeyboardArrowUpIcon />} disabled={disabled}>
                    {up}
                </Button>
                <Button title={selfVote === false ? t('Withdraw My Vote') : t('Vote Down')} className={clsx(classes.downButton, {
                    [classes.selectedVote]: selfVote === false
                })} onClick={handleDown} classes={{
                    disabled: clsx(classes.upButton, {
                        [classes.selectedVote]: selfVote === false
                    })
                }} startIcon={<KeyboardArrowDownIcon />} disabled={disabled}>
                    {down}
                </Button>
            </ButtonGroup>
        </Paper>
    )
}

Vote.propTypes = {
    up: PropTypes.number.isRequired,
    down: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    selfVote: PropTypes.oneOf([null, true, false]),
    disabled: PropTypes.bool,
    className: PropTypes.string,
}

Vote.defaultProps = {
    selfVote: null,
    disabled: false,
}

export default Vote

