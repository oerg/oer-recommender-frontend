import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ConfirmDialog from '../ConfirmDialog';
import Button from '../CustomButton/Button';
import { sendExpertRequest } from './utils';

const adminEmail = 'edoer@admin.tib.eu'
const secondsToClose = 6;

function ExpertRequestButton(rawProps) {
    const { t } = useTranslation(['dashboard']);

    const {
        label = t("Become an Expert"),
        title = t("Send a request to become an expert here"),
        disabled = false,
        object_type,
        object_id,
        ...props
    } = rawProps;

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [openConfirm, setOpenConfirm] = useState(false)
    const [requestId, setRequestId] = useState(0);
    const [counting, setCounting] = useState(false);
    const [remainingSeconds, setRemainingSeconds] = useState(secondsToClose);

    const startCounting = useCallback(
        () => {
            setCounting(true);
            var counter = window.setInterval(
                () => {
                    setRemainingSeconds(
                        s => {
                            if (s === 1) {
                                window.clearInterval(counter);
                                setCounting(false);
                                return secondsToClose;
                            }
                            return s - 1;
                        }
                    );
                },
                1000
            )
        },
        []
    )

    const handleClick = useCallback(
        () => setOpenConfirm(true),
        []
    )

    const handleConfirm = useCallback(
        () => {
            setLoading(true);
            sendExpertRequest(
                object_type,
                object_id,
                res => {
                    setRequestId(res.request_id);
                    setOpen(true);
                    startCounting();
                },
                null,
                () => {
                    setOpenConfirm(false)
                    setLoading(false)
                }
            );
        },
        [object_id, object_type, startCounting]
    );

    const handleNotConfirm = useCallback(
        () => setOpenConfirm(false),
        []
    )

    const handleClose = useCallback(
        () => setOpen(false),
        []
    );

    return (<>
        <Button
            color="info"
            title={title}
            disabled={loading || disabled}
            {...props}
            onClick={handleClick}
        >
            {label}
        </Button>
        <ConfirmDialog
            title={label}
            onAccept={handleConfirm}
            onReject={handleNotConfirm}
            open={openConfirm}
            loading={loading}
        >
            {t("Requesting expert requires you to send related documents to eDoer. Do you want to proceed?")}
        </ConfirmDialog>
        <Dialog
            open={open}
        >
            <DialogTitle>{t("Request Sent")}</DialogTitle>
            <DialogContent dividers>
                <DialogContentText>
                    <i>{t("Your request has been sent successfully.")}</i><br />
                    {t("In order to proceed please send all your related documents (e.g. Résumé, certificated, etc.) with proper subject to the following email:")}<br />
                    {t("Email")}: <a href={`mailto:${adminEmail}?subject=Request ${requestId} documents`}>{adminEmail}</a><br />
                    {t("Subject")}: <b>Request {requestId}</b>
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button color="success" component="a" href={`mailto:${adminEmail}?subject=Request ${requestId}`}>{t("Send an email now")}</Button>
                <Button color="warning" onClick={handleClose} disabled={counting}>{t("Close")}{counting ? `(${remainingSeconds})` : ""}</Button>
            </DialogActions>
        </Dialog>
    </>)
}

ExpertRequestButton.propTypes = {
    label: PropTypes.string,
    title: PropTypes.string,
    disabled: PropTypes.bool,
    object_type: PropTypes.string.isRequired,
    object_id: PropTypes.number.isRequired,
}

export default ExpertRequestButton
