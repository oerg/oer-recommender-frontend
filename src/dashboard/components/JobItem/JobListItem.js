import { Box, Chip, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, Typography } from '@material-ui/core';
import LaunchIcon from '@material-ui/icons/Launch';
import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import LangIcon from '../LangIcon';
import GoalLogo from '../Logos/Goal';
import { getLocation } from './utils';

const JobListItem = React.forwardRef(
    ({ jobId, jobTitle, jobLang, picture, city, country, company, industry, children, formatLocation,
        listItemTextProps, showLink = false, actions, ...props }, ref) => {
        const { t } = useTranslation(['dashboard']);
        const language = useLanguage();
        const location = useMemo(
            () => formatLocation ?
                formatLocation(getLocation(city, country))
                :
                getLocation(city, country),
            [city, country, formatLocation]
        );

        const subtitle = useMemo(
            () => {
                if (industry || location) {
                    var st = industry;
                    st += industry && location ? ", " : "";
                    st += location;
                    return st;
                }
                return "";
            },
            [industry, location, t]
        );

        const title = useMemo(
            () => company ?
                <>
                    <Typography variant="caption" component={Box} display="block">
                        {company}
                    </Typography>
                    {jobTitle} <LangIcon language={jobLang} small />
                </>
                :
                <>
                    {jobTitle} <LangIcon language={jobLang} small />
                </>
            ,
            [jobTitle, company]
        )

        return (
            <ListItem ref={ref} {...props}>
                <ListItemIcon>
                    <GoalLogo picture={picture} />
                </ListItemIcon>
                <ListItemText
                    primary={title}
                    secondary={subtitle}
                    {...listItemTextProps}
                />
                <ListItemSecondaryAction>
                    {showLink &&
                        <Chip component={Link} to={`/${language}/dashboard/goals/${jobId}`} title={t("Open this item in a new tab")} edge="end"
                            label={t("Open Page")} variant="outlined" target="_blank" clickable icon={<LaunchIcon />} />
                    }
                    {actions && actions.map(
                        (act) => act
                    )}
                </ListItemSecondaryAction>
            </ListItem>
        )
    }
)

JobListItem.propTypes = {
    jobTitle: PropTypes.string.isRequired,
    city: PropTypes.string,
    country: PropTypes.string,
    company: PropTypes.string,
    industry: PropTypes.string.isRequired,
    formatIndustry: PropTypes.func,
    formatTitle: PropTypes.func,
    formatLocation: PropTypes.func,
    showLink: PropTypes.bool,
}

export default JobListItem
