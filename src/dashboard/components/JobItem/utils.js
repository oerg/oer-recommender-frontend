export function getLocation(city, country) {
    if (country || city) {
        var loc = city;
        loc += country && city ? ", " : "";
        loc += country;
        return loc;
    }
    return "";
}