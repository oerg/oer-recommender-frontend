import { Avatar, IconButton, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, withStyles } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import clsx from 'classnames';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';


const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
        float: 'right'
    },
    clickable: {
        cursor: 'pointer',
    },
    root: ({ height }) => ({
        maxHeight: height,
    }),
    hidden: {
        display: 'none',
    },
    comment: {
        display: 'inline-block',
        float: 'right'
    },
    headRow: {
        height: 60,
    }
})

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);


function UserTable({ data, title, height, collapsable, defaultOpen, className, formatTitle,
    headClass, formatLink, showVote, emptyText: propEmptyText, ...props }) {
    const classes = useStyles({ height });
    const [open, setOpen] = useState(defaultOpen);
    const { t } = useTranslation(['dashboard']);

    const emptyText = useMemo(
        () => propEmptyText ? propEmptyText : t("No Users."),
        [t, propEmptyText]
    );

    const handleShow = useCallback(() => {
        setOpen(p => !p)
    }, [])

    return (<>
        <TableContainer component={Paper} className={clsx(classes.root, className)} {...props}>
            <Table stickyHeader>
                <TableHead>
                    <TableRow className={clsx(classes.headRow, {
                        [classes.clickable]: collapsable,
                    })} onClick={handleShow}>
                        <TableCell component="th" className={headClass} colSpan={3}>
                            {collapsable && <IconButton size="small">
                                {open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
                            </IconButton>}
                            {title}
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody className={clsx({ [classes.hidden]: collapsable && !open })}>
                    {data &&
                        (data.length === 0 ?
                            <TableRow>
                                <TableCell colSpan={2}>
                                    <em>{t(emptyText)}</em>
                                </TableCell>
                            </TableRow>
                            :
                            data.map((i, index) => (
                                <StyledTableRow key={index}>
                                    <TableCell style={{ width: 30 }}>
                                        <Avatar src={i.user.profile_picture} />
                                    </TableCell>
                                    <TableCell>
                                        <Link to={`/users/${i.user.user.id}`}>
                                            <span>
                                                {`${i.user.user.first_name} ${i.user.user.last_name}`}<br />
                                                {i.user.country && i.user.city && <Typography variant="caption">{i.user.city} {i.user.country}</Typography>}
                                            </span>
                                        </Link>
                                    </TableCell>
                                    <TableCell>
                                        <Avatar style={{ backgroundColor: "green", float: "right" }}
                                            title={`${t("Score: ")}${i.points}`}>
                                            {i.points}
                                        </Avatar>
                                    </TableCell>
                                </StyledTableRow>
                            )))}
                </TableBody>
            </Table>
        </TableContainer>
    </>)
}

UserTable.defaultProps = {
    height: '200px',
    collapsable: true,
    defaultOpen: false,
    disabled: false,
}

export default UserTable
