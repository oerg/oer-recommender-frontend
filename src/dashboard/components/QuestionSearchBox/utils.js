import { MultiCall } from "../../views/helpers";

const LQC = "QC_loadQuestions";

export function loadQuestions(type, id, search, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(LQC);
    return mc.call(`/assessments/questions/general-search-questions?object_type=${type}&object_id=${id}&search_term=${search}`, onLoad, onError, onEnd);
}

export function cancelLoadQuestions() {
    return MultiCall.getInstance(LQC).cancel();
}