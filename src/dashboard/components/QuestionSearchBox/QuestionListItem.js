import { ListItem, ListItemText } from '@material-ui/core';
import React, { useCallback, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';

function QuestionListItem({ question, onClick, links = false, ...props }) {
    const language = useLanguage();

    const handleClick = useCallback(
        () => {
            onClick && onClick(question);
        },
        [question, onClick]
    );

    // extra props based on links for listitem.
    const extraProps = useMemo(
        () => links ? {
            component: Link,
            to: `/${language}/dashboard/discovery/questions/${question.id}`
        }
        :
        {
            onClick: handleClick
        },
        [links, language, question, handleClick]
    );

    return <ListItem button  {...props} {...extraProps}>
        <ListItemText 
            primary={question.raw_text}
            primaryTypographyProps={{
                style: {
                    overflow: "hidden",
                    whiteSpace: "nowrap",
                    textOverflow: "ellipsis",
                }
            }} />
    </ListItem>;
}

export default QuestionListItem;
