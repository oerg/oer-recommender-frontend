import { Box, Button, CircularProgress, Divider, List, Paper, TextField } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import QuestionListItem from './QuestionListItem';
import { cancelLoadQuestions, loadQuestions } from './utils';

function QuestionSearchBox({ objectType, objectId, linkExternalData, showAdd = false, onAddQuestion, showSelfAssessment = false, ...props }) {
    const { t } = useTranslation(['dashboard']);

    const [loading, setLoading] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [questions, setQuestions] = useState([]);
    const [questionsNumber, setQuestionsNumber] = useState(0);
    const language = useLanguage();

    const handleChange = useCallback(
        e => {
            cancelLoadQuestions();
            const text = e ? e.target.value : "";
            setSearchText(text);
            if ((e && text) || !e) {
                setLoading(true);
                loadQuestions(
                    objectType,
                    objectId,
                    text,
                    ({ data }) => {
                        setQuestions(data.results);
                        setQuestionsNumber(data.count);
                    },
                    null,
                    () => setLoading(false)
                );
            }
        },
        []
    );

    useEffect(
        handleChange,
        [handleChange]
    );

    // const handleSelect = useCallback(
    //     q => history.push(`/${language}/dashboard/discovery/questions/${q.id}`),
    //     [history]
    // );

    const addQuestionButtonProps = useMemo(
        () => onAddQuestion ?
            {
                onClick: onAddQuestion
            }
            :
            {
                component: Link,
                to: {
                    pathname: `/${language}/dashboard/discovery/questions/add`,
                    state: linkExternalData
                },
            },
        [onAddQuestion, language, linkExternalData]
    )

    return <Paper
        style={{
            padding: 8
        }}
    >
        <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent={searchText ? "flex-start" : "center"}
            {...props}
        >
            <TextField
                fullWidth
                variant="outlined"
                label={t("Search")}
                InputProps={{
                    placeholder: t("Search questions...")
                }}
                value={searchText}
                onChange={handleChange}
            />
            {searchText ? (loading ?
                <Box p={2} textAlign="center">
                    <CircularProgress />
                </Box>
                : (questions.length === 0 ?
                    <Box p={1}>
                        <em>{t("No results.")}</em>
                    </Box>
                    :
                    <>
                        <List style={{ width: "100%", flex: "1" }}>
                            {questions.map(
                                (q, index) => (
                                    <QuestionListItem
                                        key={index}
                                        question={q}
                                        links
                                    />
                                )
                            )}
                        </List>
                        {questionsNumber > 0 &&
                            <div>
                                <Pagination count={Math.ceil(questionsNumber / 10.0)} variant="outlined" />
                            </div>}
                    </>
                )) :
                <Box p={1}>
                    <em>{t("Type to start the search in questions.")}</em>
                </Box>
            }
            {showAdd &&
                <Button
                    fullWidth
                    {...addQuestionButtonProps}
                >
                    {t("Add a Question")}
                </Button>}
        </Box>
        <Divider />
        <Button
            style={{
                margin: "10px 0 5px 0",
            }}
            component={Link}
            fullWidth
            to={`/${language}/dashboard/discovery/skills/${objectId}/self-assessment`}
            variant="outlined"
        >
            {t("Manage Self-Assessment")}
        </Button>
    </Paper>
}

QuestionSearchBox.propTypes = {

};

export default QuestionSearchBox;
