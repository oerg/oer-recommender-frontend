import { Autocomplete } from '@material-ui/lab'
import React, { useCallback, useEffect, useState } from 'react'

function SearchAutocomplete({ onSearch, ...props }) {
    const [open, setOpen] = useState(false)
    const [inputValue, setInputValue] = useState('')

    const handleOpen = useCallback(
        () => setOpen(true),
        []
    )

    const handleClose = useCallback(
        () => setOpen(false),
        []
    )

    const handleInput = useCallback(
        (e, value) => {
            setInputValue(value)
        },
        []
    )

    useEffect(
        () => {
            if (open) {
                onSearch(inputValue)
            }
        },
        [open, inputValue, onSearch]
    )

    return (
        <Autocomplete
            {...props}
            open={open}
            onOpen={handleOpen}
            onClose={handleClose}
            inputValue={inputValue}
            onInputChange={handleInput}
        />
    )
}

export default SearchAutocomplete