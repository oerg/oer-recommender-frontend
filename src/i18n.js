import i18next from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next'
import deLanding from './translations/de/landing.json'
import deDashboard from './translations/de/dashboard.json'

i18next
.use(initReactI18next)
.init({
    // debug: process.env.NODE_ENV === 'development',
    debug: false,
    fallbackLng: "en",
    resources: {
        de: {
            landing: deLanding,
            dashboard: deDashboard,
        }
    },
    interpolation: {
        escapeValue: false
    },
    saveMissing: process.env.NODE_ENV === 'development',
});

export function detectLanguage(i18n, matchedUrl) {
    if (matchedUrl !== null)
        i18n.changeLanguage(matchedUrl.params.lang === undefined ? 'en' : matchedUrl.params.lang);
}

export function loadResource(i18n, ns, onLoad, onError, onEnd) {
    i18next.addResourceBundle(
        i18n.language,
        ns,

    )
}

export function useLanguage() {
    const { i18n } = useTranslation();
    return i18n.language
}

export default i18next;