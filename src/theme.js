import { createTheme } from "@material-ui/core";

const theme = createTheme({
    overrides: {
        MuiCssBaseline: {
            '@global': {
                a: {
                    textDecoration: "none",
                },
            },
        },
    },
    palette: {
        primary: {
            main: '#00888d',
        },
        secondary: {
            main: '#f50057',
        },
        common: {
            darkBlack: "rgb(36, 40, 44)",
        }
    },
})

export default theme