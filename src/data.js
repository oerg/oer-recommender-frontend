import { loadExistingLangs } from "./utils";

export const allIndustries = [
    {
        "code": "GE",
        "label": "General"
    },
    {
        "code": "AFF",
        "label": "Agriculture, Forestry and Fishing"
    },
    {
        "code": "MQ",
        "label": "Mining and Quarrying"
    },
    {
        "code": "MA",
        "label": "Manufacturing"
    },
    {
        "code": "EGS",
        "label": "Electricity, Gas, Steam and Air Conditioning Supply"
    },
    {
        "code": "WS",
        "label": "Water Supply; Sewerage, Waste Management and Remediation Activities"
    },
    {
        "code": "CO",
        "label": "Construction"
    },
    {
        "code": "WTR",
        "label": "Wholesale and Trade Retail"
    },
    {
        "code": "TS",
        "label": "Transportation and Storage"
    },
    {
        "code": "AFS",
        "label": "Accommodation and Food Service Activities"
    },
    {
        "code": "IC",
        "label": "Information and Communication"
    },
    {
        "code": "FI",
        "label": "Financial and Insurance Activities"
    },
    {
        "code": "RE",
        "label": "Real Estate Activities"
    },
    {
        "code": "PST",
        "label": "Professional, Scientific, and Technical Activities"
    },
    {
        "code": "ASS",
        "label": "Administrative and Support Service Activities"
    },
    {
        "code": "PAD",
        "label": "Public Administration and Defence"
    },
    {
        "code": "ED",
        "label": "Education"
    },
    {
        "code": "HHSW",
        "label": "Human Health and Social Work"
    },
    {
        "code": "AER",
        "label": "Arts, Entertainment and Recreation"
    },
    {
        "code": "OSA",
        "label": "Other Service Activities"
    },
    {
        "code": "AHE",
        "label": "Activities of Households as Employers"
    },
    {
        "code": "AEO",
        "label": "Activities of Extraterritorial Organisations and Bodies"
    }
]


export const allFormatTypes = [
    {
        code: 'VI',
        label: 'Video',
    },
    {
        code: 'BC',
        label: 'Book Chapter',
    },
    {
        code: 'WE',
        label: 'Webpage',
    },
    {
        code: 'LN',
        label: 'Lecture Note',
    },
    {
        code: 'SL',
        label: 'Slide',
    },
    {
        code: 'PA',
        label: 'Paper',
    },
    {
        code: 'MI',
        label: 'Mixed',
    },
]

export const allStrategies = [
    {
        code: "TH",
        label: "Theory",
    },
    {
        code: "EX",
        label: "Example",
    },
    {
        code: "TE",
        label: "Theory and Example",
    }
]

export const allLanguages = [
    "af",
    "sq",
    "am",
    "ar",
    "hy",
    "az",
    "eu",
    "be",
    "bn",
    "bs",
    "bg",
    "ca",
    "ceb",
    "ny",
    "zh-cn",
    "zh-tw",
    "co",
    "hr",
    "cs",
    "da",
    "nl",
    "en",
    "eo",
    "et",
    "tl",
    "fi",
    "fr",
    "fy",
    "gl",
    "ka",
    "de",
    "el",
    "gu",
    "ht",
    "ha",
    "haw",
    "he",
    "hi",
    "hmn",
    "hu",
    "is",
    "ig",
    "id",
    "ga",
    "it",
    "ja",
    "jw",
    "kn",
    "kk",
    "km",
    "ko",
    "ku",
    "ky",
    "lo",
    "la",
    "lv",
    "lt",
    "lb",
    "mk",
    "mg",
    "ms",
    "ml",
    "mt",
    "mi",
    "mr",
    "mn",
    "my",
    "ne",
    "no",
    "or",
    "ps",
    "fa",
    "pl",
    "pt",
    "pa",
    "ro",
    "ru",
    "sm",
    "gd",
    "sr",
    "st",
    "sn",
    "sd",
    "si",
    "sk",
    "sl",
    "so",
    "es",
    "su",
    "sw",
    "sv",
    "tg",
    "ta",
    "te",
    "th",
    "tr",
    "uk",
    "ur",
    "ug",
    "uz",
    "vi",
    "cy",
    "xh",
    "yi",
    "yo",
    "zu"
]

export function getAllLanguages(currentLang) {
    const languageNames = new Intl.DisplayNames([currentLang], {type: 'language'});
    const langs = []
    for (const lang of allLanguages) {
        langs.push(
            {
                code: lang,
                label: languageNames.of(lang)
            }
        )
    }
    return langs.sort((a, b) => a.label > b.label)
}


export function getExistingLanguages(currentLang, onLoad) {
    const languageNames = new Intl.DisplayNames([currentLang], {type: 'language'});
    const result = []
    loadExistingLangs((res) => {
        for (const lang of res.lang_list) {

            result.push(
                {
                    code: lang,
                    label: languageNames.of(lang)
                }
            )
        }
        onLoad(result.sort((a, b) => a.label > b.label))
    }
    )
}