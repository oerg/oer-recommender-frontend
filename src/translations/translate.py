import deepl
import sys
import json
import os

JSON_FILES = [
    'landing.json',
    'dashboard.json',
]

translator = deepl.Translator(r"3ca4f3b8-45f3-374b-6e98-c0b8f1d764a8:fx")

# result = translator.translate_text("Where are you from my friend?", source_lang="EN", target_lang="DE")

def print_account_usage():
    usage = translator.get_usage()
    if usage.character.limit_exceeded:
        print("Character limit exceeded.")
    else:
        print(f"Character usage: {usage.character.count} of {usage.character.limit}")

if len(sys.argv) != 2:
    print("please enter the folder on json files.")
    sys.exit(1)

path = sys.argv[1]

for file in JSON_FILES:
    with open(os.path.join(path, file)) as fp:
        texts = json.load(fp)
    terms = []
    for term in texts:
        if term == texts[term]:
            terms.append(term)
    results = translator.translate_text(terms, source_lang="EN", target_lang="DE", formality="less")
    i = 0
    for term in texts:
        if term == texts[term]:
            texts[term] = results[i].text
            i += 1

    with open(os.path.join(path, f"{file}.new.json"), "w") as fp:
        json.dump(texts, fp, indent=4, ensure_ascii=False)
