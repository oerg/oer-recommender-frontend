import os, sys, json, csv

JSON_FILES = [
    './de/landing.json',
    './de/dashboard.json',
]

COLS = [
    "Comment",
    "English",
    "German"
]

for file in JSON_FILES:
    with open(file) as fp:
        texts = json.load(fp)

    
    with open(f"{file}.csv", "w") as fp:
        writer = csv.writer(fp)
        writer.writerow(COLS)
        for row in texts:
            writer.writerow(["", row, texts[row]])

    
