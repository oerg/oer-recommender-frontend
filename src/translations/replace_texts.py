# export as csv
# put files names "landing.csv" and "dashboard.csv" here

from asyncore import read
import csv
import os
import sys

TRANS_SECTIONS = {
    "landing.csv": "../landing",
    "dashboard.csv": "../dashboard",
}

def replace_in_code(source_term, target_term, dir):
    path = os.walk(dir)

    for root, _, files in path:
        for file in files:
            if file[-3:] != ".js":
                continue
            file_path = os.path.join(root, file)
            with open(file_path, "r") as fp:
                filedata = fp.read()
            replaced = filedata.replace(source_term, target_term)
            if replaced != filedata:
                print(f"Found in {file_path}")
                with open(file_path, 'w') as fp:
                    fp.write(replaced)

def do_replacement(file, dir):
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        header_read = False
        for row in reader:
            if not header_read:
                header_read = True
                continue
            if row[1] != row[2]:
                print(f"> Change term: {row[1]}")
                replace_in_code(row[1], row[2], dir)

for file in TRANS_SECTIONS:
    do_replacement(file, TRANS_SECTIONS[file])