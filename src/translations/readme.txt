- update_translations.py:
Gathers all texts in t function and stores them in json files in all languages

- update_translations.py new <LANG>:
Gathers all texts in t function and stores them in json files in a new folder called <LANG>

- update_translations.py csv
Gathers all texts in t function and stores them in a csv file with two columns: "comments", "original text"

- replace_texts.py
Change the data gathered from shared CSV files. You should change the code based on the format of the CSV files. (Check lines 38-40)

- translate.py <LANG>
Reads json files in a <LANG> folder and translates them into <LANG> using deepl.
Note: "deepl" library must be installed using pip. (pip install deepl)

- export_csv.py
Converts json files into csv file. Edit the code for your specific needs.