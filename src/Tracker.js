import Axios from 'axios';
import { useEffect } from 'react'
import { useLocation } from 'react-router-dom';

function Tracker() {
    const location = useLocation();

    useEffect(() => {
        const path = location.pathname + location.search;
        Axios.post('/trackings/add-tracking/', {
            tracking_type: "VI",
            related_data: {
                url: path
            }
        })
        .then(() => {})
        .catch(() => {})
    }, [location.pathname, location.search])

    return null;
}

export default Tracker
