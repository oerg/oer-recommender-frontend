import axios from "axios";
import { format, formatISO, parseISO } from "date-fns";
import { requestAddHandlers } from "./dashboard/views/helpers";

export function loadExistingLangs(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get('/jobs/get-all-langs'),
        onLoad,
        onError,
        onEnd
    );
}

export const DATETIME_FORMAT = "yyyy-MM-dd H:mm:ss"
export const DATE_FORMAT = "yyyy-MM-dd"

export function parseDateTime(date, str_format=DATETIME_FORMAT) {
    return format(parseISO(date), str_format)
}

export function parseDate(date, str_format=DATE_FORMAT) {
    return format(date, str_format)
}

export function parseISOToDate(iso_date, str_format=DATE_FORMAT) {
    return format(parseISO(iso_date), str_format)
}

export function dateToISO(date) {
    return formatISO(date)
}